create or replace package LOAD_PSI_DATA as

procedure LOAD_ALL_DATA_S;

procedure LOAD_ALL_DATA_5MI;

end LOAD_PSI_DATA;
/

create or replace package body  LOAD_PSI_DATA as

wait_seconds constant integer := 300; 

procedure LOAD_ALL_DATA_S is
begin
   dbms_output.put_line('LOAD_ALL_DATA_S');
   execute immediate 'truncate table OD_SE_RTD_S';
   -- Test for timeout
   dbms_lock.sleep(wait_seconds);
   
end;

procedure LOAD_ALL_DATA_5MI is
begin
   dbms_output.put_line('LOAD_ALL_DATA_5MI');
   execute immediate 'truncate table OD_SE_RTD_5MI';
   -- Test for timeout
   dbms_lock.sleep(wait_seconds);
end;

end LOAD_PSI_DATA;
/