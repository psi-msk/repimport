--  ******************************************************************
--  (c) PSI AG, 2014
--
-- Creation of a view on the PSIportal master data objects for ATGS
--
--  Date        Author       Comments
--  ----------  -----------  -------------------------------------------
--  10.11.2014  K. Ulbrich   Creation
--  11.11.2015  K. Ulbrich   Select transport objects as well
--
--  ******************************************************************

PROMPT ================================================================================================
PROMPT Creation of master data view. Press ENTER to continue, CTRL-C to exit.
PROMPT ================================================================================================
PAUSE

ACCEPT por_usr PROMPT "Enter the PSIportal schema, which contains the tables to be read by the view: "

-------------------------------------------------------------------------- 
-- The newest data set for versioned object data
--------------------------------------------------------------------------
CREATE OR REPLACE VIEW aux_object_vi AS
  SELECT label AS key, description, objid AS internal_id
  FROM
    (SELECT label, description, vers_date, objid, row_number()
      OVER (PARTITION BY objid ORDER BY vers_date DESC) rn
      FROM mtr_object_ta
      WHERE objecttype LIKE 'V7_%' OR objecttype like 'TP_%')
  WHERE rn=1
  ORDER BY label;
                   
-------------------------------------------------------------------------- 
-- The newest data set for versioned primary topological nodes per object
--------------------------------------------------------------------------
CREATE OR REPLACE VIEW aux_topology_vi AS
  SELECT id, fk_objid, fk_topo FROM
    (SELECT id, fk_objid, fk_topo, row_number() OVER (PARTITION BY fk_objid ORDER BY vers_date DESC) rn FROM mtr_topology_ta)
  WHERE rn=1;

--------------------------------------------------------------------------
-- The newest data set of SAP-ID variables
-------------------------------------------------------------------------- 
CREATE OR REPLACE VIEW aux_sap_id AS
  SELECT value AS SAP_ID, fk_objid as internal_id FROM
    (SELECT attr_id, value, fk_objid, row_number() OVER (PARTITION BY fk_objid, attr_id ORDER BY vers_date DESC) rn FROM mtr_var_def_ta) vd, mtr_variables_ta va
    WHERE va.attr_name = 'SAP_ID' AND
    vd.attr_id = va.id AND
    vd.rn = 1;

-------------------------------------------------------------------------- 
-- All attribute detail tables as a union for the name
-- Newest version
-------------------------------------------------------------------------- 
CREATE OR REPLACE VIEW aux_name_vi AS
SELECT fk_obj as internal_id, name FROM (
  SELECT fk_obj, row_number() OVER (PARTITION BY fk_obj ORDER BY vers_date DESC) rn, name FROM (
    SELECT fk_obj, vers_date, name FROM mtr_av7_net_ta
      UNION ALL
    SELECT fk_obj, vers_date, name FROM mtr_av7_subnet_ta
      UNION ALL
    SELECT fk_obj, vers_date, name FROM mtr_av7_group_ta
      UNION ALL
    SELECT fk_obj, vers_date, name FROM mtr_av7_station_ta
      UNION ALL
    SELECT fk_obj, vers_date, name FROM mtr_av7_un_ta
      UNION ALL
    SELECT fk_obj, vers_date, name FROM mtr_av7_mes_ta
      UNION ALL
    SELECT fk_obj, vers_date, name FROM mtr_av7_pe_ta
      UNION ALL
    SELECT fk_obj, vers_date, name FROM mtr_av7_mv_ta
      UNION ALL
    SELECT fk_obj, vers_date, name FROM mtr_av7_cv_ta
    )
  ) where rn=1
;

-------------------------------------------------------------------------- 
-- Object data with parents, newest version
--------------------------------------------------------------------------
CREATE OR REPLACE VIEW psiportal_masterdata_vi AS
-- Upper level objects
SELECT
   oc.key, oc.description, null as parent_key, oc.internal_id
FROM
   aux_object_vi oc,
   aux_topology_vi tc
WHERE
  tc.fk_objid = oc.internal_id AND
  tc.fk_topo is null
UNION
-- Objects below upper level
SELECT
 oc.key, oc.description, op.key as parent_key, oc.internal_id
FROM
   aux_object_vi oc,
   aux_object_vi op,
   aux_topology_vi tc,
   aux_topology_vi tp
WHERE
  tc.fk_objid = oc.internal_id AND
  tc.fk_topo = tp.id AND
  op.internal_id = tp.fk_objid
;

-------------------------------------------------------------------------- 
-- ========================================================
-- Now, bring it all together, these are the data for ATGS.
-- ========================================================
-------------------------------------------------------------------------- 
CREATE OR REPLACE VIEW PSIPORTAL_VIEW AS
SELECT first_level.key, first_level.description, first_level.parent_key, first_level.sap_id, nm.name FROM
  aux_name_vi nm RIGHT OUTER JOIN
  (SELECT
    md.key, md.description, md.parent_key, si.sap_id, md.internal_id
    FROM
    aux_sap_id si RIGHT OUTER JOIN psiportal_masterdata_vi md ON md.internal_id = si.internal_id
  ) first_level ON first_level.internal_id = nm.INTERNAL_ID
  ORDER BY first_level.key
;

/
