---  ******************************************************************
--  Copyright (C) PSI AG, 2015
--  PSI AG, 2015
-- 
--  Data model MTR
--
--  $Author: emshl $
--  $Date: 2015/18/07 07:46:08 $
--  $Revision: 1.1.1.1 $
--  $State: Exp $
--
--  Date        Author       Comments
--  ----------  -----------  -------------------------------------------
--  18.07.2015  H. Liebhold  Generate PSItransport specific units and quantities (Standard + UGS-specific); generate importadapter for xml-Import of PSItransport-data with mappings for UGS
--
--  ******************************************************************
set serveroutput on
set term on 

spool tp_scripts.log

@ntd\insert_TPstandard_quan.sql
@ntd\insert_TPspec_quan.sql

@ntd\insert_UGSspec_units.sql
@ntd\insert_UGSspec_quan.sql

@ntd\mtr_insert_mtr_fs_ta.sql
@ntd\mtr_insert_mtr_fs_param_ta.sql
@ntd\mtr_insert_mtr_fs_quantities_ta.sql
@ntd\mtr_insert_mtr_fs_status_ta.sql
@ntd\mtr_insert_mtr_fs_units_ta.sql

commit;
spool off

--  *************************** STOP - SUCCESS ****************************
prompt STOP - Success

