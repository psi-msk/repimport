--  ******************************************************************
--  Copyright (C) PSI AG, 2015
--  PSI AG, 2015
-- 
--  Data model MTR
--
--  $Author: emshl $
--  $Date: 2015/18/07 07:46:08 $
--  $Revision: 1.1.1.1 $
--  $State: Exp $
--
--  Date        Author       Comments
--  ----------  -----------  -------------------------------------------
--  18.07.2015  H. Liebhold  Create
--
--  ******************************************************************
SET DEFINE OFF;
insert into MTR_FS_STATUS_TA (FS_ID, STATUS, SYSTEM_STATUS, USER_STATUS, POSITION) values( 'IMP_TP','M','M','missing',null);
insert into MTR_FS_STATUS_TA (FS_ID, STATUS, SYSTEM_STATUS, USER_STATUS, POSITION) values( 'IMP_TP','V','V','valid',null);
SET DEFINE ON;   
COMMIT;
