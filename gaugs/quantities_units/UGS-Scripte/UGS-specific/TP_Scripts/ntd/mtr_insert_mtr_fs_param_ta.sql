--  ******************************************************************
--  Copyright (C) PSI AG, 2015
--  PSI AG, 2015
-- 
--  Data model MTR
--
--  $Author: emshl $
--  $Date: 2015/18/07 07:46:08 $
--  $Revision: 1.1.1.1 $
--  $State: Exp $
--
--  Date        Author       Comments
--  ----------  -----------  -------------------------------------------
--  18.07.2015  H. Liebhold  Create
--
--  ******************************************************************
SET DEFINE OFF;
insert into MTR_FS_PARAM_TA (FS_ID, PARAM_NAME, PARAM_VALUE) values( 'IMP_TP','ACCEPT_INIT','N');
insert into MTR_FS_PARAM_TA (FS_ID, PARAM_NAME, PARAM_VALUE) values( 'IMP_TP','CHECK_DATAGAPS_ABSVAL','Y');
insert into MTR_FS_PARAM_TA (FS_ID, PARAM_NAME, PARAM_VALUE) values( 'IMP_TP','DIR_ERROR','//gobugs.local/cdfs/transfer/ref/portal/psitransport/error');
insert into MTR_FS_PARAM_TA (FS_ID, PARAM_NAME, PARAM_VALUE) values( 'IMP_TP','DIR_INPUT','//gobugs.local/cdfs/transfer/ref/portal/psitransport/input');
insert into MTR_FS_PARAM_TA (FS_ID, PARAM_NAME, PARAM_VALUE) values( 'IMP_TP','DIR_PROCESSED','//gobugs.local/cdfs/transfer/ref/portal/psitransport/ok');
insert into MTR_FS_PARAM_TA (FS_ID, PARAM_NAME, PARAM_VALUE) values( 'IMP_TP','FOREIGN_KEYS','N');
insert into MTR_FS_PARAM_TA (FS_ID, PARAM_NAME, PARAM_VALUE) values( 'IMP_TP','OBJECT_DATA','Y');
insert into MTR_FS_PARAM_TA (FS_ID, PARAM_NAME, PARAM_VALUE) values( 'IMP_TP','RETENTION_ERROR','1m');
insert into MTR_FS_PARAM_TA (FS_ID, PARAM_NAME, PARAM_VALUE) values( 'IMP_TP','RETENTION_PROCESSED','1m');
insert into MTR_FS_PARAM_TA (FS_ID, PARAM_NAME, PARAM_VALUE) values( 'IMP_TP','SYNCHRONISE_FOREIGN_KEYS','N');
insert into MTR_FS_PARAM_TA (FS_ID, PARAM_NAME, PARAM_VALUE) values( 'IMP_TP','TIMESERIES_DATA','Y');
insert into MTR_FS_PARAM_TA (FS_ID, PARAM_NAME, PARAM_VALUE) values( 'IMP_TP','WITHOUT_FOREIGN_KEYS','N');

SET DEFINE ON;
COMMIT;
