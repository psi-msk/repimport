--  ******************************************************************
--  Copyright (C) PSI AG, 2015
--  PSI AG, 2015
-- 
--  Data model MTR
--
--  $Author: emshl $
--  $Date: 2015/18/07 07:46:08 $
--  $Revision: 1.1.1.1 $
--  $State: Exp $
--
--  Date        Author       Comments
--  ----------  -----------  -------------------------------------------
--  18.07.2015  H. Liebhold  Create  UGS specific units
--
--  ******************************************************************
SET DEFINE OFF;
insert into  MTR_UNITS_TA ( ID, VERS_DATE, SHORT_NAME, DESCRIPTION, BASE_UNIT, FACTOR, SUMMAND, PRECISION, ROUND ) values( '%', TO_TIMESTAMP('01.01.99 06:00:00,000000','DD.MM.YY HH24:MI:SS.FF'),'%','Prozent','NA',1,'0,'2,'TRADING');
insert into  MTR_UNITS_TA ( ID, VERS_DATE, SHORT_NAME, DESCRIPTION, BASE_UNIT, FACTOR, SUMMAND, PRECISION, ROUND ) values( '-', TO_TIMESTAMP('01.01.99 06:00:00,000000','DD.MM.YY HH24:MI:SS.FF'),'-','ohne Einheit','NA',1,'0,'2,'TRADING');
insert into  MTR_UNITS_TA ( ID, VERS_DATE, SHORT_NAME, DESCRIPTION, BASE_UNIT, FACTOR, SUMMAND, PRECISION, ROUND ) values( 'Тыс.м³', TO_TIMESTAMP('19.05.99 14:20:53,595000','DD.MM.YY HH24:MI:SS.FF'),'Тыс.м³','Тыс м³','m³',1000,'0,'2,'TRADING');
insert into  MTR_UNITS_TA ( ID, VERS_DATE, SHORT_NAME, DESCRIPTION, BASE_UNIT, FACTOR, SUMMAND, PRECISION, ROUND ) values( 'м³', TO_TIMESTAMP('19.05.99 14:20:41,189000','DD.MM.YY HH24:MI:SS.FF'),'м³','м³','m³',1,'0,'1,'TRADING');
insert into MTR_UNITS_TA (ID, VERS_DATE, SHORT_NAME, DESCRIPTION, BASE_UNIT, FACTOR, SUMMAND, PRECISION, ROUND) values ('Wh',TO_TIMESTAMP('01/01/1999 06:00:00.000000','DD/MM/YYYY HH24:MI:SS.FF'),'Wh','Wattstunde','J','3600','0','2','TRADING');
insert into MTR_UNITS_TA (ID, VERS_DATE, SHORT_NAME, DESCRIPTION, BASE_UNIT, FACTOR, SUMMAND, PRECISION, ROUND) values ('Wh/m³',TO_TIMESTAMP('01/01/1999 06:00:00.000000','DD/MM/YYYY HH24:MI:SS.FF'),'Wh/m³','Wattstunde je m³','J/m³','3600','0','2','TRADING');
update MTR_UNITS_TA set ROUND='TRADING' where ID not like 'QU_%';

SET DEFINE ON;   
COMMIT;
