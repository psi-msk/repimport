--  ******************************************************************
--  Copyright (C) PSI AG, 2015
--  PSI AG, 2015
-- 
--  Data model MTR
--
--  $Author: emshl $
--  $Date: 2015/18/07 07:46:08 $
--  $Revision: 1.1.1.1 $
--  $State: Exp $
--
--  Date        Author       Comments
--  ----------  -----------  -------------------------------------------
--  18.07.2015  H. Liebhold  Create
--
--  ******************************************************************
SET DEFINE OFF;
Insert into MTR_FS_TA
  (ID, SHORT_NAME, DESCRIPTION, DIRECTION, COMMENT_TEXT, DRIVER, LAST_IMP,
   ACTIVE, EXECUTION, FK_SCHEDULE, ORIG_VALUES, ORIG_VALUES_SINCE, ONLY_ERRORS, PROTOCOL_SINCE, LABEL_EQUALS_FK,
   BIT_ORIENTATION, UNKNOWN_QUANTITIES, DAY_BEGIN, MONTH_BEGIN, YEAR_BEGIN, TIMEZONE, FK_BA, MEDIUM, SRC_MODDATE_POLICY)
Values
   ('IMP_TP', 'IMP_TP_XML', 'Import PSItransport (xml)', 'I', null, 'PSIportalXMLImp', null,
    'N', 'MAN', null, 'Y', 'MI3', 'N', 'MI3', 'N',
	'N', 'N', null, null, null,null, (select ID from MTR_BUSINESS_AREA_TA where LABEL='Gas'),'FILESYSTEM','E');


SET DEFINE ON;   
COMMIT;
