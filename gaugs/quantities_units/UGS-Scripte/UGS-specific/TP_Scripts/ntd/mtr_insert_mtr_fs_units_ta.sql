--  ******************************************************************
--  Copyright (C) PSI AG, 2015
--  PSI AG, 2015
-- 
--  Data model MTR
--
--  $Author: emshl $
--  $Date: 2015/18/07 07:46:08 $
--  $Revision: 1.1.1.1 $
--  $State: Exp $
--
--  Date        Author       Comments
--  ----------  -----------  -------------------------------------------
--  18.07.2015  H. Liebhold  Create
--
--  ******************************************************************
SET DEFINE OFF;
--  insert here
insert into MTR_FS_UNITS_TA (FS_ID, FK_UNITS, ALIAS_NAME) values( 'IMP_TP','m³','м³');
SET DEFINE ON;   
COMMIT;
