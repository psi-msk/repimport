Package PSI_TP_Pckg AUTHID DEFINER
is
--------------------------------------------------------------------------------
-- package mit Funktionen f�r den Aufbau der Gr��en/Einheiten/Adapter-Mapping-Eintr�ge f�r PSItransport
-------------------------------------------------------------------------------- 
-- LIH 19.08.2015	PrepGVQuan verbessert   
-- LIH 14.08.2015	Neu: PrepGVQuan, Crea_Mapping umbenannt
-- LIH 20.05.2015   Neu: PrepSTQuan
-- LIH 19.05.2015	PrepFVQuan erweitert - auch russ. Kurznamen/Einheiten mappings erzeugen
-- LIH 12.05.2015 	Crea_Mapping erstellt
-- LIH 11.05.2015	Ersterstellung

--------------------------------------------------------------------------------
-- Konstanten
--------------------------------------------------------------------------------
	iGasjahrStartMonat	integer := 10;

function last2( cs in varchar2 ) return varchar2;	  				-- String nach vorletztem Punkt extrahieren	
function last1( cs in varchar2 ) return varchar2;					-- String nach letztem Punkt extrahieren	
function NextQu( nOff in number ) return number;					-- n�chste FK_QUN nach vorgeg. nOffr und < nOff+1000
procedure PrepSTQuan( cFS in varchar2 default 'IMP_TP_XML' ); 		-- Aufbau neuer Standardgr��en  
procedure PrepFVQuan;												-- Aufbau der  Eintr�ge in MTR_QUANTITIES_TA, MTR_QUANTITY_UNITS_TA  f�r Freie Variable 
procedure PrepGVQuan( cFS in varchar2 default 'IMP_TP_XML' );		-- Aufbau der  Eintr�ge in MTR_QUANTITIES_TA, MTR_QUANTITY_UNITS_TA  f�r Globale Variable 
procedure PrepSGQuan( cFS in varchar2 default 'IMP_TP_XML' );		-- Aufbau der Mappingliste f�r TP-Adapter f�r spez. Gr��en

end;
/

Package body PSI_TP_Pckg
is

function last1( cs in varchar2 ) return varchar2 
as        			
	cRest	varchar2(100) := cs;	
	ipos	integer := instr(cs, '.');	
	iC		integer := 0;
begin			
	-- String nach letztem Punkt extrahieren		
	-- INSTR( string, substring [, start_position [,  nth_appearance ] ] )		
			
	While ipos > 0 loop		
		iC := iC + 1;	
		cRest := substr(cRest, ipos+1);	
		ipos := instr(cRest, '.');	
	end loop;		
	if iC >= 1 then		
		return substr(cs, instr(cs, '.', 1, iC) + 1);	
	end if;		
	return cs;		
end;

function last2( cs in varchar2 ) return varchar2 
as        			
	cRest	varchar2(100) := cs;	
	ipos	integer := instr(cs, '.');	
	iC		integer := 0;
begin			
	-- String nach vorletztem Punkt extrahieren		
	-- INSTR( string, substring [, start_position [,  nth_appearance ] ] )		
			
	While ipos > 0 loop		
		iC := iC + 1;	
		cRest := substr(cRest, ipos+1);	
		ipos := instr(cRest, '.');	
	end loop;		
	if iC > 1 then		
		return substr(cs, instr(cs, '.', 1, iC - 1) + 1);	
	end if;		
	return cs;		
end;

function MaxQu return number
as  
	nQuId		number := 0;
begin                                                                                        
	select GEN_VALUE into nQuId     from por.MTR_SEQUENCE_TA where GEN_KEY='Quantity_Units_Id';
	return (nQuId);
end;

function NextQu( nOff in number ) return number
as
	nQuId		number;
begin                                                                                        
    if nOff < 0 then	
    	-- Suche n�chste neg. Zahl kleiner nOff, die nicht belegt ist.
	    select min(FK_QUN) into nQuId from QUANT where FK_QUN < nOff and FK_QUN < nOff - 1000;
    else
    	-- Suche n�chste pos. Zahl gr��er nOff, die nicht belegt ist.
	    select max(FK_QUN) into nQuId from QUANT where FK_QUN > nOff and FK_QUN < nOff + 1000;
    end if;
    if nQuId is null then
    	if nOff < 0 then
    		nQuId := nOff - 1;
    	else
	    	nQuId := nOff + 1;
	    end if;
    end if;
    return nQuId;
end;
        
function GetBaseUnit( nNr in out number, cUnit in varchar2 ) return varchar2
as 
	-- Input kann Unit-ID oder Unit-Shortname sein
	cBaseEht 	UNITS.ID%type := 'NA';            
	bFound	boolean := false;
begin                                                         
	for rec in (select BASE_UNIT from UNITS where ID = cUnit) loop
		cBaseEht := rec.BASE_UNIT;
		bFound := true;
	end loop;	         
	if not bFound then
		for rec in (select BASE_UNIT from UNITS where SHORT_NAME = cUnit) loop
			cBaseEht := rec.BASE_UNIT;
		end loop;	         
	end if;                 
	if cBaseEht = 'NA' then
		-- Wenn die Einheit fehlt ->Eintrag in UNIT-Tabelle erforderlich
		psi_pckg.InsHit2( nNr,  'insert into por.MTR_UNITS_TA   (ID, SHORT_NAME, DESCRIPTION, BASE_UNIT, FACTOR, SUMMAND, PRECISION, ROUND) values(''' 
		 || cUnit || ''',''' || cUnit || ''',''' || cUnit || ''',''' || cUnit || ''',1,0,2,''TRADING'');');
		cBaseEht := cUnit;
	end if;		
	return( cBaseEht );
end;

function GetType_StdEht( cEht in varchar2, cType in out varchar2 ) return varchar2
as                                                                                
	cStdEht		QUANT.DEFAULT_UNIT%type;
begin
	cType := 'Z';
	if cEht = '%' or cEht = '�C' or cEht = 'Wh/m�' or cEht = 'bar' or cEht='???/??�'  or cEht='-'  or cEht='?/?�' or cEht='??/?�'or cEht= '??/??�' or cEht='??. ??. ??' then
		cType := 'M';
	end if;
	if cEht = 'Wh' then
		cStdEht := 'kWh'; 
	elsif cEht = '?�' then
		cStdEht := '???.?�'; 
	else
		cStdEht := cEht; 
	end if;
	return cStdEht;
end;

procedure PrepFVQuan  
as
	-- Erzeugen der neuen Gr��en f�r freie Variable 
	-- Gr��eneintrag mit ID wie Standardgr��e, jedoch mit positiven FK_QUN  (statt negativ)
	-- Vorauss.: in QUANZUO sind die Daten eingetragen, erzeugt durch Abgriff aus der TP- Kunden-DB
	/*	select 'insert into QUANZUO(KNZTEIL, EHT, QUANT, STDEHT, SHORT_NAME, DESCRIPTION, FK_QUAN)
	 values(''' || last2(ZRKNZ) || ''','''|| EHT || ''','''|| last2(ZRKNZ)||''',null,'''  || last2(ZRKNZ) || ''','''||    last2(ZRKNZ)|| ''',null );'
	 from TPMAS_VI
	 where gueltend is null
	 and ARVTYP='T' and EHT is not null   and ZRKNZ like '%./_%' ESCAPE  '/' and ZRKNZ not like 'GLOVAR.%'
	 order by ZRKNZ;
	 */
	-- Die Eintr�ge enthalten nach Selektion noch keine FK_QUAN; alle beginnen im KNZTEIL mit "_"

	cQuant	varchar2(40);  
	nQuMin	number := 1000;       -- Pr�fen: Es wird vorausgesetzt, dass die Nummern bis 1000 nicht besetzt sind 
	nQuMax	number := MaxQu();                      
	nQuId		number;
	cType   varchar2(2);   
	cEht	varchar2(40);
	cStdEht		QUANT.DEFAULT_UNIT%type;
	iC		integer;
	cKz		varchar2(40);
	bFound	boolean;
	cId		varchar2(100);
	nNr		number(12) :=0;

begin
	psi_pckg.SaveHit;
	psi_pckg.InsHit2( nNr,  ' ' );

    -- W�hle die n�chste Nr. > 1000 (und <2000)                                      
    nQuId := NextQu( nQuMin ) - 1;

	for rec in (select * from QUANZUO where substr(KNZTEIL,1,1) = '_' and FK_QUAN is null and QUANT_TYP='FV' order by QUANT, EHT) loop   
		-- Erzeuge eine FK_QUAN
		-- Trage die Gr��e in die Gr��entabelle MTR_QUANTITIES_TA und die  MTR_QUANTITY_UNITS_TA ein, wenn noch nicht vorhanden
		cQuant := rec.QUANT;
		cKz := rec.KNZTEIL;
		cEht := rec.EHT;
		if rec.EHT='NA' then
			cId := rec.KNZTEIL  || '.-';	                  
		else
			cId := rec.KNZTEIL  || '.' || rec.EHT;	                  
		end if;
		if cQuant is not null then
			select count(*) into iC from   QUANT  where ID = cId;
			if iC >= 0 then
				nQuId := nQuId + 1;
                
				-- Typ und Std-Unit
				cStdEht := GetType_StdEht( rec.EHT, cType );
				psi_pckg.InsHit2( nNr,  
	 			'insert into por.MTR_QUANTITIES_TA (ID, VERS_DATE, SHORT_NAME, DESCRIPTION, TYPE, DIRECTION, FK_QUN, DEFAULT_UNIT)
	 			 values ( ''' || cId || ''',''01/01/2000 06:00:00'',''' || rec.SHORT_NAME|| ''',''' || 
	 			  rec.DESCRIPTION|| ''',''' || cType|| ''',null ,' ||  nQuId|| ',''' ||  cStdEht|| ''');');
	 			  	
	            for recUnit in (select  ID from UNITS where SHORT_NAME= cEHT) loop
					psi_pckg.InsHit2( nNr,  
					'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuId || ',''' || recUnit.ID || '''); ');
									
					if cEht <> cStdEht then
						psi_pckg.InsHit2( nNr,  
						'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuId || ',''' || cStdEht || '''); ');
						--Insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(nQuId, cStdEht);
					end if;                                                                          
				end loop;
				
				if rec.EHT = 'Wh' then
					psi_pckg.InsHit2( nNr,  
						'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuId || ',''MWh'');');
				elsif rec.EHT = 'm�' then
					psi_pckg.InsHit2( nNr,  
						'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuId || ',''?m�'');');
				end if;
				commit;
				
			end if;    
		end if;
	end loop;  
	-- Aktualisieren der Seq-ID Quantity_Units_Id in MTR_SEQUENCE_TA
	psi_pckg.InsHit2( nNr,  'update por.MTR_SEQUENCE_TA set GEN_VALUE=(select max(FK_QUN) from QUANT) where GEN_KEY=''Quantity_Units_Id'';');
    commit;

end;

procedure PrepGVQuan( cFS in varchar2 default 'IMP_TP_XML' )     
as
	-- Erzeugen der neuen Gr��en f�r globale Variable 
	-- Die in quanzuo enthaltenen GV- Gr��en  werden erzeugt und f�r den Transport-Adapter auf bestimmte, daf�r vorgesehene Gr��en gemappt
	-- 
	
	cLast1 varchar2(20);
	cDesc	varchar2(80);  
	nQuMin	number := 3000;       
	nQuMax	number := MaxQu(); 
	nQuId	number; 
	cQuId		QUANT.ID%type;                    
	cInsQuId	QUANT.ID%type;                    
	cType   varchar2(2);   
	cEht	UNITS.ID%type;
	cStdEht		QUANT.DEFAULT_UNIT%type;
	nFs_Id	number;            
	cKz		varchar2(40);
	nNr		number(12) := 0;
	bFound  	boolean := false;
	bQuanFound	boolean := false;
	bMap	boolean;
	cLastQuId	QUANT.ID%type;
	cLastEht  	UNITS.ID%type;                  

begin     
	psi_pckg.SaveHit;
	psi_pckg.InsHit2( nNr,  ' ' );

    -- W�hle die n�chste Nr. > 2000 (und <3000)                                      
    nQuId := NextQu( nQuMin ) - 1;
	
	for recFS in (select ID from por.MTR_FS_TA where SHORT_NAME = cFS) loop 
		-- Achtung - es muss vermieden werden, dass die gleiche Gr��e mehrfach angelegt wird - daher Sortierung nach Gr��e und anlegen nur, wenn Abweichung von voriger Gr��e
		for rec in (select KNZTEIL, EHT, last1(KNZTEIL) QUANT, SHORT_NAME from QUANZUO where QUANT_TYP = 'GV' order by QUANT, EHT) loop
			cEht := rec.EHT;
			cQuId := rec.QUANT;
			cKz	  := rec.SHORT_NAME;
		    bfound     := false;
		    bQuanFound := false;
		    if  cLastQuId is not null and rec.QUANT || '_' || rec.EHT = cLastQuid then
		    	-- ist identisch mit letztem Eintrag - nur mapping durchf�hren
		    	cQuId := rec.QUANT || '_' || rec.EHT;
		    	bMap := true;
		    else	
			    if  cLastQuId is null or (cLastQuId <> cQuId or cLastEht <> cEht) then
					-- pr�fen, ob Gr��e mit der Einheitenkombi existiert
					for recQ in (select * from QUANT where ID = rec.QUANT) loop
						bQuanFound := true;
						for recQU in (select * from por.MTR_QUANTITY_UNITS_TA where ID = recQ.FK_QUN) loop
							if recQU.FK_UNITS = rec.EHT then
								-- Kombi ist bereits eingetragen
								bFound := true;
							end if; 
						end loop;
						if not bFound then
							-- pr�fe, ob die Gr��e die gleiche BaseUnit hat wie die neue Einheit
							if GetBaseUnit( nNr, recQ.DEFAULT_UNIT) = GetBaseUnit( nNr, rec.EHT) then
								-- nur die Einheit muss zugeordnet werden
								psi_pckg.InsHit2( nNr,  
									'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || recQ.FK_QUN || ',''' || rec.EHT || ''');');
								bFound := true;
							end if;
						end if;
					end loop; 
					-- wenn Gr��e nicht vorhanden oder Gr��e vorh, aber EHT andere Baseunit hat
					if not bFound then
						if bQuanFound then
							-- Gr��e erzeugen mit neuem Namen
							cQuId := rec.QUANT || '_' || rec.EHT;
						elsif (cLastQuId = cQuId and cLastEht <> cEht) then
							if GetBaseUnit(nNr,cLastEht) <>	GetBaseUnit(nNr,cEht) then
								cQuId := rec.QUANT || '_' || rec.EHT;
							end if;
						end if; 
						bMap := true;
						if cLastQuId is null or cLastQuId <> cQuId then
							nQuId := nQuId + 1; 
							-- zugeh�rige Einheit
							-- Typ und Std-Unit
							cStdEht := GetType_StdEht( rec.EHT, cType );
							psi_pckg.InsHit2( nNr,  
				 			'insert into por.MTR_QUANTITIES_TA (ID, VERS_DATE, SHORT_NAME, DESCRIPTION, TYPE, DIRECTION, FK_QUN, DEFAULT_UNIT)
				 			 values ( ''' || cQuId || ''',''01/01/2000 06:00:00'',''' || cQuId|| ''',''' || 
				 			  cQuId || ''',''' || cType|| ''',null ,' ||  nQuId|| ',''' ||  cStdEht|| ''');');
						end if;
						if cLastEht <> cEht or cLastQuid <> cQuId then		 			 
				 			-- Zuordnung der Einheit(en)
				            for recUnit in (select  ID from UNITS where SHORT_NAME= cEHT) loop
								psi_pckg.InsHit2( nNr,  
								'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuId || ',''' || recUnit.ID || '''); ');
												
								if cEht <> cStdEht then
									psi_pckg.InsHit2( nNr,  
									'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuId || ',''' || cStdEht || '''); ');
									--Insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(nQuId, cStdEht);
								end if;                                                                          
							end loop;
						end if;
					end if;
			        cLastQuId := cQuId;
			        cLastEht  := cEht;
				else
					bMap := true;
				end if;
			end if;
			-- Mapping f�r Adapter
			if bMap then
				psi_pckg.InsHit2( nNr,  
				'insert into por.MTR_FS_QUANTITIES_TA (FS_ID, FK_QUANT, ALIAS_NAME)
				values (''' || recFS.Id || ''',''' || cQuId || ''',''' || cKz || ''');' );
			end if;
			commit;
		end loop;
	end loop;          
	-- Aktualisieren der Seq-ID Quantity_Units_Id in MTR_SEQUENCE_TA
	psi_pckg.InsHit2( nNr,  'update por.MTR_SEQUENCE_TA set GEN_VALUE=(select max(FK_QUN) from QUANT) where GEN_KEY=''Quantity_Units_Id'';');
	commit;
end;

procedure PrepSGQuan( cFS in varchar2 default 'IMP_TP_XML' ) 
as
	-- Die in quanzuo enthaltenen spezifischen Gr��en (Kennung SG=spez. Gr��e) werden f�r den Transport-Adapter auf bestimmte, daf�r vorgesehene Gr��en gemappt
	-- Vorauss in por: grant insert,update on MTR_FS_QUANTITIES_TA to porrep;
	
	cLast1 varchar2(20);
	cQuant	varchar2(40);  
	cDesc	varchar2(80);  
	nQuan	number := 1000;
	cStdeht	varchar2(40);                     
	nFs_Id	number;  
	nNr		number(12) :=0;
	bFound  boolean := false;

begin     
	psi_pckg.SaveHit;
	psi_pckg.InsHit2( nNr,  ' ' );
	
	
	--delete from  por.MTR_FS_QUANTITIES_TA;                  
	for recFS in (select ID from por.MTR_FS_TA where SHORT_NAME = cFS) loop
		for rec in (select KNZTEIL, EHT, last1(KNZTEIL) QUANT from QUANZUO where QUANT_TYP = 'SG' ) loop   
			-- zun�chst zuzuordnende Gr��en ermitteln und in QUANZUO eintragen
			--  Beispieleintr�ge: 
			if rec.EHT = 'Wh' then
				rec.QUANT := rec.QUANT || '_WH' ;
			elsif  rec.EHT = 'm�' then
				rec.QUANT := rec.QUANT || '_M3' ;
			end if;
			cQuant := rec.Quant;
			bFound := false;
			for recQ in (select * from QUANT where ID = cQuant) loop
				bFound := true;
				psi_pckg.InsHit2( nNr,  
					'insert into por.MTR_FS_QUANTITIES_TA (FS_ID, FK_QUANT, ALIAS_NAME)
					values (''' || recFS.ID || ''',''' || recQ.ID || ''',''' || rec.KNZTEIL || '.' || rec.EHT || ''');' );
			end loop;                                                                                                     
			if not bFound then
				psi_pckg.InsHit2( nNr,  'Gr��e mit ID=' || cQuant || ' ist nicht in Quantity-Tabelle enthalten!');
			end if;

		end loop;
	end loop;          
	commit;
end;

procedure PrepSTQuan( cFS in varchar2 default 'IMP_TP_XML' )     
as
	-- Erzeugen neuer Standardgr��en 
	-- Gr��eneintrag mit ID wie Standardgr��e und mit negativer FK_QUN  
	-- Vorauss.: in QUANZUO sind die Daten eingetragen, erzeugt durch Abgriff aus der TP- Kunden-DB
	-- Die Eintr�ge enthalten nach Selektion noch die falsche Einheit - eingetragen ist die Standardeinheit in Feld EHT; die Einheit ist aus dem Kennzeichenteil nach letztem Punkt zu extrahieren
	-- Inhaltsbsp.: insert into QUANZUO(KNZTEIL, EHT, QUANT, STDEHT, SHORT_NAME, DESCRIPTION, FK_QUAN, QUANT_TYP) values ('MNG.IST.-','-','MNG.IST.-','','MNG.IST.-','MNG.IST.-','','ST');
	-- Eingetragen werden in Quanzuo alle Gr��e/Einheiten aus dem select:
/*	select distinct last2(ZRKNZ) ZRKNZ, EHT from TPMAS_VI
	where gueltend is  null  --and ARVTYP='T'
	and EHT is not null
	and ZRKNZ not like '%./_%' ESCAPE  '/'
	and last1(ZRKNZ) not in ('BST','BSTPRZ','BEW','NTZISM','ABEMAX')
	and ZRKNZ not like '%./_%' ESCAPE  '/'
	order by ZRKNZ
*/
	
	cLast1 varchar2(20);
	cDesc	varchar2(80);  
	nQuMin	number := -1000;       
	nQuMax	number := MaxQu(); 
	nQuId	number; 
	cQuId	QUANT.ID%type;                    
	cType   varchar2(2);   
	cEht	UNITS.ID%type;
	cStdEht		QUANT.DEFAULT_UNIT%type;
	nFs_Id	number;            
	cKz		varchar2(40);
	nNr		number(12) := 0;
	bFound  	boolean := false;
	bQuanFound	boolean := false;
	bMap	boolean;
	cLastQuId	QUANT.ID%type;
	cLastEht  	UNITS.ID%type;                  

begin     
	psi_pckg.SaveHit;
	psi_pckg.InsHit2( nNr,  ' ' );

    -- W�hle die n�chste Nr. <-1000                                      
    nQuId := NextQu( nQuMin ) + 1;
	
	for recFS in (select ID from por.MTR_FS_TA where SHORT_NAME = cFS) loop 
		-- Achtung - es muss vermieden werden, dass die gleiche Gr��e mehrfach angelegt wird - daher Sortierung nach Gr��e und anlegen nur, wenn Abweichung von voriger Gr��e
		-- Eintrag in quanzuo:  in SHORT_NAME ist quantity eingetragen, in KNZTeil ist quantity+"."+Einheit
		for rec in (select KNZTEIL, EHT, SHORT_NAME AS QUANT from QUANZUO where QUANT_TYP = 'ST' order by QUANT, EHT) loop
			cEht := rec.EHT;
			cQuId := rec.QUANT;
			cKz	  := rec.KNZTEIL;
	    	-- zun�chst muss nach zusammen gesetzter Gr��e gesucht werden, wenn cLastQuId angeh�ngte einheit hat (wenn zwei Punkte im String)
	    	if  cLastQuId is not null and instr(cLastQuId,'.',1,2) > 0 then
		 		if rec.QUANT || '_' || rec.EHT = cLastQuid then
		 			cQuId := cLastQuId;
		 		end if;
		 	end if;	
		    bfound     := false;
		    bQuanFound := false;
		    if  cLastQuId is null or (cLastQuId <> cQuId or cLastEht <> cEht) then
		    	if cLastQuId is null or cLastQuId <> cQuId then
					for recQ in (select * from QUANT where ID = cQuId) loop
						bQuanFound := true;
						for recQU in (select * from por.MTR_QUANTITY_UNITS_TA where ID = recQ.FK_QUN) loop
							if recQU.FK_UNITS = rec.EHT then
								-- Kombi ist bereits eingetragen
								bFound := true;
							end if; 
						end loop;
			    	end loop;
					-- pr�fen, ob Gr��e mit der Einheitenkombi existiert
					for recQ in (select * from QUANT where ID = cQuId) loop
						bQuanFound := true;
						for recQU in (select * from por.MTR_QUANTITY_UNITS_TA where ID = recQ.FK_QUN) loop
							if recQU.FK_UNITS = rec.EHT then
								-- Kombi ist bereits eingetragen
								bFound := true;
							end if; 
						end loop;
						if not bFound then
							-- pr�fe, ob die Gr��e die gleiche BaseUnit hat wie die neue Einheit
							if GetBaseUnit( nNr, recQ.DEFAULT_UNIT) = GetBaseUnit( nNr, rec.EHT) then
								-- nur die Einheit muss zugeordnet werden
								psi_pckg.InsHit2( nNr,  
									'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || recQ.FK_QUN || ',''' || rec.EHT || ''');');
								bFound := true;
							end if;
						end if;
					end loop;
				else
					-- die Gr��e kann in vorigem Satz neu erzeugt worden sein (ist also noch nicht in QUANT); wenn die Einheiten unterschiedelich Baseunit haben, muss
					-- die Gr��e auch neu erfasst werden
					if cLastQuId = cQuId then
						if GetBaseUnit( nNr, cLastEht) <> GetBaseUnit( nNr, rec.EHT) then
							-- Gr��e erzeugen mit neuem Namen
							cQuId := rec.QUANT || '_' || rec.EHT;
							nQuId := nQuId - 1; 
							-- zugeh�rige Einheit
							-- Typ und Std-Unit
							cStdEht := GetType_StdEht( rec.EHT, cType );
							psi_pckg.InsHit2( nNr,  
				 			'insert into por.MTR_QUANTITIES_TA (ID, VERS_DATE, SHORT_NAME, DESCRIPTION, TYPE, DIRECTION, FK_QUN, DEFAULT_UNIT)
				 			 values ( ''' || cQuId || ''',''01/01/2000 06:00:00'',''' || cQuId|| ''',''' || 
				 			  cQuId || ''',''' || cType|| ''',null ,' ||  nQuId|| ',''' ||  cStdEht|| ''');'); 
				 			-- Einheit zuordnen
				            for recUnit in (select  ID from UNITS where ID = cEht or SHORT_NAME= cEht) loop
								psi_pckg.InsHit2( nNr,  
								'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuId || ',''' || recUnit.ID || '''); ');
							end loop;
							bFound := true;
				 		end if;            
				 	end if;
					if not bFound then	
						-- die Einheit kann eine andere sein 
						if cEht is not null and cEht <> cLastEht then
							if GetBaseUnit( nNr, cLastEht) = GetBaseUnit( nNr, rec.EHT) then
								-- nur die Einheit muss zugeordnet werden
								psi_pckg.InsHit2( nNr,  
									'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuId || ',''' || rec.EHT || ''');');
								bFound := true;
							end if;
						end if;
					end if;
				end if; 
				-- wenn Gr��e nicht vorhanden oder Gr��e vorh, aber EHT andere Baseunit hat
				if not bFound then
					if bQuanFound then	
						-- Gr��e erzeugen mit neuem Namen
						cQuId := rec.QUANT || '_' || rec.EHT;
					end if; 
					bMap := true;
					if cLastQuId is null or cLastQuId <> cQuId then
						nQuId := nQuId - 1; 
						-- zugeh�rige Einheit
						-- Typ und Std-Unit
						cStdEht := GetType_StdEht( rec.EHT, cType );
						psi_pckg.InsHit2( nNr,  
			 			'insert into por.MTR_QUANTITIES_TA (ID, VERS_DATE, SHORT_NAME, DESCRIPTION, TYPE, DIRECTION, FK_QUN, DEFAULT_UNIT)
			 			 values ( ''' || cQuId || ''',''01/01/2000 06:00:00'',''' || cQuId|| ''',''' || 
			 			  cQuId || ''',''' || cType|| ''',null ,' ||  nQuId|| ',''' ||  cStdEht|| ''');');
					end if;
					if (cLastEht is null or cLastEht <> cEht) or (cLastQuId is null or cLastQuid <> cQuId) then		 			 
			 			-- Zuordnung der Einheit(en)
			            for recUnit in (select  ID from UNITS where ID = cEht or SHORT_NAME= cEht) loop
							psi_pckg.InsHit2( nNr,  
							'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuId || ',''' || recUnit.ID || '''); ');
											
							if cEht <> cStdEht then
								psi_pckg.InsHit2( nNr,  
								'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuId || ',''' || cStdEht || '''); ');
							end if;                                                                          
						end loop;
						-- Erg�nze ggf. noch zus�tliche Einheiten
						if rec.EHT = 'Wh' then
							psi_pckg.InsHit2( nNr,  
								'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuId || ',''MWh'');');
						elsif rec.EHT = 'm�' then
							psi_pckg.InsHit2( nNr,  
								'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuId || ',''Tm�'');');
						elsif rec.EHT = '???.?�' then
							psi_pckg.InsHit2( nNr,  
								'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuId || ',''?�'');');
						elsif rec.EHT = '?�' then
							psi_pckg.InsHit2( nNr,  
								'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuId || ',''???.?�'');');
						end if; 

					end if;
				end if;
		        cLastQuId := cQuId;
		        cLastEht  := cEht;
			else
				bMap := true;
			end if;
			-- Mapping f�r Adapter
			if bMap then
				psi_pckg.InsHit2( nNr,  
				'insert into por.MTR_FS_QUANTITIES_TA (FS_ID, FK_QUANT, ALIAS_NAME)
				values (''' || recFS.Id || ''',''' || cQuId || ''',''' || cKz || ''');' );
			end if;
			commit;
		end loop;
	end loop;          
	-- Aktualisieren der Seq-ID Quantity_Units_Id in MTR_SEQUENCE_TA
	psi_pckg.InsHit2( nNr,  'update por.MTR_SEQUENCE_TA set GEN_VALUE=(select max(FK_QUN) from QUANT) where GEN_KEY=''Quantity_Units_Id'';');
	commit;
end;


end; -- pckg
/
