Package PERFORM_Pckg AUTHID DEFINER
is 
--------------------------------------------------------------------------------
-- package mit Funktionen f�r Performanztests 
--------------------------------------------------------------------------------
-- LIH 02.05.2014 Erstellung   
	
--------------------------------------------------------------------------------
-- Konstanten
--------------------------------------------------------------------------------
	iGasjahrStartMonat	integer := 10;

	function WaitSec( iSek in number ) return  number;

	procedure InsHit2( nLfdNr in out number, cLine in varchar2 );
	procedure Longtest( nRuns in number, cTesttext in varchar2, nStartTisId in number default 0 );
	procedure Performanztest (cTesttext in varchar2, nStartTisId in number default 0, nNr in out number);
	Procedure SaveHit;
end;
/

Package body PERFORM_Pckg
is

function WaitSec( iSek in number ) return  number
as
	-- vorgeg. Anzahl Sekunden warten
	dSys		date := sysdate; 
	iSecPerDay	integer := 24*60*60;
	nC			integer := 0;
begin                       
	While ( (sysdate - dSys)*iSecPerDay < iSek ) loop
		nC := nC + 1;
	end loop;
	return nC;
end;
		
Procedure SaveHit
as
-- Sichern der Tabelle HIT2 in die Langzeit-Tabelle HIT3
-- Ablauf:
--  Max(LFDNR) into nMax3 in HIT3 ermitteln
--  nMax3 auf LFDNR in HIT2 aufaddieren
--  alle S�tze aus HIT2 in HIT3 inserten
--  HIT2 l�schen
--
	nMax3	integer;
	nNr		number(12) := 0;
	iC		integer := 0;
begin
	select max(LFDNR) into nMax3 from HIT3;
	if nMax3 is null then
		nMax3 := 0;
	end if;
	for rec in (select rowid from HIT2 where LFDNR is null) loop
		iC := iC + 1;
		update HIT2 set LFDNR=iC where rowid = rec.rowid;
	end loop;
	update HIT2 set LFDNR = LFDNR + nMax3;
	commit;
	insert into HIT3 (select * from HIT2);
	commit;
	delete from HIT2;
	commit;
end;

procedure InsHit2( nLfdNr in out number, cLine in varchar2 )
as
begin
	if nLfdNr = 0 then
		delete from hit2;
		commit;
	end if;

	nLfdNr := nLfdNr + 1;
	insert into HIT2 values( nLfdNr, cLine );
	commit;
end;


procedure Longtest( nRuns in number, cTesttext in varchar2, nStartTisId in number default 0 )
as
	-- Mehrfaches Ausf�hren des Performantests mit Ergebnisauswertung (Bestimmung der mittleren Lese- und Schreibzeiten)
	iC		integer := 0;
	nNr		number(12) :=0;
	nAnz	number := 100;
	nAnzTis		number := 0;
	nAnzRead	number := 0;
	nSumRead	number := 0;
	nSumWrite	number := 0;
	nSek		number(20,2);
	nNextId		number := nStartTisId; 
	dStart		date := sysdate;
	dDatum		date := sysdate;
	nSid		number;
	nLoop		number;	
	
begin
	select userenv('sessionid') into nSid from dual;
	-- Hit2 sichern in Hit3
	PERFORM_Pckg.SaveHit;
	PERFORM_Pckg.InsHit2( nNr,  ' ' );
	PERFORM_Pckg.InsHit2( nNr,  '-- Start Langzeit-Performanz-Messung mit ' || to_char(nRuns) || ' Durchl�ufen am ' || to_char(dStart,'dd.mm.yy hh24:mi:ss') );
	if (nStartTisId is null or nStartTisId = 0) then
		for recP in (select LAST_ID from PERFORMANZ where TABELLE='DD_SYS' and 
			DATUM = (select max(DATUM) from PERFORMANZ where TABELLE='DD_SYS'  and LAST_ID > 0)) loop
			nNextId := recP.LAST_ID;
		end loop;
	end if;
		
	for ni in 1..nRuns loop
		Performanztest( cTesttext, nNextId, nNr );
		for recP in (select LAST_ID from PERFORMANZ where TABELLE='DD_SYS' and 
			DATUM = (select max(DATUM) from PERFORMANZ where TABELLE='DD_SYS'  and LAST_ID > 0)) loop
			nNextId := recP.LAST_ID;
		end loop;
		nLoop := WaitSec(2);
	end loop;
	
	-- Bestimmung der mittleren Lese- und Schreibzeiten
	for recP in (select * from PERFORMANZ where DATUM >= dStart order by DATUM) loop
		if recP.ZUGRIFFSFORM = 'Lesen' then
			iC := iC + 1;
			nSumRead := nSumRead + recP.DAUER_IN_SEK;
			nAnzRead := nAnzRead + recP.BEARB_SAETZE;
			nAnzTis  := nAnzTis + recP.DURCHLAEUFE;
		else
			nSumWrite := nSumWrite + recP.DAUER_IN_SEK;
		end if;
	end loop;
	insert into PERFORMANZ (SID,DATUM, TABELLE, ZUGRIFFSFORM, BEARB_SAETZE, DURCHLAEUFE, DAUER_IN_SEK, BEMERKUNG, LAST_ID)
		 values( nSid,sysdate+1/(24*60*60), 'Auswertung Mittelwert', 'Lesen', nAnzRead, nAnzTis, round(nSumRead/iC,1),  cTesttext, 0 );
	insert into PERFORMANZ (SID,DATUM, TABELLE, ZUGRIFFSFORM, BEARB_SAETZE, DURCHLAEUFE, DAUER_IN_SEK, BEMERKUNG, LAST_ID)
		 values( nSid,sysdate+2/(24*60*60), 'Auswertung Mittelwert', 'Lesen+Schreiben', nAnzRead, nAnzTis, round(nSumWrite/iC,1),  cTesttext, 0 );
	commit;
	
	nSek := (sysdate - dStart)*24*60*60;
	PERFORM_Pckg.InsHit2( nNr, 'Gesamtlaufzeit Performanztest mit ' || to_char(nRuns) || ' Durchl�ufen in Sek: ' || to_char(nSek,'9999.99') );


end;
	
procedure Performanztest (cTesttext in varchar2, nStartTisId in number default 0, nNr in out number)
as
	-- Ermitteln von Performanzwerten zum Portalsystem
	
	iC		integer;
	--nNr		number(12) :=0;
	nAnz	number := 100;
	nAnzTis		number := 0;
	nAnzRead	number := 0;
	nSek		number(20,2);
	nLastId		number := 0;
	nNextId		number := nStartTisId; 
	dStart1		date := sysdate;
	dStart		date := sysdate;
	dDatum		date := sysdate;
	cText0		PERFORMANZ.BEMERKUNG%type := cTesttext;
	cText		PERFORMANZ.BEMERKUNG%type;
	nSid		number;	
	
	
begin
	-- Evtl sicherstellen, dass Cache gecleared ist - erfordert besondere Rechte (evtl als sysdba)
	-- execute immediate 'alter system flush buffer_cache';
	-- execute immediate 'alter system flush shared_pool';
	select userenv('sessionid') into nSid from dual;

	--PERFORM_Pckg.SaveHit;
	if (nStartTisId is null or nStartTisId = 0) then
		for recP in (select LAST_ID from PERFORMANZ where DATUM = (select max(DATUM) from PERFORMANZ where LAST_ID > 0)) loop
			nNextId := recP.LAST_ID;
		end loop;
	end if;

	PERFORM_Pckg.InsHit2( nNr,  ' ' );
	PERFORM_Pckg.InsHit2( nNr,  '-- Start Performanz-Messung ' || to_char(dStart,'dd.mm. hh24:mi:ss') );


	-- Schleife mit Lesezugriffen auf die Bewegungsdatentabelle
	cText := cText0 || ': Lesen je 30 Tage ab TIS-ID > ' || to_char(nNextId);
	-- Es werden f�r unterschiedliche Zeitreihen jeweils Daten f�r Monatszeitr�ume gelesen		
	for rec in (select ID, FROM_DATE_TS, UNTIL_DATE_TS from TIS where ID > nNextId and ARCHIVETYPE=1 and TIME_LEVEL='MI15' and FROM_DATE_TS is not null and (UNTIL_DATE_TS - FROM_DATE_TS) > 100 order by ID ) loop 
	    nAnzTis := nAnzTis + 1;
	    if nAnzTis > nAnz then
	    	exit;
	    end if;
	    if nAnzTis = 1 then
			dStart	:= sysdate;
		end if;
	    
		for ddrec in (select * from DD_SYS where FK_TIS = rec.ID and REF_DATE between rec.UNTIL_DATE_TS-60 and  rec.UNTIL_DATE_TS-30) loop
			nAnzRead := nAnzRead + 1;
		end loop;
		nLastId := rec.ID;
	end loop;

	-- Eintragen in Performanztabelle
	cText := cText || ' bis TIS-ID = ' || to_char(nlastId);    	
	nSek := (sysdate - dStart)*24*60*60;
	insert into PERFORMANZ (SID,DATUM, TABELLE, ZUGRIFFSFORM, BEARB_SAETZE, DURCHLAEUFE, DAUER_IN_SEK, BEMERKUNG, LAST_ID)
		 values( nSid, sysdate, 'DD_SYS', 'Lesen', nAnzRead, nAnzTis-1, nSek,  cText, nLastId );
	commit;
	
	-- Schleife mit Schreib- und Lesezugriffen mit Bewegungsdaten  (�bertrag DD_SYS auf portalrep->DDSYS) 
	execute immediate 'truncate table DDSYS';
	dStart := sysdate;
	nAnzTis := 0;
	cText := cText0 || ': Lesen+Schreiben in PORTALREP.DDSYS - # S�tze = ' || to_char(nAnzRead);
	
	-- Es werden f�r unterschiedliche Zeitreihen jeweils Daten f�r Monatszeitr�ume gelesen und geschrieben		
	for rec in (select ID, FROM_DATE_TS, UNTIL_DATE_TS from TIS where ID > nNextId and ARCHIVETYPE=1 and TIME_LEVEL='MI15' and FROM_DATE_TS is not null and (UNTIL_DATE_TS - FROM_DATE_TS) > 100 order by ID ) loop 
	    nAnzTis := nAnzTis + 1;
	    if nAnzTis > nAnz then
	    	exit;
	    end if;
		for ddrec in (select * from DD_SYS where FK_TIS = rec.ID and REF_DATE between rec.UNTIL_DATE_TS-60 and  rec.UNTIL_DATE_TS-30) loop
			insert into DDSYS values ddrec;
		end loop;
		commit;
	end loop;

	-- Eintragen in Performanztabelle
	nSek := (sysdate - dStart)*24*60*60;
	insert into PERFORMANZ (SID, DATUM, TABELLE, ZUGRIFFSFORM, BEARB_SAETZE, DURCHLAEUFE, DAUER_IN_SEK, BEMERKUNG, LAST_ID)
		 values( nSid, sysdate, 'DD_SYS', 'Lesen+Schreiben', nAnzRead, nAnzTis-1, nSek,  cText, nLastId );
	commit;

	nSek := (sysdate - dStart1)*24*60*60;
	PERFORM_Pckg.InsHit2( nNr, 'Laufzeit in Sek: ' || to_char(nSek,'999.99') );

end;	

end; -- pckg
/
