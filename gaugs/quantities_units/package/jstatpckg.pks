Package JStatpckg AUTHID DEFINER
is                          
	-- LIH 23.04.2014 Ersterstellung
/*	                                
	Diese package enth�lt Module zur JSTAT-Auswertung
	

*/	

	type array_of_text is table of varchar2(300) index by binary_integer;
    ftFile 	utl_file.file_type;
	crlf 	constant varchar2( 2 ) := chr(10 );
    LOG_FILE_MAXLINE constant binary_integer := 4000;	-- max. Zeichen pro Zeile
	c_location  varchar2(100) := 'JSTAT_DIR';
	-- f�r Oracle10: select * from all_directories; -> ggf. Directory setzen
	--	OWNER	DIRECTORY_NAME	DIRECTORY_PATH
	--	SYS		JSTAT_DIR		/usr/PROZESS/eeswha/jStat
	-- Directory setzen (mit sys/<syspw> as sysdba oder system/<systempw>@<db>;
	-- connect system/systempw@eeswpor.psi;
	-- create or replace directory JSTAT_DIR as '/usr/PROZESS/eeswha/jStat';
	-- GRANT READ ON DIRECTORY SYS.JSTAT_DIR TO PORTALREP WITH GRANT OPTION;

	c_filename  varchar2(100) := 'swh_jstat_gc_140411132600.log';
	c_open_mode varchar2(2)   := 'w';
	c_read_mode varchar2(2)   := 'r';
	c_file      UTL_FILE.FILE_TYPE;

PROCEDURE ReadJStat ( cVerz in varchar2 default null, cDatei in varchar2, cParm in varchar2, cjjmmtthhmiss in varchar2, nIV in number );
	
end;
/

Package body JStatpckg
is


function GetValues( cString in varchar2, cSep in varchar2, aTxt in out NOCOPY array_of_text ) return integer
as
    nPos 	number := 0;
    nLast 	number := 1;
    nLen 	number := length( cString );
    nI	 	number := 1;	-- Anzahl array-Elemente
    nSep 	number := 1;    -- Z�hler f�r Trennzeichen
begin         
	
	-- instr( string1, string2 [, start_position [, nth_appearance ] ] )
	-- Beispiel-Satz: 557248.0 557248.0  0.0    0.0   4458112.0 4375245.9 16717056.0  639428.6  524288.0 306736.3    328  162.156  381  1193.308 1355.464
	While ( nLast <= nLen ) loop
		nPos := instr( cString, cSep, 1, nSep );
		if nPos > nLast then
			aTxt(nI) := substr( cString, nLast, nPos - nLast );
			nI := nI + 1;
		nSep := nSep + 1;
		else
			if nPos = 0 then
				-- Rest abholen nach letztem ;			
				aTxt(nI) := substr( cString, nLast, nLen - nLast );
				nLast := nLen + 1;
			else
				if nPos = nLast and cSep = ' ' then
					-- erneutes Leerzeichen
					nSep := nSep + 1;
				else	
					aTxt(nI) := '';
					nSep := nSep + 1;
				end if;
			end if;
		end if;
		if nPos > 0 then
			nLast := nPos + 1;
		end if;
	end loop;
	return ( nI );
end;		
	

/*------------------------------------------------------------------------------
 ReadJStat ( cVerz in varchar2 default 'CSV_VERZ', cDatei in varchar2 );
		  -- Ergebnisdaten aus jStat Messung lesen und in Tabelle JSTAT eintragen
Input
	cVerz	- Verzeichnis
	cDatei  - Name der zu verarbeitenden CSV-Datei 
	cParm   - zugelassen: gc oder vmstat
	cjjmmtthhmiss - Startzeit der Messung in vorgeg. Datei
	nIV		- Messintervall in Sekunden
------------------------------------------------------------------------------*/
PROCEDURE ReadJStat ( cVerz in varchar2 default null, cDatei in varchar2, cParm in varchar2, cjjmmtthhmiss in varchar2, nIV in number )
as
	cLine	varchar2(400);
	cSep	varchar2(1) := ' ';
	iC		integer := 0;
	bFound	boolean;
	iLen	integer;
	iGesLen integer := 0;
	nNr		number(12) :=0;
	nAnz	number;
	nSek	number(20,2); 
	dStart	date := sysdate;
	nI		integer;
	aText	array_of_text;
	cFirst	varchar2(1);    
	cStmt	varchar2(1000);
	nOk		number := 0;
	nErr    number := 0;
	nSid	number;
	dBegin	date := to_date(cjjmmtthhmiss,'yymmddhh24miss');
	dZp		date;
	nDeltaT	number := nIV/(60*60*24);
	nS0C     number; 
	nS1C     number; 
	nS0U     number; 
	nS1U     number; 
	nEC      number; 
	nEU      number; 
	nOC      number; 
	nOU      number; 
	nPC      number; 
	nPU      number; 
	nYGC     number; 
	nYGCT    number; 
	nFGC     number; 
	nFGCT    number; 
	nGCT		number;
	cGCT	varchar2(20);

--   	TYPE CSVREC is table of SAT_MASTERDATA%rowtype;
--   	rSMD	CSVREC;
begin 
	execute immediate 'alter session set NLS_LENGTH_SEMANTICS=''CHAR''';
	-- da Daten mit Dezimalpunkt gelesen werden - der aber als Komma zu interpretieren ist
	execute immediate 'alter session set nls_numeric_characters = ''.,''';                  
	     
	select userenv('sessionid') into nSid from dual;
	
	if cVerz is not null then
		c_location := cVerz;
	end if;
	if cDatei is not null then
		c_filename := cDatei;
	end if;
	
	/*SaveHit;

	InsHit2( nNr,  ' ' );
	InsHit2( nNr,  '-- Start Import Stammdaten f�r Sparte ' || cMedium || ' ' || to_char(dStart,'dd.mm. hh24:mi:ss') );
	InsHit2( nNr,  'Parameter: Verzeichnis=' || c_location || ', Eingangsdatei: ' || c_filename );  
	*/
	
    /*
    -- ------------------------------------------------------------------------
    -- Datei �ffnen
	-- Struktur    
     S0C    S1C    S0U    S1U      EC       EU        OC         OU       PC     PU    YGC     YGCT    FGC    FGCT     GCT   
557248.0 557248.0  0.0    0.0   4458112.0 4375245.9 16717056.0  639428.6  524288.0 306736.3    328  162.156  381  1193.308 1355.464

	--    1. Zeile ist identisch mit Strukturbeschreibung
    --  Werte sind mit whitespace (Leerzeichen) voneinander abgegrenzt
    */
    -- ------------------------------------------------------------------------
    UTL_FILE.FCLOSE_ALL;
    c_file := UTL_FILE.FOPEN (c_location, c_filename, c_read_mode);
    
    -- Strukturzeile lesen
    UTL_FILE.GET_LINE (c_file, cLine); 
    
	-- weitere Datens�tze lesen und verarbeiten
	-- Achtung: wird eine Datei mit falscher Satzstruktur eingestellt, so klappt der Import nicht !
	loop
        begin
            UTL_FILE.GET_LINE (c_file, cLine); 
            -- Achtung: Zeilen k�nnen Header statt Zahlen enthalten           
            nAnz := GetValues( cLine, cSep, aText );               
            begin
            	-- Abrage f�hrt zu Fehler, wenn Headerzeile gelesen wurde
            	if to_number(aText(1)) >= 0 then
		            iC	:= iC + 1;
		            dZP := dBegin + iC*nDeltaT;
		            
		            if cParm = 'gc' then
		            	--  
		            	cStmt :=  '1:' || aText(1) || '2:' || aText(2) || '3:' ||  aText(3) || '4:' ||  aText(4) || '5:' ||  aText(5) || '6:' ||  aText(6) || '7:' ||  aText(7) 
		            	 	|| '8:' ||  aText(8) || '9:' ||  aText(9) || '10:' ||  aText(10) || '11:' || 
		            	 	aText(11) || '12:' || aText(12) || '13:' ||  aText(13) || '14:' ||  aText(14) || '15:' ||  aText(15);
		            	-- 	
		            	nS0C     := to_number(aText(1)); 
						nS1C     := to_number(aText(2)); 
						nS0U     := to_number(aText(3)); 
						nS1U     := to_number(aText(4)); 
						nEC      := to_number(aText(5)); 
						nEU      := to_number(aText(6)); 
						nOC      := to_number(aText(7)); 
						nOU      := to_number(aText(8)); 
						nPC      := to_number(aText(9)); 
						nPU      := to_number(aText(10)); 
						nYGC     := to_number(aText(11)); 
						nYGCT    := to_number(aText(12)); 
						nFGC     := to_number(aText(13)); 
						nFGCT    := to_number(aText(14)); 
						cGCT	 := aText(15);
						nGCT	 := to_number(cGCT);	--nvl(aText(15),0));
		            	-- 		                            1		2	3	4	5	6	7	8	9	10	11		12	13	14		15
		            	begin
			            	insert into JSTAT(SID, ZEITPKT, S0C, S1C, S0U, S1U, EC, EU, OC, OU, PC, PU, YGC, YGCT, FGC, FGCT, GCT)
			            	 values( nSid, dZP, nS0C, nS1C, nS0U, nS1U, nEC, nEU, nOC, nOU, nPC, nPU, nYGC, nYGCT, nFGC, nFGCT, nGCT);
			            	 
			            	 --to_number(aText(1)),to_number(aText(2)), to_number(aText(3)), to_number(aText(4)), to_number(aText(5)), to_number(aText(6)), to_number(aText(7)), to_number(aText(8)), to_number(aText(9)), to_number(aText(10)),
			            	  --	to_number(aText(11)),to_number(aText(12)), to_number(aText(13)), to_number(aText(14)), to_number(aText(15)));   
						 exception
				        	when others then
				               	raise;	
				         end;
   					elsif cParm = 'vmstat' then
		            	--                               1		2	3			4		5		6		7			8		9	10		11	 12		13		14		15   16		17	
		            	cStmt :=  '1:' || aText(1) || ' 2:' || aText(2) || ' 3:' ||  aText(3) || ' 4:' ||  aText(4) || ' 5:' ||  aText(5) || ' 6:' ||  aText(6) || ' 7:' ||  aText(7) 
		            	 	|| ' 8:' ||  aText(8) || ' 9:' ||  aText(9) || ' 10:' ||  aText(10) || ' 11:' || 
		            	 	aText(11) || ' 12:' || aText(12) || ' 13:' ||  aText(13) || ' 14:' ||  aText(14) || ' 15:' ||  aText(15)|| ' 16:' ||  aText(16)|| ' 17:' ||  aText(17);
		            	insert into JSTAT(SID, ZEITPKT, VM_R, VM_B, VM_SWPD, VM_FREE, VM_BUFF, VM_CACHE, VM_SI, VM_SO, VM_BI, VM_BO, VM_IN, VM_CS, VM_US, VM_SY, VM_ID, VM_WA, VM_ST)
		            	 values( nSid, dZP, 
			            	 to_number(aText(1)),to_number(aText(2)), to_number(aText(3)), to_number(aText(4)), to_number(aText(5)), to_number(aText(6)), to_number(aText(7)), to_number(aText(8)), to_number(aText(9)), to_number(aText(10)),
			            	 to_number(aText(11)),to_number(aText(12)), to_number(aText(13)), to_number(aText(14)), to_number(aText(15)), to_number(aText(16)), to_number(aText(17)));
		            	 
		            	 --aText(1),aText(2), aText(3), aText(4), aText(5), aText(6), aText(7), aText(8), aText(9), aText(10),
		            	 	--aText(11),aText(12), aText(13), aText(14), aText(15), aText(16), aText(17));   
					else
		        		nErr := nErr + 1;
						exit;
					end if;				            	 
		           	nOk := nOk + 1;
		            commit;
		   		end if;
		   exception
        	when others then
		   		continue;
		   end;
        exception
		 	when NO_DATA_FOUND then
		 		EXIT;
        
        	when others then
        		--InsHit2( nNr, 'Fehler: ' || sqlerrm );
        		--InsHit2( nNr, 'f�r Zeile ' || iC || ':' || cLine );
        		nErr := nErr + 1;
               	raise;
        end;           
    end loop;
	commit; 
	
    -- ------------------------------------------------------------------------
    -- Dateien schlie�en
    -- ------------------------------------------------------------------------
    UTL_FILE.FCLOSE_ALL;
	/*nSek := (sysdate - dStart)*24*60*60;
	InsHit2( nNr, 'Anzahl �bernommene S�tze: ' || nOk );
	InsHit2( nNr, 'Anzahl fehlerhafte S�tze: ' || nErr );
	InsHit2( nNr, 'Laufzeit in Sek: ' || to_char(nSek,'999.99') || ',   S�tze gesamt: ' || iC );
    */
exception
    when others then
   		--InsHit2( nNr, 'Fehler: ' || sqlerrm );
        UTL_FILE.FCLOSE_ALL;
        raise;
END;

end;
/
