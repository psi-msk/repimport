create or replace Package SWHReport AUTHID DEFINER
is
	-- LIH 11.09.2014 Ersetzt MD_OBJECTS durch MD_OBJECTS; ChkObjects() eliminiert
	-- LIH 11.03.2014 R_GAS_D1 - Ber�cksichtigung der Stunde 25 (von 5-6 Uhr) bei Sommer-/Winterzeitumschaltung; W_GANG_Values - dito
	-- LIH 05.03.2014 R_GAS_D1 Aufrufparameter ge�ndert
	-- LIH 05.03.2014 GetPict - Umstellung nach �nderung Synonym UNITS - Zuordnung zu MTR_EXT_UNITS_VI
	-- LIH 17.12.2013 Ermitteln des Stationsnamens �ber Station-Objekt-ID statt Kennzeichen (da Medium fehlt)
	-- LIH 16.12.2013 R_WASSER_TB Fehlerhandling eingebaut                                                
	-- LIH 23.10.2013 R_GAS_MAX - Fehlerhandling eingebaut
	-- LIH 22.10.2013 DelOldReports - L�schen alter Reportergebnisdaten
	-- LIH 21.10.2013 R_WASSER_TB - �berarbeitung
	-- LIH 18.10.2013 R_GAS_H Nachbesserung (dVon nicht in UTC umsetezn)
	-- LIH 17.10.2013 R_GAS_H �berarbeitung
	-- LIH 15.10.2013 R_W_GANG - Funktionsaufruf ge�ndert
	-- LIH 03.09.2013 MAXAbgabeValues - nPosOffset eingef�hrt
	-- LIH 27.08.2013 R_GAS_MAX - max 15 Positionen f�r Tab. 1 erzeugen
	-- LIH 14.08.2013 R_GAS_H - Zeitreihen-Select auf nicht abgeleitete Gr��en beschr�nken
	-- LIH 13.08.2013 R_GAS_H - erweitert um Parameter nObjlGasbezugId in number
	-- LIH 12.08.2013 R_GAS_H - nur Zeitreihen ber�cksichtigen, die Daten mit vorgeg. Datum haben
	-- LIH 27.05.2013 Ersetzt: OBJECTS durch MD_OBJECTS
	-- LIH 23.05.2013 R_GAS_H - Umstellung auf DD_SYS und Ausgabe Statuswerte integriert
	-- LIH 21.05.2013 R_GAS_D1 - Umbau: Verwendung PTABHEAD statt POR_TABVALUES_TA
	-- LIH 17.05.2013 R_W_GANG fertigcodiert
	-- LIH 16.05.2013 Neu: R_W_GANG 
	-- LIH 07.05.2013 R_GAS_MAX - Code f�r Kavernentabelle erg�nzt; r_9list entfernt - nach PSIReports
	-- LIH 30.04.2013 R_GAS_MAX - Code f�r Maxwerte-Tabelle erg�nzt
	-- LIH 27.04.2013 R_GAS_MAX - Code f�r Gasabgabetabellen erg�nzt
	-- LIH 26.04.2013 R_GAS_MAX - bearbeitet Value-Erzeugung Haupttabelle
	-- LIH 25.04.2013 R_GAS_MAX - fertiggestellt Header-Erzeugung
	-- LIH 24.04.2013 R_GAS_MAX - erg�nzt Header
	-- LIH 25.03.2013 R_GAS_D1 umgestellt auf PTABHEAD statt PTABHEAD und Anpassung R9_List
	-- LIH 04.03.2013 R_GAS_MAX - 1. Entwurf
	-- LIH 18.02.2013 R_WASSER_TB - Ausgabe Wetter- und HB-Inhalt erg�nzt
	-- LIH 14.02.2013 R_W_TB_Hochb_Ausgabe und R_W_TB_Hochb erg�nzt
	-- LIH 13.02.2013 R_W_TB_Prod bzgl. Druckerh�hung und Hochb. erg�nzt
	-- LIH 12.02.2013 R_W_TB_Prod erg�nzt
	-- LIH 01.02.2013 R_WASSER_TB erweitert
	-- LIH 31.01.2013 R_WASSER_TB erg�nzt
	-- LIH 30.01.2013 R_GAS_D1 - L�schen in TABHEAD erg�nzt; Setzen dVon auf Gastagbeginn-UTC integriert
	-- LIH 29.01.2013 R_9List - Ausgabe Short_name und Ber�cksichtigung .AVG-Gr��en; Verbesserung in R_GAS_H, OBJECTS durch MD_OBJECTS ersetzt
	-- LIH 17.01.2013 Optimierung R_9List  
	-- LIH 16.01.2013 Neu: R_9List
	-- LIH 23.11.2012 L�sche R_Wetter in R_GAS_H
	-- LIH 19.11.2012 Detaillierung R_GAS_H
	-- LIH 16.11.2012 Module f�r R_GAS_H
	-- LIH 12.11.2012 Entwurf R_GAS_H
	-- LIH 09.11.2012 GenD1HeaderCols - Unit in Header ausweisen (statt kWh)
	-- LIH 03.11.2012 Verb. R_GAS_D1
	-- LIH 02.11.2012 Umstellung R_GAS_D1 - nur 1 Objektliste �bergeben
	-- LIH 01.11.2012 Verbesserung R_GAS_D1
	-- LIH 30.10.2012 Ersterstellung
/*	                                
	Diese package enth�lt alle spezifisch f�r SW Hannover entwickelten Reportfunktionen.
	
	Die folgenden Reports werden abgedeckt:
	1. R_GAS_D1 - Tagesprotokoll Gas
	   3 Tabellen mit je bis zu 21 Zeitreihen 
	   verwendet die speziell daf�r erzeugten Tabellen PTABHEAD und PTABVAL 
	   Das F�llen der Tabelle �bernimmt die Funktion R_GAS_D1; das zugeh�rige jrxml-File ist R_GAS_D1-Vx-y
	   Parameter: 3 Objektliste, jeweils mit ID �bergeben
	   			  Datum - Gastag
	   			  
	2. R_GAS_H - Stundenprotokoll Gas

*/	

--	TYPE OLDHEAD is table of PTABHEAD%rowtype;
	TYPE OUTHEAD is table of PTABHEAD%rowtype;
	type array_of_nums is table of number index by binary_integer;
	
	-- Funktionen f�r Report R_GAS_D1
	function GenD1HeaderCols( nSid in number, nObjlist in number )	return varchar2;
--	function GenD1Values( nSid in number, nObjlist in number, cVon in varchar2, cBis in varchar2, cDataSrc in varchar2 )	return varchar2;

	function R_GAS_D1( nObjlistId1 in number, dUtcVon in date ) return OUTHEAD pipelined;
	--function R_GAS_D1( nObjlistId1 in number, dUtcVon in date ) return varchar2;

	function R_GAS_MAX( nObjlistId1 in number,  nObjlGasabgeId in number, nObjlMaxId in number, nObjlKaverneId in number, dVon in date )  return OutHEAD pipelined;
--	function R_GAS_MAX( nObjlistId1 in number,  nObjlGasabgeId in number, nObjlMaxId in number, nObjlKaverneId in number, dVon in date ) return varchar2; -- f�r Debug-Test 

	function R_GAS_H( nObjlZwId in number, nObjlBilanzId in number, nObjlGasbezugId in number, nObjlWetterId in number, nObjlFiktiveId in number, cVon in varchar2 ) return number;

	function R_WASSER_TB( nObjlBezugId in number, nObjlDruckerhId in number, nObjlHochbId in number, nObjlWetterId in number, nObjlHbInhaltId in number, 
					cBemerkung in varchar2, dVon in date ) return number;
--	function R_W_GANG( nObjlistId1 in number,  nObjlistId2 in number, dVon in date ) return varchar2;
	function R_W_GANG( nObjlistId1 in number,  nObjlistId2 in number, dVon in date ) return OUTHEAD pipelined;

	function GetDateTime return varchar2;	-- gibt aktuelle Datum und Zeit in der Form dd.mm.yyyy hh24:mi zur�ck		
	function GetMin( cTimelevel in varchar2) return number;
	
	procedure DelOldReports;	-- L�schen von Ergebnisdaten �lter als 1 Tag
	
end;
/

create or replace Package body SWHReport
is
	-- �nderungsinfos siehe Header

---------------------------------------------------------------------------------------------

	DATE_FORMAT_GER 				constant varchar2(21) := 'DD.MM.YYYY HH24:MI:SS';

---------------------------------------------------------------------------------------------
procedure ExecSqlDirect( cStmt IN varchar2 )
AS
-- LIH 07.05.2010 Eliminieren dbms_sql / Umstellen auf execute immediate
BEGIN
	execute immediate cStmt;
EXCEPTION
	WHEN OTHERS THEN
		raise;
END;

procedure Delete_Sessiondata ( nSid in number, cTable in varchar2 )
is
    nSession	number;
    cStmt     varchar2(200);
    pragma autonomous_transaction;                                                  
begin
    cStmt := 'delete from ' || cTable || ' where SID = ' || to_char(nSid);
    ExecSqlDirect( cStmt );
    commit;
end;

procedure DelSid( nSid in number )
as
	-- L�schen der SID-bezogenen Daten in allen Reportuser-Tabellen
begin
	delete from R_WASSER_TMP where SID=nSid;
	delete from R_GAS_H where SID=nSid;
	delete from R_WETTER where SID=nSid;
	delete from PTABHEAD where SID=nSid;
	delete from PTABVAL where SID=nSid;
--	delete from TABHEAD where SID=nSid;
--	delete from TABVAL where SID=nSid;
	delete from REPORTOBJ_TA where SID=nSid;
	delete from REPORTGRP_TA where SID=nSid;
	commit;
end;	

procedure DelOldReports
as 
	-- L�schen von Ergebnisdaten �lter als 1 Tag
begin
	for rec in (select * from REPORTGRP_TA where ERSTELLT_AM < sysdate - 1 or ERSTELLT_AM is null) loop
		DelSid( rec.SID );
	end loop;
end;

function Text4Attr( nObjId in number, cAttribut in varchar2 ) return varchar2
as
	cText MD_VARIABLES.VALUE%type;
begin
	for recVar in (select VALUE from MD_VARIABLES where OBJID = nObjId and ATTR_NAME = cAttribut) loop
		cText := recVar.VALUE;
	end loop;
	return cText;
end;
		
function ReturnWert( bError in boolean, cError in varchar2 )
return varchar2
as
begin
	if bError then
		return cError;
	else
		return 'OK';
	end if;
end;

function GetDateTime return varchar2
	-- gibt aktuelle Datum und Zeit in der Form dd.mm.yyyy hh24:mi zur�ck		
as
begin
		return to_char(sysdate, 'dd.mm.yyyy hh24:mi');
end;

function GetMin( cTimelevel in varchar2) return number
as                  
	nMin	number;
begin              
	if cTimelevel = 'S' then
		nMin := 1/60;
	elsif cTimelevel = 'MI3' then
		nMin := 3;
	elsif cTimelevel = 'MI5' then
		nMin := 5;
	elsif cTimelevel = 'MI15' then
		nMin := 15;
	elsif cTimelevel = 'H' then
		nMin := 60;
	elsif cTimelevel = 'H2' then
		nMin := 120;
	elsif cTimelevel = 'D' then
		nMin := 24*60;
	elsif cTimelevel = 'M' then
		nMin := 1440*30;
	elsif cTimelevel = 'Q' then
		nMin := 1440*90;
	elsif cTimelevel = 'Y' then
		nMin := 1440*365;
	else                
		nMin := 1;
	end if;
	return nMin;
end;
  
function GetPict( nTis in number ) return varchar2
as
	cPict REPOBJ.PICTURE%type := '999G999G990';
	cDec	varchar2(20) := '99999999999999999999';
	cUnitId	UNITS.ID%type;
begin
	select UNIT into cUnitId from TIS where ID = nTis;
	-- Standard verwenden - Quantity->UNIT  (PK auf ID+VERS_DATE)
	for recUni in (select PRECISION from UNITS where ID = cUnitId 
		--and VERS_DATE=(select max(VERS_DATE) from UNITS where ID = cUnitId) 
		) loop
		if recUni.PRECISION > 0 then		
			cPict := cPict || 'D' ||  substr(cDec,1,recUni.PRECISION);
		end if;
	end loop;
	
	return cPict;
end;	

function GenD1HeaderCols( nSid in number, nObjlist in number )	return varchar2
	-- erzeugt Header-Zeile f�r Report R_GAS_D1 f�r Zeitreihen-Reporttabelle zur Session-ID; R�ckgabe: OK oder ERROR
as	
	bError	boolean 		:= FALSE;   
	cError	varchar2(1024) 	:= null;
	bFound	boolean 		:= FALSE;   
	cStmt	varchar2(2000);
	cCols	varchar2(1000) := '';
	cSets	varchar2(1000) := '';
	cUnit	varchar2(30);
begin
	-- sucht sich zu den Zeitreihen alle Spalteninfos f�r die Header-Zeilen und speichert sie in DB-Tabelle PTABHEAD_TA
	--  1. Zeile: aus Label (letzte 3 Zeichen des Kennz.) oder R_D1_NR Obj-Shortname, 
	--  2. Zeile: Kurzname Station oder R_D1_NAME,  aus Obj-Description 
	--  3. Einheit - hier immer {kWh]
                           
	-- die Objekte aus REPOBJ verarbeiten              
	for recObj in (select POS,FK_TIS,SHORT_NAME,DESCRIPTION,TIS.QUANTITY,TIS.UNIT from REPOBJ, TIS where SID = nSid and FK_TIS=TIS.ID order by POS) loop
		-- �ber TIS die Daten laden
		-- in PTABHEAD werden f�r 1-21 Zeitreihen je drei Headerspalten erzeugt: R1H1, R1H2, R1H3  etc
		-- set R1h1=Nr, r1h2=Kurzname, r1h3=Einheit ... 
		cUnit := recObj.UNIT;
		if cUnit = 'Dimensionslos' then
			cUnit := '-';
		end if;
		cCols := cCols || 'S'||recObj.POS || 'Z1,S'|| recObj.POS || 'Z2,S'|| recObj.POS || 'Z3,';
		if recObj.UNIT = 'kWh/m�' then
			cSets := cSets || '''' || recObj.SHORT_NAME || ''',''' || recObj.DESCRIPTION || ''',''[Wh/m�]'','; 
		else
			cSets := cSets || '''' || recObj.SHORT_NAME || ''',''' || recObj.DESCRIPTION || ''',''[' || cUnit || ']'','; 
		end if;
		bFound := true;
	end loop;
	if bFound then
		begin
			cStmt := 'insert into PTABHEAD (SID, FK_OBJLIST,' || substr(cCols,1, length(cCols)-1) ||
					 ') values(' || nSid || ','  || nObjlist || ','  || substr(cSets,1, length(cSets)-1) || ')';
		    ExecSqlDirect( cStmt );
		    commit;
		exception    
			when others then
				cError := sqlerrm;
				bError := true;
		end;	
	end if;			

	return ReturnWert( bError, cError );
end;	                         


function GenD1Values( nSid in number, nObjlist in number, cVon in varchar2, cBis in varchar2, cDataSrc in varchar2 )	return varchar2
	-- erzeugt Zeilen in Zeitreihen-Reporttabelle zur Session-ID; R�ckgabe: OK oder ERROR
as	
	bError	boolean 		:= FALSE;   
	cError	varchar2(1024) 	:= null;
	bFound	boolean 		:= FALSE;   
	cStmt	varchar2(4000);
	cCols	varchar2(2000) := '';
	cSel	varchar2(2000) := '';
	cWhere	varchar2(3000) := '';
	cFrom	varchar2(1000) := '';
	cAlias	varchar2(6);     
	iNr		integer := 1;
	iMax	integer := 20; 
	iMaxPos	integer;
	iStart	integer;        
	iEnd	integer;
	iLen	integer;
	dVon	date := to_date(cVon,DATE_FORMAT_GER);
	dBis	date := to_date(cBis,DATE_FORMAT_GER);
	cBis2	varchar2(20) := cBis;
begin
	-- erh�lt die gleiche zu erzeugende Session-ID sowie Von und Bis-Zeit f�r die Datenselektion
	-- holt sich aus Tabelle REPORTGRP_TA und REPOBJ die Objekte zur Gruppe und baut dynamisch die Datenselektion aus cDataSrc (DD_ORIGINAL oder DD_SYSTEM)
	-- auf, f�hrt sie aus und schreibt das Ergebnis in Tabelle TABVALUES_TA
	execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
    
    -- Ber�cksichtigung der Stunde 25 (von 5-6 Uhr) bei Sommer-/Winterzeitumschaltung
    if psi_pckg.isMesz(dVon) = 1 and psi_pckg.isMesz(dBis) = 0 then
    	dBis := dBis + 1/24;
    	cBis2 := to_char( dBis, DATE_FORMAT_GER );
    end if;
    -- 21 Spalten bedingen ein Statement > 4000 Zeichen ->daher muss eine Unterteilung erfolgen
   	bFound := false;
   	iEnd := iStart+iMax;
	-- die Objekte aus REPOBJ verarbeiten              
	for recObj in (select POS,FK_TIS,VALUE_CATEGORY, UNIT from REPOBJ where SID = nSid and FK_OBJLIST = nObjlist 
		and POS <= iMax order by POS) loop
		-- �ber TIS die Daten laden
		-- in <cDataSrc> stehen die Zeitreihenwerte  (select between cVon and cBis - liefert alle Werte >=cVon und <= cBis
		-- Aufbau eines outer-join in der Form:
		cCols := cCols || 'VAL'||recObj.POS || ',' || 'STATUS'||recObj.POS || ',' ;
		if recObj.UNIT = 'kWh/m�' then
			-- Ausgabe in Wh/m� gefordert
			cSel := cSel || 'round(a'||recObj.POS || '.VAL*1000,0),' || 'a'||recObj.POS || '.CONDITION,';
		else
			cSel := cSel || 'round(a'||recObj.POS || '.VAL,0),' || 'a'||recObj.POS || '.CONDITION,';
		end if;
		cFrom := cFrom || cDataSrc || ' a'||recObj.POS || ',';                             
		if not bFound then
			cWhere := cWhere || 'a'||recObj.POS || '.FK_TIS=' || recObj.FK_TIS || ' and a'||recObj.POS || '.REF_DATE between ''' || cVon || ''' and ''' || cBis2 || '''';
			cAlias := 'a'||recObj.POS || '.';
			bFound := true;
		else
			cWhere := cWhere || ' and a'||recObj.POS || '.FK_TIS(+)=' || recObj.FK_TIS || ' and a1.REF_DATE=a'||recObj.POS || '.REF_DATE(+)' ||
				' and a'||recObj.POS || '.REF_DATE(+) between ''' || cVon || ''' and ''' || cBis2 || '''';
		end if;
		iNr := iNr + 1;
	end loop;
	if bFound then 
		begin                                    
			-- es sind max. 20 Spalten m�glich, um Statementl�nge <4000 Z. zu halten
			iLen := length(cCols) + length(cSel) + length(cFrom) + length(cWhere);
			cStmt := 'insert into PTABVAL (SID, FK_OBJLIST, REFDATE, REFCDATE,' || substr(cCols,1, length(cCols)-1) || 
					 ') (select ' || nSid || ',' || nObjlist || ',' || cAlias || 'REF_DATE,psi_pckg.UTC2MESZhour(' || cAlias || 'REF_DATE),'  || substr(cSel,1, length(cSel)-1) || 
					 ' from ' || substr(cFrom,1, length(cFrom)-1) || 
					 ' where '|| cWhere || ' )';
		    ExecSqlDirect( cStmt );
		    commit;
			-- Erg�nze Zusatzspalten > 20 einzeln
			for recObj in (select POS,FK_TIS,VALUE_CATEGORY, UNIT from REPOBJ where SID = nSid and FK_OBJLIST = nObjlist 
				and POS > iMax order by POS) loop
				cCols := cCols || 'VAL'||recObj.POS || ',' || 'STATUS'||recObj.POS || ',' ;
				for recVal in (select REFDATE from PTABVAL where SID = nSid and FK_OBJLIST=nObjlist order by REFDATE) loop
					if recObj.UNIT = 'kWh/m�' then
						cStmt := 'update PTABVAL set VAL'||recObj.POS ||
						 '=(select round(VAL*1000,0) from DD_SYS where FK_TIS=' || recObj.FK_TIS || ' and REF_DATE=''' || to_date(recVal.REFDATE,DATE_FORMAT_GER) || '''),' ||
						 'STATUS'||recObj.POS ||'= (select CONDITION from DD_SYS where FK_TIS=' || recObj.FK_TIS || ' and REF_DATE=''' || to_date(recVal.REFDATE,DATE_FORMAT_GER) || ''')' ||  
							' where SID=' || nSid || ' and FK_OBJLIST=' || nObjlist || ' and REFDATE=''' || to_date(recVal.REFDATE,DATE_FORMAT_GER) || '''';
					else
						cStmt := 'update PTABVAL set VAL'||recObj.POS ||
						 '=(select round(VAL,0) from DD_SYS where FK_TIS=' || recObj.FK_TIS || ' and REF_DATE=''' || to_date(recVal.REFDATE,DATE_FORMAT_GER) || '''),' ||
						 'STATUS'||recObj.POS ||'= (select CONDITION from DD_SYS where FK_TIS=' || recObj.FK_TIS || ' and REF_DATE=''' || to_date(recVal.REFDATE,DATE_FORMAT_GER) || ''')' ||  
							' where SID=' || nSid || ' and FK_OBJLIST=' || nObjlist || ' and REFDATE=''' || to_date(recVal.REFDATE,DATE_FORMAT_GER) || '''';
					end if;
				    ExecSqlDirect( cStmt );
				    commit;
				end loop;							
			end loop;
		    -- Erg�nze Mittelwerte oder Summenwerte
		    cSel := '';
			for recObj in (select POS,FK_TIS,VALUE_CATEGORY from REPOBJ where SID = nSid and FK_OBJLIST = nObjlist order by POS) loop
				if recObj.VALUE_CATEGORY = 'M' then		-- Messwert => average bilden
					cSel := cSel || '(select round(avg(VAL' || recObj.POS || '),0) from PTABVAL where SID=' ||  nSid || '),''V'',';
				else
					cSel := cSel || '(select sum(VAL' || recObj.POS || ') from PTABVAL where SID=' ||  nSid || '),''V'',';
				end if;
			end loop;
			cStmt := 'insert into PTABVAL (SID, FK_OBJLIST,REFDATE,' || substr(cCols,1, length(cCols)-1) || ') values (' ||  nSid || ',' || nObjlist || ',sysdate+365,' 
					 || substr(cSel,1, length(cSel)-1) || ')';
		    ExecSqlDirect( cStmt );
		    commit;
		exception    
			when others then
				cError := sqlerrm;
				bError := true;
		end;
	end if;
	return ReturnWert( bError, cError );
end;	
  

function R_GAS_D1( nObjlistId1 in number, dUtcVon in date ) return OUTHEAD pipelined
--function R_GAS_D1( nObjlistId1 in number, dUtcVon in date ) return varchar2

	/* R_GAS_D1 - Tagesprotokoll Gas
	   3 Tabellen mit je bis zu 21 Zeitreihen - in der Objektliste enthalten sind nur HON-Objekte
	   verwendet die speziell daf�r erzeugten Tabellen PTABHEAD und PTABVAL, PTABVAL 
	   Das F�llen der Tabelle �bernimmt die Funktion R_GAS_D1; das zugeh�rige jrxml-File ist R_GAS_D1-Vx-y
	   Parameter: 3 Objektlisten, jeweils mit ID �bergeben
	   			  Datum - Gastag in der Form dd.mm.yyyy
	*/
as
    --tHEAD		OUTHEAD_TYPE;
	cError	varchar2(1024) 	:= null;
	bError	boolean 		:= FALSE;   
    cVon  	varchar2(12)  	:= to_char(psi_pckg.UTCtoMESZ(dUtcVon),'dd.mm.yyyy');
	dVon	date			:= psi_pckg.UTCGasDayFirstHour(to_date(cVon || '07:00:00', DATE_FORMAT_GER));	-- Zeit in UTC 
	dBis	date			:= dVon + 23/24;
	nSid	number;
	iLfdnr	integer	:= 0; 
	cDataSrc	varchar2(12) := 'DD_SYS';
	cArchivtyp  varchar2(1) := '1';    
	cTimelevel	varchar2(4) := 'H';
	cPict	REPOBJ.PICTURE%type;
	cRet	varchar2(10);
	cText	varchar2(80);
	cNr		varchar2(20);
	cLab1	varchar2(20);
	nStaObjId	number;
	pragma autonomous_transaction;  -- necessary because of insert or deletes while the function is used in a select                                                 
	
begin                                         
	execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
	select userenv('sessionid') into nSid from dual;

	-- Reportgruppe und -objekte erzeugen
	DelSid( nSid );
	insert into REPORTGRP_TA (SID, REPNAME, ERSTELLT_AM) values( nSid, 'Report GAS_D1 f�r den ' || cVon, sysdate );
	
	-- f�r alle Objekte einer Liste werden die Zeitreihen-Daten geholt und in REPORTOBJ_TA gespeichert
	for rec in (select POSITION, FK_OBJID, SHORT_NAME, LABEL from MD_OBJLIST where ID = nObjlistId1) loop
		-- Variablentext R_D1_NAME holen oder den Namen der Station
		cLab1 :=   substr(rec.LABEL,1, instr(rec.LABEL,'.',1)-1);             
		nStaObjId := psi_pckg.GetParentId( rec.FK_OBJID, 'SC_ST' );
		-- ermittle Objekt(=Station) und Kurzname dazu
		cText := rec.LABEL;
		for recObj in (select  SHORT_NAME from MD_OBJECTS where OBJID = nStaObjId) loop
			cText := recObj.SHORT_NAME;
		end loop;
		for recVar in (select VALUE from MD_VARIABLES where OBJID = rec.FK_OBJID and ATTR_NAME='R_D1_NAME') loop
			cText := recVar.VALUE;
		end loop;
		cNr := substr(cLab1,length(cLab1)-2, 3);
		for recVar in (select VALUE from MD_VARIABLES where OBJID = rec.FK_OBJID and ATTR_NAME='R_D1_NR') loop
			cNr := recVar.VALUE;
		end loop;               
		
		-- Zeitreihe:  Stundenwerte (System) 
		for recTis in (select ID, UNIT, QUANTITY, FK_OBJ, VALUE_CATEGORY from TIS
			where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = cTimelevel and TIS.ARCHIVETYPE=cArchivtyp 
				and (TIS.QUANTITY like '%.AVG' or instr(TIS.QUANTITY,'.',1,1)=0)
			  	and dVon between TIS.FROM_DATE_TS and TIS.UNTIL_DATE_TS)
		loop
			iLfdNr := rec.POSITION;
			-- cPict  := Getpict(recTis.ID);		Aufbereitung der Werte erfolgt immer als ganzzahlige Werte mit Tausendertrennzeichen
			insert into REPORTOBJ_TA  (SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, UNIT, QUANTITY, FK_OBJID)
				values (nSid, nObjlistId1,iLfdNr, '0',recTis.ID, cPict, cNr, cText, recTis.VALUE_CATEGORY, rec.LABEL, recTis.UNIT, recTis.QUANTITY, rec.FK_OBJID );
		end loop;
		commit;
	end loop;   
	
	if iLfdNr > 0 then
		-- Tabellenheader erzeugen - 3 zeilig: 1. Zeile: aus Label (letzte 3 Zeichen des Kennz.) oder R_D1_NR Obj-Shortname, 
		--   2. Zeile: Kurzname Station oder R_D1_NAME, 3. Einheit - hier immer {kWh]

		cRet := GenD1HeaderCols( nSid, nObjlistId1 );             
		        
		if cRet = 'OK' then
			-- Tabellenwerte erzeugen
			cRet := GenD1Values( nSid, nObjlistId1, to_char(dVon,DATE_FORMAT_GER),to_char(dBis,DATE_FORMAT_GER), cDataSrc ); 
			
			if cRet = 'OK' then
				-- Headerzeile zur�ckgeben
				-- select SID, FK_OBJLIST, S1Z1, S1Z2, S1Z3, S2Z1, S2Z2, S2Z3, S3Z1, S3Z2, S3Z3, S4Z1, S4Z2, S4Z3, S5Z1, S5Z2, ...
				--/*
				for r in (select * from PTABHEAD where SID = nSid) loop
					pipe row( r );
				end loop;
				--Delete_Sessiondata( nSid, 'PTABHEAD' );
				--*/
				--return 'Ok';		-- f�r Debug-Test
			end if;
		end if;
	end if;	
end;  

function R_H_Aktiv( cObjLabel in varchar2, dUTC in date ) return integer
as
	cTimelevel	varchar2(4) := 'H';
	cArchivtyp  varchar2(1) := '1';    
	iAktiv		integer := 1;
begin
	for rec in (select KZ_MELDG_1, KZ_MELDG_2 from REIHENSCHALTUNG where KZ_STRANG_1 = cObjLabel) loop
		iAktiv := 0;
		for recTis in (select TIS.ID from TIS, MD_OBJECTS o where TIS.FK_OBJ = o.OBJID and o.LABEL = rec.KZ_MELDG_1 and TIS.TIME_LEVEL = cTimelevel and TIS.ARCHIVETYPE=cArchivtyp )
		loop
    		for recDD in (select VAL from DD_SYS where FK_TIS = recTis.ID and REF_DATE = dUTC) loop
    			iAktiv := recDD.VAL;
    		end loop;
  		end loop;
  		if iAktiv = 0 then
			for recTis2 in (select TIS.ID from TIS, MD_OBJECTS o where TIS.FK_OBJ = o.OBJID and o.LABEL = rec.KZ_MELDG_2 and TIS.TIME_LEVEL = cTimelevel and TIS.ARCHIVETYPE=cArchivtyp )
			loop
	    		for recDD2 in (select VAL from DD_SYS where FK_TIS = recTis2.ID and REF_DATE = dUTC) loop
	    			iAktiv := recDD2.VAL;
	    		end loop;
	  		end loop;
		end if;
	end loop;  			
	return iAktiv;	
end;

function R_H_BilanzRows( nSid in number, nObjlBilanzId1 in number, dVon in date ) return varchar2
as 
	-- Die Objektliste enth�lt je Station 1-4 Vn-Signale, 1 HS Signal und 1 Q-Signal
	cNr		varchar2(40);
	cLastNr	varchar2(40) := '-';
	iVnNr	integer;           
	cZwPict	varchar2(12) := '999G999G990';
	--dUtc	date := Psi_pckg.UTCtime( dVon );
	dUtc	date := dVon ;
	dBegin	date := Psi_pckg.UTCGasDayFirstHour( dVon );		-- ermittelt erste registrierte Stunde (7 Uhr gesetzl. ->UTC)
	nVal1	number;
	nVal2	number;
	nVal3	number;
	nVal4	number;
	nQday	number;
	nI		integer := 2;
	cName	varchar2(40);
	cLastName	varchar2(40) := '-';
	cRet	varchar2(5) := 'OK';
	nPos	number := 0;
	
     	
begin                                                                               
     for rec in (select * from REPORTOBJ_TA where SID = nSid and FK_OBJLIST=nObjlBilanzId1 order by POS) loop 
   		cNr	 	:= substr(rec.NR,1,3);
   		cName	:= rec.DESCRIPTION;
     	-- erst kommen je 2 Signalwerte f�r Einlag. und Auslag., dann ein Signalwert f�r Speicher Empelde
	    if cLastName <> cName then
	    	if cLastName <> '-' and cLastNr <> '000' then
	    		-- Satz f�r Station in Tabelle R_GAS_H eintragen
		    	nPos := nPos + 1;                                                                       
	    		nVal4 := nVal3 - nVal2;
	    		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2, VAL3, VAL4)
	    			values( nSid, nObjlBilanzId1, nPos, cLastNr, cLastName, dUtc, nVal1, nVal2, nVal3, nVal4);
	    		commit;
	    		nVal1 := null; nVal2 := null; nVal3 := null; nVal4 := null;
	    	end if;                    
	    	if cNr = '000' then		
		    	nPos := nPos + 1;                                                                       
    			-- hole den Vortageswert  (Speicher Empelde)
	    		select round(VAL,0) into nVal1 from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE = dBegin-1/24;
		    		
	    		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2, VAL3, VAL4)
	    			values( nSid, nObjlBilanzId1, nPos, '-', cName, dUtc, nVal1, null, null, null);
    			commit;
	    	else
	    		-- 1. Durchgang: hole den ersten Signalwert (in VAL2 - Wert der 2. Spalte)  - Einlagerung  od. Auslagerung
	    		select round(sum(VAL),0) into nVal2 from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE >=dBegin and REF_DATE <= dUTC;
	   			nI := 3;
	    	end if;
	    	cLastNr   := cNr;
	    	cLastName := cName;  -- in 1. Durchgang: lastname nun = Name              
	    else
	    	if nI = 2 then
	    		select round(sum(VAL),0) into nVal2 from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE >=dBegin and REF_DATE <= dUTC;
	    	elsif nI = 3 then
	    		select round(sum(VAL),0) into nVal3 from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE >=dBegin and REF_DATE <= dUTC;
	    	end if;
    		if nVal2 is null and nVal3 is null then		-- keine Daten f�r Schiene 1 gefunden -> abbrechen und Fehlereintrag 
    			cName := 'Daten fehlen';
    			cRet := 'ERROR';
    		end if;	
   			nI := nI + 1;
	    end if;
	end loop;
	return cRet;
end;

function R_H_GasbezugRows( nSid in number, nObjlGasbezugId1 in number, dVon in date ) return varchar2
as 
	-- Die Objektliste enth�lt je Station 1-4 Vn-Signale, 1 HS Signal und 1 Q-Signal
	cNr		varchar2(40);
	cLastNr	varchar2(40) := '-';
	iVnNr	integer;           
	cZwPict	varchar2(12) := '999G999G990';
	--dUtc	date := Psi_pckg.UTCtime( dVon );
	dUtc	date := dVon ;
	dBegin	date := Psi_pckg.UTCGasDayFirstHour( dVon );		-- ermittelt erste registrierte Stunde (7 Uhr gesetzl. ->UTC)
	nVal1	number;
	nVal2	number;
	nVal3	number;
	nVal4	number;
	nQday	number;
	nI		integer := 2;
	cName	varchar2(40);
	cLastName	varchar2(40) := '-';
	cRet	varchar2(5) := 'OK';
	nPos	number := 0;
	bOk		boolean := false;
     	
begin                                                                               
     for rec in (select * from REPORTOBJ_TA where SID = nSid and FK_OBJLIST=nObjlGasbezugId1 order by POS) loop 
   		cNr	 	:= substr(rec.NR,1,3);
   		cName	:= rec.DESCRIPTION;
     	-- erst kommen je 2 Signalwerte f�r Einlag. und Auslag., dann ein Signalwert f�r Speicher Empelde
	    if cLastName <> cName then
	    	if cNr = '000' then		
		    	nPos := nPos + 1;                                                                       
    			-- hole den Vortageswert  (H-Gasbezug-Saldo)
	    		select round(VAL,0) into nVal1 from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE = dBegin-1/24;
		    		
    			-- H-Gasbezug - es m�ssen noch die Werte 2 (Plan) und 3 (Ist) geholt werden
    			nI := 2;
	    	end if;
	    	cLastNr   := cNr;
	    	cLastName := cName;                    -- auch f�r Signal zum 2. Wert muss H-Gasbezug als Name vorgegeben werden, jedoch darf NR nicht 000 sein
	    else
	    	if nI = 2 then
	    		-- hole den Signalwert (Plan) (in VAL2 - Wert der 2. Spalte)
	    		select round(sum(VAL),0) into nVal2 from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE >=dBegin and REF_DATE <= dUTC;
	   			nI := 3;	-- f�r den 3. Wert muss als Name wieder H-Gasbezug gesetzt sein !!
	    	elsif nI = 3 then
	    		-- hole den Signalwert (Ist) (in VAL3 - Wert der 3. Spalte)
	    		select round(sum(VAL),0) into nVal3 from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE >=dBegin and REF_DATE <= dUTC;
	    		if cName = 'H-Gasbezug' then    	-- letztes Objekt der Liste berechnen
			    	nPos := nPos + 1;                                                                       
		    		nVal4 := nVal3 - nVal2;
		    		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2, VAL3, VAL4)
		    			values( nSid, nObjlGasbezugId1, nPos, cLastNr, cLastName, dUtc, nVal1, nVal2, nVal3, nVal4);
		    		commit;
		    		bOk := true;
		    	end if;
		    else
    			cName := 'Daten/Var. fehlen';
    			cRet := 'ERROR';
	    		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2, VAL3, VAL4)
		    			values( nSid, nObjlGasbezugId1, nPos, cLastNr, cLastName, dUtc, nVal1, nVal2, nVal3, nVal4);
	    		commit;
    		end if;	
	    end if;
	end loop;
	if not bOk then
		cName := 'Daten/Var. fehlen';
		cRet := 'ERROR';
		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2, VAL3, VAL4)
 			values( nSid, nObjlGasbezugId1, nPos, cLastNr, cLastName, dUtc, nVal1, nVal2, nVal3, nVal4);
		commit;
	end if;	
	return cRet;
end;

function R_H_WetterRows( nSid in number, nObjlWetterId1 in number, dVon in date, cVorgabe in varchar2 default null ) return varchar2
as 
	-- Wetterdaten werden ausgegeben mit: Text, Wert, Einheit
	cNr		varchar2(40);
	--cPict	varchar2(12) := '99G990D99';
	--dUtc	date := Psi_pckg.UTCtime( dVon );
	dUtc	date := dVon ;
	dBegin	date := Psi_pckg.UTCGasDayFirstHour( dVon );		-- ermittelt erste registrierte Stunde (7 Uhr gesetzl. ->UTC)
	nVal1	number;
	nSum	number := 0;
	cName	varchar2(40);
	cRet	varchar2(5) := 'OK';
	nPos	number := 0;
	
     	
begin                                                                               
     for rec in (select * from REPORTOBJ_TA where SID = nSid and FK_OBJLIST=nObjlWetterId1 order by POS) loop 
     	-- Die Einheit zum Wert wird in R_GAS_H.NR gespeichert
   		cName	:= rec.DESCRIPTION;
     	-- einfacher Werteeintrag ungruppiert
   		-- Satz f�r Station in Tabelle R_GAS_H eintragen
    	nPos := nPos + 1;   
    	if cVorgabe = 'Tagesmittel' then
    		-- Mittelwert f�r vorgeg. Tag aus h-Werten holen
	    	for recDD in (select VAL from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE >= dBegin and REF_DATE < dBegin + 1 ) loop
	    		nSum := nSum + recDD.VAL;
	    	end loop;                    
	    	nVal1 := round( nSum/24, 0 );
	    else
	    	-- Wert aus Zeitreihe auslesen
	    	for recDD in (select VAL from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE = dUTC ) loop
	    		nVal1 := round(recDD.VAL,2);
	    	end loop;
	    end if;
   		insert into R_WETTER (SID, FK_OBJLIST, POS, NAME, REFDATE, VAL1, UNIT1)
   			values( nSid, nObjlWetterId1, nPos, cName, dUtc, nVal1, rec.UNIT);
   		commit;
	end loop;
	return cRet;
end;

function R_H_ZwRows( nSid in number, nObjlZwId1 in number, dVon in date ) return varchar2
as 
	-- Die Objektliste enth�lt je Station 1-4 Vn-Signale, 1 HS Signal und 1 Q-Signal
	cNr		varchar2(4);
	cLastNr	varchar2(4) := '-';
	iVnNr	integer;           
	cZwPict	varchar2(12) := '999G999G990';
	--dUtc	date := Psi_pckg.UTCtime( dVon );
	dUtc	date := dVon ;
	dBegin	date := Psi_pckg.UTCGasDayFirstHour( dVon );		-- ermittelt erste registrierte Stunde (7 Uhr gesetzl. ->UTC)
	nVn1	number;
	nVn2	number;
	nVn3	number;
	nVn4	number;
	cStatus1	varchar2(4);
	cStatus2	varchar2(4);
	cStatus3	varchar2(4);
	cStatus4	varchar2(4);
	iAktiv1	integer;
	iAktiv2	integer;
	iAktiv3	integer;
	iAktiv4	integer;
	nVnSum	number;
	nHs		number;
	nQ		number;
	nQday	number;
	nI		integer;
	cName	varchar2(40);
	cRet	varchar2(5) := 'OK';
	nPos	number := 0;
	
     	
begin                                                                               
     for rec in (select * from REPORTOBJ_TA where SID = nSid and FK_OBJLIST=nObjlZwId1 order by POS) loop 
   		cNr	 := substr(rec.NR,1,3);
     	-- erst kommen 1-4 Vn-Werte, dann kommt HS (andere category), dann Q
	    if cLastNr <> cNr then                                                                       
	    	if cLastNr <> '-' then
	    		-- pr�fen, ob Objektliste f�r Station korrekt war
	    		if nHs is null then
	    			cName := 'Fehler in Objektliste';
	    			cRet := 'ERROR';
	    		end if;
	    		-- Satz f�r Station in Tabelle R_GAS_H eintragen
	    		nPos := nPos + 1;
	    		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, STATUS1, AKTIV1, VAL2, STATUS2, AKTIV2, VAL3, STATUS3, AKTIV3, VAL4, STATUS4, AKTIV4, VAL5, STATUS5, VAL6, STATUS6, VAL7, STATUS7, VAL8, STATUS8, MIN_ZEIT, MAX_ZEIT, KAMMER)
	    		 values( nSid, nObjlZwId1, nPos, cLastNr, cName, dUtc, nVn1, cStatus1, iAktiv1, nVn2, cStatus2, iAktiv2, nVn3, cStatus3, iAktiv3, nVn4, cStatus4, iAktiv4, nVnSum, null,nHs, null,nQ, null,nQday,null, null, null, null );
	    		commit;
	    		nVn1 := null;
	    	end if;
	    	for recDD in (select VAL, CONDITION from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE = dUTC) loop
	    		nVn1 := round(recDD.VAL,0);
	    		cStatus1 := recDD.CONDITION;
	    		iAktiv1 := R_H_Aktiv( rec.OBJLABEL, dUtc );
	    		cName := rec.DESCRIPTION;
	    	end loop;      
	    	cLastNr := cNr;                
	    	if cNr = '001' then		-- Sonderfall Station 001  (es existieren nur die Schienen 3 und 4)
	    		nI := 4;
	    		nVn3 := nVn1;               
	    		cStatus3 := cStatus1;
	    		iAktiv3 := iAktiv1;
	    		nVnSum := nVn3;
	    		nVn1 := null; nVn2 := null; nVn4 := null; nHs := null; cStatus1 := null;
	    	else
	    		if nVn1 is null then		-- keine Daten f�r Schiene 1 gefunden -> abbrechen und Fehlereintrag 
	    			cName := 'Daten fehlen';
	    			cRet := 'ERROR';
	    		end if;	
		    	nI := 2;	-- n�chstes Element
		    	nVn2 := null; nVn3 := null; nVn4 := null; nHs := null; nVnSum := nVn1;
		    end if;
	    else
	    	for recDD in (select VAL, CONDITION from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE = dUTC) loop
		    	if nI <= 5 then
		    		if rec.VALUE_CATEGORY = 'M' then
		    			-- es folgt der HS-Wert  (der Systemwert sollte in kWh/m� gespeichert sein - sonst umrechnen aus Wh/m�)
		    			if recDD.VAL > 100 then          
			    			nHs := recDD.VAL / 1000;
			    		else
			    			nHs := recDD.VAL;
			    		end if;
			    		nI := 5;	-- Sicherstellen, dass nur noch nQ folgt
		    		else
		    			if nI = 2 then 
		    				nVn2     := round(recDD.VAL,0); 
				    		cStatus2 := recDD.CONDITION;
		    				iAktiv2  := R_H_Aktiv( rec.OBJLABEL, dUtc );
		    				if iAktiv2 = 1 then	nVnSum := nVnSum + nVn2; end if;
		    			end if;
		    			if nI = 3 then 
		    				nVn3    := round(recDD.VAL,0); 
				    		cStatus3 := recDD.CONDITION;
		    				iAktiv3 := R_H_Aktiv( rec.OBJLABEL, dUtc );
		    				if iAktiv3 = 1 then	nVnSum := nVnSum + nVn3; end if;
		    			end if;
		    			if nI = 4 then 
		    				nVn4    := round(recDD.VAL,0); 
				    		cStatus4 := recDD.CONDITION;
		    				iAktiv4 := R_H_Aktiv( rec.OBJLABEL, dUtc );
		    				if iAktiv4 = 1 then	nVnSum := nVnSum + nVn4; end if;
		    			end if;
		    		end if;
	    			nI := nI + 1;
		    	else    
		    		-- Q fehlt noch
		    		nQ := round(recDD.VAL,0);
		    		-- Gesamt-Q des Tages
			    	select round(sum(VAL),0) into nQday from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE >= dBegin and REF_DATE <= dUTC;
		    	end if;
	    	end loop;      
	    end if;
	end loop;
	nPos := nPos + 1;
	insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, STATUS1, AKTIV1, VAL2, STATUS2, AKTIV2, VAL3, STATUS3, AKTIV3, VAL4, STATUS4, AKTIV4, VAL5, STATUS5, VAL6, STATUS6, VAL7, STATUS7, VAL8, STATUS8, MIN_ZEIT, MAX_ZEIT, KAMMER)
	 values( nSid, nObjlZwId1, nPos, cLastNr, cName, dUtc, nVn1, cStatus1, iAktiv1, nVn2, cStatus2, iAktiv2, nVn3, cStatus3, iAktiv3, nVn4, cStatus4, iAktiv4, nVnSum, null,nHs, null,nQ, null,nQday,null, null, null, null );
	commit;
	return cRet;	    	

end;

function R_H_FiktiveRows( nSid in number, nObjlFiktiveId in number, dVon in date ) return varchar2
as 
	-- Die Objektliste enth�lt je Zeile nur 1 Objekt mit der Zeitreihe f�r die Q-Werte
	cNr		varchar2(4);
	cZwPict	varchar2(12) := '999G999G990';
	dUtc	date := dVon;
	dBegin	date := Psi_pckg.UTCGasDayFirstHour( dVon );		-- ermittelt erste registrierte Stunde (7 Uhr gesetzl. ->UTC)
	nQ		number;
	nQday	number;
	cName	varchar2(40);
	cRet	varchar2(5) := 'OK';
	nPos	number := 0;
	
     	
begin                                                                               
     for rec in (select * from REPORTOBJ_TA where SID = nSid and FK_OBJLIST=nObjlFiktiveId order by POS) loop 
   		cNr	 := substr(rec.NR,1,3);
   		cName := rec.DESCRIPTION;
     	-- ausgegeben werden: Nr, Name, Wert der h, Summe Werte seit Tagesbeginn bis zur Stunde
     	
    	nPos := nPos + 1;      
    	-- Wert 1 aus Zeitreihe auslesen
    	nQ := null;
    	for recDD in (select VAL from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE = dUTC ) loop
    		nQ := round(recDD.VAL,0);
    	end loop;  
    	
    	-- Summenwert ermitteln
    	select round(sum(VAL),0) into nQday from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE >= dBegin and REF_DATE <= dUTC;     	
		if nQ is null then
			cName := 'Wert(e) fehlen';
		end if;
   		-- Satz f�r Station in Tabelle R_GAS_H eintragen
   		nPos := nPos + 1;
   		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2 )
   			values( nSid, nObjlFiktiveId, nPos, cNr, cName, dUtc, nQ, nQday );
	end loop;
	commit;
	return cRet;	    	

end;

function R_GAS_H( nObjlZwId in number, nObjlBilanzId in number, nObjlGasbezugId in number, nObjlWetterId in number, nObjlFiktiveId in number, cVon in varchar2 ) return number


	/* R_GAS_H - Stundenprotokoll Gas
	   5 Tabellen mit unterschiedlichen Spalten auf 1 Seite (Querformat)
	   1. Liste der Z�hlwerte-objekte
	   2. Liste der Bilanzwerte-Objekte
	   3. Liste der Wetterdatenobjekte 
	   4. Liste der Objekte zu Fiktiven Stationen
	   Die Daten der Z�hlwertobjekte werden f�r die vorgegebenen Stunde gelesen und in Tabelle R_GAS_H gespeichert.
	   Parameter: 4 Objektlisten, jeweils mit ID �bergeben
	   			  Datum - Gasstunde in der Form dd.mm.yyyy hh
	*/
as
	cError	varchar2(1024) 	:= null;
	bError	boolean 		:= FALSE;      
	dVon	date			:= to_date(substr(cVon,1,13) || ':00:00', DATE_FORMAT_GER);	-- Zeit in UTC (7 Uhr gesetzl. Zeit ist erste Stunde)
	nSid	number;
	iLfdnr	integer	:= 0; 
	cDataSrc	varchar2(12) := 'DD_SYS';
	cArchivtyp  varchar2(1) := '1';    
	cTimelevel	varchar2(4) := 'H';
	cPict	REPOBJ.PICTURE%type;
	cRet	varchar2(10);
	cText	varchar2(80);
	cNr		varchar2(20);
	cLab1	varchar2(20);
	cLablast	varchar2(20) := '-';
	nStaObjId	number;
	pragma autonomous_transaction;  -- necessary because of insert or deletes while the function is used in a select                                                 
	
begin                                         
	execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
	select userenv('sessionid') into nSid from dual;

	-- Reportgruppe und -objekte erzeugen
	DelSid( nSid );
	insert into REPORTGRP_TA (SID, REPNAME, ERSTELLT_AM) values( nSid, 'Report GAS_H f�r den ' || cVon, sysdate );
	
	-- f�r alle Objekte einer Liste werden die Zeitreihen-Daten geholt und in REPORTOBJ_TA gespeichert
	for rec in (select POSITION, FK_OBJID, SHORT_NAME, LABEL from MD_OBJLIST where ID = nObjlZwId) loop
		-- Variablentext R_D1_NAME holen oder den Namen der Station
		cLab1 := substr(rec.LABEL,1, instr(rec.LABEL,'.',1)-1);
		nStaObjId := psi_pckg.GetParentId( rec.FK_OBJID, 'SC_ST' );
		cText := Text4Attr( rec.FK_OBJID, 'R_GAS_H_NAME' );
		if cText is null then
			if cLablast <> cLab1 then             
				-- ermittle Objekt(=Station) und Kurzname dazu
				for recObj in (select  SHORT_NAME from MD_OBJECTS where OBJID = nStaObjId) loop
					cText := recObj.SHORT_NAME;
				end loop;
				cLablast := cLab1;
			end if;
		end if;
		cNr   := Text4Attr( rec.FK_OBJID, 'R_GAS_H_NR' );
		if cNr is null then
			cNr   := substr(cLab1,length(cLab1)-2, 3);
		end if;
		
		-- Zeitreihe:  Stundenwerte (System)
		for recTis in (select TIS.ID, TIS.VALUE_CATEGORY from TIS
			where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = cTimelevel and TIS.ARCHIVETYPE=cArchivtyp  and
					instr(TIS.QUANTITY,'.',1,1)=0 and dVon between TIS.FROM_DATE_TS and TIS.UNTIL_DATE_TS )
		loop
			iLfdNr := rec.POSITION;
			-- cPict  := Getpict(recTis.ID);		Aufbereitung der Werte erfolgt immer als ganzzahlige Werte mit Tausendertrennzeichen
			insert into REPORTOBJ_TA  ( SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL )
				values (nSid, nObjlZwId,iLfdNr, cNr, recTis.ID, cPict, rec.SHORT_NAME, cText, recTis.VALUE_CATEGORY, rec.LABEL );
		end loop;
		commit;
	end loop;   
	
	if iLfdNr > 0 then
		-- �bernahmez�hler-Zeilen aufbauen
		cRet := R_H_ZwRows( nSid, nObjlZwId, dVon );    
	end if;	    
	
	iLfdNr := 0;
	-- Aufbau Bilanzdaten
	for rec in (select POSITION, FK_OBJID, SHORT_NAME, LABEL from MD_OBJLIST where ID = nObjlBilanzId) loop
		-- Variablentext R_D1_NAME holen - im Signal muss der Name stehen, z.B. Kaverne Einlag.
		cText := Text4Attr( rec.FK_OBJID, 'R_GAS_H_NAME' );
		if cText is null then
			cText := 'R_GAS_H_NAME fehlt';
		end if;
		cNr   := Text4Attr( rec.FK_OBJID, 'R_GAS_H_NR' );
		if cNr is null then
			cLab1 := substr(rec.LABEL,1, instr(rec.LABEL,'.',1)-1);
			cNr   := substr(cLab1,length(cLab1)-2, 3);
		end if;
		
		-- Zeitreihe:  Stundenwerte (System)
		for recTis in (select TIS.ID, TIS.VALUE_CATEGORY from TIS
			where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = cTimelevel and TIS.ARCHIVETYPE=cArchivtyp and  
					instr(TIS.QUANTITY,'.',1,1)=0 and dVon between TIS.FROM_DATE_TS and TIS.UNTIL_DATE_TS )
		loop
			iLfdNr := rec.POSITION;
			-- cPict  := Getpict(recTis.ID);		Aufbereitung der Werte erfolgt immer als ganzzahlige Werte mit Tausendertrennzeichen
			insert into REPORTOBJ_TA  ( SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL )
				values (nSid, nObjlBilanzId,iLfdNr, cNr, recTis.ID, cPict, rec.SHORT_NAME, cText, recTis.VALUE_CATEGORY, rec.LABEL );
		end loop;
		commit;
	end loop;   
	
	if iLfdNr > 0 then
		-- Bilanz-Zeilen aufbauen
		cRet := R_H_BilanzRows( nSid, nObjlBilanzId, dVon );    
	end if;	

	iLfdNr := 0;
	-- Aufbau Gasbezug
	for rec in (select POSITION, FK_OBJID, SHORT_NAME, LABEL from MD_OBJLIST where ID = nObjlGasbezugId) loop
		-- Variablentext R_D1_NAME holen - im Signal muss der Name stehen, z.B. Kaverne Einlag.
		cText := Text4Attr( rec.FK_OBJID, 'R_GAS_H_NAME' );
		if cText is null then
			cText := 'R_GAS_H_NAME fehlt';
		end if;
		cNr   := Text4Attr( rec.FK_OBJID, 'R_GAS_H_NR' );
		if cNr is null then
			cLab1 := substr(rec.LABEL,1, instr(rec.LABEL,'.',1)-1);
			cNr   := substr(cLab1,length(cLab1)-2, 3);
		end if;
		
		-- Zeitreihe:  Stundenwerte (System)
		for recTis in (select TIS.ID, TIS.VALUE_CATEGORY from TIS
			where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = cTimelevel and TIS.ARCHIVETYPE=cArchivtyp and  
					instr(TIS.QUANTITY,'.',1,1)=0 and dVon between TIS.FROM_DATE_TS and TIS.UNTIL_DATE_TS )
		loop
			iLfdNr := rec.POSITION;
			-- cPict  := Getpict(recTis.ID);		Aufbereitung der Werte erfolgt immer als ganzzahlige Werte mit Tausendertrennzeichen
			insert into REPORTOBJ_TA  ( SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL )
				values (nSid, nObjlGasbezugId,iLfdNr, cNr, recTis.ID, cPict, rec.SHORT_NAME, cText, recTis.VALUE_CATEGORY, rec.LABEL );
		end loop;
		commit;
	end loop;   
	
	if iLfdNr > 0 then
		-- Gasbezug-Zeilen aufbauen
		cRet := R_H_GasbezugRows( nSid, nObjlGasbezugId, dVon );    
	end if;	

	iLfdNr := 0;
	-- Aufbau Fiktivedaten
	for rec in (select POSITION, FK_OBJID, SHORT_NAME, LABEL from MD_OBJLIST where ID = nObjlFiktiveId) loop
		-- Variablentext R_D1_NAME holen - im Signal muss der Name stehen
		cText := Text4Attr( rec.FK_OBJID, 'R_GAS_H_NAME' );
		if cText is null then
			cText := 'R_GAS_H_NAME fehlt';
		end if;
		cNr   := Text4Attr( rec.FK_OBJID, 'R_GAS_H_NR' );
		if cNr is null then
			cLab1 := substr(rec.LABEL,1, instr(rec.LABEL,'.',1)-1);
			cNr   := substr(cLab1,length(cLab1)-2, 3);
		end if;
		
		-- Zeitreihe:  Stundenwerte (System)
		for recTis in (select TIS.ID, TIS.VALUE_CATEGORY from TIS
			where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = cTimelevel and TIS.ARCHIVETYPE=cArchivtyp and 
					instr(TIS.QUANTITY,'.',1,1)=0 and dVon between TIS.FROM_DATE_TS and TIS.UNTIL_DATE_TS )
		loop
			iLfdNr := rec.POSITION;
			insert into REPORTOBJ_TA  ( SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL )
				values (nSid, nObjlFiktiveId,iLfdNr, cNr, recTis.ID, cPict, rec.SHORT_NAME, cText, recTis.VALUE_CATEGORY, rec.LABEL );
		end loop;
		commit;
	end loop;   
	
	if iLfdNr > 0 then
		-- �bernahmez�hler-Zeilen aufbauen
		cRet := R_H_FiktiveRows( nSid, nObjlFiktiveId, dVon );    
	end if;	

	iLfdNr := 0;
	-- Aufbau Wetterdaten
	for rec in (select POSITION, FK_OBJID, SHORT_NAME, LABEL from MD_OBJLIST where ID = nObjlWetterId) loop
		-- Variablentext R_D1_NAME holen - im Signal muss der Name stehen
		cText := Text4Attr( rec.FK_OBJID, 'R_GAS_H_NAME' );
		if cText is null then
			cText := 'R_GAS_H_NAME fehlt';
		end if;
		cNr   := Text4Attr( rec.FK_OBJID, 'R_GAS_H_NR' );
		if cNr is null then
			cLab1 := substr(rec.LABEL,1, instr(rec.LABEL,'.',1)-1);
			cNr   := substr(cLab1,length(cLab1)-2, 3);
		end if;
		
		-- Zeitreihe:  Stundenwerte (System)
		for recTis in (select TIS.ID, TIS.VALUE_CATEGORY, TIS.UNIT from TIS
			where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = cTimelevel and TIS.ARCHIVETYPE=cArchivtyp and 
					instr(TIS.QUANTITY,'.',1,1)=0 and dVon between TIS.FROM_DATE_TS and TIS.UNTIL_DATE_TS )
		loop
			iLfdNr := rec.POSITION;
			cPict  := Getpict(recTis.ID);
			insert into REPORTOBJ_TA  ( SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, UNIT )
				values (nSid, nObjlWetterId,iLfdNr, cNr, recTis.ID, cPict, rec.SHORT_NAME, cText, recTis.VALUE_CATEGORY, rec.LABEL,
				recTis.UNIT );
		end loop;
		commit;
	end loop;   
	
	if iLfdNr > 0 then
		-- �bernahmez�hler-Zeilen aufbauen
		cRet := R_H_WetterRows( nSid, nObjlWetterId, dVon );    
	end if;	

	return nSid;
end;

 
function R_W_TB_Prod( nSid in number, cTyp in varchar2, nObjlistId in number, nObjlHochbId in number, dVon in date, nHBInh in number, nHBInhVortag in number ) 
return varchar2
as 
	-- 
	cNr		varchar2(4);
	cZwPict	varchar2(12) := '999G999G990';
	dUtc	date := dVon;		-- Psi_pckg.UTCtime( dVon );
	dBegin	date := Psi_pckg.UTCGasDayFirstHour( dVon );		-- ermittelt erste registrierte Stunde (7 Uhr gesetzl. ->UTC)
	dLast	date := dBegin + 23/24;
	nQ		number;
	nQday	number;
	nQVday	number;				-- Tageswert Vorwoche
	nGes	number := 0;
	nDiff	number;
	nMit	number;
	nVMit	number;				-- Mittelwert Vorwoche
	cName	varchar2(40);
	cRet	varchar2(5) := 'OK';
	nPos	number := 0;
	nStaId	number;				-- Stations-ID
	nLastStaId	number := 0;
	nProz	number;
	nInhalt number;
	nVol	number;
	nVolMin number;
   	nVolMax number;
   	cMin 	varchar2(5);
   	cMax 	varchar2(5);
   	nMin	number;
   	nMax	number;
   	dMin	date;
   	dMax	date;
     	
begin                                                                               
     for rec in (select * from REPORTOBJ_TA where SID = nSid and FK_OBJLIST=nObjlistId order by POS) loop 
   		cNr	 := substr(rec.NR,1,3);
   		cName := rec.DESCRIPTION;
   		
   		if cTyp = 'Bezug' then
	     	-- ausgegeben werden: Nr, Name, Tages-Wert
	    	nPos := nPos + 1;      
	    	if rec.FK_TIS = 0 then
	    		-- Zwischensumme ausgeben
		   		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, AKTIV1 )
   					values( nSid, nObjlistId, nPos, cNr, cName, dUtc, nGes, 1 );
   				if rec.Pos = 6 then
   					-- positionen 11,12,14 und 15 erg�nzen
			   		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1 ) 
			   			values( nSid, nObjlistId, 11, '11', 'Hochbeh�lter-Inhalt(7+8+9)', dUtc, nHBInh );
			   		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1 ) 
			   			values( nSid, nObjlistId, 12, '12', 'Hochbeh�lter-Inhalt Vortag', dUtc-1, nHBInhVortag );
   					nDiff := nHBInh - nHBInhVortag;
			   		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1 ) 
			   			values( nSid, nObjlistId, 14, '14', 'Hochbeh�lter-Differenz(11-12)', dUtc-1, nDiff );
   					nGes := nGes - nDiff;
			   		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, AKTIV1 ) 
			   			values( nSid, nObjlistId, 15, '15', 'Abgabe Hannover(6-14)', dUtc-1, nGes, 1 );
			   	end if;
			else	    		
		    	-- gelesen und aufsummiert werden alle Stundenwerte des Wassertages (7 Uhr bis 6 Uhr)
		    	nQ := null;             
		    	nQday := 0;
		    	for recDD in (select VAL from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE >= dBegin and REF_DATE <= dLast ) loop
		    		nQ := round(recDD.VAL,0);
		    		nQday := nQday + nQ;
		    	end loop;
		    	nGes := nGes + nQday;  
		   		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1 )
	   				values( nSid, nObjlistId, nPos, cNr, cName, dUtc, nQday );
		   end if;
		elsif cTyp = 'Druckerh' then
	    	nPos := nPos + 1;      
	    	-- gelesen und aufsummiert werden alle Stundenwerte des Wassertages (7 Uhr bis 6 Uhr)
	    	nQ := null;             
	    	nQday := 0;
	    	for recDD in (select VAL from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE >= dBegin and REF_DATE <= dLast ) loop
	    		nQ := round(recDD.VAL,0);
	    		nQday := nQday + nQ;
	    	end loop;
	    	-- Mittelwert bilden
	    	nMit := round(nQday / 24,0);                                     
	    	-- Vorwochen-Daten behandeln
			dBegin	 := Psi_pckg.UTCGasDayFirstHour( dVon - 7 );	-- ermittelt erste registrierte Stunde (7 Uhr gesetzl. ->UTC)
			dLast	 := dBegin + 23/24;
	    	nQVday := 0;
	    	for recDD in (select VAL from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE >= dBegin and REF_DATE <= dLast ) loop
	    		nQ := round(recDD.VAL,0);
	    		nQVday := nQVday + nQ;
	    	end loop;
	    	nVMit := round(nQVday / 24,0);                                     
	    	
	   		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2, VAL3, VAL4 )
   				values( nSid, nObjlistId, nPos, cNr, cName, dUtc, nQday, nMit, nQVday, nVMit );
   				
		end if;    	
	end loop;
	commit;
	return cRet;	    	

end;

function R_W_TB_Hochb_Ausgabe( nSid in number, nObjlistId in number, nStaId in number, aGeoVol in array_of_nums, aProz in array_of_nums, iAnz in integer, cNr in varchar2, cName in varchar2, nPos in out number ) 
return number
as
	dLast	date := sysdate;
	nVol	number;
	nVolMin number;
   	nVolMax number;
   	nVolSum number;
	nSum	number;
	nSumMin number := null;
   	nSumMax number := null;
   	cMin 	varchar2(5);
   	cMax 	varchar2(5);
   	nMin	number;
   	nMax	number;
   	dMin	date;
   	dMax	date;
   	dSumMin	date;
   	dSumMax	date;
   	nLastKammer	number := 0;
   	nInhalt	number;
   	nInhGes number := 0;
   	dUtc	date := sysdate;
   	
 begin 

	-- Min und Max der Einzelzeitreihen ermitteln und ausgeben
   	for recDD in (select KAMMER, REFDATE, VAL1 from R_WASSER_TMP where SID = nSid and STATION = nStaId order by KAMMER,REFDATE ) loop
   		-- Bei Kammerwechsel ->Ausgabe
   		if recDD.KAMMER <> nLastKammer then
   			if nLastkammer > 0 then
		    	-- Inhalte ermitteln
		    	nInhalt := round(aProz(nLastkammer) * aGeoVol(nLastkammer)/100,0);
		    	nInhGes := nInhGes + nInhalt;
		    	nVolMin := round(nMin * aGeoVol(nLastkammer)/100,0); 
		    	nVolMax := round(nMax * aGeoVol(nLastkammer)/100,0);
		    	cMin := to_char(PSI_Pckg.UtcTime( dMin ),'hh24:mi');
		    	cMax := to_char(PSI_Pckg.UtcTime( dMax ),'hh24:mi');
		    	nPos := nPos + 1;
		    	if nLastkammer = 1 then
			   		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2, VAL3, VAL4, VAL5, VAL6, MIN_ZEIT, MAX_ZEIT, KAMMER )
		   				values( nSid, nObjlistId, nPos, cNr, cName, dUtc, aProz(nLastkammer), nInhalt, nMax, nVolMax, nMin, nVolMin, cMin, cMax, to_char(nLastkammer) );
		   		else
			   		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2, VAL3, VAL4, VAL5, VAL6, MIN_ZEIT, MAX_ZEIT, KAMMER )
		   				values( nSid, nObjlistId, nPos, cNr, null, dUtc, aProz(nLastkammer), nInhalt, nMax, nVolMax, nMin, nVolMin, cMin, cMax, to_char(nLastkammer) );
		   		end if;
   			end if;
   			nLastkammer := recDD.KAMMER;
   			nMin := null;
   		end if;
   		if nMin is null then
   			nMin := recDD.VAL1;
   			nMax := recDD.VAL1;
   			dMin := recDD.REFDATE;
   			dMax := recDD.REFDATE;
   		else
    		if recDD.VAL1 < nMin then
    			nMin := recDD.VAL1;
    			dMin := recDD.REFDATE;
    		end if;
    		if recDD.VAL1 > nMax then
    			nMax := recDD.VAL1;
    			dMax := recDD.REFDATE;
    		end if;               
    	end if;
    	commit;
    end loop;
    -- letzte Kammer ausgeben
    if nLastKammer > 0 then
    	nInhalt := round(aProz(nLastkammer) * aGeoVol(nLastkammer)/100,0);
    	nInhGes := nInhGes + nInhalt;
    	nVolMin := round(nMin * aGeoVol(nLastkammer)/100,0); 
    	nVolMax := round(nMax * aGeoVol(nLastkammer)/100,0);
    	cMin := to_char(PSI_Pckg.UtcTime( dMin ),'hh24:mi');
    	cMax := to_char(PSI_Pckg.UtcTime( dMax ),'hh24:mi');
    	nPos := nPos + 1;
   		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2, VAL3, VAL4, VAL5, VAL6, MIN_ZEIT, MAX_ZEIT, KAMMER )
  				values( nSid, nObjlistId, nPos, cNr, null, dUtc, aProz(nLastkammer), nInhalt, nMax, nVolMax, nMin, nVolMin, cMin, cMax, to_char(nLastkammer) );
	end if;
	commit;    
	-- Min und Max der Summen ermitteln
	nSum := null;
   	for rec in (select VAL1, REFDATE from R_WASSER_TMP where SID = nSid and STATION = nStaId order by REFDATE,KAMMER ) loop
   		-- Wechsel Zeitpunkt 
   		if rec.REFDATE <> dLast then
	   		if nSumMin is null and nSum is not null then
	   			nSumMin := nSum;
	   			nSumMax := nSum;
	   			dSumMin := dLast;
	   			dSummax := dLast;
	   		else
	  			if nSum < nSumMin then 
	  				dSumMin := dLast;
	  				nSumMin := nSum;
	  			end if;
	  			if nSum > nSumMax then 
	  				dSumMax := dLast;
	  				nSumMax := nSum;
	  			end if;
	  		end if;
   			dLast := rec.REFDATE;   
  			nSum  := rec.VAL1;
		else
			nSum := nSum + rec.VAL1;   			
		end if;
	end loop;
	-- Ausgabe Summenzeile
   	
    if nLastKammer > 0 then
	   	nVolMin := round(nSumMin * aGeoVol(nLastkammer)/100,0); 
	   	nVolMax := round(nSumMax * aGeoVol(nLastkammer)/100,0);
	   	cMin := to_char(PSI_Pckg.UtcTime( dSumMin ),'hh24:mi');
	   	cMax := to_char(PSI_Pckg.UtcTime( dSumMax ),'hh24:mi');
	   	nPos := nPos + 1;
		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2, VAL3, VAL4, VAL5, VAL6, MIN_ZEIT, MAX_ZEIT, KAMMER )
	 				values( nSid, nObjlistId, nPos, cNr, null, dUtc, null, nInhGes, null, nVolMax, null, nVolMin, cMin, cMax, 'Summe' );
		commit;  
	end if;
	return nInhGes;  
end;

 
function R_W_TB_Hochb( nSid in number, cTyp in varchar2, nObjlistId in number, dVon in date, nHBInh in out number, nHBInhVortag in out number ) return varchar2
as 
	aGeoVol  array_of_nums; 	-- geom. Volumina
	aProz    array_of_nums; 	-- % H�henstand 6 Uhr

    iAnz	integer;
    iKammer	integer;
    nKamId	number;
	cNr		varchar2(4);
	dUtc	date := dVon; --Psi_pckg.UTCtime( dVon );
	dBegin	date := Psi_pckg.UTCGasDayFirstHour( dVon );		-- ermittelt erste registrierte Stunde (7 Uhr gesetzl. ->UTC)
	dLast	date := dBegin + 23/24;
	cName	varchar2(40);
	cRet	varchar2(5) := 'OK';
	nPos	number := 0;
	nStaId	number;				-- Stations-ID
	nLastStaId	number := 0;
	nLastKamId	number := 0;
   	cText	varchar2(100); 
   	nInhHB	number;        
   	
begin 
    for rec in (select * from REPORTOBJ_TA where SID = nSid and FK_OBJLIST=nObjlistId order by POS) loop 
		-- Stationswechsel pr�fen
		nStaId := psi_pckg.GetParentId( rec.FK_OBJID, 'SC_ST' );
		if nStaId <> nLastStaId then
			if nLastStaId > 0 then
				-- jetzt komplett Daten zur Station ausgeben
				commit; 
				nInhHB := R_W_TB_Hochb_Ausgabe( nSid, nObjlistId, nLastStaId, aGeoVol, aProz, iKammer-1, cNr, cName, nPos );
				nHBInh := nHBInh + nInhHB;
			end if;
			nLastStaId := nStaId;
			iKammer := 1;
	   		cNr	 := substr(rec.NR,1,3);
	   		cName := rec.DESCRIPTION;
		end if;
		
		-- Die Werte aller Kammern werden in Tabelle R_WASSER_TMP vorselektiert und dann stationsweise ausgewertet
		   				
		nKamId := psi_pckg.GetParentId( rec.FK_OBJID, 'SC_SP' );
		cText := Text4Attr( nKamId, 'R_WASSER_GEOM_VOL' );
   		aGeoVol(iKammer) := to_number(nvl(cText, 0));
    	-- % Wert f�r 6 Uhr holen
    	for recDD in (select VAL from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE = dLast ) loop
    		aProz(iKammer) := recDD.VAL;
    	end loop;
    	
    	-- Werte zu Vortag holen (Ausgabe in Trinkwasser Produktion)
    	for recDD in (select VAL from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE = dLast - 1 ) loop
    		nInhHB := round(recDD.VAL * aGeoVol(iKammer)/100, 0);
    		nHBInhVortag := nHBInhVortag + nInhHB;
    	end loop;
		
		-- Werte aus Minutenarchiv in R_WASSER_TMP �berf�hren
		for recTis in (select TIS.ID from TIS
			where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = 'MI' and TIS.ARCHIVETYPE='1' ) loop
			insert into R_WASSER_TMP -- (SID, STATION, KAMMER, REFDATE, VAL1)
			 (select nSid, nStaId, iKammer, REF_DATE, VAL from DD_SYS
			   	where FK_TIS = recTis.ID and REF_DATE > dBegin-1/24 and REF_DATE <= dLast);
		end loop;	
		commit;												
		iKammer := iKammer + 1;
	end loop;
	commit;
	-- nur wenn Minutenwerte existieren (und in R_WASSER_TMP eingetragen wurden), erfolgt eine Ausgabe der HB-Inhalte
	select count(*) into iAnz from R_WASSER_TMP where SID = nSid;
	if iAnz = 0 then
		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2, VAL3, VAL4, VAL5, VAL6, MIN_ZEIT, MAX_ZEIT, KAMMER )
	 				values( nSid, nObjlistId, 7, '7', 'keine Min-Werte', dUtc, null, null, null, null, null, null, null, null, '-' );
	else	
		-- Ausgabe letzte Station
		nInhHB := R_W_TB_Hochb_Ausgabe( nSid, nObjlistId, nStaId, aGeoVol, aProz, iKammer-1, cNr, cName, nPos );
		nHBInh := nHBInh + nInhHB;
	end if;
	commit;
	return cRet;	    	
end;
	
function R_WASSER_TB( nObjlBezugId in number, nObjlDruckerhId in number, nObjlHochbId in number, nObjlWetterId in number, nObjlHbInhaltId in number, 
					cBemerkung in varchar2, dVon in date ) return number


	/* Tagesbericht Trinkwasser
	   5 Tabellen mit unterschiedlichen Spalten auf 1 Seite (Querformat)
	   1. Liste der Bezugobjekte
	   2. Liste der Druckerh�hungs-Objekte
	   3. Liste der Hochbeh�lter
	   3. Liste der Wetterdatenobjekte 
	   4. Liste der Hochbeh�lterinhalte
	   Parameter: 5 Objektlisten, jeweils mit ID �bergeben
	   			  Bemerkung - �ber mehrere Zeilen	
	   			  Datum - Tag als date     
	Die Speicherung der Zeitreihen zu den diversen Objektlisten erfolgt in Tabelle R_GAS_H mit Ausnahme der Wetterdaten in R_WETTER
	*/
as
	cError	varchar2(1024) 	:= null;
	bError	boolean 		:= FALSE;      
	nSid	number;
	iLfdnr	integer	:= 0; 
	cDataSrc	varchar2(12) := 'DD_SYS';
	cArchivtyp  varchar2(1) := '1';    
	cTimelevel	varchar2(4) := 'H';
	cPict	REPOBJ.PICTURE%type;
	cRet	varchar2(10);
	cText	varchar2(80);
	cNr		varchar2(20);
	cLab1	varchar2(20);
	cLablast	varchar2(20) := '-'; 
	nAktuell	number;
	nStaObjId	number;
	nHBInh		number := 0;
	nHBInhVortag number := 0;
	nTis		number;
   	nMin	number;
   	nMax	number;
   	dMin	date;
   	dMax	date;
    nKamId	number;
	dBegin	date := Psi_pckg.UTCGasDayFirstHour( dVon );		-- ermittelt erste registrierte Stunde (7 Uhr gesetzl. ->UTC)
	dLast	date := dBegin + 23/24;								-- entspricht 6 Uhr Folgetag = Datum f�r Tages-Wasserstand 6 Uhr
	nStaId	number;				-- Stations-ID
	nGeoVol	number;

	pragma autonomous_transaction;  -- necessary because of insert or deletes while the function is used in a select                                                 
	
begin                                         
	execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
	select userenv('sessionid') into nSid from dual;

	-- Reportgruppe und -objekte erzeugen
	DelSid( nSid ); 
	commit;            
	--nAktuell := ChkObjects();	-- sicherstellen, dass die Objektliste aktuell ist
	
	insert into REPORTGRP_TA (SID, REPNAME, ERSTELLT_AM) values( nSid, 'Report WASSER_TB f�r den ' || to_char(dVon,'dd.mm.yyyy'), sysdate );

	iLfdNr := 0;
	-- Aufbau Hochbeh�lter-Daten - erfolgt zuerst, weil Gesamtinhalt der HBs f�r 1. Tabelle ben�tigt wird.
	for rec in (select POSITION, FK_OBJID, SHORT_NAME, LABEL from MD_OBJLIST where ID = nObjlHochbId) loop
		cLab1 := substr(rec.LABEL,1, instr(rec.LABEL,'.',1)-1);
		nStaObjId := psi_pckg.GetParentId( rec.FK_OBJID, 'SC_ST' );
		cText := Text4Attr( rec.FK_OBJID, 'R_WASSER_TB_NAME' );
		if cText is null then
			-- ermittle Objekt(=Station) und Kurzname dazu
			for recObj in (select  SHORT_NAME from MD_OBJECTS where OBJID = nStaObjId ) loop
				cText := recObj.SHORT_NAME;
			end loop;                                        
		end if;
		cNr   := Text4Attr( nStaObjId, 'R_WASSER_TB_NR' );
		if cNr is null then
			cNr   := substr(cLab1,length(cLab1)-1, 2);
		end if;
		-- Zeitreihe:  Stundenwerte (System)
		for recTis in (select TIS.ID, TIS.VALUE_CATEGORY from TIS
			where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = cTimelevel and TIS.ARCHIVETYPE=cArchivtyp and instr(TIS.QUANTITY,'.',1,1)=0
			and dVon between TIS.FROM_DATE_TS and TIS.UNTIL_DATE_TS )                  -- erforderlich, wenn mehr als 1 Zeitreihe f�r Objekt+Zeitstufe existiert
		loop
			iLfdNr := rec.POSITION;
			insert into REPORTOBJ_TA  ( SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, FK_OBJID )
				values (nSid, nObjlHochbId,iLfdNr, cNr, recTis.ID, cPict, cText, cText, recTis.VALUE_CATEGORY, rec.LABEL, rec.FK_OBJID );
		end loop;
		commit;
	end loop;   
	if iLfdNr > 0 then
		-- Tab. 3: Hochbeh�lter-Zeilen aufbauen
		cRet := R_W_TB_Hochb( nSid, 'Hochb', nObjlHochbId, dVon, nHBInh, nHBInhVortag );    
	end if;	    
	 
	iLfdNr := 0;
	-- f�r alle Objekte einer Liste werden die Zeitreihen-Daten geholt und in REPORTOBJ_TA gespeichert
	for rec in (select POSITION, FK_OBJID, SHORT_NAME, LABEL from MD_OBJLIST where ID = nObjlBezugId order by POSITION) loop
		-- Variablentext R_WASSER_TB_NAME holen oder den Kurznamen der Station
		cLab1 := substr(rec.LABEL,1, instr(rec.LABEL,'.',1)-1);
		nStaObjId := psi_pckg.GetParentId( rec.FK_OBJID, 'SC_ST' );
		cText := Text4Attr( nStaObjId, 'R_WASSER_TB_NAME' );
		if cText is null then
			-- ermittle Objekt(=Station) und Kurzname dazu
			for recObj in (select  SHORT_NAME from MD_OBJECTS where OBJID = nStaObjId) loop
				cText := recObj.SHORT_NAME;
			end loop;                                        
		end if;
		cNr   := Text4Attr( nStaObjId, 'R_WASSER_TB_NR' );
		if cNr is null then
			cNr   := substr(cLab1,length(cLab1)-1, 2);
		end if;
		
		-- Zeitreihe:  Stundenwerte (System)
		for recTis in (select TIS.ID, TIS.VALUE_CATEGORY from TIS
			where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = cTimelevel and TIS.ARCHIVETYPE=cArchivtyp and instr(TIS.QUANTITY,'.',1,1)=0
			and dVon between TIS.FROM_DATE_TS and TIS.UNTIL_DATE_TS )
		loop
			iLfdNr := iLfdNr + 1;
			--iLfdNr := to_number( cNr );
			-- cPict  := Getpict(recTis.ID);		Aufbereitung der Werte erfolgt immer als ganzzahlige Werte mit Tausendertrennzeichen
			insert into REPORTOBJ_TA  ( SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL )
				values (nSid, nObjlBezugId,iLfdNr, cNr, recTis.ID, cPict, cText, cText, recTis.VALUE_CATEGORY, rec.LABEL );
			if iLfdNr = 3 then
				-- erg�nze Summe Produktion                       
				iLfdNr := iLfdNr + 1;
				cText := 'Produktion (1+2+3)';
				insert into REPORTOBJ_TA  ( SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL )
					values (nSid, nObjlBezugId,iLfdNr, '4', 0, cPict, cText, cText, null, 'Summe' );
			end if;
			if iLfdNr = 5 then
				-- erg�nze Summe Produktion + Bezug
				iLfdNr := iLfdNr + 1;
				cText := 'Produktion und Bezug (4+5)';
				insert into REPORTOBJ_TA  ( SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL )
					values (nSid, nObjlBezugId,iLfdNr, '6', 0, cPict, cText, cText, null, 'Summe' );
			end if;
		end loop;
		commit;
	end loop;   
	
	if iLfdNr > 0 then
		-- Tab. 1: Trinkwasser-Prod.+Bezug-Zeilen aufbauen
		cRet := R_W_TB_Prod( nSid, 'Bezug', nObjlBezugId, nObjlHochbId, dVon, nHBInh, nHBInhVortag );    
	end if;	    
	
	iLfdNr := 0;
	-- Aufbau Druckerh�hung-Daten
	for rec in (select POSITION, FK_OBJID, SHORT_NAME, LABEL from MD_OBJLIST where ID = nObjlDruckerhId) loop
		cLab1 := substr(rec.LABEL,1, instr(rec.LABEL,'.',1)-1);
		nStaObjId := psi_pckg.GetParentId( rec.FK_OBJID, 'SC_ST' );
		cText := Text4Attr( rec.FK_OBJID, 'R_WASSER_TB_NAME' );
		if cText is null then
			-- ermittle Objekt(=Station) und Kurzname dazu
			for recObj in (select  SHORT_NAME from MD_OBJECTS where OBJID = nStaObjId) loop
				cText := recObj.SHORT_NAME;
			end loop;                                        
		end if;
		-- Zeitreihe:  Stundenwerte (System)
		for recTis in (select TIS.ID, TIS.VALUE_CATEGORY from TIS
			where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = cTimelevel and TIS.ARCHIVETYPE=cArchivtyp and instr(TIS.QUANTITY,'.',1,1)=0
			and dVon between TIS.FROM_DATE_TS and TIS.UNTIL_DATE_TS )
		loop
			iLfdNr := rec.POSITION;
			insert into REPORTOBJ_TA  ( SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL )
				values (nSid, nObjlDruckerhId,iLfdNr, cNr, recTis.ID, cPict, cText, cText, recTis.VALUE_CATEGORY, rec.LABEL );
		end loop;
		commit;
	end loop;   
	if iLfdNr > 0 then
		-- Tab. 2: Druckerh�hungs-Zeilen aufbauen
		cRet := R_W_TB_Prod( nSid, 'Druckerh', nObjlDruckerhId, nObjlHochbId, dVon, nHBInh, nHBInhVortag );    
	end if;	    

	commit;
	

	iLfdNr := 0;
	-- Tab. 4: Aufbau Wetterdaten
	for rec in (select POSITION, FK_OBJID, SHORT_NAME, LABEL from MD_OBJLIST where ID = nObjlWetterId) loop
		cText := Text4Attr( rec.FK_OBJID, 'R_W_TB_WETTERNAME' );
		if cText is null then
			cText := rec.SHORT_NAME;
		end if;
		
		-- Zeitreihe:  Tagesmittelwert (System) -> aus h-Werten selbst ermitteln, da auch akt. Tag m�glich; 
		-- Achtung: Eingangswerte sind Daten aus Sparte Fernw�rme, die in 15min geliefert werden => h-Werte sind .AVG-Gr��en
		for recTis in (select TIS.ID, TIS.VALUE_CATEGORY, TIS.UNIT from TIS
			where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = 'H' and TIS.ARCHIVETYPE=cArchivtyp and instr(TIS.QUANTITY,'.AVG',1,1)>0
			and dVon between TIS.FROM_DATE_TS and TIS.UNTIL_DATE_TS )
		loop
			iLfdNr := rec.POSITION;
			cPict  := Getpict(recTis.ID);
			insert into REPORTOBJ_TA  ( SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, UNIT )
				values (nSid, nObjlWetterId,iLfdNr, cNr, recTis.ID, cPict, rec.SHORT_NAME, cText, recTis.VALUE_CATEGORY, rec.LABEL,
				recTis.UNIT );
		end loop;
		commit;
	end loop;   
	
	if iLfdNr > 0 then
		-- �bernahmez�hler-Zeilen aufbauen
		cRet := R_H_WetterRows( nSid, nObjlWetterId, dVon, 'Tagesmittel' );    
	end if;	

	iLfdNr := 0;
	-- Tab. 5: Aufbau HB-Inhalt (gesamt)
	for rec in (select POSITION, FK_OBJID, SHORT_NAME, LABEL from MD_OBJLIST where ID = nObjlHbInhaltId) loop
		cLab1 := substr(rec.LABEL,1, instr(rec.LABEL,'.',1)-1);
		nStaObjId := psi_pckg.GetParentId( rec.FK_OBJID, 'SC_ST' );
		cText := Text4Attr( rec.FK_OBJID, 'R_WASSER_TB_NAME' );
		if cText is null then
			-- ermittle Objekt(=Station) und Kurzname dazu
			for recObj in (select  SHORT_NAME from MD_OBJECTS where OBJID = nStaObjId) loop
				cText := recObj.SHORT_NAME;
			end loop;                                        
		end if;
		-- Zeitreihe:  Stundenwerte (System)
		for recTis in (select TIS.ID, TIS.VALUE_CATEGORY from TIS
			where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = cTimelevel and TIS.ARCHIVETYPE=cArchivtyp and instr(TIS.QUANTITY,'.',1,1)=0
			and dVon between TIS.FROM_DATE_TS and TIS.UNTIL_DATE_TS )
		loop
			iLfdNr := rec.POSITION;
			insert into REPORTOBJ_TA  ( SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, FK_OBJID )
				values (nSid, nObjlHbInhaltId,iLfdNr, cNr, recTis.ID, cPict, cText, cText, recTis.VALUE_CATEGORY, rec.LABEL, rec.FK_OBJID );
		end loop;
		commit;
	end loop;   
	if iLfdNr > 0 then
	    for rec in (select * from REPORTOBJ_TA where SID = nSid and FK_OBJLIST=nObjlHbInhaltId order by POS) loop 
			-- Stationswechsel pr�fen
			nStaId := psi_pckg.GetParentId( rec.FK_OBJID, 'SC_ST' );
			-- Gesamtwert liegt bereits vor
	   		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1 ) 
	   			values( nSid, nObjlHbInhaltId, 1, '1', 'Gesamt', dLast, nHBInh );
			
			-- Min und Max aus Minutenwert-Tabelle ermitteln  / Werte sind in m� abgelegt
			--nKamId := psi_pckg.GetParentId( rec.FK_OBJID, 'SC_SP' );
			--cText := Text4Attr( nKamId, 'R_WASSER_GEOM_VOL' );
	   		--nGeoVol := to_number(nvl(cText, 0));            
 			for recTis in (select TIS.ID from TIS
				where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = 'MI' and TIS.ARCHIVETYPE='1' ) loop
				nTis := recTis.ID;
			end loop;
			if nTis is not null then    
				nMin := null;
				for recDD in (select REF_DATE, VAL, CONDITION from DD_SYS
				   	where FK_TIS = nTis and REF_DATE > dBegin-1/24 and REF_DATE <= dLast) loop
			   		if nMin is null then
			   			nMin := recDD.VAL;
			   			nMax := recDD.VAL;
			   			dMin := recDD.REF_DATE;
			   			dMax := recDD.REF_DATE;
			   		else
			    		if recDD.VAL < nMin then
			    			nMin := recDD.VAL;
			    			dMin := recDD.REF_DATE;
			    		end if;
			    		if recDD.VAL > nMax then
			    			nMax := recDD.VAL;
			    			dMax := recDD.REF_DATE;
			    		end if;               
			    	end if;  
			 	end loop;
			 	if dMin is not null then
			   		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1 ) 
			   			values( nSid, nObjlHbInhaltId, 2, '2', 'Min.', dMin, round(nMin,0) );
			   		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1 ) 
			   			values( nSid, nObjlHbInhaltId, 3, '3', 'Max.', dMax, round(nMax,0) );
			   		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1 ) 
			   			values( nSid, nObjlHbInhaltId, 4, '4', 'Differenz', sysdate+1, round((nMax - nMin),0) );
			  	else
			   		insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1 ) 
			   			values( nSid, nObjlHbInhaltId, 2, '2', 'keine Werte', dBegin, 0 );
			    end if;
		   		commit;
			 end if;
		end loop;
	end if;	    

	commit;
	

	return nSid;
end;

function GenMAXHeaderCols( nSid in number, nObjlist in number, dVon in date )	return varchar2
	-- erzeugt Header-Zeile f�r Report R_GAS_MAX f�r Zeitreihen-Reporttabelle zur Session-ID; R�ckgabe: OK oder ERROR
as	
	bError	boolean 		:= FALSE;   
	cError	varchar2(1024) 	:= null;
	bFound	boolean 		:= FALSE;   
	cStmt	varchar2(2000);
	cCols	varchar2(800) := '';
	cSets	varchar2(800) := '';
	cUnit	varchar2(30);
	cJahr		varchar2(4) := to_char(dVon,'YYYY');
	cVorjahr	varchar2(4) := to_char(dVon-365,'YYYY');
begin
	-- sucht sich zu den Zeitreihen alle Spalteninfos f�r die Header-Zeilen und speichert sie in DB-Tabelle PTABHEAD_TA
	--  1. Zeile: R_MAX_NR des Signals - bestehend aus Text+Nr  
	--  2. Zeile: R_MAX_NAME 
	--  3. Einheit und Jahr ; das Jahr muss aus dem Vorgabedatum abgeleitet werden
                           
	-- die Objekte aus REPOBJ verarbeiten              
	for recObj in (select POS,FK_TIS,SHORT_NAME,DESCRIPTION,QUANTITY,UNIT from REPOBJ where SID = nSid order by POS) loop
		-- �ber TIS die Daten laden
		-- in PTABHEAD werden  je drei Headerspalten erzeugt: S1Z1, .. S1Z3  etc besetzt
		-- set S1Z1=Nr, S1Z2=Kurzname, S1Z3=Einheit ... 
		cUnit := recObj.UNIT;
		if cUnit = 'Dimensionslos' or cUnit = 'NA' then
			cUnit := '-'; 
		elsif cUnit = 'kWh' then
			cUnit := 'GWh';
		end if;
		if recObj.POS < 16 then
			if recObj.POS  in (1, 3, 5,6,7, 10, 13) then
				cCols := cCols || 'S'||recObj.POS || 'Z1,S'|| recObj.POS || 'Z2,';
				cSets := cSets || '''' || recObj.SHORT_NAME || ''',''' || recObj.DESCRIPTION || ''',';
			elsif recObj.POS in (9,12,15) then		-- Differenzspalten
				cCols := cCols || 'S'||recObj.POS || 'Z1,S'|| recObj.POS || 'Z2,';
				cSets := cSets || '''' || recObj.SHORT_NAME || ''',''' || recObj.DESCRIPTION || ''',';
			end if;	
			cCols := cCols || 'S'|| recObj.POS || 'Z3,';
			cSets := cSets || ''' [' || cUnit || '] '; 
			-- F�r die Zeile 3 in den Spaltenk�pfen wird die Jahres- oder Vorjahreszahl oder aber sonstiges eingetragen
			if recObj.POS in (1, 3, 7, 10, 13) then
				cSets := cSets || cJahr || ''',';
			elsif recObj.POS in (2, 4, 8, 11, 14) then
				cSets := cSets || cVorjahr || ''',';
			elsif recObj.POS = 5 then
				cSets := cSets || '(-Ausl.)'',';
			elsif recObj.POS in ( 6, 9, 12, 15) then
				cSets := cSets || ''',';
			end if;		
			bFound := true;
		end if;
	end loop;
	if bFound then
		begin
			cStmt := 'insert into PTABHEAD (SID, FK_OBJLIST,' || substr(cCols,1, length(cCols)-1) ||
					 ') values(' || nSid || ','  || nObjlist || ','  || substr(cSets,1, length(cSets)-1) || ')';
		    ExecSqlDirect( cStmt );
		    commit;
		exception    
			when others then
				cError := sqlerrm;
				bError := true;
		end;	
	end if;			

	return ReturnWert( bError, cError );
end;	                         

procedure InsUpdTabval( nSid in number, nObjList in number, nPos in number, cUnit in varchar2, rData in out DD_SYS%rowtype )
as
	bFound 	boolean := false;
	cStmt	varchar2(4000);
begin
	if cUnit = 'kWh' then
		-- setze um in GWh
		rData.VAL := round(rData.VAL/1000000,3);
	else
		-- Temp.-spalte
		rData.VAL := round(rData.VAL,2);
	end if;		
	for rec in (select SID, FK_OBJLIST, REFDATE from PTABVAL where SID=nSid and FK_OBJLIST=nObjList and REFDATE=rData.REF_DATE) loop
		bFound := true;
		cStmt := 'update PTABVAL set VAL' || nPos || '=' || rData.VAL || ', STATUS' || nPos || '=''' || upper(substr(rData.CONDITION,1,1)) || 
			 ''' where SID='|| nSid || ' and FK_OBJLIST=' || nObjList || ' and REFDATE=''' || to_char(rData.REF_DATE) || '''';
	end loop;
	if not bFound then
		-- insert
		cStmt := 'insert into PTABVAL (SID, FK_OBJLIST, REFDATE, REFCDATE, VAL' || nPos || ', STATUS' || nPos ||
		 ' ) values ('  || nSid || ',' ||  nObjList || ', ''' || to_char(rData.REF_DATE) || ''',''' || psi_pckg.Gasday(rData.REF_DATE) || ''',' ||
		 rData.VAL  || ',''' || upper(substr(rData.CONDITION,1,1)) || ''')';
	end if;
	begin
	    ExecSqlDirect( cStmt );
	    commit;
	exception    
		when others then
			--cError := sqlerrm;
			raise;
	end;	

end;

function MAXAbgabeValues( nSid in number, nObjlist in number, dVon in date, dBis in date)	return varchar2
	-- erzeugt Zeilen in Zeitreihen-Reporttabelle zur Session-ID; R�ckgabe: OK oder ERROR
as	                                                                                       
	-- Parameter:   dVon - Gasjahrbeginn
	--				dBis - Vorgabedatum oder Monatsende (bei Altmonat-Vorgabe)
	dBisVj	date := dVon;	-- letzter Tag des Monats im Vorjahr  (Tageswert sollte f�r 1.10. 4:00 UTC gespeichert sein)
    dVonVj	date := add_months(dVon,-12);
    dVonKj	 	date := to_date('01.01.' || to_char(dVon,'yyyy') || ' 05:00', 'dd.mm.yyyy hh24:mi');
    dVonKjVj	date := add_months(dVonKj,-12);		-- Vor-Kalenderjahr
    nValVj	number;
    nValGj	number; 
    nDiff	number;                         
    cText	REPORTOBJ_TA.DESCRIPTION%type;
	bError	boolean 		:= FALSE;   
	cError	varchar2(1024) 	:= null;
	nPosOffset	number;

begin
	begin
		-- f�r alle Objekte einer Liste werden die Zeitreihen-Daten geholt und in REPORTOBJ_TA gespeichert
		for rec in (select POSITION, FK_OBJID, SHORT_NAME, LABEL from MD_OBJLIST where ID = nObjlist) loop
			select substr(DESCRIPTION,1,75) into cText from MD_OBJECTS where OBJID = rec.FK_OBJID;
			-- Zeitreihe:  Tageswerte (System)  - nur die .AVG-Zeitreihe !!
			for recVar in (select VALUE from MD_VARIABLES where OBJID = rec.FK_OBJID and ATTR_NAME='R_MAX_NAME') loop
				cText := recVar.VALUE;
			end loop;
			for recTis in (select ID, UNIT, QUANTITY, FK_OBJ, VALUE_CATEGORY from TIS
				where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = 'D' and TIS.ARCHIVETYPE=1 and (TIS.QUANTITY like '%.AVG' or instr(TIS.QUANTITY,'.',1,1)=0) )
			loop
				-- cPict  := Getpict(recTis.ID);		Aufbereitung der Werte erfolgt immer als ganzzahlige Werte mit Tausendertrennzeichen
				insert into REPORTOBJ_TA  (SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, UNIT, QUANTITY, FK_OBJID)
					values (nSid, nObjlist,rec.POSITION, '0',recTis.ID, '', '', cText, recTis.VALUE_CATEGORY, rec.LABEL, recTis.UNIT, recTis.QUANTITY, rec.FK_OBJID );
			end loop;
			commit;
		end loop;   
		-- Gasabgabe Gesch�ftsjahr
		for recObj in (select POS,FK_TIS,DESCRIPTION,QUANTITY,UNIT from REPOBJ where SID = nSid and FK_OBJLIST = nObjlist order by POS) loop
			-- Werte seit Gasjahrbeginn aufsummieren
			-- Vorjahressumme
			select sum(VAL)/1000000 into nValVj from DD_SYS where FK_TIS = recObj.FK_TIS and REF_DATE > dVonVj and REF_DATE <= dBisVj;
			select sum(VAL)/1000000 into nValGj from DD_SYS where FK_TIS = recObj.FK_TIS and REF_DATE > dVon and REF_DATE <= dBis; 
			if nValVj is null then nValVj := 0; end if;
			if nValGj is null then nValGj := 0; end if;
			nDiff := nValGj - nValVj; 
			if nValVj <> 0 then
				-- Eintrag in R_GAS_H
				insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2, VAL3, VAL4)
					values( nSid, nObjlist, recObj.POS, 0, recObj.DESCRIPTION, dVon, nValGj, nValVj,  nDiff, round(100*nDiff/nValVj,1));
			else
				insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2, VAL3, VAL4)
					values( nSid, nObjlist, recObj.POS, 0, recObj.DESCRIPTION, dVon, nValGj, nValVj,  nDiff, null);
			end if;
			-- Pos.nr im primary key - daher �ber Variable erh�hen
			nPosOffset := recObj.POS;
		end loop;
		commit;
		-- Gasabgabe Kalenderjahr
		for recObj in (select POS,FK_TIS,DESCRIPTION,QUANTITY,UNIT from REPOBJ where SID = nSid and FK_OBJLIST = nObjlist order by POS) loop
			select sum(VAL)/1000000 into nValVj from DD_SYS where FK_TIS = recObj.FK_TIS and REF_DATE > dVonKjVj and REF_DATE <= dVonKj;
			select sum(VAL)/1000000 into nValGj from DD_SYS where FK_TIS = recObj.FK_TIS and REF_DATE > dVonKj and REF_DATE <= add_months(dVonKj,12); 
			if nValVj is null then nValVj := 0; end if;
			if nValGj is null then nValGj := 0; end if;
			nDiff := nValGj - nValVj; 
			if nValVj <> 0 then
				-- Eintrag in R_GAS_H
				insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2, VAL3, VAL4)
					values( nSid, nObjlist, recObj.POS + nPosOffset, 0, recObj.DESCRIPTION, dVonKj, nValGj, nValVj,  nDiff, round(100*nDiff/nValVj,1));
			else
				insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2, VAL3, VAL4)
					values( nSid, nObjlist, recObj.POS + nPosOffset, 0, recObj.DESCRIPTION, dVonKj, nValGj, nValVj,  nDiff, null);
			end if;
		end loop;
		commit;
	exception    
		when others then
			cError := sqlerrm;
			bError := true;
	end;	

	return ReturnWert( bError, cError );
end;
  
function MAX_MaxValues( nSid in number, nObjlist in number, dVon in date, dBis in date)	return varchar2
	-- erzeugt Zeilen in Zeitreihen-Reporttabelle zur Session-ID; R�ckgabe: OK oder ERROR
as	                                                                                       
	-- Parameter:   dVon - Gasjahrbeginn
	--				dBis - Vorgabedatum oder Monatsende (bei Altmonat-Vorgabe)
    nMax	number; 
    nDiff	number;
    nTemp	number;  
    dMax	date;
	bError	boolean 		:= FALSE;   
	cError	varchar2(1024) 	:= null;
    nTempTis	number;                       
    cText		REPORTOBJ_TA.DESCRIPTION%type;
begin
	begin
		-- f�r alle Objekte einer Liste werden die Zeitreihen-Daten geholt und in REPORTOBJ_TA gespeichert
		for rec in (select POSITION, FK_OBJID, SHORT_NAME, LABEL from MD_OBJLIST where ID = nObjlist) loop
			select substr(DESCRIPTION,1,75) into cText from MD_OBJECTS where OBJID = rec.FK_OBJID;
			-- Zeitreihe:  Tageswerte (System)  - nur die .AVG-Zeitreihe !!
			for recVar in (select VALUE from MD_VARIABLES where OBJID = rec.FK_OBJID and ATTR_NAME='R_MAX_NAME') loop
				cText := recVar.VALUE;
			end loop;
			cText := 'max. ' || cText;
			for recTis in (select ID, UNIT, QUANTITY, FK_OBJ, VALUE_CATEGORY from TIS
				where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = 'D' and TIS.ARCHIVETYPE=1 and (TIS.QUANTITY like '%.AVG' or instr(TIS.QUANTITY,'.',1,1)=0) )
			loop
				-- cPict  := Getpict(recTis.ID);		Aufbereitung der Werte erfolgt immer als ganzzahlige Werte mit Tausendertrennzeichen
				insert into REPORTOBJ_TA  (SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, UNIT, QUANTITY, FK_OBJID)
					values (nSid, nObjlist,rec.POSITION, '0',recTis.ID, '', '', cText, recTis.VALUE_CATEGORY, rec.LABEL, recTis.UNIT, recTis.QUANTITY, rec.FK_OBJID );
			end loop;
			commit;
		end loop; 
		-- Temp.-Zeitreihe merken (von 1. Objekt) 
		select FK_TIS into nTempTis from REPOBJ where SID = nSid and FK_OBJLIST = nObjlist and POS = 1; 
		-- Maxwerte im Gesch�ftsjahr
		for recObj in (select POS,FK_TIS,DESCRIPTION,QUANTITY,UNIT from REPOBJ where SID = nSid and FK_OBJLIST = nObjlist and POS > 1 order by POS) loop
			-- Max-Wert seit Gasjahrbeginn - Achtung: bei Kaverne ist Minimum zu suchen, da Auslagerung mit negativen Zahlen eingetragen wird.
			if instr(recObj.DESCRIPTION, 'Kaverne',1,1) > 0 then
				select min(VAL) into nMax from DD_SYS where FK_TIS = recObj.FK_TIS and REF_DATE > dVon and REF_DATE <= dBis; 
			else
				select max(VAL) into nMax from DD_SYS where FK_TIS = recObj.FK_TIS and REF_DATE > dVon and REF_DATE <= dBis; 
			end if;
			select REF_DATE into dMax from DD_SYS where FK_TIS = recObj.FK_TIS and REF_DATE > dVon and REF_DATE <= dBis and VAL = nMax and rownum<2; 
			-- zugeh�riges Datum suchen (von 1. Max-Wert, falls mehrere vorhanden)
			nTemp := null;
			for recTemp in (select VAL from DD_SYS where FK_TIS = nTempTis and REF_DATE = dMax) loop
				nTemp := recTemp.VAL;
			end loop;
			
			-- Eintrag in R_GAS_H
			insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2)
				values( nSid, nObjlist, recObj.POS, 0, recObj.DESCRIPTION, dMax, round(nMax/1000000,3), round(nTemp,1));
		end loop;
		commit;
	exception    
		when others then
			cError := sqlerrm;
			bError := true;
	end;	

	return ReturnWert( bError, cError );
end;

function MAX_Kaverne( nSid in number, nObjlist in number, dVon in date, dBis in date)	return varchar2
	-- erzeugt Zeilen f�r Kaverne im Report GAS_MAX; R�ckgabe: OK oder ERROR
as	                                                                                       
	-- Parameter:   dVon - Gasjahrbeginn
	--				dBis - Vorgabedatum oder Monatsende (bei Altmonat-Vorgabe)
    nWert	number; 
    nDiff	number;
    nTemp	number; 
    nProz	number; 
    nEinl	number;
    nAusl	number;
    nMax	number; 
    nFuellstand	number := 0;
    iLfdnr	integer	:= 0;
    dWert	date;
	bError	boolean 		:= FALSE;   
	cError	varchar2(1024) 	:= null;
    nTempTis	number;                       
    cText		REPORTOBJ_TA.DESCRIPTION%type;
    cLabel      MD_OBJLIST.LABEL%type;
begin
	begin
		begin
			-- f�r alle Objekte einer Liste werden die Zeitreihen-Daten geholt und in REPORTOBJ_TA gespeichert
			for rec in (select POSITION, FK_OBJID, SHORT_NAME, LABEL from MD_OBJLIST where ID = nObjlist) loop
				cLabel := rec.LABEL;
				select substr(DESCRIPTION,1,75) into cText from MD_OBJECTS where OBJID = rec.FK_OBJID;
				-- Zeitreihe:  Tageswerte (System)  - nur die .AVG-Zeitreihe !!
				for recVar in (select VALUE from MD_VARIABLES where OBJID = rec.FK_OBJID and ATTR_NAME='R_MAX_NAME') loop
					cText := recVar.VALUE;
				end loop;
				iLfdNr := rec.POSITION;
				for recTis in (select ID, UNIT, QUANTITY, FK_OBJ, VALUE_CATEGORY from TIS
					where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = 'D' and TIS.ARCHIVETYPE=1 and (TIS.QUANTITY like '%.AVG' or instr(TIS.QUANTITY,'.',1,1)=0) )
				loop
					-- cPict  := Getpict(recTis.ID);		Aufbereitung der Werte erfolgt immer als ganzzahlige Werte mit Tausendertrennzeichen
					insert into REPORTOBJ_TA  (SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, UNIT, QUANTITY, FK_OBJID)
						values (nSid, nObjlist,rec.POSITION, rec.POSITION,recTis.ID, '', '', cText, recTis.VALUE_CATEGORY, rec.LABEL, recTis.UNIT, recTis.QUANTITY, rec.FK_OBJID );
				end loop;
				commit;
			end loop; 
		exception    
			when others then
				cError := sqlerrm;
				-- wenn in obigem Loop ein Fehler auftritt, dann weil mehr als 1 Zeitreihe f�r ein Objekt gefunden wurde
				-- die fehlerhafte Zeitreihe muss entfernt werden -> Meldung an den Anwender 
				insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2)
					values( nSid, nObjlist, iLfdNr, iLfdNr+1, '2 Tages-Zeitreihen unzul�ssig f�r '|| cLabel, dVon, null, null);
				commit;	
				bError := true;
		end;	
		if not bError then
			-- 1. Objekt: nutzbare Arbeitsgasmenge G-Jahr-Beginn
			-- 2. Objekt: F�llstand
			-- 3. + 4. Objekt: Einlagerung und Auslagerung
			for recObj in (select POS,FK_TIS,DESCRIPTION,QUANTITY,UNIT from REPOBJ where SID = nSid and FK_OBJLIST = nObjlist order by POS) loop
				iLfdnr := iLfdnr + 1;
				cText := recObj.DESCRIPTION;
				if recObj.POS in ( 1, 2 ) then 
					nWert := null;
					dWert := dVon;
					for recDD in (select VAL from DD_SYS where FK_TIS = recObj.FK_TIS and REF_DATE = dVon) loop
						nWert := recDD.VAL;
					end loop;
					if recObj.POS = 1 then
						nMax := nWert;	-- nutzbare Arbeitsgasmenge G-Jahr-Beginn
						nProz := 100;
					else
						nFuellstand := nWert;
						if nMax > 0 then
							nProz := 100 * nWert / nMax;
						else
							nProz := 0;
						end if;
					end if;
				else
					-- Ein- oder Auslagerungsmenge seit G-Jahr-Beginn (Summe)
					select SUM(VAL) into nWert from DD_SYS where FK_TIS = recObj.FK_TIS and REF_DATE > dVon and REF_DATE <= dBis; 
					dWert := dBis;				
					nProz := null;
					if recObj.POS = 3 then	-- Einlag.
						nEinl := nWert;
					else
						nAusl := nWert;
					end if;					
				end if; 
				-- Eintrag in R_GAS_H  - Wert in GWh
				if nWert is null then
					cText := substr(cText,1,40) || '- Fehler: Wert fehlt';
				end if;
				insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2)
					values( nSid, nObjlist, iLfdnr, iLfdnr, cText, dWert, round(nWert/1000000,3), round(nProz,1));
				commit;	
			end loop;
			-- Inhalt Speicher am Berichtstag 
			iLfdnr := iLfdnr + 1;
			nWert := nFuellstand + nEinl - nAusl;
			nProz := 100 * nWert / nMax;
			insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2)
				values( nSid, nObjlist, iLfdnr, iLfdnr, 'Inhalt Speicher am Berichtstag', dBis, round(nWert/1000000,3), round(nProz,1) );
			commit;
		else
			bError := false;	-- wir wollen, dass eine Ausgabe erfolgt im aufrufenden Programm
		end if;
	exception    
		when others then
			cError := sqlerrm;
			bError := true;
			insert into R_GAS_H (SID, FK_OBJLIST, POS, NR, NAME, REFDATE, VAL1, VAL2)
				values( nSid, nObjlist, iLfdNr, iLfdNr+1, 'Fehler '|| substr(cError,1,70), dVon, null, null);
			commit;	
	end;	

	return ReturnWert( bError, cError );
end;

function GenMAXValues( nSid in number, nObjlist in number, dVon in date, dBis in date)	return varchar2
	-- erzeugt Zeilen in Zeitreihen-Reporttabelle zur Session-ID; R�ckgabe: OK oder ERROR
as	
	TYPE DataTable IS TABLE OF	DD_SYS%rowtype;
	DataStore DataTable;

	bError	boolean 		:= FALSE;   
	cError	varchar2(1024) 	:= null;
	bFound	boolean 		:= FALSE;   
	bFound2	boolean 		:= FALSE;   
	cStmt	varchar2(4000);
	cVon	varchar(20)		:= to_char(dVon,DATE_FORMAT_GER);
	cBis	varchar(20)		:= to_char(dBis,DATE_FORMAT_GER);
	
	dBisVj	date := add_months(psi_pckg.UTCMonatsendeStd(dVon),-12);	-- letzter Tag des Monats im Vorjahr
    dVonVj	date := add_months(dVon,-12);
	cVonVj	varchar(20)		:= to_char(dVonVj,DATE_FORMAT_GER);
	cBisVj	varchar(20);
	cCols	varchar2(2000) := '';
--	cStats	varchar2(1000) := '';
	cSel	varchar2(2000) := '';
	cWhere	varchar2(4000) := '';
	cFrom	varchar2(1000) := '';
	cAlias	varchar2(6); 

	cCols2	varchar2(2000) := '';
	cSel2	varchar2(2000) := '';	-- f�r Selektion auf Vorjahresdaten
	cWhere2	varchar2(4000) := '';
	cFrom2	varchar2(1000) := '';
	cAlias2	varchar2(6); 

	iDez	integer;  
	nVal9	number;
	nVal12	number;
	nVal15	number;
	iLen	integer;
	
begin                                    
	-- Im Schaltjahr (mit 29.2.) wird mit dem 1.3. des Vorjahres verglichen.
/*	if to_char(dBis,'dd.mm') =  '29.02' then
		dBisVj := dBis - 365;
	end if;
	-- Wenn im aktuellen Jahr nur 28 Tage im Februar existieren, sind nur 28 Tage im Bericht auszuweisen, auch wenn das Vorjahr ein Schaltjahr mit 29 Tagen war.
	if to_char(dBisVj,'dd.mm') =  '29.02' then
		dBisVj := dBisVj - 1;
	end if;
*/
	cBisVj	:= to_char(dBisVj,DATE_FORMAT_GER);
	
	-- erh�lt die gleiche zu erzeugende Session-ID sowie Von und Bis-Zeit f�r die Datenselektion
	-- holt sich aus Tabelle REPORTGRP_TA und REPOBJ die Objekte zur Gruppe und baut dynamisch die Datenselektion aus DD_SYS
	-- auf, f�hrt sie aus und schreibt das Ergebnis in Tabelle TABVALUES_TA
	execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
    execsqldirect( 'alter session set nls_numeric_characters = ''.,''');                       
	-- die Objekte aus REPOBJ verarbeiten              
	for recObj in (select * from REPOBJ where SID = nSid and FK_OBJLIST = nObjlist order by POS) loop
		-- �ber TIS die Daten laden
		-- in <cDataSrc> stehen die Zeitreihenwerte  (select between cVon and cBis - liefert alle Werte >=cVon und <= cBis
		-- Differenz in % hat 1, Temp.werte haben 2, GWh-Werte im Report haben 3 Nachkommastellen
		if recObj.UNIT='GWh' or recObj.UNIT='kWh' then
			iDez := 3;
		elsif recObj.UNIT='%' then
			iDez := 1;
		else
			iDez := 2;
		end if;
		if recObj.POS not in (9,12,15) then
			if recObj.POS in (2, 4, 8, 11, 14) then   
		   		select FK_TIS, REF_DATE, VAL, CONDITION, APPROVAL, VALIDITY BULK COLLECT INTO DataStore from DD_SYS
			  	 where FK_TIS = recObj.FK_TIS and REF_DATE between dVonVj and dBisVj;
			  	if DataStore.First is not null then
				    FOR x in DataStore.First..DataStore.Last loop
				    	InsUpdTabval( nSid, recObj.FK_OBJLIST, recObj.POS, recObj.UNIT, DataStore(x) );
					end loop;
				end if;
			else
		   		select FK_TIS, add_months(REF_DATE,-12), VAL, CONDITION, APPROVAL, VALIDITY BULK COLLECT INTO DataStore from DD_SYS
			  	 where FK_TIS = recObj.FK_TIS and REF_DATE between dVon and dBis;
			  	
			  	if DataStore.First is not null then
				    FOR x in DataStore.First..DataStore.Last loop
				    	InsUpdTabval( nSid, recObj.FK_OBJLIST, recObj.POS, recObj.UNIT, DataStore(x) );
					end loop;
				end if;				
			end if;				
        end if;
	end loop;			
	-- Erg�nze Differenz-Spalten
	for recTab in (select * from PTABVAL where SID = nSid and FK_OBJLIST = nObjlist order by REFDATE) loop
		nVal9 := null; nVal12 := null; nVal15 := null;
		if recTab.VAL8 is not null then
			nVal9  := round(100 * (recTab.VAL7 - recTab.VAL8)/recTab.VAL8 ,2);
		end if;
		if recTab.VAL11 is not null then
			nVal12 := round(100 * (recTab.VAL10 - recTab.VAL11)/recTab.VAL11 ,2);
		end if;
		if recTab.VAL14 is not null then
			nVal15 := round(100 * (recTab.VAL13 - recTab.VAL14)/recTab.VAL14 ,2);
		end if;
		update PTABVAL set VAL9 = nVal9, STATUS9 = 'V', 
						   VAL12 = nVal12, STATUS12 = 'V',
						   VAL15 = nVal15, STATUS15 = 'V' 
						   where SID = nSid and FK_OBJLIST = recTab.FK_OBJLIST and REFDATE = recTab.REFDATE;
		commit;
	end loop;
	commit;
    -- Erg�nze Mittelwerte oder Summenwerte
    cSel := '';
	for recObj in (select POS,FK_TIS,VALUE_CATEGORY from REPOBJ where SID = nSid and FK_OBJLIST = nObjlist order by POS) loop
		cCols := cCols || 'VAL'||recObj.POS || ','; 
		if recObj.VALUE_CATEGORY = 'M' then		-- Messwert => average bilden
			cSel := cSel || '(select round(avg(VAL' || recObj.POS || '),2) from PTABVAL where SID=' ||  nSid || '),';
		else
			cSel := cSel || '(select round(sum(VAL' || recObj.POS || '),3) from PTABVAL where SID=' ||  nSid || '),';
		end if;
	end loop;
	begin
		cStmt := 'insert into PTABVAL (SID, FK_OBJLIST,REFDATE,' || substr(cCols,1, length(cCols)-1) || ') values (' ||  nSid || ',' || nObjlist || ',sysdate+365,' 
				 || substr(cSel,1, length(cSel)-1) || ')';
	    ExecSqlDirect( cStmt );
	    commit;
	exception    
		when others then
			cError := sqlerrm;
			bError := true;
	end;	

	return ReturnWert( bError, cError );
end;	

function R_GAS_MAX( nObjlistId1 in number,  nObjlGasabgeId in number, nObjlMaxId in number, nObjlKaverneId in number, dVon in date ) return OutHEAD pipelined
--function R_GAS_MAX( nObjlistId1 in number,  nObjlGasabgeId in number, nObjlMaxId in number, nObjlKaverneId in number, dVon in date ) return varchar2 -- f�r Debug-Test 
	/* R_GAS_MAX - Monatsbericht Gasabgabe und MAX-Werte

	   verwendet die Tabellen PTABHEAD und PTABVAL, PTABVAL 
	   Das F�llen der Tabelle �bernimmt die Funktion R_GAS_MAX; das zugeh�rige jrxml-File ist R_GAS_MAX-Vx-y
	   Parameter: 4 Objektlisten, jeweils mit ID �bergeben
	   			  Datum - Gastag (im Monat) - ausgegeben werden alle Daten von Monatsbeginn bis zum letzten abgeschlossenen Gastag des Monats
	*/
as
    --tHEAD		OUTHEAD_TYPE;
	cError	varchar2(1024) 	:= null;
	bError	boolean 		:= FALSE;      
--	dVon	date			:= psi_pckg.UTCGasDayFirstHour(to_date(cVon || '07:00:00', DATE_FORMAT_GER));	-- Zeit in UTC 
	dBis	date;
	cVon	varchar2(20) := to_char(dVon, 'dd.mm.yyyy');  
	dMonBeginn		date	:= psi_pckg.UTCMonatsbeginn(dVon)+1;	-- erster Tagessatz ist am 2. des Monats
	cVonMonat		varchar2(20) := to_char(dVon, 'mm.yyyy');
	cAktMonat		varchar2(20) := to_char(sysdate, 'mm.yyyy');
	dMonEnde		date    := psi_pckg.UTCMonatsendeStd(dVon);
	
	nSid	number;
	iLfdnr	integer	:= 0; 
	cDataSrc	varchar2(12) := 'DD_SYS';
	cArchivtyp  varchar2(1) := '1';     -- Systemwerte
	cTimelevel	varchar2(4) := 'D';		-- Tageswerte-Archiv
	cPict	REPOBJ.PICTURE%type;
	cRet	varchar2(2000);
	cText	varchar2(80);
	cNr		varchar2(20);
	cLab1	varchar2(20);  
	nStaObjId	number;
	pragma autonomous_transaction;  -- necessary because of insert or deletes while the function is used in a select                                                 
	
begin 
	if cAktMonat = cVonMonat then
		-- Report nur von Monatsbeginn bis zum vorgegebenen Tag ausgeben
		dBis := psi_pckg.UTCGasDayFirstHour(dVon + 1);
	else   
		dBis := dMonEnde;
	end if;                                        
	execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
	select userenv('sessionid') into nSid from dual;

	-- Reportgruppe und -objekte erzeugen
	DelSid( nSid );
	insert into REPORTGRP_TA (SID, REPNAME, ERSTELLT_AM) values( nSid, 'Report GAS_MAX f�r den ' || cVon, sysdate );
	
	-- f�r alle Objekte einer Liste werden die Zeitreihen-Daten geholt und in REPORTOBJ_TA gespeichert
	for rec in (select POSITION, FK_OBJID, SHORT_NAME, LABEL from MD_OBJLIST where ID = nObjlistId1) loop
		-- Variablentext R_MAX_NAME holen oder den Namen der Station
		cLab1 :=   substr(rec.LABEL,1, instr(rec.LABEL,'.',1)-1);             
		nStaObjId := psi_pckg.GetParentId( rec.FK_OBJID, 'SC_ST' );
		-- ermittle Objekt(=Station) und Kurzname dazu
		cText := rec.LABEL;
		for recObj in (select  SHORT_NAME from MD_OBJECTS where OBJID = nStaObjId) loop
			cText := recObj.SHORT_NAME;
		end loop;
		for recVar in (select VALUE from MD_VARIABLES where OBJID = rec.FK_OBJID and ATTR_NAME='R_MAX_NAME') loop
			cText := recVar.VALUE;
		end loop;
		cNr := ''; --substr(cLab1,length(cLab1)-2, 3);
		for recVar in (select VALUE from MD_VARIABLES where OBJID = rec.FK_OBJID and ATTR_NAME='R_MAX_NR') loop
			cNr := recVar.VALUE;
		end loop;               
		
		-- Zeitreihe:  Tageswerte (System)  - nur die .AVG-Zeitreihe !!
		for recTis in (select ID, UNIT, QUANTITY, FK_OBJ, VALUE_CATEGORY from TIS
			where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = cTimelevel and TIS.ARCHIVETYPE=cArchivtyp and (TIS.QUANTITY like '%.AVG' or instr(TIS.QUANTITY,'.',1,1)=0) )
		loop
			iLfdNr := iLfdNr + 1;
			-- cPict  := Getpict(recTis.ID);		Aufbereitung der Werte erfolgt immer als ganzzahlige Werte mit Tausendertrennzeichen
			insert into REPORTOBJ_TA  (SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, UNIT, QUANTITY, FK_OBJID)
				values (nSid, nObjlistId1,iLfdNr, '0',recTis.ID, cPict, cNr, cText, recTis.VALUE_CATEGORY, rec.LABEL, recTis.UNIT, recTis.QUANTITY, rec.FK_OBJID );
			-- Vorjahres-Zeitreihen (nur f�r bestimmte Positionen) - gleiche Zeitreih nochmal eintragen
			if iLfdNr = 1 or iLfdNr = 3 or iLfdNr = 7 or iLfdNr = 10 or iLfdNr = 13 then
				iLfdNr := iLfdNr + 1;
				insert into REPORTOBJ_TA  (SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, UNIT, QUANTITY, FK_OBJID)
					values (nSid, nObjlistId1,iLfdNr, '0',recTis.ID, cPict, '', '', recTis.VALUE_CATEGORY, rec.LABEL, recTis.UNIT, recTis.QUANTITY, rec.FK_OBJID );
			end if;
			if iLfdNr = 8 or iLfdNr = 11 or iLfdNr = 14 then	-- Differenzspalten erzeugen
				iLfdNr := iLfdNr + 1;
				insert into REPORTOBJ_TA  (SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, UNIT, QUANTITY, FK_OBJID)
					values (nSid, nObjlistId1,iLfdNr, '0',0, cPict, '', 'Differenz', 'M', '', '%', '', 0 );
			end if;
		end loop;
		commit;
	end loop;   
	
	if iLfdNr > 0 then
		-- Tabellenheader erzeugen - 3 zeilig: 
		--   1. Zeile: leer oder R_MAX_NR des Signalobjekts
		--   2. Zeile: Kurzname Station oder R_MAX_NAME, 
		--   3. Einheit 
		cRet := GenMAXHeaderCols( nSid, nObjlistId1, dVon );             
		        
		if cRet = 'OK' then
			-- Tabellenwerte erzeugen
			cRet := GenMAXValues( nSid, nObjlistId1, dMonBeginn, dBis ); 
			 
			-- 2.+3. Tabelle: Gasabgabe Gasjahr und Kalenderjahr (mit gleicher Obj.liste)
			cRet := MAXAbgabeValues( nSid, nObjlGasabgeId, psi_pckg.UTCGasjahrbeginn(dMonBeginn), dBis ); 
			
			-- 4. Tabelle: Max-Werte im Gasjahr
			cRet := MAX_MaxValues( nSid, nObjlMaxId, psi_pckg.UTCGasjahrbeginn(dMonBeginn), dBis ); 

			-- 5. Tabelle: Speicher Empelde
			cRet := MAX_Kaverne( nSid, nObjlKaverneId, psi_pckg.UTCGasjahrbeginn(dMonBeginn), dBis ); 

			--if cRet = 'OK' then
				-- Headerzeile zur�ckgeben
				-- select SIDSID, FK_OBJLIST, S1Z1, S1Z2, S1Z3, S2Z1, S2Z2, S2Z3, S3Z1, S3Z2, S3Z3,...
				
				for r in (select * from PTABHEAD where SID = nSid and FK_OBJLIST = nObjlistId1) loop
					pipe row( r );
				end loop;
				--Delete_Sessiondata( nSid, 'PTABHEAD' );
				--*/
				--return 'Ok';		-- f�r Debug-Test
			--end if;
		end if;
	end if;
	commit;
	--return 'Error';	
end;  
function W_GANG_HeaderCols( nSid in number, nObjlist in number, dVon in date ) return varchar2
as
       -- In PTABHEAD: werden die Kopfdaten f�r vier Signale eingetragen, die in der Objektliste 1 enthalten sind.
	   -- Je Objekt werden eingetragen: R<n>H1 - Inhalt von R_W_GANG_NAME, R<n>H2 - Tageswert
	dTag	date := PSI_Pckg.UTCGasDayFirstHour( dVon+1 ) + 23/24;		-- Vorgabewert ist dd.mm.yyyy 00 Uhr; Wert um 6 Uhr Tagesende (Wasser hat auch Tagesbeginn 6 Uhr)
	cRet	varchar2(10) := 'OK';
	bError	boolean 		:= FALSE;   
	cError	varchar2(1024) 	:= null;
	bFound	boolean 		:= FALSE;   
	cStmt	varchar2(2000);
	cCols	varchar2(800) := '';
	cSets	varchar2(800) := '';
	cWert	varchar2(20);
begin
	for recObj in (select * from REPOBJ where SID = nSid and FK_OBJLIST = nObjlist order by POS) loop 
		-- �ber TIS die Daten laden     
		cWert := '<Wert fehlt>';
		for rec in (select VAL from DD_SYS where FK_TIS = recObj.FK_TIS and REF_DATE = dTag) loop
			cWert := to_char(rec.VAL, '999G990');
		end loop;
		-- in PTABHEAD werden  je zwei Headerspalten erzeugt: S1Z1, S1Z2  etc besetzt
		-- set S1Z1=Name, S1Z2=Tageswert
		cCols := cCols || 'S'||recObj.POS || 'Z1,S'|| recObj.POS || 'Z2,';
		cSets := cSets || '''' || recObj.SHORT_NAME || ''',''' || cWert || ''',';
		bFound := true;
	end loop;
	if bFound then
		begin
			cStmt := 'insert into PTABHEAD (SID, FK_OBJLIST,' || substr(cCols,1, length(cCols)-1) ||
					 ') values(' || nSid || ','  || nObjlist || ','  || substr(cSets,1, length(cSets)-1) || ')';
		    ExecSqlDirect( cStmt );
		    commit;
		exception    
			when others then
				cError := sqlerrm;
				bError := true;
		end;	
	end if;			

	return ReturnWert( bError, cError );
end;	                         

function W_GANG_Values( nSid in number, nObjlist in number, nTis1 in number, nTis2 in number, dVon in date ) return varchar2
as
    -- f�r jede Zeitreihe wird ein Fl�chenchart gezeichnet, dessen Farbe sich �ber die bereits gezeichneten Fl�chen legt
    -- Zeitreihe 1: EW1234 ...,  Zr2: PR9000...
    -- F1 - Werte oberhalb Grenzlinie (gr�n) - gebildet aus Wert-Zr1 (wenn > Zr2) bzw. Zr2, wenn Zr1 kleiner
    -- F2 - Werte aus Zr2  (rot)             
    -- F3 - wei�, Werte : = Zr2-Wert, wenn Zr1 > Zr2 bzw. = Zr1, wenn Zr1 kleiner Zr2
	dAb		date := PSI_Pckg.UTCGasDayFirstHour( dVon ) -1/24;		-- Wert um 6 Uhr auch lesen (Wasser hat auch Tagesbeginn 6 Uhr)
	dBis	date := dAb + 1;
	cBis	varchar2(10);
	cRet	varchar2(10) := 'OK';
	nVal1	number;
	nVal3	number;
begin
    if psi_pckg.isMesz(dVon) = 1 and psi_pckg.isMesz(dBis) = 0 then
    	dBis := dBis + 1/24;
    end if;

	for rec in (select a.REF_DATE, a.VAL "VAL1", b.VAL "VAL2" from DD_SYS a, DD_SYS b 
				where a.FK_TIS= nTis1 and b.FK_TIS= nTis2 and a.REF_DATE = b.REF_DATE and a.REF_DATE between dAb and dBis ) loop
		if rec.VAL1 > rec.VAL2 then
			nVal1 := rec.VAL1;
			nVal3 := rec.VAL2;
		else                  
			nVAL1 := null;
			nVal3 := rec.VAL1;
		end if;
		-- Minutenzeitreihe - immer gleiche Stunde in REFCDATE eintragen
		cBis := substr(PSI_Pckg.UTC2MESZhour(rec.REF_DATE),9,3);
		if substr(cBis,3,1) = ':' then
			cBis := '| ' || substr( cBis, 1,2 );
		else
			cBis := '| ' || cBis;
		end if;
		begin
			insert into PTABVAL (SID, FK_OBJLIST, REFDATE, REFCDATE, VAL1, VAL2, VAL3)
				values( nSid, nObjlist, rec.REF_DATE, cBis, round(nVal1,0), round(rec.VAL2,0), round(nVal3,0) );
		exception    
			when others then
				cRet := 'ERROR';
		end;	
	end loop;
	commit;
	return cRet;
end;	            

--function R_W_GANG( nObjlistId1 in number,  nObjlistId2 in number, dVon in date ) return varchar2
function R_W_GANG( nObjlistId1 in number,  nObjlistId2 in number, dVon in date ) return OUTHEAD pipelined
	/* R_W_GANG - Tagesganglinie Wasser
       verwendet die Tabellen PTABHEAD und PTABVAL
       In PTABHEAD: werden die Kopfdaten f�r vier Signale eingetragen, die in der Objektliste 1 enthalten sind.
	   Je Objekt werden eingetragen: R<n>H1 - Inhalt von R_W_GANG_NAME, R<n>H2 - Tageswert
	   In PTABVAL: Minutenwert-Zeitreihen - gebildet aus den Zeitreihen der beiden Objekte in Obj.liste 2  
	   - f�r jede Zeitreihe wird ein Fl�chenchart gezeichnet, dessen Farbe sich �ber die bereits gezeichneten Fl�chen legt
	   - F1 - Werte oberhalb Grenzlinie (gr�n) - gebildet aus Wert-Zr1 (wenn > Zr2) bzw. Zr2, wenn Zr1 kleiner
	   - F2 - Werte aus Zr2  (rot)             
	   - F3 - wei�, Werte : = Zr2-Wert, wenn Zr1 > Zr2 bzw. = Zr1, wenn Zr1 kleiner Zr2
	   Parameter: 2 Objektlisten, jeweils mit ID �bergeben
	   			  Datum - Datum - ausgegeben werden alle Daten ab Tagesbeginn  6 Uhr bis Tagesende 6 Uhr
	*/
as
    --tHEAD		OUTHEAD_TYPE;
	cError	varchar2(1024) 	:= null;
	bError	boolean 		:= FALSE;      
--	dVon	date			:= psi_pckg.UTCGasDayFirstHour(to_date(cVon || '07:00:00', DATE_FORMAT_GER));	-- Zeit in UTC 
	dBis	date;
	cVon	varchar2(20) := to_char(dVon, 'dd.mm.yyyy');  
	
	nSid	number;
	iLfdnr	integer	:= 0; 
	cArchivtyp  varchar2(1) := '1';     -- Systemwerte
	cTimelevel	varchar2(4) := 'D';		-- Tageswerte-Archiv
	cPict	REPOBJ.PICTURE%type;
	cRet	varchar2(10);
	cText	varchar2(80);
	cLab1	varchar2(20);
	nTis1	number;
	nTis2	number;
	nStaObjId	number;
	pragma autonomous_transaction;  -- necessary because of insert or deletes while the function is used in a select                                                 
	
begin 
	execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
	execsqldirect( 'ALTER SESSION SET NLS_NUMERIC_CHARACTERS= '',.'' ');

	select userenv('sessionid') into nSid from dual;

	-- Reportgruppe und -objekte erzeugen
	DelSid( nSid );
	insert into REPORTGRP_TA (SID, REPNAME, ERSTELLT_AM) values( nSid, 'Report W_GANG f�r den ' || cVon, sysdate );
	
	-- f�r alle Objekte einer Liste werden die Zeitreihen-Daten geholt und in REPORTOBJ_TA gespeichert
	for rec in (select POSITION, FK_OBJID, SHORT_NAME, LABEL from MD_OBJLIST where ID = nObjlistId1) loop
		-- Variablentext R_MAX_NAME holen oder den Namen der Station
		cLab1 :=   substr(rec.LABEL,1, instr(rec.LABEL,'.',1)-1);             
		nStaObjId := psi_pckg.GetParentId( rec.FK_OBJID, 'SC_ST' );
		-- ermittle Objekt(=Station) und Kurzname dazu
		cText := rec.LABEL;
		for recObj in (select  SHORT_NAME from MD_OBJECTS where OBJID=nStaObjId) loop
			cText := recObj.SHORT_NAME;
		end loop;
		for recVar in (select VALUE from MD_VARIABLES where OBJID = rec.FK_OBJID and ATTR_NAME='R_W_GANG_NAME') loop
			cText := recVar.VALUE;
		end loop;
		
		-- Zeitreihe:  Tageswerte (System)  - nur die .AVG-Zeitreihe !!
		for recTis in (select ID, UNIT, QUANTITY, FK_OBJ, VALUE_CATEGORY from TIS
			where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = cTimelevel and TIS.ARCHIVETYPE=cArchivtyp and (TIS.QUANTITY like '%.AVG' or instr(TIS.QUANTITY,'.',1,1)=0) )
		loop
			iLfdNr := iLfdNr + 1;
			-- cPict  := Getpict(recTis.ID);		Aufbereitung der Werte erfolgt immer als ganzzahlige Werte mit Tausendertrennzeichen
			insert into REPORTOBJ_TA  (SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, UNIT, QUANTITY, FK_OBJID)
				values (nSid, nObjlistId1,iLfdNr, '0',recTis.ID, cPict, cText, cLab1, recTis.VALUE_CATEGORY, rec.LABEL, recTis.UNIT, recTis.QUANTITY, rec.FK_OBJID );
		end loop;
		commit;
	end loop;   
	
	if iLfdNr > 0 then
		-- Tabellenheader erzeugen 
		cRet := W_GANG_HeaderCols( nSid, nObjlistId1, dVon );             
		        
		if cRet = 'OK' then
			-- Minutenwert-Zeitreihen aus Objliste 2 bearbeiten
			for rec in (select POSITION, FK_OBJID, SHORT_NAME, LABEL from MD_OBJLIST where ID = nObjlistId2) loop
				-- Zeitreihe:  Minutenwerte (System) 
				for recTis in (select ID, UNIT, QUANTITY, FK_OBJ, VALUE_CATEGORY from TIS
					where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = 'MI' and TIS.ARCHIVETYPE=cArchivtyp and (TIS.QUANTITY like '%.AVG' or instr(TIS.QUANTITY,'.',1,1)=0) )
				loop                                         
					if rec.POSITION = 1 then
						nTis1 := recTis.ID;
					else
						nTis2 := recTis.ID;
					end if;
				end loop;
				commit;
			end loop;   

			cRet := W_GANG_Values( nSid, nObjlistId2, nTis1, nTis2, dVon );             
			

			if cRet = 'OK' then
				-- Headerzeile zur�ckgeben
				-- select SIDSID, FK_OBJLIST, S1Z1, S1Z2, S1Z3, S2Z1, S2Z2, S2Z3, S3Z1, S3Z2, S3Z3,...
				
				for r in (select * from PTABHEAD where SID = nSid and FK_OBJLIST = nObjlistId1) loop
					pipe row( r );
				end loop;
				--Delete_Sessiondata( nSid, 'PTABHEAD' );
				--*/
				--return 'Ok';		-- f�r Debug-Test
			end if;
		end if;
	end if;	
end;  

end; -- body ReportPckg
/
