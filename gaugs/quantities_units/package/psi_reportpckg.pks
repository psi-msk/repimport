Create or replace Package PSI_ReportPckg AUTHID DEFINER
is         
	-- LIH 30.07.2012 spec for universal reports integrated
	-- LIH 10.05.2012 average and sum handling added
	-- LIH 09.05.2012 optimization (rowtype verwenden etc)
	-- LIH 08.05.2012 creation of package for reports with up to 9 archiv data columns
/*	
	In zwei weiteren Elementen des Reports werden die Headerzeile(n) und die Tabellenzeilen durch Selektionen auf die Tabellen TABHEAD_TA
	und TABROWS_TA erzeugt.

	ArchivReport_Node( nObjId number, cVon in date, cBis in date, cTimelevel in varchar2, cSource in varchar2 default 'O' ) return OutHEAD pipelined
	- erh�lt die Objekt-ID des gew�hlten Baumobjekts, welches Kindobjekte enthalten muss
	-  description, quantity and unit will be added to the header table TABHEAD 
	- die Zeitreihen zum Timelevel der (max 9) Kindobjekte werden in die Tabelle TABVAL eingetragen
	 
	GenTisHeaderCols
	- erh�lt die gleiche zu erzeugende Session-ID 
	- sucht sich zu den Zeitreihen alle Spalteninfos f�r die Header-Zeilen und speichert sie in DB-Tabelle TABHEAD_TA
	 1. Headerzeile: description
	 2. Headerzeile: quantity and unit

	GenTisValues
	- erh�lt die gleiche zu erzeugende Session-ID sowie Von und Bis-Zeit f�r die Datenselektion
	- holt sich aus Tabelle REPORTGRP_TA und REPOBJ die Objekte zur Gruppe und baut dynamisch die Datenselektion auf, f�hrt sie aus und
	  schreibt das Ergebnis in Tabelle TABVALUES_TA

*/	

	TYPE OUTHEAD is table of TABHEAD%rowtype;
	
	function Data4Node( nObjId number, cVon in varchar2, cBis in varchar2, cTimelevel in varchar2, cSource in varchar2 default 'O' ) return varchar2;
	function ArchivReport_Node( nObjId number, cVon in varchar2, cBis in varchar2, cTimelevel in varchar2, cSource in varchar2 default 'O' ) return OutHEAD pipelined;
		              
	function GenTisHeaderCols( nSid in number )	return varchar2;	-- erzeugt Header-Zeile(n) f�r Zeitreihen-Reporttabelle zur Session-ID; R�ckgabe: OK oder ERROR
		
	function GenTisValues( nSid in number, cVon in varchar2, cBis in varchar2, cDataSrc in varchar2 )	return varchar2;
	-- erzeugt Zeilen in Zeitreihen-Reporttabelle zur Session-ID; R�ckgabe: OK oder ERROR

	function GetDateTime return varchar2;	-- gibt aktuelle Datum und Zeit in der Form dd.mm.yyyy hh24:mi zur�ck		

	function GetMin( cTimelevel in varchar2) return number;
end;
/

Create or replace Package body PSI_ReportPckg
is
	-- �nderungsinfos siehe Header

---------------------------------------------------------------------------------------------

	DATE_FORMAT_GER 				constant varchar2(21) := 'DD.MM.YYYY HH24:MI:SS';

---------------------------------------------------------------------------------------------
procedure ExecSqlDirect( cStmt IN varchar2 )
AS
-- LIH 07.05.2010 Eliminieren dbms_sql / Umstellen auf execute immediate
BEGIN
	execute immediate cStmt;
EXCEPTION
	WHEN OTHERS THEN
		raise;
END;

function ReturnWert( bError in boolean, cError in varchar2 )
return varchar2
as
begin
	if bError then
		return cError;
	else
		return 'OK';
	end if;
end;

function GetDateTime return varchar2
	-- gibt aktuelle Datum und Zeit in der Form dd.mm.yyyy hh24:mi zur�ck		
as
begin
		return to_char(sysdate, 'dd.mm.yyyy hh24:mi');
end;

function GetMin( cTimelevel in varchar2) return number
as                  
	nMin	number;
begin              
	if cTimelevel = 'S' then
		nMin := 1/60;
	elsif cTimelevel = 'MI3' then
		nMin := 3;
	elsif cTimelevel = 'MI5' then
		nMin := 5;
	elsif cTimelevel = 'MI15' then
		nMin := 15;
	elsif cTimelevel = 'H' then
		nMin := 60;
	elsif cTimelevel = 'H2' then
		nMin := 120;
	elsif cTimelevel = 'D' then
		nMin := 24*60;
	elsif cTimelevel = 'M' then
		nMin := 1440*30;
	elsif cTimelevel = 'Q' then
		nMin := 1440*90;
	elsif cTimelevel = 'Y' then
		nMin := 1440*365;
	else                
		nMin := 1;
	end if;
	return nMin;
end;
  
function GetPict( nTis in number ) return varchar2
as
	cPict REPOBJ.PICTURE%type := '99999999990';
	cDec	varchar2(20) := '99999999999999999999';
	cUnitId	UNITS.ID%type;
begin
	select UNIT into cUnitId from TIS where ID = nTis;
	-- Standard verwenden - Quantity->UNIT  (PK auf ID+VERS_DATE)
	for recUni in (select PRECISION from UNITS where ID = cUnitId and VERS_DATE=(select max(VERS_DATE) from UNITS where ID = cUnitId) ) loop
		if recUni.PRECISION > 0 then		
			cPict := cPict || 'D' ||  substr(cDec,1,recUni.PRECISION);
		end if;
	end loop;
	
	return cPict;
end;	

function GenTisHeaderCols( nSid in number )	return varchar2
	-- erzeugt Header-Zeile f�r Zeitreihen-Reporttabelle zur Session-ID; R�ckgabe: OK oder ERROR
as	
	bError	boolean 		:= FALSE;   
	cError	varchar2(1024) 	:= null;
	bFound	boolean 		:= FALSE;   
	cStmt	varchar2(1000);
	cCols	varchar2(400) := '';
	cSets	varchar2(400) := '';
begin
	-- sucht sich zu den Zeitreihen alle Spalteninfos f�r die Header-Zeilen und speichert sie in DB-Tabelle TABHEAD_TA
	-- 1. Headerzeile: Shortname od description
	-- 2. Headerzeile: quantity[unit]
                           
	-- die Objekte aus REPOBJ verarbeiten              
	for recGrp in ( select * from REPGRP where SID = nSid ) loop
		for recObj in (select POS,FK_TIS,SHORT_NAME,DESCRIPTION,TIS.QUANTITY,TIS.UNIT from REPOBJ, TIS where SID = recGrp.SID and FK_TIS=TIS.ID order by POS) loop
			-- �ber TIS die Daten laden
			-- in TABHEAD werden f�r 1-9 Zeitreihen einer Gruppe REPGRP_FK je zwei Headerspalten erzeugt: R1H1, R1H2 
			-- set R1h1=description, r1h2=quan[Unit] ...
			bFound := true;
			cCols := cCols || 'R'||recObj.POS || 'H1,R'|| recObj.POS || 'H2,';
			cSets := cSets || '''' || recObj.SHORT_NAME || ''',''' || recObj.QUANTITY || '[' || recObj.UNIT || ']'',';
		end loop;
		if bFound then
			begin
				cStmt := 'insert into TABHEAD (SID,' || substr(cCols,1, length(cCols)-1) || ', REPNAME' || 
						 ') values(' || recGrp.SID || ','  || substr(cSets,1, length(cSets)-1) || ','''|| recGrp.REPNAME || ''')';
			    ExecSqlDirect( cStmt );
			    commit;
			exception    
				when others then
					cError := sqlerrm;
					bError := true;
			end;	
		else
			cError := 'Gruppe '|| recGrp.REPNAME || ' hat keine zugeordneten Zeitreihenobjekte!';
			bError := true;
		end if;			
	end loop;

	return ReturnWert( bError, cError );
end;	

function GenTisValues( nSid in number, cVon in varchar2, cBis in varchar2, cDataSrc in varchar2 )	return varchar2
	-- erzeugt Zeilen in Zeitreihen-Reporttabelle zur Session-ID; R�ckgabe: OK oder ERROR
as	
	bError	boolean 		:= FALSE;   
	cError	varchar2(1024) 	:= null;
	bFound	boolean 		:= FALSE;   
	cStmt	varchar2(4000);
	cCols	varchar2(1000) := '';
	cSel	varchar2(1000) := '';
	cWhere	varchar2(3000) := '';
	cFrom	varchar2(1000) := '';
begin
	-- erh�lt die gleiche zu erzeugende Session-ID sowie Von und Bis-Zeit f�r die Datenselektion
	-- holt sich aus Tabelle REPORTGRP_TA und REPOBJ die Objekte zur Gruppe und baut dynamisch die Datenselektion aus cDataSrc (DD_ORIGINAL oder DD_SYSTEM)
	-- auf, f�hrt sie aus und schreibt das Ergebnis in Tabelle TABVALUES_TA
	execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
                           
	-- die Objekte aus REPOBJ verarbeiten              
	for recGrp in ( select * from REPGRP where SID = nSid ) loop
		delete from TABVAL where SID = recGrp.SID;
		for recObj in (select POS,FK_TIS,VALUE_CATEGORY from REPOBJ where SID = recGrp.SID order by POS) loop
			-- �ber TIS die Daten laden
			-- in <cDataSrc> stehen die Zeitreihenwerte  (select between cVon and cBis - liefert alle Werte >=cVon und <= cBis
			-- Aufbau eines outer-join in der Form:
			bFound := true;
			cCols := cCols || 'VAL'||recObj.POS || ',';
			cSel := cSel || 'a'||recObj.POS || '.VAL,';
			cFrom := cFrom || cDataSrc || ' a'||recObj.POS || ',';                             
			if recObj.POS=1 then
				cWhere := cWhere || 'a'||recObj.POS || '.FK_TIS=' || recObj.FK_TIS || ' and a'||recObj.POS || '.REF_DATE between ''' || cVon || ''' and ''' || cBis || '''';
			else
				cWhere := cWhere || ' and a'||recObj.POS || '.FK_TIS(+)=' || recObj.FK_TIS || ' and a1.REF_DATE=a'||recObj.POS || '.REF_DATE(+)' ||
					' and a'||recObj.POS || '.REF_DATE(+) between ''' || cVon || ''' and ''' || cBis || '''';
			end if;
		end loop;
		if bFound then
			begin
				cStmt := 'insert into TABVAL (SID,REFDATE,' || substr(cCols,1, length(cCols)-1) || 
						 ') (select ' || recGrp.SID || ',a1.REF_DATE,'  || substr(cSel,1, length(cSel)-1) || 
						 ' from ' || substr(cFrom,1, length(cFrom)-1) || 
						 ' where '|| cWhere || ')';
			    ExecSqlDirect( cStmt );
			    commit;
			    -- Erg�nze Mittelwerte oder Summenwerte
			    cSel := '';
				for recObj in (select POS,FK_TIS,VALUE_CATEGORY from REPOBJ where SID = recGrp.SID order by POS) loop
					if recObj.VALUE_CATEGORY = 'M' then		-- Messwert => average bilden
						cSel := cSel || '(select avg(VAL' || recObj.POS || ') from TABVAL where SID=' ||  recGrp.SID || '),';
					else
						cSel := cSel || '(select sum(VAL' || recObj.POS || ') from TABVAL where SID=' ||  recGrp.SID || '),';
					end if;
				end loop;
				cStmt := 'insert into TABVAL (SID,REFDATE,' || substr(cCols,1, length(cCols)-1) || ') values (' ||  recGrp.SID || ',sysdate+365,' 
						 || substr(cSel,1, length(cSel)-1) || ')';
			    ExecSqlDirect( cStmt );
			    commit;
			exception    
				when others then
					cError := sqlerrm;
					bError := true;
			end;	
		else
			cError := 'Gruppe '|| recGrp.REPNAME || ' hat keine zugeordneten Zeitreihenobjekte!';
			bError := true;
		end if;			
	end loop;

	return ReturnWert( bError, cError );
end;	

function ArchivReport_Node( nObjId number, cVon in varchar2, cBis in varchar2, cTimelevel in varchar2, cSource in varchar2 default 'O' ) return OutHEAD pipelined

	-- erzeugt Objektliste, Header-Info und Zeitreihen-Daten ausgehend von Parametern Objekt-ID, cVon und cBis
	-- holt die Werte aus cSource = 'O' - dd_original oder 'S' - dd_system 
as
    --tHEAD		OUTHEAD_TYPE;
	cError	varchar2(1024) 	:= null;
	bError	boolean 		:= FALSE;      
	dVon	date			:= to_date(cVon, DATE_FORMAT_GER);
	dBis	date			:= to_date(cBis, DATE_FORMAT_GER);
	nSid	number;
	iLfdnr	integer	:= 0; 
	cDataSrc	varchar2(12) := 'DD_ORIGINAL';
	cArchivtyp  TIS.ARCHIVETYPE%type := '4';
	cPict	REPOBJ.PICTURE%type;
	cRet	varchar2(10);
	pragma autonomous_transaction;  -- necessary because of insert or deletes while the function is used in a select                                                 
	
begin                                         
	if cSource = 'S' then
		cDataSrc := 'DD_SYSTEM';
		cArchivtyp := '1';
	end if;
	
	execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
	select userenv('sessionid') into nSid from dual;

	-- Reportgruppe und -objekte erzeugen

	delete from TABHEAD where SID=nSid;
	delete from TABVAL where SID=nSid;
	delete from REPORTOBJ_TA where SID=nSid;
	delete from REPORTGRP_TA where SID=nSid;
	
	for recObj in (select TREE_ID, DESCRIPTION, LABEL from MD_OBJECTS where OBJID = nObjId) loop
		insert into REPORTGRP_TA (SID, REPNAME) values( nSid, recObj.LABEL || ' : ' || recObj.DESCRIPTION );

		for recTis in (select TIS.ID, TIS.VALUE_CATEGORY, o.SHORT_NAME,o.DESCRIPTION from TIS, MD_OBJECTS o
			where TIS.FK_OBJ = o.OBJID and TIS.TIME_LEVEL = cTimelevel and TIS.ARCHIVETYPE=cArchivtyp 
			  and OBJID in (select OBJID from MD_OBJECTS where PARENT_TREE_ID = recObj.TREE_ID and rownum<10) )
		loop
			iLfdNr := iLfdNr + 1;
			cPict  := Getpict(recTis.ID);
			insert into REPORTOBJ_TA values (nSid, iLfdNr, recTis.ID, cPict, recTis.SHORT_NAME, recTis.DESCRIPTION, recTis.VALUE_CATEGORY );
		end loop;
		commit;
	end loop;   
	
	if iLfdNr > 0 then
		-- Tabellenheader erzeugen - 2 zeilig: 1. Zeile: Obj-Shortname, 2. Zeile: Quantity[unit]
		--delete from TABHEAD where SID = nSid; 
		cRet := GenTisHeaderCols( nSid );             
		        
		if cRet = 'OK' then
			-- Tabellenwerte erzeugen
			cRet := GenTisValues( nSid, cVon, cBis, cDataSrc ); 
			
			if cRet = 'OK' then
				-- Headerzeile zur�ckgeben
				-- select SID,R1H1,R1H2,R2H1,R2H2,R3H1,R3H2,R4H1,R4H2,R5H1,R5H2,R6H1,R6H2,R7H1,R7H2,R8H1,R8H2,R9H1,R9H2
				
				for r in (select * from TABHEAD where SID = nSid) loop
					pipe row( r );
				end loop;
				CFun.Delete_Sessiondata( nSid, 'TABHEAD' );
			end if;
		end if;
	end if;	
end;

function Data4Node( nObjId number, cVon in varchar2, cBis in varchar2, cTimelevel in varchar2, cSource in varchar2 default 'O' ) return varchar2

	-- erzeugt Objektliste, Header-Info und Zeitreihen-Daten ausgehend von Parametern Objekt-ID, cVon und cBis
	-- holt die Werte aus cSource = 'O' - dd_original oder 'S' - dd_system 
as
    --tHEAD		OUTHEAD_TYPE;
	cError	varchar2(1024) 	:= null;
	bError	boolean 		:= FALSE;      
	dVon	date			:= to_date(cVon, DATE_FORMAT_GER);
	dBis	date			:= to_date(cBis, DATE_FORMAT_GER);
	nSid	number;
	iLfdnr	integer	:= 0; 
	cDataSrc	varchar2(12) := 'DD_ORIGINAL';
	cArchivtyp  TIS.ARCHIVETYPE%type := '4';
	cPict	REPOBJ.PICTURE%type;
	cRet	varchar2(100) := 'ERROR - no data found';
	--pragma autonomous_transaction;  -- necessary because of insert or deletes while the function is used in a select                                                 
	
begin                                         
	if cSource = 'S' then
		cDataSrc := 'DD_SYSTEM';
		cArchivtyp := '1';
	end if;
	
	execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
	--execsqldirect( 'alter session set NLS_LANG=German_Germany.AL32UTF8');
	select userenv('sessionid') into nSid from dual;
	
	-- Reportgruppe und -objekte erzeugen   
	
	delete from TABHEAD where SID=nSid;
	delete from TABVAL where SID=nSid;
	delete from REPORTOBJ_TA where SID=nSid;
	delete from REPORTGRP_TA where SID=nSid;
	
	for recObj in (select TREE_ID, DESCRIPTION, LABEL from MD_OBJECTS where OBJID = nObjId) loop
		insert into REPORTGRP_TA (SID, REPNAME) values( nSid, recObj.LABEL || ' : ' || recObj.DESCRIPTION );

		for recTis in (select TIS.ID, TIS.VALUE_CATEGORY, o.SHORT_NAME,o.DESCRIPTION from TIS, MD_OBJECTS o
			where TIS.FK_OBJ = o.OBJID and TIS.TIME_LEVEL = cTimelevel and TIS.ARCHIVETYPE=cArchivtyp 
			  and OBJID in (select OBJID from MD_OBJECTS where PARENT_TREE_ID = recObj.TREE_ID and rownum<10) )
		loop
			iLfdNr := iLfdNr + 1;
			cPict  := Getpict(recTis.ID);
			insert into REPORTOBJ_TA values (nSid, iLfdNr, recTis.ID, cPict, recTis.SHORT_NAME, recTis.DESCRIPTION, recTis.VALUE_CATEGORY );
		end loop;
		commit;
	end loop;   
        
	if iLfdNr > 0 then
		-- Tabellenheader erzeugen - 2 zeilig: 1. Zeile: Obj-Shortname, 2. Zeile: Quantity[unit]
		cRet := GenTisHeaderCols( nSid );             
		        
		if cRet = 'OK' then
			-- Tabellenwerte erzeugen
			cRet := GenTisValues( nSid, cVon, cBis, cDataSrc ); 
			
		end if;
	end if;	
	return cRet;
end;


/*
Aufgabenstellung

Vorgegeben sind je Report x
- eine Liste mit Objekten (Archiv-Ids)
- das Datum (Tag), f�r das die Daten der Objekte auszugeben sind
- die Position (x,y) auf dem Ausgabeblatt, an der die Daten eines Objekts auszugeben sind

Die Reports k�nnen parallel von diversen Anwendern angefordert werden.
Jeder angeforderte Report muss sessionbezogen autark abgearbeitet werden.

Tabelle REP_PAR mit Vorgabedaten:
- Reportkennung/-nr
- Objekt
- Archiv-Zeitstufe (timelevel) oder -ID
- Von - relativer Zeitbezug (zum Vorgabedatum)
- Bis - relativer Zeitbezug (zum Vorgabedatum)
- Start_X
- Start_y
- Anzahl Zeichen vor dem Komma - anz
- Dezimalstellen dec

L�sung 1
Es wird ein Standartreportformular verwendet, in dem vordefiniert sind
- Logo
- Kopf (Reportbezeichnung, Datum)
- Fusszeile (Seite/Gesamt-Seitenzahl)
- das Format (hoch, quer)
- ein Tabellen-Kopfbereich (die Daten f�r die Spalten werden �ber Tabelle TABHEAD vorgegeben)
- ein Tabellen-Datenbereich (die Daten f�r die Spalten werden �ber Tabelle TABDATA vorgegeben)

Die kompletten Zeilen f�r Head und Data werden �ber die jeweilige zum Report entwickelte Funktion aufgebaut und mit Sessionnummer, Report- und Userkennung in TABHEAD und TABDATA gespeichert.

Achtung: die Ausgabe im Tabellenbereich muss mit einer Schriftart mit konstanter Zeichenbreite erfolgen

Ablauf
-- Zwischenspeicherung der Daten in array aTx aus 1000 Elementen a 256 Zeichen (mit Leerzeichen bef�llt)

-- Header erzeugen
-- (f�r Tabelle mit n Objekten / Start_x gibt horiz. Pos vor
while rec in (select * from objlist where Reportnr=x)
    -- suche/lade obj.parameter
    while par in (select * from REP_PAR where reportnr=x and Object=rec.object)
        nX    = par.start_x
    end
    cObjData := <Kennung> || <unit>
    cHead := substr(cHead,1,nX-1) || cObjData
end loop
insert into TABHEAD (cSession, cRepnr, cHead)    

-- Tabellendaten erzeugen
-- Summen/Mittelwerte werden in array aGes aufgebaut
iCol := 0
while rec in (select * from objlist where Reportnr=x)
    iCol := iCol + 1
    -- suche/lade obj.parameter
    while par in (select * from REP_PAR where reportnr=x and Object=rec.object)
        dVon = par.von
        dBis = par.bis
        nX    = par.start_x
        ny  = par.start_y
        cpic = substr('999999999999',1,par.anz-1) || '0D' || substr('99999999',1,par.dez)
    end
    -- Daten lesen
    i := 0
    aAnz(iCol) := 0
    for arc in (select * from archiv_01h where ID=rec.arc_id and time >= dVon and time <= dBis) loop
        i := i + 1
        if i = 1 then
            -- Datum/Zeit eintragen
            aTx(i) := to_char(arc.time,'tt.mm hh24')
            -- ggf MESZ ber�cksichtigen
        end if
        aTx(i) := substr(aTx(i),1,nX-1) || to_char(arc.value,cpic)
        -- abh. von Objekttyp - Mittelwert oder Summe bilden
        if objlist.typ=ZAEHLER then
            aGes(iCol) := aGes(iCol) + arc.value
        else
            if arc.value is not null then
                aGes(iCol) := aGes(iCol) + arc.value
                aAnz(iCol) := aAnz(iCol) + 1
            end if
        end if
    end loop
    -- Summen-/Mittelwertzeile
    if aAnz(iCol) = 0 then aAnz(iCol) := 1; end if;
    aTx(i+1) := substr(aTx(i+1),1,nX-1) || to_char(aGes(iCol)/aAnz(iCol),cpic)
end loop
-- Tab.zeilen in TABDATA eintragen
for j = 1 .. i+1 loop
    insert into TABDATA  (cSession, cRepnr, rtrim(aTx(j)))    
end loop
*/

end; -- body ReportPckg

		