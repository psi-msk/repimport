CREATE OR REPLACE package CustomerReportPckg AUTHID DEFINER is
--------------------------------------------------------------------------------
-- package for user report procedures and functions
--
-- Change history
-- LIH 10.05.2012 procedure Delete_Sessiondata declared explicit
-- LIH 27.04.2012 package creation with procedures ExecSqlDirect,InsertGPA,LogError,GetNewAtext
--                and functions CreateGPA and PipeGPA

--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Constants
--------------------------------------------------------------------------------

TYPE OUTGPA_TYPE is record(
LABEL	VARCHAR2(1000),
RBT		NUMBER,
RMN		NUMBER,
RZR		NUMBER);

TYPE OUTGPA is table of OUTGPA_TYPE;

--------------------------------------------------------------------------------
-- Prozedures and functions
--------------------------------------------------------------------------------
procedure ExecSqlDirect( cStmt IN varchar2 );

-- create GPA table entries
function CreateGPA ( nObjId number default 0 ) return number;                  

-- delete session data
procedure Delete_Sessiondata ( nSid in number, cTable in varchar2 );

-- pipe the GPA data records to the caller
function PipeGPA ( nObjId in number default null  ) return OutGPA pipelined;

-- insert GPA table entries
procedure InsertGPA (nSession in number, cLabel in varchar2, cShort in varchar2, nVal in number); 

-- store Error message in PSIERRORS
procedure LogError( cFunc in varchar2, cErrText in varchar2 );

-- store new records of ATEXT (in main schema) into PSIATEXT
procedure GetNewAtext;

end;
/


CREATE OR REPLACE package body CustomerReportPckg is
--------------------------------------------------------------------------------
-- Global variables
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Globale constants
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- prozedures and functions
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- store Error message in PSIERRORS
--------------------------------------------------------------------------------
procedure LogError( cFunc in varchar2, cErrText in varchar2 )
is
begin
  insert into PSIERRORS values (User, sysdate, cFunc, cErrText);
exception
		when others then
			raise;
end;
 

--------------------------------------------------------------------------------
-- insert GPA table entries
--------------------------------------------------------------------------------
procedure InsertGPA (nSession in number, cLabel in varchar2, cShort in varchar2, nVal in number)
is
	bfound		boolean := false;
	cPrefix		varchar2(1000) := substr(cLabel,1,INSTRC(cLabel, '__GPA', 1, 1) -5);
--    pragma autonomous_transaction;                                                  
begin
	for rec in (select * from GPA_TA where SID = nSession and LABEL = cPrefix) loop
		if cShort = 'QRBT__GPA' then
			update GPA_TA set RBT = nVal where SID = nSession and LABEL = cPrefix;
		elsif  cShort = 'QRMN__GPA' then
			update GPA_TA set RMN = nVal where SID = nSession and LABEL = cPrefix;
		else
			-- cShort = 'QRZR__GPA' then
			update GPA_TA set RZR = nVal where SID = nSession and LABEL = cPrefix;
		end if;
		bfound := true;
	end loop;					
	if not bfound then
		if cShort = 'QRBT__GPA' then
			insert into GPA_TA (SID,LABEL,RBT,RMN,RZR) values (nSession, cPrefix, nVal, 0, 0);
		elsif  cShort = 'QRMN__GPA' then
			insert into GPA_TA (SID,LABEL,RBT,RMN,RZR) values (nSession, cPrefix, 0, nVal, 0);
		else
			-- cShort = 'QRZR__GPA' then
			insert into GPA_TA (SID,LABEL,RBT,RMN,RZR) values (nSession, cPrefix, 0, 0, nVal);
		end if;
	end if;
end;	

--------------------------------------------------------------------------------
-- create GPA table entries
--------------------------------------------------------------------------------
--function CreateGPA ( cNetz in varchar2 default null )
function CreateGPA ( nObjId number default 0 )
return number
is
	nSession	number;
	cLabel		varchar2(1000) := '';
	cError		varchar2(1000) := 'CreateGPA: ';
  nId       number := nObjId;
  
  pragma autonomous_transaction;  -- necessary because of insert or deletes while the function is used in a select                                                 
begin
  -- check wether root element was selected
  for recObj in (select OBJECTTYPE from MD_OBJECTS where OBJID = nObjId) loop
    if recObj.OBJECTTYPE = 'FL' then
      nId := 0;
    end if;
  end loop;
	if nId <> 0 then
		select LABEL into cLabel from MD_OBJECTS where OBJID = nId;
	end if;	
	select userenv('sessionid') into nSession from dual;
	
	begin
		delete from GPA_TA where SID = nSession; 

		-- select all GPA objects	
		for rec in (select LABEL, VAL, SHORT_NAME from DD_ORIGINAL d where SHORT_NAME in ('QRBT__GPA','QRMN__GPA','QRZR__GPA') 
					 and REF_DATE = (select max(ref_date) from DD_ORIGINAL where LABEL=d.LABEL) 
					 and LABEL like '%'|| cLabel ||'%' order by LABEL) loop
			InsertGPA( nSession, rec.LABEL, rec.SHORT_NAME, rec.VAL );
		end loop;
    
		commit;
 	exception
		when others then
			cError := cError || sqlerrm;
			rollback;                   
			-- Log the error in table PSIErrors
			LogError('CreateGPA', cError);
			raise;
	end;
	return nSession;
end;
--------------------------------------------------------------------------------
-- delete GPA table entries
--------------------------------------------------------------------------------
procedure Delete_Sessiondata ( nSid in number, cTable in varchar2 )
is
    nSession	number;
    cStmt     varchar2(200);
    pragma autonomous_transaction;                                                  
begin
    cStmt := 'delete from ' || cTable || ' where SID = ' || to_char(nSid);
    ExecSqlDirect( cStmt );
    commit;
end;

--------------------------------------------------------------------------------
-- pipe GPA table entries 
--------------------------------------------------------------------------------
function PipeGPA ( nObjId in number default null  ) return OutGPA pipelined
is
    nSession	number := 100;
    tGPA		OUTGPA_TYPE;
	
begin
	nSession := CreateGPA( nObjId );
	
	for r in (select LABEL,RBT,RMN,RZR from GPA_TA where SID = nSession) loop
		tGPA.LABEL := r.LABEL;
		tGPA.RBT := r.RBT;
		tGPA.RMN := r.RMN;
		tGPA.RZR := r.RZR;
		pipe row( tGPA );
	end loop;
	Delete_Sessiondata( nSession, 'GPA_TA' );	
end;
 
--------------------------------------------------------------------------------
-- store new records of ATEXT (in main schema) into PSIATEXT
--------------------------------------------------------------------------------
procedure GetNewAtext
is
  n number;
begin
  for rec in (select DESCRIPTION from por2.MTR_ATTRIBTEXT_TA where DESCRIPTION not in (
      select DESCRIPTION from PSIATEXT) ) loop
      insert into PSIATEXT (DESCRIPTION) values (rec.DESCRIPTION);
  end loop;
  commit;
end;


--------------------------------------------------------------------------------
-- execute Sql statement
--------------------------------------------------------------------------------
procedure ExecSqlDirect( cStmt IN varchar2 )
AS

BEGIN
	execute immediate cStmt;
EXCEPTION
	WHEN OTHERS THEN
		raise;
END;


end CustomerReportPckg;
/
