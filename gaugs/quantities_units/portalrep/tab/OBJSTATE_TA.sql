--  ******************************************************************
--  Copyright (C) PSI AG, 2013
--  PSI AG, 2013
-- 
--  Data model PSIportal user portalrep
--
--  $Author: emslih $
--  $Date: 2013/05/27 15:11:00 $
--  $Revision: 1.0 $
--  $State: Exp $
--
--  Date        Author       Comments
--  ----------  -----------  -----------------------------------------
--  27.05.2013  H. Liebhold  Creation
--  ******************************************************************

prompt Entity OBJSTATE_TA

WHENEVER SQLERROR CONTINUE
drop table OBJSTATE_TA cascade constraints;
WHENEVER SQLERROR exit SQL.SQLCODE

/*==============================================================*/
/* Table: OBJSTATE_TA                                           */
/*==============================================================*/
create table OBJSTATE_TA  (
   AKTUELL              NUMBER,
   LASTUPDATE           DATE
)
  tablespace &1;

comment on table OBJSTATE_TA is
'zeigt an, ob der Inhalt von Tabelle POR_OBJECTS_TA aktuell ist
Das Attribut AKTUELL 
- wird �ber einen insert,update,delete-Trigger auf POR.MTR_OBJECTS_TA auf 0 (=nicht aktuell) gesetzt
- �ber die Funktion ChkObjects() auf 1 gesetzt, nachdem die Tabelle POR_OBJECTS_TA zuvor von ihr aktualisiert wurde. ';
