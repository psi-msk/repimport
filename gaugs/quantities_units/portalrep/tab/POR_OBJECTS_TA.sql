--  ******************************************************************
--  Copyright (C) PSI AG, 2013
--  PSI AG, 2013
-- 
--  Data model PSIportal user portalrep
--
--  $Author: emslih $
--  $Date: 2013/05/27 15:11:00 $
--  $Revision: 1.0 $
--  $State: Exp $
--
--  Date        Author       Comments
--  ----------  -----------  -----------------------------------------
--  27.05.2013  H. Liebhold  Creation
--  ******************************************************************

prompt Entity POR_OBJECTS_TA

WHENEVER SQLERROR CONTINUE
drop table POR_OBJECTS_TA cascade constraints;
WHENEVER SQLERROR exit SQL.SQLCODE

/*==============================================================*/
/* Table: POR_OBJECTS_TA                                        */
/*==============================================================*/
create table POR_OBJECTS_TA  (
   OBJID                NUMBER(19)                      not null,
   SHORT_NAME           VARCHAR2(40 char)               not null,
   LABEL                VARCHAR2(300 char)              not null,
   DESCRIPTION          VARCHAR2(200 char),
   OBJECTTYPE           VARCHAR2(10 char)               not null,
   OBJTYPELONG          VARCHAR2(4000),
   PARENTID             NUMBER,
   PARENTLABEL          VARCHAR2(4000),
   PARENT_TREE_ID       NUMBER(19),
   TREE_ID              NUMBER(19),
   ID_PATH              VARCHAR2(100 char)
)
  tablespace &1;

comment on table POR_OBJECTS_TA is
'Diese Tabelle enth�lt einen Abzug aus der Portal-DB f�r den zu Synonym MD_OBJECTS bereitgestellten View.
Neben dem primary key auf die OBJID verf�gt sie zus�tzlich �ber einen Index auf Label.';

comment on column POR_OBJECTS_TA.OBJID is
'Eindeutige ID �ber alle Objekte';

comment on column POR_OBJECTS_TA.SHORT_NAME is
'Kurzname des Objektes. Eindeutig ueber letzte Version';

comment on column POR_OBJECTS_TA.DESCRIPTION is
'Bezeichnung des Objektes';

comment on column POR_OBJECTS_TA.OBJECTTYPE is
'Objekttyp';

alter table POR_OBJECTS_TA
   add constraint PK_POR_OBJECTS_TA primary key (OBJID)
        using index  tablespace &2;
/*==============================================================*/
/* Index: POROBJ_LABEL_IX                                       */
/*==============================================================*/
create index POROBJ_LABEL_IX on POR_OBJECTS_TA (
   LABEL ASC
);
