--  ******************************************************************
--  Copyright (C) PSI AG, 2014
--  PSI AG, 2014
-- 
--  Data model PSIportal user portalrep
--
--  $Author: emslih $
--  $Date: 2014/04/23 15:11:00 $
--  $Revision: 1.0 $
--  $State: Exp $
--
--  Date        Author       Comments
--  ----------  -----------  -----------------------------------------
--  23.04.2014  H. Liebhold  Creation
--  ******************************************************************

prompt Entity JSTAT

WHENEVER SQLERROR CONTINUE
alter table JSTAT
   drop primary key cascade;
drop table JSTAT cascade constraints;
WHENEVER SQLERROR exit SQL.SQLCODE

/*==============================================================*/
/* Table: JSTAT                                                 */
/*==============================================================*/
create table JSTAT  (
   SID                  NUMBER                          not null,
   ZEITPKT              DATE                            not null,
   S0C                  NUMBER,
   S1C                  NUMBER,
   S0U                  NUMBER,
   S1U                  NUMBER,
   EC                   NUMBER,
   EU                   NUMBER,
   OC                   NUMBER,
   OU                   NUMBER,
   PC                   NUMBER,
   PU                   NUMBER,
   YGC                  NUMBER,
   YGCT                 NUMBER,
   FGC                  NUMBER,
   FGCT                 NUMBER,
   GCT                  NUMBER,
   VM_R                 NUMBER,
   VM_B                 NUMBER,
   VM_SWPD              NUMBER,
   VM_FREE              NUMBER,
   VM_BUFF              NUMBER,
   VM_CACHE             NUMBER,
   VM_SI                NUMBER,
   VM_SO                NUMBER,
   VM_BI                NUMBER,
   VM_BO                NUMBER,
   VM_IN                NUMBER,
   VM_CS                NUMBER,
   VM_US                NUMBER,
   VM_SY                NUMBER,
   VM_ID                NUMBER,
   VM_WA                NUMBER,
   VM_ST                NUMBER
)
  tablespace &1;

comment on table JSTAT is
'Tabelle zur Aufnahme der mit JSTAT -gc und JSTAT -vmstat aufgenommen  Messwerte';

alter table JSTAT
   add constraint PK_JSTAT primary key (SID, ZEITPKT);
