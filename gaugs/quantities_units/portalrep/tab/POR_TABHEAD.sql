--  ******************************************************************
--  Copyright (C) PSI AG, 2013
--  PSI AG, 2013
-- 
--  Data model PSIportal user portalrep
--
--  $Author: emslih $
--  $Date: 2013/05/27 15:11:00 $
--  $Revision: 1.0 $
--  $State: Exp $
--
--  Date        Author       Comments
--  ----------  -----------  -----------------------------------------
--  27.05.2013  H. Liebhold  Creation
--  ******************************************************************

prompt Entity POR_TABHEAD

WHENEVER SQLERROR CONTINUE
drop table POR_TABHEAD cascade constraints;
WHENEVER SQLERROR exit SQL.SQLCODE

/*==============================================================*/
/* Table: POR_TABHEAD                                           */
/*==============================================================*/
create table POR_TABHEAD  (
   SID                  NUMBER(19)                      not null,
   FK_OBJLIST           NUMBER(19)                      not null,
   S1Z1                 VARCHAR2(80),
   S1Z2                 VARCHAR2(80),
   S1Z3                 VARCHAR2(80),
   S2Z1                 VARCHAR2(80),
   S2Z2                 VARCHAR2(80),
   S2Z3                 VARCHAR2(80),
   S3Z1                 VARCHAR2(80),
   S3Z2                 VARCHAR2(80),
   S3Z3                 VARCHAR2(80),
   S4Z1                 VARCHAR2(80),
   S4Z2                 VARCHAR2(80),
   S4Z3                 VARCHAR2(80),
   S5Z1                 VARCHAR2(80),
   S5Z2                 VARCHAR2(80),
   S5Z3                 VARCHAR2(80),
   S6Z1                 VARCHAR2(80),
   S6Z2                 VARCHAR2(80),
   S6Z3                 VARCHAR2(80),
   S7Z1                 VARCHAR2(80),
   S7Z2                 VARCHAR2(80),
   S7Z3                 VARCHAR2(80),
   S8Z1                 VARCHAR2(80),
   S8Z2                 VARCHAR2(80),
   S8Z3                 VARCHAR2(80),
   S9Z1                 VARCHAR2(80),
   S9Z2                 VARCHAR2(80),
   S9Z3                 VARCHAR2(80),
   S10Z1                VARCHAR2(80),
   S10Z2                VARCHAR2(80),
   S10Z3                VARCHAR2(80),
   S11Z1                VARCHAR2(80),
   S11Z2                VARCHAR2(80),
   S11Z3                VARCHAR2(80),
   S12Z1                VARCHAR2(80),
   S12Z2                VARCHAR2(80),
   S12Z3                VARCHAR2(80),
   S13Z1                VARCHAR2(80),
   S13Z2                VARCHAR2(80),
   S13Z3                VARCHAR2(80),
   S14Z1                VARCHAR2(80),
   S14Z2                VARCHAR2(80),
   S14Z3                VARCHAR2(80),
   S15Z1                VARCHAR2(80),
   S15Z2                VARCHAR2(80),
   S15Z3                VARCHAR2(80),
   S16Z1                VARCHAR2(80),
   S16Z2                VARCHAR2(80),
   S16Z3                VARCHAR2(80),
   S17Z1                VARCHAR2(80),
   S17Z2                VARCHAR2(80),
   S17Z3                VARCHAR2(80),
   S18Z1                VARCHAR2(80),
   S18Z2                VARCHAR2(80),
   S18Z3                VARCHAR2(80),
   S19Z1                VARCHAR2(80),
   S19Z2                VARCHAR2(80),
   S19Z3                VARCHAR2(80),
   S20Z1                VARCHAR2(80),
   S20Z2                VARCHAR2(80),
   S20Z3                VARCHAR2(80),
   S21Z1                VARCHAR2(80),
   S21Z2                VARCHAR2(80),
   S21Z3                VARCHAR2(80),
   REPNAME              VARCHAR2(200)
)
  tablespace &1;

comment on table POR_TABHEAD is
'Spalten-Kopfzeilen (bis zu 3) zu einer Tabelle';

comment on column POR_TABHEAD.S1Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S1Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S1Z3 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S2Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S2Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S2Z3 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S3Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S3Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S3Z3 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S4Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S4Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S4Z3 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S5Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S5Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S5Z3 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S6Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S6Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S6Z3 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S7Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S7Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S7Z3 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S8Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S8Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S8Z3 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S9Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S9Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S9Z3 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S10Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S10Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S10Z3 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S11Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S11Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S11Z3 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S12Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S12Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S12Z3 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S13Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S13Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S13Z3 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S14Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S14Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S14Z3 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S15Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S15Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S15Z3 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S16Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S16Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S16Z3 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S17Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S17Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S17Z3 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S18Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S18Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S18Z3 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S19Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S19Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S19Z3 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S20Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S20Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S20Z3 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S21Z1 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S21Z2 is
'Spalte n, m. Überschriftszeile';

comment on column POR_TABHEAD.S21Z3 is
'Spalte n, m. Überschriftszeile';

alter table POR_TABHEAD
   add constraint PK_POR_TABHEAD primary key (SID, FK_OBJLIST)
        using index  tablespace &2;
