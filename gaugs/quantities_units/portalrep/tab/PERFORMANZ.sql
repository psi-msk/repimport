--  ******************************************************************
--  Copyright (C) PSI AG, 2014
--  PSI AG, 2014
-- 
--  Data model PSIportal user portalrep
--
--  $Author: emslih $
--  $Date: 2014/04/23 15:11:00 $
--  $Revision: 1.0 $
--  $State: Exp $
--
--  Date        Author       Comments
--  ----------  -----------  -----------------------------------------
--  23.04.2014  H. Liebhold  Creation
--  ******************************************************************
prompt Entity PERFORMANZ

WHENEVER SQLERROR CONTINUE
alter table PERFORMANZ
   drop primary key cascade;
drop table PERFORMANZ cascade constraints;
WHENEVER SQLERROR exit SQL.SQLCODE

/*==============================================================*/
/* Table: PERFORMANZ                                            */
/*==============================================================*/
create table PERFORMANZ  (
   SID                  NUMBER                          not null,
   DATUM                DATE                            not null,
   TABELLE              VARCHAR2(32),
   ZUGRIFFSFORM         VARCHAR2(20),
   BEARB_SAETZE         NUMBER,
   DURCHLAEUFE          NUMBER,
   DAUER_IN_SEK         NUMBER,
   BEMERKUNG            VARCHAR2(100),
   LAST_ID              NUMBER
)
  tablespace &1;

comment on table PERFORMANZ is
'Tabelle zum Speichern von Performanzmessungen von DB-Zugriffen';

alter table PERFORMANZ
   add constraint PK_PERFORMANZ primary key (SID, DATUM)
   using index tablespace &2;
