--  ******************************************************************
--  Copyright (C) PSI AG, 2013
--  PSI AG, 2013
-- 
--  Data model PSIportal user portalrep
--
--  $Author: emslih $
--  $Date: 2014/04/11 15:11:00 $
--  $Revision: 1.0 $
--  $State: Exp $
--
--  Date        Author       Comments
--  ----------  -----------  -----------------------------------------
--  11.04.2014  H. Liebhold  Creation
--  ******************************************************************

prompt Entity POR_BAD_TIS

WHENEVER SQLERROR CONTINUE
drop index POR_BAD_TIS_IX;
drop table POR_BAD_TIS cascade constraints;
WHENEVER SQLERROR exit SQL.SQLCODE

/*==============================================================*/
/* Table: POR_BAD_TIS                                           */
/*==============================================================*/
create table POR_BAD_TIS  (
   SID                  NUMBER(19)                      not null,
   FK_OBJID             NUMBER(19)                      not null,
   FK_TIS_ORI           NUMBER,
   BESCHREIBUNG         VARCHAR2(100),
   FK_TIS_SYS           NUMBER,
   VON                  DATE,
   BIS                  DATE
)
  tablespace &1;

comment on table POR_BAD_TIS is
'Tabelle mit erkannten fehlerhaften oder obsoleten Zeitreihen';

/*==============================================================*/
/* Index: POR_BAD_TIS_IX                                        */
/*==============================================================*/
create index POR_BAD_TIS_IX on POR_BAD_TIS (
   SID ASC,
   FK_TIS_SYS ASC
)
  tablespace &2;
