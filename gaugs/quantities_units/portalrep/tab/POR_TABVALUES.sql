--  ******************************************************************
--  Copyright (C) PSI AG, 2013
--  PSI AG, 2013
-- 
--  Data model PSIportal user portalrep
--
--  $Author: emslih $
--  $Date: 2013/05/27 15:11:00 $
--  $Revision: 1.0 $
--  $State: Exp $
--
--  Date        Author       Comments
--  ----------  -----------  -----------------------------------------
--  27.05.2013  H. Liebhold  Creation
--  ******************************************************************

prompt Entity POR_TABVALUES

WHENEVER SQLERROR CONTINUE
drop table POR_TABVALUES cascade constraints;
WHENEVER SQLERROR exit SQL.SQLCODE

/*==============================================================*/
/* Table: POR_TABVALUES                                         */
/*==============================================================*/
create table POR_TABVALUES  (
   SID                  NUMBER(19)                      not null,
   FK_OBJLIST           NUMBER(19)                      not null,
   REFDATE              DATE                            not null,
   REFCDATE             VARCHAR2(20),
   VAL1                 NUMBER,
   STATUS1              VARCHAR2(20),
   VAL2                 NUMBER,
   STATUS2              VARCHAR2(20),
   VAL3                 NUMBER,
   STATUS3              VARCHAR2(20),
   VAL4                 NUMBER,
   STATUS4              VARCHAR2(20),
   VAL5                 NUMBER,
   STATUS5              VARCHAR2(20),
   VAL6                 NUMBER,
   STATUS6              VARCHAR2(20),
   VAL7                 NUMBER,
   STATUS7              VARCHAR2(20),
   VAL8                 NUMBER,
   STATUS8              VARCHAR2(20),
   VAL9                 NUMBER,
   STATUS9              VARCHAR2(20),
   VAL10                NUMBER,
   STATUS10             VARCHAR2(20),
   VAL11                NUMBER,
   STATUS11             VARCHAR2(20),
   VAL12                NUMBER,
   STATUS12             VARCHAR2(20),
   VAL13                NUMBER,
   STATUS13             VARCHAR2(20),
   VAL14                NUMBER,
   STATUS14             VARCHAR2(20),
   VAL15                NUMBER,
   STATUS15             VARCHAR2(20),
   VAL16                NUMBER,
   STATUS16             VARCHAR2(20),
   VAL17                NUMBER,
   STATUS17             VARCHAR2(20),
   VAL18                NUMBER,
   STATUS18             VARCHAR2(20),
   VAL19                NUMBER,
   STATUS19             VARCHAR2(20),
   VAL20                NUMBER,
   STATUS20             VARCHAR2(20),
   VAL21                NUMBER,
   STATUS21             VARCHAR2(20)
)
  tablespace &1;

comment on table POR_TABVALUES is
'Tabellen mit bis zu 21 Werten und Stati (f�r tab. Ausgaben)';

alter table POR_TABVALUES
   add constraint PK_POR_TABVALUES primary key (SID, FK_OBJLIST, REFDATE)
        using index  tablespace &2;

