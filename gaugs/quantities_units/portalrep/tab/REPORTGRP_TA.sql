--  ******************************************************************
--  Copyright (C) PSI AG, 2013
--  PSI AG, 2013
-- 
--  Data model PSIportal user portalrep
--
--  $Author: emslih $
--  $Date: 2013/05/27 15:11:00 $
--  $Revision: 1.0 $
--  $State: Exp $
--
--  Date        Author       Comments
--  ----------  -----------  -----------------------------------------
--  27.05.2013  H. Liebhold  Creation
--  17.10.2013  H. Liebhold  Erweiterung um erstellt_am
--  ******************************************************************

prompt Entity REPORTGRP_TA

WHENEVER SQLERROR CONTINUE
drop table REPORTGRP_TA cascade constraints;
WHENEVER SQLERROR exit SQL.SQLCODE

/*==============================================================*/
/* Table: REPORTGRP_TA                                          */
/*==============================================================*/
create table REPORTGRP_TA  (
   SID                  NUMBER(19)                      not null,
   REPNAME              VARCHAR2(200),
   ERSTELLT_AM          DATE
)
  tablespace &1;

comment on table REPORTGRP_TA is
'Report-Informationstabelle
- enth�lt je generiertem Report einen Eintrag';

alter table REPORTGRP_TA
   add constraint PK_REPORTGRP_TA primary key (SID)
        using index  tablespace &2;
