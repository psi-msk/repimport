--  ******************************************************************
--  Copyright (C) PSI AG, 2013
--  PSI AG, 2013
-- 
--  Data model PSIportal user portalrep
--
--  $Author: emslih $
--  $Date: 2013/05/27 15:11:00 $
--  $Revision: 1.0 $
--  $State: Exp $
--
--  Date        Author       Comments
--  ----------  -----------  -----------------------------------------
--  27.05.2013  H. Liebhold  Creation
--  ******************************************************************

prompt Entity REPORTOBJ_TA

WHENEVER SQLERROR CONTINUE
drop table REPORTOBJ_TA cascade constraints;
WHENEVER SQLERROR exit SQL.SQLCODE

/*==============================================================*/
/* Table: REPORTOBJ_TA                                          */
/*==============================================================*/
create table REPORTOBJ_TA  (
   SID                  NUMBER(19)                      not null,
   FK_OBJLIST           NUMBER(19)                      not null,
   POS                  NUMBER                          not null,
   NR                   VARCHAR2(10)                    not null,
   FK_TIS               NUMBER(10)                      not null,
   PICTURE              VARCHAR2(20),
   SHORT_NAME           VARCHAR2(80),
   DESCRIPTION          VARCHAR2(80),
   VALUE_CATEGORY       VARCHAR2(1),
   OBJLABEL             VARCHAR2(80),
   UNIT                 VARCHAR2(32),
   QUANTITY             VARCHAR2(32),
   FK_OBJID             NUMBER(19)
)
  tablespace &1;

comment on table REPORTOBJ_TA is
'Liste der Reportobjekte f�r aktuellen Report';

alter table REPORTOBJ_TA
   add constraint PK_REPORTOBJ_TA primary key (SID, FK_OBJLIST, POS)
        using index  tablespace &2;
