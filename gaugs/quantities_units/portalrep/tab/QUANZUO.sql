--  ******************************************************************
--  Copyright (C) PSI AG, 2015
--  PSI AG, 2015
-- 
--  Data model PSIportal user portalrep
--
--  $Author: emslih $
--  $Date: 19.08.2015
--  $Revision: 1.0 $
--  $State: Exp $
--
--  Date        Author       Comments
--  ----------  -----------  -----------------------------------------
--  19.08.2015  H. Liebhold  Creation
--  ******************************************************************

prompt Entity QUANZUO

WHENEVER SQLERROR CONTINUE
drop table QUANZUO cascade constraints;
WHENEVER SQLERROR exit SQL.SQLCODE

/*==============================================================*/
/* Table: QUANZUO                                          */
/*==============================================================*/
create table QUANZUO  (
   KNZTEIL              VARCHAR(80)                     not null,
   EHT                  VARCHAR(40)                     not null,
   QUANT                VARCHAR(80),
   STDEHT               VARCHAR(40),
   SHORT_NAME           VARCHAR(80),
   DESCRIPTION          VARCHAR(80),
   FK_QUAN              VARCHAR(80),
   QUANT_TYP            VARCHAR(4),
   constraint PK_QUANZUO primary key (KNZTEIL, EHT)
)
tablespace &1;

comment on table QUANZUO is
'Liste zur Organisation von Grössenzuordnungen';

