CREATE OR REPLACE
--  ******************************************************************
--  Copyright (C) PSI AG, 2013
--  PSI AG, 2013
-- 
--  Data model PSIportal user portalrep
--
--  $Author: emslih $
--  $Date: 2013/05/27 15:11:00 $
--  $Revision: 1.0 $
--  $State: Exp $
--
--  Date        Author       Comments
--  ----------  -----------  -----------------------------------------
--  17.07.2014  H. Liebhold  Creation
--  ******************************************************************

prompt Entity HIT2

WHENEVER SQLERROR CONTINUE
drop table HIT2 cascade constraints;
WHENEVER SQLERROR exit SQL.SQLCODE

/*==============================================================*/
/* Table: HIT2                                           */
/*==============================================================*/
create table HIT2  (
	LFDNR		NUMBER(22,12),
	LINE		VARCHAR2(4000)
)
  tablespace &1;
