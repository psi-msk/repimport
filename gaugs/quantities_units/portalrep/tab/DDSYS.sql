--  ******************************************************************
--  Copyright (C) PSI AG, 2013
--  PSI AG, 2013
--
--  Data model PSIportal user portalrep
--
--  $Author: emslih $
--  $Date: 2013/05/27 15:11:00 $
--  $Revision: 1.0 $
--  $State: Exp $
--
--  Date        Author       Comments
--  ----------  -----------  -----------------------------------------
--  17.07.2014  H. Liebhold  Creation
--  ******************************************************************

prompt Entity DDSYS

WHENEVER SQLERROR CONTINUE
drop table DDSYS cascade constraints;
WHENEVER SQLERROR exit SQL.SQLCODE

/*==============================================================*/
/* Table: DDSYS                                           */
/*==============================================================*/
CREATE TABLE DDSYS (
	FK_TIS     NUMBER(19) NOT NULL,
	REF_DATE   DATE NOT NULL,
	VAL        NUMBER NOT NULL,
	CONDITION  CHAR(1) NOT NULL,
	APPROVAL   CHAR(1) NOT NULL,
	VALIDITY   CHAR(1))
  tablespace &1;

ALTER TABLE DDSYS ADD (
	CONSTRAINT DDSYS_PK PRIMARY KEY (FK_TIS, REF_DATE));

