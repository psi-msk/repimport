--  ******************************************************************
--  Copyright (C) PSI AG, 2013
--  PSI AG, 2013
-- 
--  Data model PORTALREP
--
--  $Author: emshp $
--  $Date: 2010/10/07 07:46:08 $
--  $Revision: 1.1.1.1 $
--  $State: Exp $
--
--  ******************************************************************

spool cre-schema-PORTALREP.log

--
-- Defines
--
@./defines.sql

whenever sqlerror exit
declare
   v_text varchar2(100);
begin
   select 'Schema ok' 
     into v_text
     from dba_users 
    where upper(username) = upper('&poruser.');
   dbms_output.put_line(v_text);
exception when others then 
   dbms_output.put_line('!');
   dbms_output.put_line('!!!!!!!!!!!!!');
   dbms_output.put_line('!');
   dbms_output.put_line('!  Defined PORTAL-User: &poruser. does not exist!');
   dbms_output.put_line('!');
   dbms_output.put_line('!!!!!!!!!!!!!');
   dbms_output.put_line('!');
   raise no_data_found;
end;
/
whenever sqlerror continue

--
-- Drop User
--
WHENEVER SQLERROR CONTINUE
drop user &porrepuser. cascade;
WHENEVER SQLERROR exit SQL.SQLCODE

--
-- Create User
--
create user &porrepuser.
  identified by &porrepuser.
  default tablespace &ts_name.
  quota unlimited on &ts_name.
  quota unlimited on &ts_name_ind.
  profile default
  account unlock;
 
--
-- Grant - ROLE
--
GRANT CONNECT TO &porrepuser.;
ALTER USER &porrepuser. DEFAULT ROLE ALL;

--
-- Grant - SYSPRIVS
--
grant CREATE CLUSTER    to &porrepuser.;
grant CREATE INDEXTYPE  to &porrepuser.;
grant CREATE OPERATOR   to &porrepuser.;
grant CREATE PROCEDURE  to &porrepuser.;
grant CREATE SEQUENCE   to &porrepuser.;
grant CREATE SYNONYM    to &porrepuser.;
grant CREATE TABLE 	    to &porrepuser.;
grant CREATE TRIGGER    to &porrepuser.;
grant CREATE TYPE       to &porrepuser.;
grant CREATE VIEW       to &porrepuser.;

grant debug any procedure   to &porrepuser.;
GRANT DEBUG CONNECT SESSION TO &porrepuser.;


--
-- Grant - OBJPRIVS
--
begin
   for rec in (select object_name from dba_objects where object_type in ('TABLE','VIEW') and owner = '&poruser.')
   loop
      execute immediate 'grant select on &poruser..'||rec.object_name||' to &porrepuser.';
      dbms_output.put_line('grant select on &poruser..'||rec.object_name||' to &porrepuser.');
      
   end loop;
   
end;
/

GRANT EXECUTE ON &poruser..PSIPCKG TO &porrepuser.;

--GRANT SELECT ON SYS.V_$SESSION TO &porrepuser.;

--grant execute on sys.dbms_random to &porrepuser.;

--Grants: Requirements patch script management 
--grant select on SYS.V_$DATABASE to &porrepuser.;
--grant select on SYS.V_$INSTANCE to &porrepuser.;

spool off
