procedure GenKdQuan   authid definer
as
	-- Erzeugen der neuen Gr��en f�r TG
	cQuant	varchar2(40);  
	nQuanId	number := 0;                              
	cType   varchar2(2);   
	cEht	varchar2(40);
	iC		integer;
	cKz		varchar2(40);
	bFound	boolean;
	cId		varchar2(100);
	
begin
	select GEN_VALUE into nQuanId     from por.MTR_SEQUENCE_TA where GEN_KEY='Quantity_Units_Id';
	for rec in (select * from QUANZUO order by FK_QUAN) loop   
		cQuant := rec.QUANT;
		cKz := rec.KNZTEIL;
		cEht := rec.EHT;	                  
		if cQuant is not null then
			select count(*) into iC from   QUANT  where FK_QUN = rec.FK_QUAN;
			if iC = 0 then
				nQuanId := rec.FK_QUAN;
				if rec.FK_QUAN > 0 then
					cId := 'QU_' || to_char(rec.FK_QUAN);
				else
					cId := rec.QUANT;
				end if;
				cType := 'Z';
				if rec.EHT = '%' or rec.EHT = '�C' or rec.EHT = 'Wh/m�'then
					cType := 'M';
				end if;
	 			insert into por.MTR_QUANTITIES_TA (ID, VERS_DATE, SHORT_NAME, DESCRIPTION, TYPE, DIRECTION, FK_QUN, DEFAULT_UNIT)
	 			 values ( cId ,  TO_TIMESTAMP('01/01/2000 06:00:00.000000','DD/MM/YYYY HH24:MI:SS.FF'), rec.SHORT_NAME, rec.DESCRIPTION,
	 			  	cType,null, nQuanId, rec.STDEHT);
	 			  	
	            
				insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(nQuanId, rec.EHT); 
				if rec.EHT <> rec.STDEHT then
					Insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(nQuanId, rec.STDEHT);
				end if;
				commit;
				if rec.EHT = 'Wh' then
					-- pr�fen, ob MWh schon zugeordnet - wenn nicht, erzeugen
					bFound := false;
					for recQU in (select  * from por.MTR_QUANTITY_UNITS_TA where ID = rec.FK_QUAN and FK_UNITS='MWh') loop
						bFound := true;
					end loop;
					if not bFound then
						Insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(rec.FK_QUAN, 'MWh');
						commit;
					end if;
				end if;
			end if;    
		end if;
	end loop;  
    update por.MTR_SEQUENCE_TA set GEN_VALUE=(select max(FK_QUN) from QUANT) where GEN_KEY='Quantity_Units_Id';
    commit;

end;
