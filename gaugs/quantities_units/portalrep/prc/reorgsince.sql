CREATE OR REPLACE
procedure ReorgSince( nVonTisId in number default 0, nBisTisId in number default 0, cTabSyn in varchar2, iDays in integer, iCont in integer default 1)   AUTHID CURRENT_USER
as
	-- LIH 27.05.2014 Aufsetzen der Reorganisation auf ID > h�chster ID in ARC1_TA; Min-Zeitreihen nicht reorganisieren 
	-- LIH 13.05.2014 Erstellung         
	-- Lesen der Daten aus der per Synonym vorgegebenen ARC-Tabelle f�r die Zeitreihen nVon bis nBis..Id bzw. bei Vorgabe
	-- nVonTisId = 0: ab Zeitreihe mit kleinster ID
	-- nBisTisId = 0: bis Zeitreihe mit gr�sster ID
	-- cTabSyn = MTR_ARC1_RT_TA    oder MTR_ARC4_FORVAL_TA
	-- iDays - Anzahl Tage, ausgehend von akt. Zeitpunkt, die reorganisiert werden sollen
	-- iCont - 1=Reorganisation ab TIS-ID > h�chster ID in ARC1_TA, 0 = Beginn ab kleinster ID 
	
	nMinId	number := nVonTisId;
	nMaxId 	number := nBisTisId;  
	cSt		varchar2(1000);
	cArcTyp	varchar2(1);

	dStart		date := sysdate;
	nNr			number(12) :=0;
	nSek		number(20,2) := 0;
	iC			integer; 
	iZr			integer := 0;              
	dSince		date := sysdate - iDays;
	cSince		varchar2(20) := to_char(dSince,'dd.mm.yy hh24:mi:ss');
	dMax		date;
	nLastID		number := 0;

begin 

	PSISWH.SaveHit;        
	PSISWH.InsHit2( nNr,  ' ' );
	PSISWH.InsHit2( nNr,  '-- Start Reorg. ab ' || to_char(dSince,'dd.mm. hh24:mi:ss') || ' von TIS-Id ' || to_char(nVonTisId) || ' bis ID ' || 
		to_char(nBisTisId) || ' f�r Tabelle ' || cTabSyn || ': ' ||  to_char(dStart,'dd.mm. hh24:mi:ss') );

	-- Kopieren der Daten in Zusatztabelle
    /*begin
		execute immediate 'DROP TABLE ARC1_TA';
	exception
	 when others then
		null;
	end;
	execute immediate '	
		CREATE TABLE ARC1_TA (FK_TIS, REF_DATE, VERS_DATE, VAL, CONDITION, APPROVAL, VALIDITY,
	 	CONSTRAINT ARC1_PK PRIMARY KEY(FK_TIS, REF_DATE, VERS_DATE) )
	 	--ORGANIZATION INDEX 
	 	TABLESPACE MTR_TS
	 	AS SELECT FK_TIS, REF_DATE, VERS_DATE, VAL, CONDITION, APPROVAL, VALIDITY
	  	FROM MTR_ARC1_RT_TA where rownum<1';
	*/  
	if iCont = 1 then
		select max(FK_TIS) into nLastID from ARC1_TA; 
		if nLastId is null then
			nlastId := 0;
		end if;
	end if;
	execute immediate 'truncate table ARC1_TA';
	
	-- Kopieren der Daten in die Zusatztabelle und L�schen in Quelltabelle
	if nVonTisId is null or nVonTisId = 0 then
		select min(ID) into nMinId from MTR_TIMESERIES_TA where TIME_LEVEL<>'MI' and ID > nLastID;
	end if;
	if nBisTisId is null or nBisTisId = 0 then
		select max(ID) into nMaxId from MTR_TIMESERIES_TA  where TIME_LEVEL<>'MI' ;
	end if;
	if cTabSyn = 'MTR_ARC1_RT_TA' then
		cArcTyp := 1;
	else
		cArcTyp := 4;
	end if;
	
	for recTis in (select ID from MTR_TIMESERIES_TA where ID >= nMinId and ID <= nMaxId 
		and ARCHIVETYPE=cArcTyp and UNTIL_DATE > dSince 
	order by ID) loop
		if cTabSyn = 'MTR_ARC1_RT_TA' then
			insert into ARC1_TA (select * from MTR_ARC1_RT_TA	where FK_TIS = 	recTis.ID and REF_DATE > dSince);
		else
			insert into ARC1_TA (select * from MTR_ARC4_FORVAL_TA	where FK_TIS = 	recTis.ID and REF_DATE > dSince);
		end if;		
		commit;
		-- Achtung: nur das L�schen, was auch in ARC1_TA gesichert ist
		select max(REF_DATE) into dMax from ARC1_TA where FK_TIS = recTis.ID;
		iZr := iZr + 1;
		-- L�schen in MTR_ARC1_RT_TA 
		if cTabSyn = 'MTR_ARC1_RT_TA' then
			delete from MTR_ARC1_RT_TA where FK_TIS = recTis.ID and (REF_DATE > dSince and REF_DATE <= dMax);
			commit;
			insert into MTR_ARC1_RT_TA (select * from ARC1_TA where FK_TIS = recTis.ID );
		else
			delete from MTR_ARC4_FORVAL_TA where FK_TIS = recTis.ID and (REF_DATE > dSince and REF_DATE <= dMax);
			commit;
			insert into MTR_ARC4_FORVAL_TA (select * from ARC1_TA where FK_TIS = recTis.ID );
		end if;
		commit;
		if mod(iZr,100) = 0 then
			nSek := (sysdate - dStart)*24*60*60;
			PSISWH.InsHit2( nNr, '.. bisher reorganisierte Zeitreihen in ' || cTabSyn || ': ' || to_char(iZr) 
				|| ' - Laufzeit in Sek: ' || to_char(nSek,'99999.99') ); 
		end if;
	end loop;

	nSek := (sysdate - dStart)*24*60*60;
	select count(*) into iC from ARC1_TA;
	PSISWH.InsHit2( nNr, 'Reorganisierte Zeitreihen/S�tze in ' || cTabSyn || ': ' || to_char(iZr) || '/' || to_char(iC) || ' - Laufzeit in Sek: ' || to_char(nSek,'99999.99') ); 

end;

/
