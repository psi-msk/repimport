procedure Crea_QuanMapping authid definer
as
	-- LIH 17.10.2014 -- Die in quanzuo enthaltenen TG-spez. Gr��en werden auf die Gr��en von NGA gemappt
	-- Vorauss in por: grant insert,update on MTR_FS_QUANTITIES_TA to porrep;
	cLast1 varchar2(20);
	cQuant	varchar2(40);  
	cDesc	varchar2(80);  
	nQuan	number := 1000;
	cStdeht	varchar2(40);                     
	nFs_Id	number;  
	cFS		varchar2(40) := 'IMP_TP_XML';

begin
	delete from  por.MTR_FS_QUANTITIES_TA;                  
	for recFS in (select ID from por.MTR_FS_TA where SHORT_NAME = cFS) loop
		for rec in (select * from QUANZUO) loop   
			-- Die in quanzuo enthaltenen TG-spez. Gr��en werden auf die Gr��en von NGA gemappt
			for recQ in (select * from QUANT where SHORT_NAME = rec.QUANT) loop
				insert into por.MTR_FS_QUANTITIES_TA (FS_ID, FK_QUANT, ALIAS_NAME)
					values (recFS.ID, recQ.ID, rec.KNZTEIL || '.' || rec.EHT);
			end loop;
		end loop;
	end loop;
	commit;
end;
