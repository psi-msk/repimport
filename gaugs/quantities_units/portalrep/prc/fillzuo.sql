create or replace procedure fillzuo authid definer
as
	cLast1 varchar2(20);
	cQuant	varchar2(40);  
	cDesc	varchar2(80);  
	nQuan	number := 1000;
	cStdeht	varchar2(40);  

begin
	for rec in (select * from QUANZUO) loop   
		cQuant := 'NA';
		cDesc := '';
		cLast1 := '.' || last1(rec.KNZTEIL); 
		if rec.EHT='Wh' then
			cStdeht := 'kWh';
		elsif rec.EHT='%' then
			cStdeht := '%';
		else
			cStdeht := 'NA';
		end if;
		if cLast1 = '.BST' then
			cQuant := 'GK_BESTAND';
			cDesc := 'Gaskonto Bestand';
		elsif cLast1 = '.BSTPRZ' then
			cQuant := 'GK_BSTDPROZ';
			cDesc := 'Gaskonto Bestand Prozent';
		elsif cLast1 = '.BEW' then
			cQuant := 'GK_BEWEG';
			cDesc := 'Gaskonto Bewegung';
		elsif cLast1 = '.IST' then
			if rec.EHT='Wh' then
				cQuant := 'E-IST';
				cDesc := 'Energie IST';
			elsif rec.EHT='%' then
				cQuant := 'E-%';
				cDesc := 'Energie %';
			end if;
		elsif cLast1 = '.PLAN' then
			if rec.EHT='Wh' then
				cQuant := 'E-PLAN';
				cDesc := 'Energie PLAN';
			elsif rec.EHT='%' then
				cQuant := 'E-%';
				cDesc := 'Energie %';
			end if;
		elsif cLast1 = '.MNGGRS' then  
				cQuant := 'NA';
		end if;	
		if cQuant <> 'NA' then
			nQuan := nQuan + 1;
			update 	QUANZUO set QUANT = cQuant, STDEHT=cStdeht, SHORT_NAME=cQuant, DESCRIPTION=cDesc, FK_QUAN=nQuan
				where KNZTEIL = rec.KNZTEIL and EHT = rec.EHT;
		end if;
	end loop;
end;
/
