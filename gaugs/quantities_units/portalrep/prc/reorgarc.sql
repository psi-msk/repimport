CREATE OR REPLACE
procedure ReorgARC( nVonTisId in number default 0,    nBisTisId in number default 0, cTabSyn in varchar2 )   AUTHID CURRENT_USER
as          
	-- Lesen der Daten aus der per Synonym vorgegebenen ARC-Tabelle f�r die Zeitreihen nVon bis nBis..Id bzw. bei Vorgabe
	-- nVonTisId = 0: ab Zeitreihe mit kleinster ID
	-- nBisTisId = 0: bis Zeitreihe mit gr�sster ID
	
	nMinId	number := nVonTisId;
	nMaxId 	number := nBisTisId;  
	cSt		varchar2(1000);
	cArcTyp	varchar2(1);

	dStart		date := sysdate;
	nNr			number(12) :=0;
	nSek		number(20,2) := 0;
	iC			integer; 
	iZr			integer := 0;

begin 

	PSISWH.SaveHit;        
	PSISWH.InsHit2( nNr,  ' ' );
	PSISWH.InsHit2( nNr,  '-- Start Reorganisation von TIS-Id ' || to_char(nVonTisId) || ' bis ID ' || 
		to_char(nBisTisId) || ' f�r Tabelle ' || cTabSyn || ': ' ||  to_char(dStart,'dd.mm. hh24:mi:ss') );

	-- Kopieren der Daten in Zusatztabelle
    /*begin
		execute immediate 'DROP TABLE ARC1_TA';
	exception
	 when others then
		null;
	end;
	execute immediate '	
		CREATE TABLE ARC1_TA (FK_TIS, REF_DATE, VERS_DATE, VAL, CONDITION, APPROVAL, VALIDITY,
	 	CONSTRAINT ARC1_PK PRIMARY KEY(FK_TIS, REF_DATE, VERS_DATE) )
	 	ORGANIZATION INDEX TABLESPACE MTR_TS
	 	AS SELECT FK_TIS, REF_DATE, VERS_DATE, VAL, CONDITION, APPROVAL, VALIDITY
	  	FROM MTR_ARC1_RT_TA where rownum<1';
	*/
	execute immediate 'truncate table ARC1_TA';
	
	-- Kopieren der Daten in die Zusatztabelle und L�schen in Quelltabelle
	if nVonTisId = 0 then
		select min(ID) into nMinId from MTR_TIMESERIES_TA;
	end if;
	if nBisTisId = 0 then
		select max(ID) into nMaxId from MTR_TIMESERIES_TA;
	end if;
	
	if cTabSyn = 'MTR_ARC1_RT_TA' then
		cArcTyp := 1;
	else
		cArcTyp := 4;
	end if;
	
	for recTis in (select ID from MTR_TIMESERIES_TA where ID >= nMinId and ID <= nMaxId 
		and ARCHIVETYPE=cArcTyp and TIME_LEVEL='MI15' and FROM_DATE is not null 	and (UNTIL_DATE - FROM_DATE) > 100
	order by ID) loop
		cSt := 'insert into ARC1_TA (select * from ' || cTabSyn || ' where FK_TIS = ' || recTis.ID || ')';
		execute immediate cSt;
		commit;
		-- L�schen in MTR_ARC1_RT_TA
		cSt := 'delete from ' || cTabSyn || ' where FK_TIS = ' || recTis.ID;
		execute immediate cSt;
		commit;
	end loop;

	-- Neubef�llen der Quelltabelle - Daten sollten dann Lese-optimal in die Datenbl�cke eingef�gt werden
	for recTis in (select ID from MTR_TIMESERIES_TA where ID >= nMinId and ID <= nMaxId 
		and ARCHIVETYPE=cArcTyp and TIME_LEVEL='MI15' and FROM_DATE is not null 	and (UNTIL_DATE - FROM_DATE) > 100
	order by ID) loop
		iZr := iZr + 1;
		cSt := 'insert into ' || cTabSyn || ' (select * from ARC1_TA where FK_TIS = ' || recTis.ID || ')';
		execute immediate cSt;
		commit;
	end loop;
	nSek := (sysdate - dStart)*24*60*60;
	select count(*) into iC from ARC1_TA;
	PSISWH.InsHit2( nNr, 'Anzahl reorganisierter Zeitreihen/S�tze: ' || to_char(iZr) || '/' || to_char(iC) || ' - Laufzeit in Sek: ' || to_char(nSek,'99999.99') ); 

end;

/
