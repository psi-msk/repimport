create or replace procedure fillStdzuo authid definer
as                                 
	-- Transport Standardgrößen in QUANZUO eintragen (stehen bereits in QUANT mit FK_QUN < -150)
	cLast1 varchar2(20);
	cQuant	varchar2(40);  
	cUnit	varchar2(80);  
	nQuan	number := 1000;
	cStdeht	varchar2(40);  
	bFound	boolean;

begin
	for recQ in (select * from QUANT where FK_QUN < -150) loop
		-- Eintragen in QUANZUO, wenn nicht vorhanden
		-- Achtung EHT ist, wenn mehrere Zuordnungen in MTR_FS_QUANTITIES_TA vorhanden sind, die Nicht-Standardeinheit
		cUnit := GetEHT( recQ.FK_QUN );
		bFound := false;
		for rec in (select * from QUANZUO where FK_QUAN = recQ.FK_QUN) loop 
			bFound := true;
		end loop;          
		if not bFound then
			insert into QUANZUO (KNZTEIL, EHT, QUANT, STDEHT, SHORT_NAME, DESCRIPTION, FK_QUAN)
				values( recQ.ID, cUnit, recQ.ID, recQ.DEFAULT_UNIT, recQ.SHORT_NAME, recQ.DESCRIPTION, recQ.FK_QUN);
		end if;		
	end loop;
end;
/
