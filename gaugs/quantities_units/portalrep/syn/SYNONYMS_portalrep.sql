--  ******************************************************************
--  Copyright (C) PSI AG, 2013
--  PSI AG, 2013
-- 
--  Data model PSIportal user portalrep - Synonyms
--
--  $Author: emslih $
--  $Date: 2013/05/27 15:11:00 $
--  $Revision: 1.0 $
--  $State: Exp $
--
--  Date        Author       Comments
--  ----------  -----------  -----------------------------------------
--  27.04.2013  H. Liebhold  Creation
--  17.07.2013  H. Liebhold  units und quant ergänzt
--  07.04.2014  H. Liebhold  SYS_ABSENT, SYS_WF, MD_BILOBJ, SYS_ERRINFO, SYS_IMPOBJ ergänzt
--  15.05.2014  H. Liebhold  SYS_PERFORM, SYS_IMPCNT  ergänzt
--  21.05.2014	H. Liebhold  Synonyme SYS_ABSENT, SYS_ERRINFO geändert
--  26.05.2014  H. Liebhold  Synonyme MD_BUS_AREA geändert
--  29.07.2014  H. Liebhold	 Synonym UNITS korrigiert - zeigt jetzt auf View
--  24.09.2014  LIH			 Speicherung in utf8 ohne BOM	
--  ******************************************************************
WHENEVER SQLERROR CONTINUE
WHENEVER SQLERROR exit FAILURE

CREATE OR REPLACE SYNONYM &porrepuser..DD_ORIGINAL FOR &poruser..MTR_EXT_ORIG4_VI;
CREATE OR REPLACE SYNONYM &porrepuser..DD_SYS FOR &poruser..MTR_EXT_ARC1_RT_VI;
CREATE OR REPLACE SYNONYM &porrepuser..DD_SYSTEM FOR &poruser..MTR_EXT_SYST1_VI;
CREATE OR REPLACE SYNONYM &porrepuser..MD_BUS_AREA FOR &poruser..MTR_EXT_BA_VI;
CREATE OR REPLACE SYNONYM &porrepuser..MD_BILOBJ FOR &poruser..MTR_EXT_BILANZOBJ_VI;
CREATE OR REPLACE SYNONYM &porrepuser..MD_OBJECTS FOR &poruser..MTR_EXT_OBJ_VI;
CREATE OR REPLACE SYNONYM &porrepuser..MD_OBJLIST FOR &poruser..MTR_EXT_OBJLIST_VI;
CREATE OR REPLACE SYNONYM &porrepuser..MD_VARIABLES FOR &poruser..MTR_EXT_VAR_VI;
CREATE OR REPLACE SYNONYM &porrepuser..REPORTPRIVGRP FOR &poruser..MTR_EXT_REPORTPRIVGRP_VI;
CREATE OR REPLACE SYNONYM &porrepuser..SEC_GRPOBJ FOR &poruser..MTR_EXT_GRPOBJ_VI;
CREATE OR REPLACE SYNONYM &porrepuser..SEC_PRIVIL FOR &poruser..MTR_EXT_PRIVIL_VI;
CREATE OR REPLACE SYNONYM &porrepuser..SEC_ROLOBJ FOR &poruser..MTR_EXT_ROLOBJ_VI;
CREATE OR REPLACE SYNONYM &porrepuser..SEC_USER FOR &poruser..MTR_EXT_USER_VI;
CREATE OR REPLACE SYNONYM &porrepuser..SEC_USRPRIV FOR &poruser..MTR_EXT_USR_ROLE_VI;
CREATE OR REPLACE SYNONYM &porrepuser..SESSOBJ FOR &poruser..MTR_SESSOBJ_VI;
CREATE OR REPLACE SYNONYM &porrepuser..SYS_EVENTS FOR &poruser..MTR_EXT_EVENT_VI;
CREATE OR REPLACE SYNONYM &porrepuser..SYS_EXPORTS FOR &poruser..MTR_EXT_EXPORT_VI;
CREATE OR REPLACE SYNONYM &porrepuser..SYS_IMPORTS FOR &poruser..MTR_EXT_IMPORT_VI;
CREATE OR REPLACE SYNONYM &porrepuser..SYS_IMPCTL FOR &poruser..MTR_EXT_IMPORTCTL_VI;
CREATE OR REPLACE SYNONYM &porrepuser..TIS FOR &poruser..MTR_EXT_TIMESERIES_VI;
CREATE OR REPLACE SYNONYM &porrepuser..UNITS FOR &poruser..MTR_EXT_UNITS_VI;
CREATE OR REPLACE SYNONYM &porrepuser..QUANT FOR &poruser..MTR_EXT_QUANTITIES_VI;
CREATE OR REPLACE SYNONYM &porrepuser..SYS_PERFORM FOR &poruser..MTR_EXT_PERFORM_VI;
CREATE OR REPLACE SYNONYM &porrepuser..SYS_WF FOR &poruser..MTR_EXT_WF_HIST_VI;
CREATE OR REPLACE SYNONYM &porrepuser..SYS_ABSENT FOR &poruser..MTR_EXT_DD_ABSENTVALUES_VI;
CREATE OR REPLACE SYNONYM &porrepuser..SYS_ERRINFO FOR &poruser..MTR_EXT_DD_ERRORINFO_VI;
CREATE OR REPLACE SYNONYM &porrepuser..SYS_IMPOBJ FOR &poruser..MTR_EXT_IMPORT_OBJ_VI;
CREATE OR REPLACE SYNONYM &porrepuser..SYS_IMPCNT FOR &poruser..MTR_EXT_IMP_ARCVALCNT_VI;

CREATE OR REPLACE SYNONYM &porrepuser..PTABHEAD FOR POR_TABHEAD;
CREATE OR REPLACE SYNONYM &porrepuser..PTABVAL FOR POR_TABVALUES;
CREATE OR REPLACE SYNONYM &porrepuser..REPGRP FOR REPORTGRP_TA;
CREATE OR REPLACE SYNONYM &porrepuser..REPOBJ FOR REPORTOBJ_TA;
/
