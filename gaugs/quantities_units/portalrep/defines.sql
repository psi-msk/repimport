--  ******************************************************************
--  PSI AG, 2013
-- 
--  Data model PSIMetering
--
--  $Author: emshp $
--  $Date: 2011/07/11 14:58:05 $
--  $Revision: 1.4 $
--  $State: Exp $
--
--  ******************************************************************

set serveroutput on
set term on 

--
-- Defines
--

-- Tablespace for data
define ts_name = 'MTR_TS'

-- Tablespace for index
define ts_name_ind = 'MTR_IND_TS'

-- Temporary tablespace 
-- define ts_temp = 'TMP_TS'

-- Dis-/Enable check-constraints
define ck_cons = 'ENABLE'

-- Main Schema (mandatory)
define poruser = 'POR'

-- Reporting Schema (optional)
define porrepuser = 'PORTALREP'
