Settings:
=========
Environment settings needed for proper UTF-8 encoding of data when
executing scripts via sqlplus (for all platforms):

	  set NLS_LANG=american_america.utf8

The "german_germany" part is not decisive, important ist the utf-8 -
part.

Defines:
========
- defines.sql: settings for all SQL-Scripts

SQL-Scripts:
============
- cre-schema-PORTALREP.sql: create schema PORTALREP
- inst-db-PORTALREP.sql: create all database objects of PORTALREP
- deinst-db-PORTALREP.sql: drops all database objects of PORTALREP
