Package PSI_TP_Pckg AUTHID DEFINER
is
--------------------------------------------------------------------------------
-- package mit Funktionen f�r den Aufbau der Gr��en/Einheiten/Adapter-Mapping-Eintr�ge f�r PSItransport
--------------------------------------------------------------------------------
-- LIH 20.05.2015   Neu: PrepSTQuan
-- LIH 19.05.2015	PrepFVQuan erweitert - auch russ. Kurznamen/Einheiten mappings erzeugen
-- LIH 12.05.2015 	Crea_Mapping erstellt
-- LIH 11.05.2015	Ersterstellung

--------------------------------------------------------------------------------
-- Konstanten
--------------------------------------------------------------------------------
	iGasjahrStartMonat	integer := 10;

function last2( cs in varchar2 ) return varchar2;	  				-- String nach vorletztem Punkt extrahieren	
function last1( cs in varchar2 ) return varchar2;					-- String nach letztem Punkt extrahieren	
procedure PrepSTQuan( cFS in varchar2 default 'IMP_TP_XML' ); 		-- Aufbau neuer Standardgr��en  
procedure PrepFVQuan;												-- Aufbau der  Eintr�ge in MTR_QUANTITIES_TA, MTR_QUANTITY_UNITS_TA  f�r Freie Variable 
procedure Crea_QuanMapping( cFS in varchar2 default 'IMP_TP_XML' ); -- Aufbau der Mappingliste f�r TP-Adapter f�r spez. Gr��en

end;
/

Package body PSI_TP_Pckg
is

function last1( cs in varchar2 ) return varchar2 
as        			
	cRest	varchar2(100) := cs;	
	ipos	integer := instr(cs, '.');	
	iC		integer := 0;
begin			
	-- String nach letztem Punkt extrahieren		
	-- INSTR( string, substring [, start_position [,  nth_appearance ] ] )		
			
	While ipos > 0 loop		
		iC := iC + 1;	
		cRest := substr(cRest, ipos+1);	
		ipos := instr(cRest, '.');	
	end loop;		
	if iC >= 1 then		
		return substr(cs, instr(cs, '.', 1, iC) + 1);	
	end if;		
	return cs;		
end;

function last2( cs in varchar2 ) return varchar2 
as        			
	cRest	varchar2(100) := cs;	
	ipos	integer := instr(cs, '.');	
	iC		integer := 0;
begin			
	-- String nach vorletztem Punkt extrahieren		
	-- INSTR( string, substring [, start_position [,  nth_appearance ] ] )		
			
	While ipos > 0 loop		
		iC := iC + 1;	
		cRest := substr(cRest, ipos+1);	
		ipos := instr(cRest, '.');	
	end loop;		
	if iC > 1 then		
		return substr(cs, instr(cs, '.', 1, iC - 1) + 1);	
	end if;		
	return cs;		
end;


procedure PrepFVQuan  
as
	-- Erzeugen der neuen Gr��en f�r freie Variable 
	-- Gr��eneintrag mit ID wie Standardgr��e, jedoch mit positiven FK_QUN  (statt negativ)
	-- Vorauss.: in QUANZUO sind die Daten eingetragen, erzeugt durch Abgriff aus der TP- Kunden-DB
	/*	select 'insert into QUANZUO(KNZTEIL, EHT, QUANT, STDEHT, SHORT_NAME, DESCRIPTION, FK_QUAN)
	 values(''' || last2(ZRKNZ) || ''','''|| EHT || ''','''|| last2(ZRKNZ)||''',null,'''  || last2(ZRKNZ) || ''','''||    last2(ZRKNZ)|| ''',null );'
	 from TPMAS_VI
	 where gueltend is null
	 and ARVTYP='T' and EHT is not null   and ZRKNZ like '%./_%' ESCAPE  '/' and ZRKNZ not like 'GLOVAR.%'
	 order by ZRKNZ;
	 */
	-- Die Eintr�ge enthalten nach Selektion noch keine FK_QUAN; alle beginnen im KNZTEIL mit "_"

	cQuant	varchar2(40);  
	nQuanId	number := 1000;                              
	cType   varchar2(2);   
	cEht	varchar2(40);
	cStdEht	varchar2(40);
	iC		integer;
	cKz		varchar2(40);
	bFound	boolean;
	cId		varchar2(100);
	nNr		number(12) :=0;

begin
	psi_pckg.SaveHit;
	psi_pckg.InsHit2( nNr,  ' ' );


	--select GEN_VALUE into nQuanId     from por.MTR_SEQUENCE_TA where GEN_KEY='Quantity_Units_Id';
	--select min(FK_QUN) into nQuanId from QUANT;
	for rec in (select * from QUANZUO where substr(KNZTEIL,1,1) = '_' and FK_QUAN is null and QUANT_TYP='FV' ) loop   
		-- Erzeuge eine FK_QUAN
		-- Trage die Gr��e in die Gr��entabelle MTR_QUANTITIES_TA und die  MTR_QUANTITY_UNITS_TA ein, wenn noch nicht vorhanden
		cQuant := rec.QUANT;
		cKz := rec.KNZTEIL;
		cEht := rec.EHT;
		if rec.EHT='NA' then
			cId := rec.KNZTEIL  || '.-';	                  
		else
			cId := rec.KNZTEIL  || '.' || rec.EHT;	                  
		end if;
		if cQuant is not null then
			select count(*) into iC from   QUANT  where ID = cId;
			if iC >= 0 then
				nQuanId := nQuanId + 1;

				cType := 'Z';
				if rec.EHT = '%' or rec.EHT = '�C' or rec.EHT = 'Wh/m�' or rec.EHT = 'bar' then
					cType := 'M';
				end if;
				if rec.EHT = 'Wh' then
					cStdEht := 'kWh'; 
				elsif rec.EHT = '-' then
					cStdEht := 'NA'; 
				elsif rec.EHT = '?�' then
					cStdEht := '???.?�'; 
				else
					cStdEht := rec.EHT; 
				
				end if;
				
				psi_pckg.InsHit2( nNr,  
	 			'insert into por.MTR_QUANTITIES_TA (ID, VERS_DATE, SHORT_NAME, DESCRIPTION, TYPE, DIRECTION, FK_QUN, DEFAULT_UNIT)
	 			 values ( ''' || cId || ''',''01/01/2000 06:00:00'',''' || rec.SHORT_NAME|| ''',''' || 
	 			  rec.DESCRIPTION|| ''',''' || cType|| ''',null ,' ||  nQuanId|| ',''' ||  cStdEht|| ''');');
	 			  	
	            for recUnit in (select  ID from UNITS where SHORT_NAME= cEHT) loop
					psi_pckg.InsHit2( nNr,  
					'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuanId || ',''' || recUnit.ID || '''); ');
									
					if cEht <> cStdEht then
						psi_pckg.InsHit2( nNr,  
						'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuanId || ',''' || cStdEht || '''); ');
						--Insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(nQuanId, cStdEht);
					end if;                                                                          
				end loop;
				
				if rec.EHT = 'Wh' then
					psi_pckg.InsHit2( nNr,  
						'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuanId || ',''MWh'');');
				elsif rec.EHT = 'm�' then
					psi_pckg.InsHit2( nNr,  
						'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuanId || ',''?m�'');');
				end if;
				commit;
				
			end if;    
		end if;
	end loop;  
	-- Aktualisieren der Seq-ID Quantity_Units_Id in MTR_SEQUENCE_TA
	psi_pckg.InsHit2( nNr,  'update por.MTR_SEQUENCE_TA set GEN_VALUE=(select max(FK_QUN) from QUANT) where GEN_KEY=''Quantity_Units_Id'';');
    commit;

end;

procedure PrepSTQuan( cFS in varchar2 default 'IMP_TP_XML' )   
as
	-- Erzeugen neuer Standardgr��en 
	-- Gr��eneintrag mit ID wie Standardgr��e und mit negativer FK_QUN  
	-- Vorauss.: in QUANZUO sind die Daten eingetragen, erzeugt durch Abgriff aus der TP- Kunden-DB
	-- Die Eintr�ge enthalten nach Selektion noch die falsche Einheit - eingetragen ist die Standardeinheit in Feld EHT; die Einheit ist aus dem Kennzeichenteil nach letztem Punkt zu extrahieren
	-- Inhaltsbsp.: insert into QUANZUO(KNZTEIL, EHT, QUANT, STDEHT, SHORT_NAME, DESCRIPTION, FK_QUAN, QUANT_TYP) values ('MNG.IST.-','-','MNG.IST.-','','MNG.IST.-','MNG.IST.-','','ST');


	cQuant		varchar2(40);  
	cLastQuan	varchar2(40);  
	nQuanId	number := -1000;                              
	cType   varchar2(2);   
	cEht	varchar2(40);
	cStdEht	varchar2(40);
	iC		integer;
	cKz		varchar2(40);
	bFound	boolean;
	cId		varchar2(100);
	nNr		number(12) :=0;  
	bMap	boolean;
	cFsId	varchar2(100);

begin
	psi_pckg.SaveHit;
	psi_pckg.InsHit2( nNr,  ' ' );

    select ID into cFsId from por.MTR_FS_TA where SHORT_NAME = cFS;
    
	--select GEN_VALUE into nQuanId     from por.MTR_SEQUENCE_TA where GEN_KEY='Quantity_Units_Id';
	--select min(FK_QUN) into nQuanId from QUANT;
	for rec in (select * from QUANZUO where QUANT_TYP='ST' order by KNZTEIL) loop   
		-- Trage die Gr��e in die Gr��entabelle MTR_QUANTITIES_TA und die  MTR_QUANTITY_UNITS_TA ein, wenn noch nicht vorhanden
		cQuant := substr(rec.QUANT, 1, instr(rec.QUANT,'.',1,2)-1);
		cKz := rec.KNZTEIL;                                          
		cId := cQuant;
		cEht := substr(cKz, instr(cKz,'.',1,2)+1);
		cStdEht := rec.EHT;  
		bMap := false;
		if cQuant is not null then
			select count(*) into iC from   QUANT  where ID = cId and DEFAULT_UNIT = cStdEht;
			if iC = 0 then
				nQuanId := nQuanId - 1;

				cType := 'Z';
				if rec.EHT = '%' or rec.EHT = '�C' or rec.EHT = 'Wh/m�' or rec.EHT = 'bar' or rec.EHT='???/??�'  or rec.EHT='-'  or rec.EHT='?/?�' or rec.EHT='??/?�'or rec.EHT= '??/??�' or rec.EHT='??. ??. ??' then
					cType := 'M';
				end if;
				bMap :=  true;
				if cLastQuan  = null then
					cLastQuan := cQuant;
					--psi_pckg.InsHit2( nNr,  'cLastquan='||cLastQuan;
				
				else   
					-- Mapping f�r Importadapter ist erforderlich, wenn gr��en.EHT nicht in QUANT				
					if cQuant = cLastQuan then  
						-- umbenennen  (1. Z. aus Einheit mit "_" getrennt anh�ngen)
						cQuant := cQuant || '_' || substr(cEht,1,1); 
						bMap := true;  
					else
						cLastQuan := cQuant;
						--psi_pckg.InsHit2( nNr,  'cLastquan='||cLastQuan;
						select count(*) into iC from   QUANT  where ID = cQuant;
						if iC <> 0 then
							cQuant := cQuant || '_' || substr(cEht,1,1);   
							bMap := true;
						end if;                                                
					end if;
				end if;
				
				psi_pckg.InsHit2( nNr,  
	 			'insert into por.MTR_QUANTITIES_TA (ID, VERS_DATE, SHORT_NAME, DESCRIPTION, TYPE, DIRECTION, FK_QUN, DEFAULT_UNIT)
	 			 values ( ''' || cQuant || ''',''01/01/2000 06:00:00'',''' || rec.SHORT_NAME|| ''',''' || 
	 			  rec.DESCRIPTION|| ''',''' || cType|| ''',null ,' ||  nQuanId|| ',''' ||  cStdEht|| ''');');
	 			  	
	            for recUnit in (select  ID from UNITS where SHORT_NAME= cEHT) loop
					psi_pckg.InsHit2( nNr,  
					'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuanId || ',''' || recUnit.ID || '''); ');
									
					if cEht <> cStdEht then
						psi_pckg.InsHit2( nNr,  
						'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuanId || ',''' || cStdEht || '''); ');
						--Insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(nQuanId, cStdEht);
					end if;                                                                          
				end loop;
				
				if rec.EHT = 'Wh' then
					psi_pckg.InsHit2( nNr,  
						'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuanId || ',''MWh'');');
				elsif rec.EHT = 'm�' then
					psi_pckg.InsHit2( nNr,  
						'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuanId || ',''Tm�'');');
				elsif rec.EHT = '???.?�' then
					psi_pckg.InsHit2( nNr,  
						'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuanId || ',''?�'');');
				elsif rec.EHT = '?�' then
					psi_pckg.InsHit2( nNr,  
						'insert into por.MTR_QUANTITY_UNITS_TA   (ID, FK_UNITS) values(' || nQuanId || ',''???.?�'');');
				end if; 
				if bMap then
					psi_pckg.InsHit2( nNr,  
					'insert into por.MTR_FS_QUANTITIES_TA (FS_ID, FK_QUANT, ALIAS_NAME)
					values (''' || cFsId || ''',''' || cQuant || ''',''' || cKz || ''');' );
				end if;
				commit;
				
			end if;    
		end if;
	end loop;  
	-- Aktualisieren der Seq-ID Quantity_Units_Id in MTR_SEQUENCE_TA
	--psi_pckg.InsHit2( nNr,  'update por.MTR_SEQUENCE_TA set GEN_VALUE=(select max(FK_QUN) from QUANT) where GEN_KEY=''Quantity_Units_Id'';');
    commit;

end;

procedure Crea_QuanMapping( cFS in varchar2 default 'IMP_TP_XML' ) 
as
	-- Die in quanzuo enthaltenen spezifischen Gr��en (Kennung SG=spez. Gr��e) werden f�r den Transport-Adapter auf bestimmte, daf�r vorgesehene Gr��en gemappt
	-- Vorauss in por: grant insert,update on MTR_FS_QUANTITIES_TA to porrep;
	
	cLast1 varchar2(20);
	cQuant	varchar2(40);  
	cDesc	varchar2(80);  
	nQuan	number := 1000;
	cStdeht	varchar2(40);                     
	nFs_Id	number;  
	nNr		number(12) :=0;
	bFound  boolean := false;

begin     
	psi_pckg.SaveHit;
	psi_pckg.InsHit2( nNr,  ' ' );
	
	
	--delete from  por.MTR_FS_QUANTITIES_TA;                  
	for recFS in (select ID from por.MTR_FS_TA where SHORT_NAME = cFS) loop
		for rec in (select KNZTEIL, EHT, last1(KNZTEIL) QUANT from QUANZUO where QUANT_TYP = 'SG' ) loop   
			-- zun�chst zuzuordnende Gr��en ermitteln und in QUANZUO eintragen
			--  Beispieleintr�ge: DB08.NTZISM	m�, E01K.BST	m�
			if rec.EHT = 'Wh' then
				rec.QUANT := rec.QUANT || '_WH' ;
			elsif  rec.EHT = 'm�' then
				rec.QUANT := rec.QUANT || '_M3' ;
			end if;
			cQuant := rec.Quant;
			bFound := false;
			for recQ in (select * from QUANT where ID = cQuant) loop
				bFound := true;
				psi_pckg.InsHit2( nNr,  
					'insert into por.MTR_FS_QUANTITIES_TA (FS_ID, FK_QUANT, ALIAS_NAME)
					values (''' || recFS.ID || ''',''' || recQ.ID || ''',''' || rec.KNZTEIL || '.' || rec.EHT || ''');' );
			end loop;                                                                                                     
			if not bFound then
				psi_pckg.InsHit2( nNr,  'Gr��e mit ID=' || cQuant || ' ist nicht in Quantity-Tabelle enthalten!');
			end if;

		end loop;
	end loop;          
	commit;
end;

end; -- pckg
/
