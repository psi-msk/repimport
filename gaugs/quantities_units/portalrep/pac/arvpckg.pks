create or replace Package ARVpckg AUTHID DEFINER
is 
	-- LIH 09.06.15 Sortierung nach LABEL, massive Beschleunigung arvcnt 
	-- LIH 08.06.15 Aufruf arvcnt statt arvcnt@arvdb
	-- LIH 05.06.15 R_CompareControl erweitert - Sicherheitsstop (max 1 Label je Report), und  and CONDITION='M' erg�nzt
	-- LIH 03.06.15 Verbesserung R_ChkTrans - L�ckenz�hlung
	-- LIH 03.06.15 Korrektur  in R_CompareControl (letzte eingetragene Werte von Control und Portal f�r gleichen Zeitstempel)
	-- LIH 27.05.15 Ersterstellung
/*	                                
	Diese package enth�lt allg. Funktionen und Reportfunktionen f�r das ARV-Schema (PSIcontrol)
	Voraussetzung f�r den Compile:  Datenbanklink arvdb:   create database link arvdb connect to arv identified by "arv" using 'gobsux02/tgsd';
	
	Die folgenden Reports werden abgedeckt:
	1. R_ChkControl
	   			  
	2. R_CompDetails

*/	

	TYPE OUTHEAD is table of PTABHEAD%rowtype;
	type array_of_nums is table of number index by binary_integer;

	PROCEDURE swzoffs_pr (datum_pio    IN OUT date);

	function BDPVers return number;
	Function ARVCnt ( nTid in number, cVon in varchar2, cBis in varchar2, nAnzOk in out number, nAnzNull in out number, dLast in out date ) return date;
	FUNCTION cycle2date_fu (skala_pi   IN   integer, cyclenr_pi IN   integer)        RETURN date;
	FUNCTION date2cycle_fu (skala_pi   IN   integer, datum_pi   IN   date)           RETURN integer;

	function R_CompareControl( cFs in varchar2, dVon in date, dBis in date, cWild in varchar2 ) return number;
 	function R_ChkControl( cFs in varchar2, dVon in date, dBis in date, cWild in varchar2 ) return number;
end;
/

create or replace Package body ARVpckg
is
	-- �nderungsinfos siehe Header

---------------------------------------------------------------------------------------------

	DATE_FORMAT_GER 				constant varchar2(21) := 'DD.MM.YYYY HH24:MI:SS';

---------------------------------------------------------------------------------------------
  strat0_l      date := to_date('15.10.1582 00:00:00','dd.mm.yyyy hh24:mi:ss');
  strat1_l      date := to_date('15.10.1582 01:00:00','dd.mm.yyyy hh24:mi:ss');
  strat6_l      date := to_date('15.10.1582 06:00:00','dd.mm.yyyy hh24:mi:ss');
  strat7_l      date := to_date('15.10.1582 07:00:00','dd.mm.yyyy hh24:mi:ss');
  strat8_l      date := to_date('15.10.1582 08:00:00','dd.mm.yyyy hh24:mi:ss');

function BDPVers return number
as
	nVersion	number(38) := 0;
begin
 	for rec in (select DABVERNR from bdp.BDPDABVV_TA@arvdb where rolle='BDP') loop
 		nVersion := rec.DABVERNR;
 	end loop;
 	return nVersion;
end;

PROCEDURE swzoffs_pr (datum_pio  IN OUT date) 
AS
  wsgrenze_l  date;
  swgrenze_l  date;
BEGIN
  wsgrenze_l := to_date('25.03.'||to_char(datum_pio,'yyyy')||' 02:00:00','dd.mm.yyyy hh24:mi:ss');
  swgrenze_l := to_date('25.10.'||to_char(datum_pio,'yyyy')||' 02:00:00','dd.mm.yyyy hh24:mi:ss');

  if datum_pio between (wsgrenze_l + (7 - to_number(to_char(wsgrenze_l,'D'))))
      and (swgrenze_l + (7 - to_number(to_char(swgrenze_l,'D')))) then
    datum_pio := datum_pio - (1/24);
  end if;

  return;
END swzoffs_pr;

FUNCTION cycle2date_fu (skala_pi   IN   integer, cyclenr_pi IN   integer) RETURN date 
IS

  datum_po      date;

BEGIN
	-- Achtung: strat0_l - bedeutet Zeiten kommen in UTC raus !!
	if skala_pi = 2 then -- mSek
	  datum_po := cyclenr_pi / 86400000 + strat0_l;
	end if;
	
	if skala_pi = 7 then -- Min3
	  datum_po := cyclenr_pi / 480 + strat0_l;
	end if;
	
	if skala_pi = 8 then -- Min5
	  datum_po := cyclenr_pi / 288 + strat0_l;
	end if;
	
	if skala_pi = 9 then -- Min6
	  datum_po := cyclenr_pi / 240 + strat0_l;
	end if;
	
	if skala_pi = 10 then -- Min15
	  datum_po := cyclenr_pi / 96 + strat0_l;
	end if;
	
	if skala_pi = 12 then -- Std
	  datum_po := cyclenr_pi / 24 + strat0_l;
	end if;
	
	if skala_pi = 13 then -- Tag
	  datum_po := cyclenr_pi + strat0_l;
	end if;
	
	if skala_pi = 14 then -- Tag6
	  datum_po := cyclenr_pi + strat6_l;
	end if;
	
	if skala_pi = 15 then -- Tag7
	  datum_po := cyclenr_pi + strat7_l;
	end if;
	
	if skala_pi = 16 then -- Tag8
	  datum_po := cyclenr_pi + strat8_l;
	end if;
	
	--swzoffs_pr (datum_po);
	
	RETURN datum_po;
	
	EXCEPTION
	  WHEN OTHERS THEN
	    RAISE;

END cycle2date_fu;

FUNCTION date2cycle_fu (skala_pi   IN   integer, datum_pi   IN   date)           RETURN integer 
IS

  cyclenr_po    integer;
  datum_po      date;

BEGIN

	datum_po := datum_pi;
	--  swzoffs_pr (datum_po);

	if skala_pi = 2 then -- mSek
	  cyclenr_po := (datum_po - strat0_l) * 86400000;
	end if;
	
	if skala_pi = 7 then -- Min3
	  cyclenr_po := (datum_po - strat0_l) * 480;
	end if;
	
	if skala_pi = 8 then -- Min5
	  cyclenr_po := (datum_po - strat0_l) * 288;
	end if;
	
	if skala_pi = 9 then -- Min6
	  cyclenr_po := (datum_po - strat0_l) * 240;
	end if;
	
	if skala_pi = 10 then -- Min15
	  cyclenr_po := (datum_po - strat0_l) * 96;
	end if;
	
	if skala_pi = 12 then -- Std
	  cyclenr_po := (datum_po - strat0_l) * 24;
	end if;
	
	if skala_pi = 13 then -- Tag
	  cyclenr_po := (datum_po - strat0_l);
	end if;
	
	if skala_pi = 14 then -- Tag6
	  cyclenr_po := (datum_po - strat6_l);
	end if;
	
	if skala_pi = 15 then -- Tag7
	  cyclenr_po := (datum_po - strat7_l);
	end if;
	
	if skala_pi = 16 then -- Tag8
	  cyclenr_po := (datum_po - strat8_l);
	end if;
	
	RETURN cyclenr_po;
	
	EXCEPTION
	  WHEN OTHERS THEN
	    RAISE;

END date2cycle_fu;

function R_CompareControl( cFs in varchar2, dVon in date, dBis in date, cWild in varchar2 ) return number
as

    dFirst		date;
    dRef		date;
    nFirst		number;
    nLast		number; 
    nSid		number; 
    nTid 		number;
    nVonCycle	number := date2cycle_fu(12,dVon) ;
    nBisCycle	number := date2cycle_fu(12,dBis) ;
    iLfdNr		integer := 0;
    bFound		boolean;
    bGleich		boolean;
    bLag1		boolean;
    bLag2		boolean;
    cLag		varchar2(10) := 'L�cke';
    cEqual		varchar2(20) := 'Werte sind gleich';
    cUnequal	varchar2(20) := 'Werte ungleich !!'; 
    nSummand	number;
    nFaktor		number;
    nDAB		number;
    nWert		number; 
    iRound		integer; 
    dLast		date;    
    iC			integer := 0;
    cLastKz		varchar2(80);

	pragma autonomous_transaction;  -- necessary because of insert or deletes while the function is used in a select                                                 
     
begin 
	 
	-- Ablauf:
	-- 1. Daten aus ARV und Portal-DD_SYS auslesen und in TabValues eintragen
	-- 2. Lauf �ber TabValues - eliminieren eintr�ge bzw. ersetzen durch Texte 
	psireports.execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
    psireports.execsqldirect( 'alter session set nls_numeric_characters = '',.''');                       
	--psireports.execsqldirect( 'alter session set NLS_LANG=German_Germany.AL32UTF8');
	select userenv('sessionid') into nSid from dual;
	
	-- Reportgruppe und -objekte erzeugen   
	psireports.delsid( nSid ); 
	
	insert into REPORTGRP_TA (SID, REPNAME, ERSTELLT_AM) values( nSid, 'R_CompareControl', sysdate );

	-- f�r alle Zeitreihen von PSIcontrol
	for recTis in (select objlabel(fk_obj) LABEL, TIS.* from TIS where ARCHIVETYPE='1' and TIME_LEVEL='H' and FK_FS= cFs and (cWild is null or cWild='*' or objlabel(fk_obj) like '%' || cWild || '%' ) order by LABEL) loop
		iLfdNr := iLfdNr + 1; 
		if cLastKz is null then
			cLastKz := recTis.LABEL;
		end if;
		--cPict  := Getpict(recTis.ID);		
		insert into REPORTOBJ_TA  (SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, UNIT, QUANTITY, FK_OBJID)
			values (nSid, 0, iLfdNr, '0',recTis.ID, '', '', recTis.LABEL, '', recTis.LABEL, recTis.UNIT, recTis.QUANTITY, recTis.FK_OBJ );
		if recTis.Label <> cLastKz then
			-- Detailvergleich nur f�r 1 Label erlaubt (aber alle Zeitreihen zum Kennzeichen)
			update REPORTOBJ_TA set OBJLABEL = 'STOP! Details nur f�r 1 Kennz.', FK_TIS=null where SID = nSid and FK_OBJLIST=0 and POS=iLfdNr;
		end if;
		commit;
	end loop;   
	
	if iLfdNr > 0 then
		nDAB := bdpvers();
		for rec in (select * from REPORTOBJ_TA where SID = nSid order by POS) loop
			if rec.FK_TIS is null then
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE) values (nSid, rec.POS, sysdate);
				commit;
			end if;				
			dFirst := null;
			nTid := null; 
			-- Werte in arv ermitteln    - dazu TID aus Fremdschl�sseltabelle holen
			for recFk in (select FOREIGNKEY from por.MTR_FK_TA where FK_OBJID = rec.FK_OBJID) loop
				nTid := to_number(recFk.FOREIGNKEY);
			end loop;
			if nTid is not null then                                
				for recUnit in (select PRECISION from UNITS where SHORT_NAME = rec.UNIT) loop
					iRound := recUnit.PRECISION;
				end loop;
				-- arv-Daten �bernehmen
				for recArv in (select * from ARVVALUECO12_TA@arvdb where TID = nTid and  CYCLENR between nVonCycle and nBisCycle order by CYCLENR) loop
					-- Umr. faktor ermitteln; dazu zun�chst DIM-Schablone �ber
					nFaktor := 1;
					bFound := false;
					for recDim in (select FAKTOR, SUMMAND from bdp.BDPDIM_VI@arvdb where TID=(select TIDDIM from bdp.BDPMWKERN_TA@arvdb where TID = nTid) and DABVERNR=nDAB ) loop 
						nFaktor  := recDim.FAKTOR;
						nSummand := recDim.SUMMAND;
						bFound := true;
					end loop;
					if not bFound then
						for recDim in (select FAKTOR, SUMMAND from bdp.BDPDIM_VI@arvdb where TID=(select TIDDIM from bdp.BDPMESKERN_TA@arvdb where TID = nTid) and DABVERNR=nDAB ) loop 
							nFaktor := recDim.FAKTOR;
							nSummand := recDim.SUMMAND;
							bFound := true;
						end loop;
					end if;
					
					-- Eintragen in POR_TABVALUES
					dRef := cycle2date_fu(12,recArv.CYCLENR);
					if dFirst is null then
						dFirst := dRef;
					end if;
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef, round(recArv.ARCVALUE0*nFaktor + nSummand,iRound));
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+1/24, round(recArv.ARCVALUE1*nFaktor + nSummand,iRound));
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+2/24, round(recArv.ARCVALUE2*nFaktor + nSummand,iRound));
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+3/24, round(recArv.ARCVALUE3*nFaktor + nSummand,iRound));
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+4/24, round(recArv.ARCVALUE4*nFaktor + nSummand,iRound));
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+5/24, round(recArv.ARCVALUE5*nFaktor + nSummand,iRound));
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+6/24, round(recArv.ARCVALUE6*nFaktor + nSummand,iRound));
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+7/24, round(recArv.ARCVALUE7*nFaktor + nSummand,iRound));
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+8/24, round(recArv.ARCVALUE8*nFaktor + nSummand,iRound));
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+9/24, round(recArv.ARCVALUE9*nFaktor + nSummand,iRound));
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+10/24, round(recArv.ARCVALUE10*nFaktor + nSummand,iRound));
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+11/24, round(recArv.ARCVALUE11*nFaktor + nSummand,iRound));
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+12/24, round(recArv.ARCVALUE12*nFaktor + nSummand,iRound));
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+13/24, round(recArv.ARCVALUE13*nFaktor + nSummand,iRound));
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+14/24, round(recArv.ARCVALUE14*nFaktor + nSummand,iRound));
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+15/24, round(recArv.ARCVALUE15*nFaktor + nSummand,iRound));
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+16/24, round(recArv.ARCVALUE16*nFaktor + nSummand,iRound));
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+17/24, round(recArv.ARCVALUE17*nFaktor + nSummand,iRound));
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+18/24, round(recArv.ARCVALUE18*nFaktor + nSummand,iRound));
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+19/24, round(recArv.ARCVALUE19*nFaktor + nSummand,iRound));

					commit;
				end loop;
				dLast := dRef + 19/24;		-- letzter gelesener Containersatz (sollte 20 Werte haben)

				-- Portal-Daten dazu eintragen
				for recSys in (select * from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE between dFirst and dLast) loop
					bFound := false;
					for recT in (select rowid from POR_TABVALUES where SID = nSid and FK_OBJLIST = rec.POS and REFDATE = recSys.REF_DATE) loop
						bFound := true;
						update POR_TABVALUES set VAL1 = recSys.VAL where rowid = recT.rowid;
					end loop;       
					if not bFound then                                                                                        
						insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL1) values (nSid, rec.POS, recSys.REF_DATE, recSys.VAL);
					end if;
					commit;
				end loop;
				
				-- 2. Lauf �ber TabValues - eliminieren eintr�ge bzw. ersetzen durch Texte
				bGleich := false;
				bLag1 := false; 
				bLag2 := false; 
				for recTV in (select rowid, p.* from POR_TABVALUES p where SID = nSid and FK_OBJLIST = rec.POS 
					and REFDATE<(select max(REFDATE) from POR_TABVALUES p where SID = nSid and FK_OBJLIST = rec.POS)
					order by REFDATE) loop
					-- auf L�cke pr�fen
					if recTV.VAL1 is null then
						bGleich := false;
						if bLag1 then
							if recTV.VAL2 is null then	-- beide Reihen mit L�cke
								if bLag2 then
									delete from POR_TABVALUES where rowid = recTV.rowid;
								else
									bLag2 := true;
									update POR_TABVALUES set STATUS2 = cLag where rowid = recTV.rowid;
								end if;
							else
								if bLag2 then
									bLag2 := false;                           
								else
									delete from POR_TABVALUES where rowid = recTV.rowid;
								end if;								
							end if;
						else
							bLag1 := true;
							update POR_TABVALUES set STATUS1 = cLag where rowid = recTV.rowid;
							if recTV.VAL2 is null then	-- beide Reihen mit L�cke
								bLag2 := true;
								update POR_TABVALUES set STATUS2 = cLag where rowid = recTV.rowid;
							end if;
						end if;
					else
						bLag1 := false;
						if recTV.VAL2 is null then
							bGleich := false;
							if not bLag2 then
								bLag2 := true;
								update POR_TABVALUES set STATUS2 = cLag where rowid = recTV.rowid;
							end if;
						else
							bLag2 := false;
							-- wenn Werte gleich sind -> 1. Satz mit Text "Werte gleich", Folges�tze l�schen
							if recTV.VAL1 = recTV.VAL2 then
								if bGleich then
									delete from POR_TABVALUES where rowid = recTV.rowid;
								else
									bGleich := true;
									update POR_TABVALUES set STATUS2 = cEqual where rowid = recTV.rowid;
								end if;
							else
								bGleich := false;
								update POR_TABVALUES set STATUS1 = cUnequal where rowid = recTV.rowid;
							end if;
						end if;            
					end if;
				end loop;
				commit;
			end if;
		end loop;    
		
	end if; 
	commit;
	return nSid;
exception	
	WHEN OTHERS THEN
		raise;
end;

                     
Function ARVCnt ( nTid in number, cVon in varchar2, cBis in varchar2, nAnzOk in out number, nAnzNull in out number, dLast in out date ) return date
as
     -- LIH, 26.05.2015 - Ermitteln der Anzahl Werte und Anzahl L�cken f�r Vorgabe im Stundenwertarchiv (jeweils f�r komplette Gastage) 
     -- Param:  nTid - TID des Objektes 
     --  		cVon - Datum von in der Form 'dd.mm.yy'
     --  		cBis - Datum von in der Form 'dd.mm.yy'
     --			nAnzOk, nAnzNull - Werte g�ltig und null
     --			dLast	Zeitstempel des letzten Containersatzes
     -- return  dFirst	Zeitstempel des ersten Containersatzes
     
     dVon		date := to_date(cVon, 'dd.mm.yy hh24');
     dBis		date := to_date(cBis, 'dd.mm.yy hh24');
     dVonTag	date := to_date(substr(cVon,1,8), 'dd.mm.yy');
     dBisTag	date := to_date(substr(cBis,1,8) || ' 23', 'dd.mm.yy hh24');
     dFirst		date;
     --dLast		date;
     nFirst		number;
     nLast		number;
     nVonCyc	number := date2cycle_fu(12,dVon);
     nBisCyc	number := date2cycle_fu(12,dBis);
     
begin 
	nAnzOk := 0;
	nAnzNull := 0;
	for rec in (select * from ARVVALUECO12_TA@arvdb where TID = nTid and  CYCLENR between nVonCyc and nBisCyc order by CYCLENR) loop
		if nFirst is null then
			nFirst := rec.CYCLENR;
		end if;  
		-- Es k�nnten komplette Containers�tze in der Aufzeichnung fehlen
		if nLast is not null then
			if rec.CYCLENR - nLast > 20 then
				nAnzNull := nAnzNull + rec.CYCLENR - nLast - 20;
			end if;
		end if;
		nLast := rec.CYCLENR;
		if rec.ARCVALUE0 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.ARCVALUE1 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.ARCVALUE2 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.ARCVALUE3 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.ARCVALUE4 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.ARCVALUE5 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.ARCVALUE6 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.ARCVALUE7 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.ARCVALUE8 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.ARCVALUE9 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.ARCVALUE10 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.ARCVALUE11 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.ARCVALUE12 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.ARCVALUE13 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.ARCVALUE14 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.ARCVALUE15 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.ARCVALUE16 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.ARCVALUE17 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.ARCVALUE18 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.ARCVALUE19 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
	end loop; 
	if nFirst is not null then 
		dFirst := cycle2date_fu(12,nFirst);
		dLast  := cycle2date_fu(12,nLast); 
	end if;
	return (dFirst);
--    return ('F�r TID ' || to_char(nTID) || ' wurden im Zeitbereich ' || to_char(dFirst, 'dd.mm.yy hh24') || ' bis ' || to_char(dLast, 'dd.mm.yy hh24') || ' gez�hlt: ' || to_char(nAnzOk) || ' g�ltige Werte und ' || to_char(nAnzNull) || ' Nullwert(e) (L�cken)');
end;

function R_ChkControl( cFs in varchar2, dVon in date, dBis in date, cWild in varchar2 ) return number

	-- Die Funktion greift  via dblink arvDB auf die PSIcontrol-DB zu 
	-- Sie iteriert �ber die TIS (f�r alle Datens�tze von Systemwertzeitreihen aus Fremdsystem IMP_TP), ermittelt je Zeitreihe die Anzahl Werte und Anzahl L�cken (in ARC_CONDITIION_TA)
	-- und ruft die Funktion ARVcnt im tpm-Schema auf, um die Daten der zugeh�rigen Archivspuren zu ermitteln
	-- Die Ergebnisdaten werden wie folgt eingetragen:                                                          
	--  in REPORTOBJ_TA: die Objekte und Zeitreihen (in NGA): POS - Lfd Nr, OBJLABEL - Kennzeichen, FK_TIS - TIS-ID, DESCRIPTION - Archivspur tpm, UNIT, UNIT, QUANTITY
	--  in POR_TABVALUES:   VAL1 - # Werte  in Fremdsystem-Zeitreihe
	--						VAL2 - # L�cken in Fremdsystem-Zeitreihe
	--						VAL3 - # Werte in NGA-Zeitreihe
	--						VAL4 - # L�cken in NGA-Zeitreihe
	--						VAL5 - Diff. # Werte 
	--						VAL6 - Diff. # L�cken 
	-- Verwendung von Report: R_PSI_ChkTrans
	-- Inputparameter:  cFs - Fremdsystem-Id, cvon, cBis - Zeiten, die im Portal in gesetzlicher Zeit vorgegeben werden , cWild - Objektkennzeichenvorgabe als Wildcard (ohne %)
	-- Return: SID
as
	cError	varchar2(1024) 	:= null;
	bError	boolean 		:= FALSE;      
--	dVon	date			:= psi_pckg.UTCtime(to_date(cVon, DATE_FORMAT_GER));
--	dBis	date			:= psi_pckg.UTCtime(to_date(cBis, DATE_FORMAT_GER));
	nSid	number;
	iLfdnr	integer	:= 0; 
	cDataSrc	varchar2(12) := 'DD_SYS';
	cArchivtyp  varchar2(1)  := '1';
	cPict	REPOBJ.PICTURE%type;
	cDesc	varchar2(100);
	cRet	varchar2(100) := 'ERROR - no data found';
	cText	varchar2(180);
	cLab1	varchar2(20); 
	cDefUnit	varchar2(50);
	cShort	QUANT.SHORT_NAME%type;  
    nAnzOk		number	 := 0;
    nAnzNull 	number	 := 0;
    nNgaOk		number	 := 0;
    nNgaNull 	number	 := 0; 
    nUeber		number;
    nTid		number;
    dMin		date;
    dMax		date;            
    cVon		varchar2(20) := to_char(dVon,'dd.mm.yy');
    cBis		varchar2(20) := to_char(dBis,'dd.mm.yy');
    dsys		date := sysdate;
    cZrKnz		varchar2(80); 
    dFirst		date;
    dLast		date;
    cAb			varchar2(20);
    dUntil		date;
    dAb			date;
	
	pragma autonomous_transaction;  -- necessary because of insert or deletes while the function is used in a select                                                 
	
begin                                         
	
	psireports.execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
    psireports.execsqldirect( 'alter session set nls_numeric_characters = '',.''');                       
	--psireports.execsqldirect( 'alter session set NLS_LANG=German_Germany.AL32UTF8');
	select userenv('sessionid') into nSid from dual;
	
	-- Reportgruppe und -objekte erzeugen   
	psireports.delsid( nSid ); 
	
	insert into REPORTGRP_TA (SID, REPNAME, ERSTELLT_AM) values( nSid, 'R_PSI_ChkControl', sysdate );

	-- f�r alle Zeitreihen von PSIcontrol
	for recTis in (select objlabel(fk_obj) LABEL, TIS.* from TIS where ARCHIVETYPE='1' and TIME_LEVEL='H' and FK_FS= cFs and (cWild is null or cWild='*' or objlabel(fk_obj) like '%' || cWild || '%' ) order by LABEL) loop
		iLfdNr := iLfdNr + 1;
		--cPict  := Getpict(recTis.ID);		
		insert into REPORTOBJ_TA  (SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, UNIT, QUANTITY, FK_OBJID)
			values (nSid, 0, iLfdNr, '0',recTis.ID, '', '', recTis.LABEL, '', recTis.LABEL, recTis.UNIT, recTis.QUANTITY, recTis.FK_OBJ );
		commit;
	end loop;   
	
	if iLfdNr > 0 then
		for rec in (select * from REPORTOBJ_TA where SID = nSid order by POS) loop
			--select SHORT_NAME into cShort from QUANT where ID = rec.QUANTITY;
			cShort := rec.QUANTITY;
			if substr(cShort,length(cShort),1) = '_' then
				cShort := substr(cShort,1,length(cShort)-1);
			end if;
			cZrKnz := rec.OBJLABEL || '.' || cShort ;  
			cAb := cVon;
			for recTis in (select FROM_DATE_TS, UNTIL_DATE_TS from TIS where ID = rec.FK_TIS) loop
				if recTis.FROM_DATE_TS > dVon then
					cAb := to_char(recTis.FROM_DATE_TS,'dd.mm.yy');
				end if;
				dAb := recTis.FROM_DATE_TS;                       
				dUntil := recTis.UNTIL_DATE_TS;
			end loop;
			
			-- Werte in arv ermitteln    - dazu TID aus Fremdschl�sseltabelle holen
			for recFk in (select FOREIGNKEY from por.MTR_FK_TA where FK_OBJID = rec.FK_OBJID) loop
				nTid := to_number(recFk.FOREIGNKEY);
				dFirst := arvcnt(nTid, cAb, cBis, nAnzOk, nAnzNull, dLast);
			end loop;
			dLast := dLast + 19/24;		-- letzter gelesener Containersatz (sollte 20 Werte haben)

			-- wenn dUntil < dVon, dann entf�llt Nachkorrektur
			if dUntil > dVon then
				-- wenn dLast > dUntil -> dann k�nnten noch Werte nacherzeugt worden sein -> nAnzOk reduzieren (sofern wir nahe sysdate sind)
				--   sonst  werden keine Werte mehr geliefert, d.h. es wurden zuviele L�cken gez�hlt
				if dLast > dUntil then
					nUeber := (dLast - dUntil) * 24;
					if dLast < sysdate  then
						if nAnzNull > 0 then
							nAnzNull := nAnzNull - nUeber;
						end if;
					else 
						nAnzOk := nAnzOk - nUeber;
					end if;
				end if;
				-- wenn dFirst < dAb -> dann wird erwartet, dass L�cke ab dFirst bis dAb vorliegt, d.h. die # L�cken wird reduziert
				if dFirst < dAb then
					nUeber := (dAb - dFirst) * 24;
					nAnzNull := nAnzNull - nUeber;
				end if;
			end if;
			-- Werte in NGA ermitteln
			nNgaOk := 0;
			select min(REF_DATE),  max(REF_DATE), count(*) into dMin, dMax, nNgaOk from DD_SYS where FK_TIS=rec.FK_TIS and REF_DATE >= dFirst and REF_DATE <= dLast;
			--  in POR_TABVALUES:   VAL1 - # Werte  in Fremdsystem-Zeitreihe
			--						VAL2 - # L�cken in Fremdsystem-Zeitreihe
			--						VAL3 - # Werte in NGA-Zeitreihe
			--						VAL4 - # L�cken in NGA-Zeitreihe
			--						VAL5 - Diff. # Werte 
			--						VAL6 - Diff. # L�cken
			-- L�cken in NGA
			select count(*) into nNgaNull from por.MTR_ARC_CONDITION_TA  where FK_TIS=rec.FK_TIS and REF_DATE >= dFirst and REF_DATE <= dLast  and CONDITION='M' ;
			
			if dFirst is not null then
				insert into POR_TABVALUES(SID, FK_OBJLIST, REFDATE, REFCDATE, VAL1, VAL2, VAL3, VAL4, VAL5, VAL6)
						values( nSid, rec.POS, dFirst, to_char(dLast,'dd.mm.yy hh24:mi'), nAnzOk, nAnzNull, nNgaOk, nNgaNull, nAnzOk-nNgaOk,nAnzNull-nNgaNull);
				commit;
			end if;
		end loop; 

	end if; 
	commit;
	return (nSid);
exception	
	WHEN OTHERS THEN
		raise;
end;

 
end; -- body ReportPckg
/
