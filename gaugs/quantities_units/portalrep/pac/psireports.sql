create or replace Package PSIReports AUTHID DEFINER
is 
	-- LIH 17.06.2015 R_TP_9List_SID - zur Ausgabe von TP-Objekt-Daten
	-- LIH 01.06.2015 R_ChkControl und R_ChkTrans in gesonderte packages (arvpckg und tpmpckg extrahiert)
	-- LIH 26.05.2015 Neu: R_ChkControl
	-- LIH 22.05.2015 Neu: R_ChkTrans    
	-- LIH 18.02.2015 R_TsVersions - Ausgabe SRC_MODDate mit UTC2 MESZ
	-- LIH 17.02.2015 Neu: R_TsVersions - Ausgabe der n letzten Datenversionen
	-- LIH 31.07.2014 R_9List_SID: Wenn 1. Position keine Daten hat, wird jetzt erste Pos. mit Daten gesucht (sonst gab es keinen output)
	-- LIH 18.06.2014 R_9List_SID: Verbesserung bzgl. cDefUnit
	-- LIH 26.05.2014 Find_bad_tis erweitert: find Zeitreihen, deren Kopfattribute von den Spartenvorgaben abweichen
	-- LIH 22.05.2014 anpassungen in R_Disturbed_Data                    (Param.�nderung)
	-- LIH 21.05.2014 Neu:  R_Disturbed_Data                    
	-- LIH 09.05.2014 Find_bad_tis erweitert: finde Objekte mit mehreren Sys-ZR mit gleicher Gr��e, Einheit, Zeitstufe ..
	-- LIH 11.04.2014 Find_bad_tis - erweitert um Systemzeitreihen (ohne ORI-Bezug und seit >2 Tagen nicht aktualisiert)
	-- LIH 10.04.2014 Neu: Find_bad_tis - finde und protokolliere Zeitreihenfehler
	-- LIH 21.02.2014 R9_List_Sid - Funktion r9_list mit R�ckgabe der SID
	-- LIH 08.01.2013 R9_List - Korrektur f�r ARCHIVTYP  (Abfrage auf '1' statt auf 'S') und Ermittlung Standardeinheit korrigiert
	-- LIH 07.01.2013 R_9List - Eintrag erstellt_am f�r Reportgruppe erg�nzt
	-- LIH 18.06.2013 R_9List verbessert, um ung�ltige Zeitreihen wegzufiltern
	-- LIH 08.05.2013 R_9List optimiert
	-- LIH 07.05.2013 Ersterstellung
/*	                                
	Diese package enth�lt universelle Reports, die PSI bereitstellen kann
	
	Die folgenden Reports werden abgedeckt:
	1. R_9List  1 Tabelle, aufgebaut aus Zeitreihendaten einer Objektliste mit max. 9 Objekten
	   Parameter: 3 Objektliste, jeweils mit ID �bergeben
	   			  Datum - Gastag
	   			  
	2. R_GAS_H - Stundenprotokoll Gas

*/	

	TYPE OUTHEAD is table of PTABHEAD%rowtype;
	type array_of_nums is table of number index by binary_integer;
	

	function Find_bad_tis( cMandant in varchar2 default null, nH_Ausbleibend in varchar2 default 0, cFehlerhaft in varchar2 default 'J' ) return number;
	function R_9List( nObjlistId number, dVon in date, dBis in date, cTimelevel in varchar2, cSource in varchar2 default 'O' ) return OUTHEAD pipelined;
--	function R_9List( nObjlistId number, dVon in date, dBis in date, cTimelevel in varchar2, cSource in varchar2 default 'O' ) return varchar2;
	function R_TP_9List_SID( nObjlistId number, dVon in date, dBis in date, cTimelevel in varchar2, cSource in varchar2 default 'O' ) return number;
	function R_9List_SID( nObjlistId number, dVon in date, dBis in date, cTimelevel in varchar2, cSource in varchar2 default 'O' ) return number;
	function R_Lastversions( nObjId number, dVon in date, dBis in date, cTimelevel in varchar2, cSource in varchar2 default 'O' ) return number;
	function R_TsVersions( nTsId number, dVon in date, dBis in date ) return number;
	function R_Disturbed_Data( nObj_Id_Parent in number, dVon in date, dBis in date) return number;
 	
	function GetDateTime return varchar2;	-- gibt aktuelle Datum und Zeit in der Form dd.mm.yyyy hh24:mi zur�ck		
	function GetMin( cTimelevel in varchar2) return number;

	procedure ExecSqlDirect( cStmt IN varchar2 );
	procedure DelSid( nSid in number );
end;
/

create or replace Package body PSIReports
is
	-- �nderungsinfos siehe Header

---------------------------------------------------------------------------------------------

	DATE_FORMAT_GER 				constant varchar2(21) := 'DD.MM.YYYY HH24:MI:SS';

---------------------------------------------------------------------------------------------
procedure ExecSqlDirect( cStmt IN varchar2 )
AS
-- LIH 07.05.2010 Eliminieren dbms_sql / Umstellen auf execute immediate
BEGIN
	execute immediate cStmt;
EXCEPTION
	WHEN OTHERS THEN
		raise;
END;

function IVjeTag( cTL in varchar2 ) return integer
as
	iAnz integer;
begin        
	if cTL = 'MI' then
		iAnz := 24*60;
	elsif cTL = 'MI15' then
		iAnz := 96;
	elsif cTL = 'H' then
		iAnz := 24;
	elsif cTL = 'D' then
		iAnz := 1;
	end if;
	return (iAnz);
end;

procedure Delete_Sessiondata ( nSid in number, cTable in varchar2 )
is
    nSession	number;
    cStmt     varchar2(200);
    pragma autonomous_transaction;                                                  
begin
    cStmt := 'delete from ' || cTable || ' where SID = ' || to_char(nSid);
    ExecSqlDirect( cStmt );
    commit;
end;

procedure DelSid( nSid in number )
as
	-- L�schen der SID-bezogenen Daten in allen Reportuser-Tabellen
begin
	delete from PTABHEAD where SID=nSid;
	delete from PTABVAL where SID=nSid;
	delete from REPORTOBJ_TA where SID=nSid;
	delete from REPORTGRP_TA where SID=nSid;
	delete from POR_BAD_TIS where SID=nSid;
end;	

function Text4Attr( nObjId in number, cAttribut in varchar2 ) return varchar2
as
	cText MD_VARIABLES.VALUE%type;
begin
	for recVar in (select VALUE from MD_VARIABLES where OBJID = nObjId and ATTR_NAME = cAttribut) loop
		cText := recVar.VALUE;
	end loop;
	return cText;
end;
		
function ReturnWert( bError in boolean, cError in varchar2 )
return varchar2
as
begin
	if bError then
		return cError;
	else
		return 'OK';
	end if;
end;

function GetDateTime return varchar2
	-- gibt aktuelle Datum und Zeit in der Form dd.mm.yyyy hh24:mi zur�ck		
as
begin
		return to_char(sysdate, 'dd.mm.yyyy hh24:mi');
end;

function GetMin( cTimelevel in varchar2) return number
as                  
	nMin	number;
begin              
	if cTimelevel = 'S' then
		nMin := 1/60;
	elsif cTimelevel = 'MI3' then
		nMin := 3;
	elsif cTimelevel = 'MI5' then
		nMin := 5;
	elsif cTimelevel = 'MI15' then
		nMin := 15;
	elsif cTimelevel = 'H' then
		nMin := 60;
	elsif cTimelevel = 'H2' then
		nMin := 120;
	elsif cTimelevel = 'D' then
		nMin := 24*60;
	elsif cTimelevel = 'M' then
		nMin := 1440*30;
	elsif cTimelevel = 'Q' then
		nMin := 1440*90;
	elsif cTimelevel = 'Y' then
		nMin := 1440*365;
	else                
		nMin := 1;
	end if;
	return nMin;
end;

  
function GetPict( nTis in number ) return varchar2
as
	cPict REPOBJ.PICTURE%type := '999G999G990';
	cDec	varchar2(20) := '99999999999999999999';
	cUnitId	UNITS.ID%type;
begin
	select UNIT into cUnitId from TIS where ID = nTis;
	-- Standard verwenden - Quantity->UNIT  (PK auf ID+VERS
	for recUni in (select PRECISION from UNITS where ID = cUnitId ) loop
		if recUni.PRECISION > 0 then		
			cPict := cPict || 'D' ||  substr(cDec,1,recUni.PRECISION);
		end if;
	end loop;
	
	return cPict;
end;	

function GenTisHeaderCols( nSid in number, nObjlist in number )	return varchar2
	-- erzeugt Header-Zeile f�r Zeitreihen-Reporttabelle zur Session-ID; R�ckgabe: OK oder ERROR
as	
	bError	boolean 		:= FALSE;   
	cError	varchar2(1024) 	:= null;
	bFound	boolean 		:= FALSE;   
	cStmt	varchar2(1000);
	cCols	varchar2(400) := '';
	cSets	varchar2(400) := '';
begin
	-- sucht sich zu den Zeitreihen alle Spalteninfos f�r die Header-Zeilen und speichert sie in DB-Tabelle TABHEAD_TA
	-- 1. Headerzeile: Shortname od description
	-- 2. Headerzeile: quantity[unit]
                           
	-- die Objekte aus REPOBJ verarbeiten              
	for recGrp in ( select * from REPGRP where SID = nSid ) loop
		for recObj in (select POS,FK_TIS,SHORT_NAME,DESCRIPTION,TIS.QUANTITY,TIS.UNIT from REPOBJ, TIS where SID = recGrp.SID and FK_TIS=TIS.ID order by POS) loop
			-- �ber TIS die Daten laden
			-- in PTABHEAD werden f�r n Zeitreihen einer Obj.liste je zwei Headerzeilen erzeugt: S1Z1, S1Z2 , dito f�r Spalte 2-9
			-- Zeile 1: Kurzname, Zeile 2: Gr��e [Einheit]
			bFound := true;
			if recObj.POS < 10 then
				cCols := cCols || 'S'||recObj.POS || 'Z1,S'|| recObj.POS || 'Z2,';
				cSets := cSets || '''' || recObj.SHORT_NAME || ''',''' || recObj.QUANTITY || '[' || recObj.UNIT || ']'',';
			end if;
		end loop;
		if bFound then
			begin
				cStmt := 'insert into PTABHEAD (SID, FK_OBJLIST,' || substr(cCols,1, length(cCols)-1) ||
					 ',REPNAME) values(' || nSid || ','  || nObjlist || ','  || substr(cSets,1, length(cSets)-1) || ',''' || recGrp.REPNAME || ''')';
			    ExecSqlDirect( cStmt );
			    commit;
			exception    
				when others then
					cError := sqlerrm;
					bError := true;
			end;	
		else
			cError := 'Gruppe '|| recGrp.REPNAME || ' hat keine zugeordneten Zeitreihenobjekte!';
			bError := true;
		end if;			
	end loop;

	return ReturnWert( bError, cError );
end;	

function GenTisValues( nSid in number, nObjlist in number, cVon in varchar2, cBis in varchar2, cDataSrc in varchar2 )	return varchar2
	-- erzeugt Zeilen in Zeitreihen-Reporttabelle zur Session-ID; R�ckgabe: OK oder ERROR
as	
	bError	boolean 		:= FALSE;   
	cError	varchar2(1024) 	:= null;
	bFound	boolean 		:= FALSE;   
	cStmt	varchar2(4000);
	cCols	varchar2(1500) := '';
	cSel	varchar2(1500) := '';
	cWhere	varchar2(3000) := '';
	cFrom	varchar2(1000) := ''; 
	iC		integer;
	iCursorName 	integer; 
	iRowsProcessed	integer;
	iCount 			integer;
	nFirstPos		number := 0;
	
begin
	-- erh�lt die gleiche zu erzeugende Session-ID sowie Von und Bis-Zeit f�r die Datenselektion
	-- holt sich aus Tabelle REPORTGRP_TA und REPOBJ die Objekte zur Gruppe und baut dynamisch die Datenselektion aus cDataSrc (DD_ORIGINAL oder DD_SYSTEM)
	-- auf, f�hrt sie aus und schreibt das Ergebnis in Tabelle TABVALUES_TA
	execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
                           
	-- die Objekte aus REPOBJ verarbeiten              
	for recGrp in ( select * from REPGRP where SID = nSid ) loop 
		-- pr�fen, ob f�r 1. Element Daten existieren
		for recObj in (select POS,FK_TIS,VALUE_CATEGORY from REPOBJ where SID = recGrp.SID order by POS) loop
			cStmt := 'begin select count(*) into :iCount from ' || cDataSrc || 
					' where FK_TIS=' || recObj.FK_TIS || ' and REF_DATE between ''' || cVon || ''' and ''' || cBis || '''; end;';
		    iCursorName := dbms_sql.open_cursor;
		    dbms_sql.parse(iCursorName, cStmt, dbms_sql.v7);
		    dbms_sql.bind_variable(iCursorName, ':iCount', iCount);
		    iRowsProcessed := dbms_sql.execute(iCursorName);
		    dbms_sql.variable_value( iCursorName, 'iCount', iCount );
			dbms_sql.close_cursor(iCursorName);
			if iCount > 0 then
				nFirstPos := recObj.POS;
				exit;
			end if;  
		end loop;
		if nFirstPos > 0 then
			for recObj in (select POS,FK_TIS,VALUE_CATEGORY from REPOBJ where SID = recGrp.SID and POS >= nFirstPos order by POS) loop
				-- �ber TIS die Daten laden
				-- in <cDataSrc> stehen die Zeitreihenwerte  (select between cVon and cBis - liefert alle Werte >=cVon und <= cBis
				-- Aufbau eines outer-join 
				bFound := true;
				cCols := cCols || 'VAL'||recObj.POS || ',' || 'STATUS'||recObj.POS || ',';
				cSel := cSel || 'a'||recObj.POS || '.VAL,' || 'a'||recObj.POS || '.CONDITION,';
				cFrom := cFrom || cDataSrc || ' a'||recObj.POS || ',';                             
				if recObj.POS = nFirstPos then
					cWhere := cWhere || 'a'||recObj.POS || '.FK_TIS=' || recObj.FK_TIS || ' and a'||recObj.POS || '.REF_DATE between ''' || cVon || ''' and ''' || cBis || '''';
				else
					cWhere := cWhere || ' and a'||recObj.POS || '.FK_TIS(+)=' || recObj.FK_TIS || ' and a'|| nFirstPos || '.REF_DATE=a'||recObj.POS || '.REF_DATE(+)' ||
						' and a'||recObj.POS || '.REF_DATE(+) between ''' || cVon || ''' and ''' || cBis || '''';
				end if;
			end loop;
			if bFound then
				begin
					cStmt := 'insert into PTABVAL (SID,FK_OBJLIST,REFDATE,' || substr(cCols,1, length(cCols)-1) || 
							 ') (select ' || recGrp.SID || ',' || nObjlist || ',a' || nFirstPos || '.REF_DATE,'  || substr(cSel,1, length(cSel)-1) || 
							 ' from ' || substr(cFrom,1, length(cFrom)-1) || 
							 ' where '|| cWhere || ')';
				    ExecSqlDirect( cStmt );
				    commit;
				    -- Erg�nze Mittelwerte oder Summenwerte
				    cSel := '';
					for recObj in (select POS,FK_TIS,VALUE_CATEGORY from REPOBJ where SID = recGrp.SID and POS >= nFirstPos order by POS) loop
						if recObj.VALUE_CATEGORY = 'M' then		-- Messwert => average bilden
							cSel := cSel || '(select avg(VAL' || recObj.POS || ') from PTABVAL where SID=' ||  recGrp.SID || '),''V'',';
						else
							cSel := cSel || '(select sum(VAL' || recObj.POS || ') from PTABVAL where SID=' ||  recGrp.SID || '),''V'',';
						end if;
					end loop;
					cStmt := 'insert into PTABVAL (SID,FK_OBJLIST,REFDATE,' || substr(cCols,1, length(cCols)-1) || ') values (' ||  recGrp.SID 
						|| ',' || nObjlist || ',sysdate+365,'  || substr(cSel,1, length(cSel)-1) || ')';
				    ExecSqlDirect( cStmt );
				    commit;
				exception    
					when others then
						cError := sqlerrm;
						bError := true;
				end;	
			else
				cError := 'Gruppe '|| recGrp.REPNAME || ' hat keine zugeordneten Zeitreihenobjekte!';
				bError := true;
			end if;
		end if;			
	end loop;

	return ReturnWert( bError, cError );
end;	



function R_9List( nObjlistId number, dVon in date, dBis in date, cTimelevel in varchar2, cSource in varchar2 default 'O' ) return OUTHEAD pipelined
--function R_9List( nObjlistId number, dVon in date, dBis in date, cTimelevel in varchar2, cSource in varchar2 default 'O' ) return varchar2

	-- �bertr�gt bis zu 9 Objekte der vorgeg. Liste in Report-Objektliste und erzeugt Header-Info und Zeitreihen-Daten f�r Zeitraum cVon und cBis
	-- holt die Werte aus cSource = 'O' - dd_original oder 'S' - dd_system
	--
	-- Inputparameter:  cvon, cBis - Zeiten, die im Portal in gesetzlicher Zeit vorgegeben werden 
as
	cError	varchar2(1024) 	:= null;
	bError	boolean 		:= FALSE;      
--	dVon	date			:= psi_pckg.UTCtime(to_date(cVon, DATE_FORMAT_GER));
--	dBis	date			:= psi_pckg.UTCtime(to_date(cBis, DATE_FORMAT_GER));
	nSid	number;
	iLfdnr	integer	:= 0; 
	cDataSrc	varchar2(12) := 'DD_ORIGINAL';
	cArchivtyp  varchar2(1)  := '4';
	cPict	REPOBJ.PICTURE%type;
	cDesc	varchar2(100);
	cRet	varchar2(100) := 'ERROR - no data found';
	cText	varchar2(80);
	cLab1	varchar2(20); 
	cDefUnit	varchar2(50);
	pragma autonomous_transaction;  -- necessary because of insert or deletes while the function is used in a select                                                 
	
begin                                         
	if cSource = 'S' then
		cDataSrc := 'DD_SYS';
		cArchivtyp := '1';
	end if;
	
	execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
    execsqldirect( 'alter session set nls_numeric_characters = '',.''');                       
	--execsqldirect( 'alter session set NLS_LANG=German_Germany.AL32UTF8');
	select userenv('sessionid') into nSid from dual;
	
	-- Reportgruppe und -objekte erzeugen   
	DelSid( nSid ); 
	
	for rec in (select distinct LIST_SHORT_NAME, DESCRIPTION from MD_OBJLIST where ID = nObjlistId) loop
		insert into REPORTGRP_TA (SID, REPNAME, ERSTELLT_AM) values( nSid, rec.LIST_SHORT_NAME || '  ' || rec.DESCRIPTION, sysdate );
	end loop;

	-- f�r alle Objekte einer Liste werden die Zeitreihen-Daten geholt und in REPORTOBJ_TA gespeichert
	for rec in (select POSITION, FK_OBJID, SHORT_NAME, LABEL from MD_OBJLIST where ID = nObjlistId order by POSITION) loop
		-- Variablentext R_9R_NAME holen oder den Namen der Station
		cText :=   rec.SHORT_NAME;            
		for recVar in (select VALUE from MD_VARIABLES where OBJID = rec.FK_OBJID and ATTR_NAME='R_9_NAME') loop
			cText := recVar.VALUE;
		end loop;
		
		-- Zeitreihen: max 9
		if rec.POSITION < 10 then
			for recTis in (select ID, UNIT, QUANTITY, FK_OBJ, VALUE_CATEGORY from TIS
				where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = cTimelevel and TIS.ARCHIVETYPE=cArchivtyp and 
					(instr(TIS.QUANTITY,'.',1,1)=0 or instr(TIS.QUANTITY,'.AVG',1,1)>0 ) )
			loop
				if cArchivtyp = '1' then
					-- nur die Archive der Standardzeitstufe bei Archivtyp S ber�cksichtigen
					if  instr(recTIS.QUANTITY,'.',1,1) = 0 then
						select DEFAULT_UNIT into cDefUnit from QUANT where SHORT_NAME = recTis.QUANTITY;
					else
						cDefUnit := recTis.UNIT;
						for recQ in (select DEFAULT_UNIT from QUANT where SHORT_NAME = substr(recTIS.QUANTITY,1, instr(recTIS.QUANTITY,'.',1,1)-1)) loop
							cDefUnit := recQ.DEFAULT_UNIT;
						end loop;
					end if;
					if cDefUnit <> recTis.UNIT then
						continue;	-- Archiv nicht ber�cksichtigen (da veraltet)
					end if;
				end if;
				if recTis.QUANTITY is not null then
					--iLfdNr := rec.POSITION;
					-- es muss sichergestellt werden, dass f�r die erste Position auch eine Zeitreihe existiert
					iLfdNr := iLfdNr + 1;
					cPict  := Getpict(recTis.ID);		
					insert into REPORTOBJ_TA  (SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, UNIT, QUANTITY, FK_OBJID)
						values (nSid, nObjlistId,iLfdNr, '0',recTis.ID, cPict, cText, rec.LABEL, recTis.VALUE_CATEGORY, rec.LABEL, recTis.UNIT, recTis.QUANTITY, rec.FK_OBJID );
				end if;
			end loop;
		end if;
		commit;
	end loop;   
	
	if iLfdNr > 0 then
		-- Tabellenheader erzeugen - 2 zeilig: 1. Zeile: Obj-Shortname, 2. Zeile: Quantity[unit]
		cRet := GenTisHeaderCols( nSid, nObjlistId );             
		        
		if cRet = 'OK' then
			-- Tabellenwerte erzeugen
			cRet := GenTisValues( nSid, nObjlistId, to_char(dVon, DATE_FORMAT_GER), to_char(dBis, DATE_FORMAT_GER), cDataSrc ); 
			if cRet = 'OK' then
				commit;
				-- Beschreibung der Objektliste und Headerzeile zur�ckgeben
				-- select SID, FK_OBJLIST, S1Z1, S1Z2, S1Z3, S2Z1, S2Z2, S2Z3, S3Z1, S3Z2, S3Z3, S4Z1, S4Z2, S4Z3, S5Z1, ..
				for r in (select * from PTABHEAD where SID = nSid) loop
					pipe row( r );
				end loop;
				--Delete_Sessiondata( nSid, 'TABHEAD' );
				--*/
				--return 'Ok';		-- f�r Debug-Test
			end if;
			
		end if;
	end if;
	--return 'Error';
exception	
	WHEN OTHERS THEN
		raise;
end;

function R_9List_SID( nObjlistId number, dVon in date, dBis in date, cTimelevel in varchar2, cSource in varchar2 default 'O' ) return number

	-- �bertr�gt bis zu 9 Objekte der vorgeg. Liste in Report-Objektliste und erzeugt Header-Info und Zeitreihen-Daten f�r Zeitraum cVon und cBis
	-- holt die Werte aus cSource = 'O' - dd_original oder 'S' - dd_system
	--
	-- Inputparameter:  cvon, cBis - Zeiten, die im Portal in gesetzlicher Zeit vorgegeben werden 
	-- Return: SID
as
	cError	varchar2(1024) 	:= null;
	bError	boolean 		:= FALSE;      
--	dVon	date			:= psi_pckg.UTCtime(to_date(cVon, DATE_FORMAT_GER));
--	dBis	date			:= psi_pckg.UTCtime(to_date(cBis, DATE_FORMAT_GER));
	nSid	number;
	iLfdnr	integer	:= 0; 
	cDataSrc	varchar2(12) := 'DD_ORIGINAL';
	cArchivtyp  varchar2(1)  := '4';
	cPict	REPOBJ.PICTURE%type;
	cDesc	varchar2(100);
	cRet	varchar2(100) := 'ERROR - no data found';
	cText	varchar2(80);
	cLab1	varchar2(20); 
	cDefUnit	varchar2(50);
	pragma autonomous_transaction;  -- necessary because of insert or deletes while the function is used in a select                                                 
	
begin                                         
	if cSource = 'S' then
		cDataSrc := 'DD_SYS';
		cArchivtyp := '1';
	end if;
	
	execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
    execsqldirect( 'alter session set nls_numeric_characters = '',.''');                       
	--execsqldirect( 'alter session set NLS_LANG=German_Germany.AL32UTF8');
	select userenv('sessionid') into nSid from dual;
	
	-- Reportgruppe und -objekte erzeugen   
	DelSid( nSid ); 
	
	for rec in (select distinct LIST_SHORT_NAME, DESCRIPTION from MD_OBJLIST where ID = nObjlistId) loop
		insert into REPORTGRP_TA (SID, REPNAME, ERSTELLT_AM) values( nSid, rec.LIST_SHORT_NAME || '  ' || rec.DESCRIPTION, sysdate );
	end loop;

	-- f�r alle Objekte einer Liste werden die Zeitreihen-Daten geholt und in REPORTOBJ_TA gespeichert
	for rec in (select POSITION, FK_OBJID, SHORT_NAME, LABEL from MD_OBJLIST where ID = nObjlistId order by POSITION) loop
		-- Variablentext R_9R_NAME holen oder den Namen der Station
		cText :=   rec.SHORT_NAME;            
		for recVar in (select VALUE from MD_VARIABLES where OBJID = rec.FK_OBJID and ATTR_NAME='R_9_NAME') loop
			cText := recVar.VALUE;
		end loop;
		
		-- Zeitreihen: max 9
		if rec.POSITION < 10 then
			for recTis in (select ID, UNIT, QUANTITY, FK_OBJ, VALUE_CATEGORY from TIS
				where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = cTimelevel and TIS.ARCHIVETYPE=cArchivtyp and 
					(instr(TIS.QUANTITY,'.',1,1)=0 or instr(TIS.QUANTITY,'.AVG',1,1)>0 ) )
			loop
				if cArchivtyp = '1' then
					-- nur die Archive der Standardzeitstufe bei Archivtyp S ber�cksichtigen
					if  instr(recTIS.QUANTITY,'.',1,1) = 0 then
						select DEFAULT_UNIT into cDefUnit from QUANT where SHORT_NAME = recTis.QUANTITY;
					else
						cDefUnit := recTis.UNIT;
						for recQ in (select DEFAULT_UNIT from QUANT where SHORT_NAME = substr(recTIS.QUANTITY,1, instr(recTIS.QUANTITY,'.',1,1)-1)) loop
							cDefUnit := recQ.DEFAULT_UNIT;
						end loop;
					end if;
					select SHORT_NAME into cDefUnit from UNITS where ID = cDefUnit;
					if cDefUnit <> recTis.UNIT and recTis.QUANTITY <> 'NA' then
						continue;	-- Archiv nicht ber�cksichtigen (da veraltet)
					end if;
				end if;
				if recTis.QUANTITY is not null then
					--iLfdNr := rec.POSITION;
					-- es muss sichergestellt werden, dass f�r die erste Position auch eine Zeitreihe existiert
					iLfdNr := iLfdNr + 1;
					cPict  := Getpict(recTis.ID);		
					insert into REPORTOBJ_TA  (SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, UNIT, QUANTITY, FK_OBJID)
						values (nSid, nObjlistId,iLfdNr, '0',recTis.ID, cPict, cText, rec.LABEL, recTis.VALUE_CATEGORY, rec.LABEL, recTis.UNIT, recTis.QUANTITY, rec.FK_OBJID );
				end if;
			end loop;
		end if;
		commit;
	end loop;   
	
	if iLfdNr > 0 then
		-- Tabellenheader erzeugen - 2 zeilig: 1. Zeile: Obj-Shortname, 2. Zeile: Quantity[unit]
		cRet := GenTisHeaderCols( nSid, nObjlistId );             
		        
		if cRet = 'OK' then
			-- Tabellenwerte erzeugen
			cRet := GenTisValues( nSid, nObjlistId, to_char(dVon, DATE_FORMAT_GER), to_char(dBis, DATE_FORMAT_GER), cDataSrc ); 
		end if;
	end if;
	return nSid;
exception	
	WHEN OTHERS THEN
		raise;
end;

function GenDataversions( nSid in number, nObjId in number, dVon in date, dBis in date, cDataSrc in varchar2 )	return varchar2
as
	bError	boolean 		:= FALSE;   
	cError	varchar2(1024) 	:= null;
	bFound	boolean 		:= FALSE;   
	cStmt	varchar2(4000);
	cCols	varchar2(1500) := '';
	cSel	varchar2(1500) := '';
	cWhere	varchar2(3000) := '';
	cFrom	varchar2(1000) := ''; 
	iC		integer;
	iCursorName 	integer; 
	iRowsProcessed	integer;
	iCount 			integer;
	nFirstPos		number := 0;
	dLast			date;
	
begin
	-- erh�lt die gleiche zu erzeugende Session-ID sowie Von und Bis-Zeit f�r die Datenselektion
	-- holt sich aus Tabelle REPORTGRP_TA und REPOBJ die Objekte zur Gruppe und baut dynamisch die Datenselektion aus cDataSrc (DD_ORIGINAL oder DD_SYSTEM)
	-- auf, f�hrt sie aus und schreibt das Ergebnis in Tabelle TABVALUES_TA
	execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
	-- hole die letzten 9 Datenversionen zu einem Zeitstempel (wenn vorhanden) und schreibe die DAten in PTABVAL
	
	for rec in (select * from REPORTOBJ_TA where SID = nSid and FK_OBJLIST=0) loop
		dLast := null;
		for recArc in (select REF_DATE, VAL, VERS_DATE, SRC_MODDATE from por.MTR_ARC1_RT_TA   where FK_TIS = rec.FK_TIS and REF_DATE between dVon and dBis order by REF_DATE asc,SRC_MODDATE desc) loop
			-- eintragen in PTABVAL - VAL<x> und SRC:MODDATE in STATUS1
			if recArc.REF_DATE <> dLast or dLast is null then
				-- neuer Satz
				insert into  PTABVAL (SID,FK_OBJLIST,REFDATE, VAL1, STATUS1)
					values  ( nSid,0, recArc.REF_DATE, recArc.VAL, to_char(recArc.SRC_MODDATE,'dd. hh24:mi:ss') );
				dLast := recArc.REF_DATE;
				iC := 1;
				commit;
			else        
				iC := iC + 1;
				cStmt := 'update PTABVAL set VAL' || to_char(iC) || '= ' || to_char(recArc.VAL) || ', STATUS'|| to_char(iC) || '= ''' ||  to_char(recArc.SRC_MODDATE,'dd. hh24:mi:ss') ||
					''' where SID = ' || nSid || ' and FK_OBJLIST=0 and REFDATE = to_date(''' || to_char(dLAST,'dd.mm.yyyy hh24:mi:ss') || ''',''dd.mm.yyyy hh24:mi:ss'')';
				
				begin
				    ExecSqlDirect( cStmt );
				    commit;
				exception    
					when others then
						cError := sqlerrm;
						bError := true;
				end;
			end if;
		end loop;	
	end loop;		
	return ReturnWert( bError, cError );
end;

function GenVersions( nSid in number, dVon in date, dBis in date, cDataSrc in varchar2 )	return varchar2
as
	bError	boolean 		:= FALSE;   
	cError	varchar2(1024) 	:= null;
	bFound	boolean 		:= FALSE;   
	cStmt	varchar2(4000);
	cCols	varchar2(1500) := '';
	cSel	varchar2(1500) := '';
	cWhere	varchar2(3000) := '';
	cFrom	varchar2(1000) := ''; 
	iC		integer;
	iCursorName 	integer; 
	iRowsProcessed	integer;
	iCount 			integer;
	nFirstPos		number := 0;
	dLast			date;
	cModdate		varchar2(20);
	
begin
	-- erh�lt die gleiche zu erzeugende Session-ID sowie Von und Bis-Zeit f�r die Datenselektion
	-- holt sich aus Tabelle REPORTGRP_TA und REPOBJ die Objekte zur Gruppe und baut dynamisch die Datenselektion aus cDataSrc (DD_ORIGINAL oder DD_SYSTEM)
	-- auf, f�hrt sie aus und schreibt das Ergebnis in Tabelle TABVALUES_TA
	execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
	-- hole die letzten 9 Datenversionen zu einem Zeitstempel (wenn vorhanden) und schreibe die DAten in PTABVAL
	
	for rec in (select * from REPORTOBJ_TA where SID = nSid and FK_OBJLIST=0) loop
		dLast := null; 
		if cDataSrc = 'DD_SYS' then
			for recArc in (select REF_DATE, VAL, VERS_DATE, SRC_MODDATE from por.MTR_ARC1_RT_TA   where FK_TIS = rec.FK_TIS and REF_DATE between dVon and dBis order by REF_DATE asc,SRC_MODDATE desc) loop
				-- eintragen in PTABVAL - VAL<x> und SRC:MODDATE in STATUS1
				cModdate := to_char(psi_pckg.UTCtoMESZ(recArc.SRC_MODDATE),'dd. hh24:mi:ss');
				if recArc.REF_DATE <> dLast or dLast is null then
					-- neuer Satz
					insert into  PTABVAL (SID,FK_OBJLIST,REFDATE, VAL1, STATUS1)
						values  ( nSid,0, recArc.REF_DATE, recArc.VAL, cModdate );
					dLast := recArc.REF_DATE;
					iC := 1;
					commit;
				else        
					iC := iC + 1;
					cStmt := 'update PTABVAL set VAL' || to_char(iC) || '= ' || to_char(recArc.VAL) || ', STATUS'|| to_char(iC) || '= ''' ||  cModdate ||
						''' where SID = ' || nSid || ' and FK_OBJLIST=0 and REFDATE = to_date(''' || to_char(dLAST,'dd.mm.yyyy hh24:mi:ss') || ''',''dd.mm.yyyy hh24:mi:ss'')';
					
					begin
					    ExecSqlDirect( cStmt );
					    commit;
					exception    
						when others then
							cError := sqlerrm;
							bError := true;
					end;
				end if;
			end loop;	
		else
			for recArc in (select REF_DATE, VAL, VERS_DATE, SRC_MODDATE from por.MTR_ARC4_FORVAL_TA   where FK_TIS = rec.FK_TIS and REF_DATE between dVon and dBis order by REF_DATE asc,SRC_MODDATE desc) loop
				-- eintragen in PTABVAL - VAL<x> und SRC:MODDATE in STATUS1
				cModdate := to_char(psi_pckg.UTCtoMESZ(recArc.SRC_MODDATE),'dd. hh24:mi:ss');
				if recArc.REF_DATE <> dLast or dLast is null then
					-- neuer Satz
					insert into  PTABVAL (SID,FK_OBJLIST,REFDATE, VAL1, STATUS1)
						values  ( nSid,0, recArc.REF_DATE, recArc.VAL, cModdate );
					dLast := recArc.REF_DATE;
					iC := 1;
					commit;
				else        
					iC := iC + 1;
					cStmt := 'update PTABVAL set VAL' || to_char(iC) || '= ' || to_char(recArc.VAL) || ', STATUS'|| to_char(iC) || '= ''' ||  cModdate ||
						''' where SID = ' || nSid || ' and FK_OBJLIST=0 and REFDATE = to_date(''' || to_char(dLAST,'dd.mm.yyyy hh24:mi:ss') || ''',''dd.mm.yyyy hh24:mi:ss'')';
					
					begin
					    ExecSqlDirect( cStmt );
					    commit;
					exception    
						when others then
							cError := sqlerrm;
							bError := true;
					end;
				end if;
			end loop;	
		end if;
	end loop;		
	return ReturnWert( bError, cError );
end;
function R_TsVersions( nTsId number, dVon in date, dBis in date ) return number

	-- Zeigt die letzten 9 Datenversionen der Zeitreihen-Daten f�r Zeitraum cVon und cBis
	-- holt die Werte aus cSource = 'O' - dd_original oder 'S' - dd_system
	--
	-- Inputparameter:  cvon, cBis - Zeiten, die im Portal in gesetzlicher Zeit vorgegeben werden 
	-- Return: SID
as
	cError	varchar2(1024) 	:= null;
	bError	boolean 		:= FALSE;      
--	dVon	date			:= psi_pckg.UTCtime(to_date(cVon, DATE_FORMAT_GER));
--	dBis	date			:= psi_pckg.UTCtime(to_date(cBis, DATE_FORMAT_GER));
	nSid	number;
	iLfdnr	integer	:= 0; 
	cDataSrc	varchar2(12) := 'DD_ORIGINAL';
	cArchivtyp  varchar2(1)  := '4';
	cPict	REPOBJ.PICTURE%type;
	nObjId	TIS.FK_OBJ%type;
	cDesc	varchar2(100);
	cRet	varchar2(100) := 'ERROR - no data found';
	cText	varchar2(80);
	cLabel	varchar2(200);
	cLab1	varchar2(20); 
	cDefUnit	varchar2(50);
	pragma autonomous_transaction;  -- necessary because of insert or deletes while the function is used in a select                                                 
	
begin
	                                         
	execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
    execsqldirect( 'alter session set nls_numeric_characters = '',.''');                       
	--execsqldirect( 'alter session set NLS_LANG=German_Germany.AL32UTF8');
	select userenv('sessionid') into nSid from dual;
	
	-- Reportgruppe und -objekte erzeugen   
	DelSid( nSid ); 
	
	insert into REPORTGRP_TA (SID, REPNAME, ERSTELLT_AM) values( nSid, 'Report letzte Datenversionen', sysdate );   
	
	-- Eintrag Kopfdaten in REPORTOBJ_TA 
	for recTis in (select ID, UNIT, QUANTITY, FK_OBJ, VALUE_CATEGORY, ARCHIVETYPE from TIS	where TIS.ID = nTsId )
	loop	
		nObjId := recTis.FK_OBJ;
		if recTis.ARCHIVETYPE = '1' then
			cDataSrc := 'DD_SYS';
			cArchivtyp := '1';
		end if;
		select SHORT_NAME, LABEL into cText, cLabel from MD_OBJECTS where OBJID = nObjId;
		if recTis.QUANTITY is not null then
			--iLfdNr := rec.POSITION;
			-- es muss sichergestellt werden, dass f�r die erste Position auch eine Zeitreihe existiert
			iLfdNr := iLfdNr + 1;
			cPict  := Getpict(recTis.ID);		
			insert into REPORTOBJ_TA  (SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, UNIT, QUANTITY, FK_OBJID)
				values (nSid, 0,iLfdNr, '0',recTis.ID, cPict, cText, cLabel, recTis.VALUE_CATEGORY, cLabel, recTis.UNIT, recTis.QUANTITY, nObjId );
		end if;
	end loop;
	commit;
	
	if iLfdNr > 0 then
			-- Tabellenwerte erzeugen
			cRet := GenVersions( nSid, dVon, dBis, cDataSrc ); 
	end if;
	return nSid;
exception	
	WHEN OTHERS THEN
		raise;
end;

function R_Lastversions( nObjId number, dVon in date, dBis in date, cTimelevel in varchar2, cSource in varchar2 default 'O' ) return number

	-- Zeigt die letzten 9 Datenversionen der Zeitreihen-Daten f�r Zeitraum cVon und cBis
	-- holt die Werte aus cSource = 'O' - dd_original oder 'S' - dd_system
	--
	-- Inputparameter:  cvon, cBis - Zeiten, die im Portal in gesetzlicher Zeit vorgegeben werden 
	-- Return: SID
as
	cError	varchar2(1024) 	:= null;
	bError	boolean 		:= FALSE;      
--	dVon	date			:= psi_pckg.UTCtime(to_date(cVon, DATE_FORMAT_GER));
--	dBis	date			:= psi_pckg.UTCtime(to_date(cBis, DATE_FORMAT_GER));
	nSid	number;
	iLfdnr	integer	:= 0; 
	cDataSrc	varchar2(12) := 'DD_ORIGINAL';
	cArchivtyp  varchar2(1)  := '4';
	cPict	REPOBJ.PICTURE%type;
	cDesc	varchar2(100);
	cRet	varchar2(100) := 'ERROR - no data found';
	cText	varchar2(80);
	cLabel	varchar2(200);
	cLab1	varchar2(20); 
	cDefUnit	varchar2(50);
	pragma autonomous_transaction;  -- necessary because of insert or deletes while the function is used in a select                                                 
	
begin                                         
	if cSource = 'S' then
		cDataSrc := 'DD_SYS';
		cArchivtyp := '1';
	end if;
	
	execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
    execsqldirect( 'alter session set nls_numeric_characters = '',.''');                       
	--execsqldirect( 'alter session set NLS_LANG=German_Germany.AL32UTF8');
	select userenv('sessionid') into nSid from dual;
	
	-- Reportgruppe und -objekte erzeugen   
	DelSid( nSid ); 
	
	insert into REPORTGRP_TA (SID, REPNAME, ERSTELLT_AM) values( nSid, 'Report letzte Datenversionen', sysdate );   
	
	-- Eintrag Kopfdaten in REPORTOBJ_TA 
	for recTis in (select ID, UNIT, QUANTITY, FK_OBJ, VALUE_CATEGORY from TIS
				where TIS.FK_OBJ = nObjId and TIS.TIME_LEVEL = cTimelevel and TIS.ARCHIVETYPE=cArchivtyp and 
					(instr(TIS.QUANTITY,'.',1,1)=0 or instr(TIS.QUANTITY,'.AVG',1,1)>0 ) )
	loop
		select SHORT_NAME, LABEL into cText, cLabel from MD_OBJECTS where OBJID = nObjId;
		if recTis.QUANTITY is not null then
			--iLfdNr := rec.POSITION;
			-- es muss sichergestellt werden, dass f�r die erste Position auch eine Zeitreihe existiert
			iLfdNr := iLfdNr + 1;
			cPict  := Getpict(recTis.ID);		
			insert into REPORTOBJ_TA  (SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, UNIT, QUANTITY, FK_OBJID)
				values (nSid, 0,iLfdNr, '0',recTis.ID, cPict, cText, cLabel, recTis.VALUE_CATEGORY, cLabel, recTis.UNIT, recTis.QUANTITY, nObjId );
		end if;
	end loop;
	commit;
	
	if iLfdNr > 0 then
			-- Tabellenwerte erzeugen
			cRet := GenDataversions( nSid, nObjId, dVon, dBis, cDataSrc ); 
	end if;
	return nSid;
exception	
	WHEN OTHERS THEN
		raise;
end;

procedure InsUpdBadTis( nSid in number, nObjId in number, nTisOriID in number, nTisSysID in number, cText in varchar2, dVon in date default null, dBis in date default null) 
as
	-- Info zu Zeitreihe eintragen in POR_BAD_TIS
begin
	insert into POR_BAD_TIS (SID, FK_OBJID, FK_TIS_ORI, BESCHREIBUNG, FK_TIS_SYS, VON, BIS )
		values( nSid, nObjId, nTisOriID, cText, nTisSysID, dVon, dBis );
end;

/*
	Parameter:
	cMandant   		- Mandantkennzeichen
	nH_Ausbleibend  - 0= ausbleibende Werte werden nicht gesucht/ausgewiesen; >0 = Anzahl Stunden ohne Aktualisierung
	cFehlerhaft		- J= fehlerhafte ZR ausweisen, N= nicht ausweisen
	-- return:  SID  
*/
function Find_bad_tis( cMandant in varchar2 default null, nH_Ausbleibend in varchar2 default 0, cFehlerhaft in varchar2 default 'J' ) return number
as
	-- LIH 07.04.2014
	  
/*
Dieser pl/sql-Modul erstellt eine tabellarische Liste mit den Signalobjekten, die fehlerhafte Zeitreihen besitzen.
Folgende Fehler von Zeitreihen sollen erkannt werden:
- (obsolete) Originalwert-Zeitreihen 
- (obsolete) Systemwert-Zeitreihen mit falscher Einheit (abweichend von Standardeinheit)
- (obsolete) Systemwert-Zeitreihen mit falscher Gr��e, abweichend von der Gr��e der Original-Zeitreihe
- Systemwert-Zeitreihen mit  UNTIL_DATE_TS < ORI-Zeitreihe.UNTIL_DATE_TS - 4*TIME_LEVEL (l�nger als 4 Intervalle zur�ck)
- fehlende Systemwert-Zeitreihen: aktuelle ORI-Zeitreihe vorhanden, aber keine System-ZR                         
- Zeitreihen, deren Kopfattribute von den Spartenvorgaben abweichen
- Objekte mit mehreren Zeitreihen, die die gleichen Kopfdaten bzgl. QUANTITY,UNIT, TIME_LEVEL,DAY_BEGIN aufweisen
*/
           
	nNr		number(12) :=0;
	nAnz	number;
	nSek	number(20,2); 
	dStart	date := sysdate;
	nLastObj	POR_BAD_TIS.FK_OBJID%type := 0;
	nLastQuant	TIS.QUANTITY%type   := '-';
	nOldTL		TIS.TIME_LEVEL%type := '-';
	cORIG_VC	varchar2(1) := 1;
	cSYS_VC		varchar2(1) := 2; 
	nOriId		number;
	nSysId		number;
	nSid		number;
	bFound		boolean;
	bH_found	boolean;
	bD_found	boolean; 
	pragma autonomous_transaction;  -- necessary because of insert or deletes while the function is used in a select                                                 

begin
/*
	psi_pckg.SaveHit;
	psi_pckg.InsHit2( nNr,  ' ' );
	psi_pckg.InsHit2( nNr,  '-- Start Find_bad_tis ' || to_char(dStart,'dd.mm. hh24:mi:ss') );  
*/
	execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
    execsqldirect( 'alter session set nls_numeric_characters = '',.''');                       
	--execsqldirect( 'alter session set NLS_LANG=German_Germany.AL32UTF8');
	select userenv('sessionid') into nSid from dual;
	
	-- Reportgruppe und -objekte erzeugen   
	DelSid( nSid ); 
	
	insert into REPORTGRP_TA (SID, REPNAME, ERSTELLT_AM) values( nSid, 'Report fehlerhafte Zeitreihen - Mandant ' || cMandant , sysdate );
	
	-- (obsolete) Originalwert-Zeitreihen 
	-- (obsolete) Systemwert-Zeitreihen mit falscher Einheit (abweichend von Standardeinheit)
	-- (obsolete) Systemwert-Zeitreihen mit falscher Gr��e, abweichend von der Gr��e der Original-Zeitreihe
	for rec in (select t.ID, FK_OBJ, nvl(substr(t.QUANTITY,1,instr(t.QUANTITY,'.',1,1)-1),t.QUANTITY) as QUAN, QUANTITY, TIME_LEVEL, 
				FROM_DATE_TS, UNTIL_DATE_TS, UNIT, q.DEFAULT_UNIT from TIS t, QUANT q  
				where nvl(substr(t.QUANTITY,1,instr(t.QUANTITY,'.',1,1)-1),t.QUANTITY) = q.ID
				  and VALUE_CLASS = cORIG_VC    and FK_OBJ is not null
				  and FK_OBJ in (select OBJID from MD_OBJECTS where MANDANT = cMandant)
				  order by FK_OBJ, QUANTITY, TIME_LEVEL, UNTIL_DATE_TS desc) loop
		-- wenn bereits eine Orig-ZR zum Objekt erkannt wurde, m�ssen weitere obsolet sein
		if rec.FK_OBJ = nLastObj and rec.QUANTITY = nLastQuant and rec.TIME_LEVEL = nOldTL then
			InsUpdBadTis( nSid, rec.FK_OBJ, rec.ID, null, 'Originalwert-Zeitreihe ist obsolet' );
		else
			nLastObj   := rec.FK_OBJ; 
			nLastQuant := rec.QUANTITY;                         
			nOldTL     := rec.TIME_LEVEL;
		end if;
		-- Systemwert_ZR pr�fen
		bFound := false;
		bH_found := false;
		bD_found := false;
		for recsys in (select ID,TIME_LEVEL, QUANTITY, UNIT, FROM_DATE_TS, UNTIL_DATE_TS 
						from TIS t  where FK_OBJ = rec.FK_OBJ and VALUE_CLASS = cSYS_VC
						            and ( QUANTITY = rec.QUANTITY or (instr(rec.QUANTITY,'.',1,1) =0 and instr(QUANTITY,'.AVG',1,1) > 0))) loop
			bFound := true;
			if recsys.TIME_LEVEL = 'H' then
				bH_found := true;
			elsif recsys.TIME_LEVEL = 'D' then
				bD_found := true;
			end if;				
			if cFehlerhaft = 'J' then	
				-- stimmen die Gr��en �berein?
				if recsys.QUANTITY <> rec.QUANTITY then
					-- m�glich: T als 15min und T.AVG als H-Wert
					if instr(rec.QUANTITY,'.',1,1) > 0 or (instr(rec.QUANTITY,'.',1,1) =0 and instr(recsys.QUANTITY,'.AVG',1,1) = 0) then
						InsUpdBadTis( nSid, rec.FK_OBJ, rec.ID, recsys.ID, 'Sys-ZR mit falscher Gr��e' );
					end if;
				end if;  
				-- stimmt die Einheit
				if recsys.UNIT <> rec.DEFAULT_UNIT then
					InsUpdBadTis( nSid, rec.FK_OBJ, rec.ID, recsys.ID, ' Sys-ZR Einheit weicht von Standard ab - ' || rec.DEFAULT_UNIT );
				end if;  
			end if;
			if nH_Ausbleibend > 0 then
				-- Systemwert-Zeitreihen, die mehr als x Stunden ausbleiben 
				if recsys.UNTIL_DATE_TS < rec.UNTIL_DATE_TS - nH_Ausbleibend/24 then
					InsUpdBadTis( nSid, rec.FK_OBJ, rec.ID, recsys.ID, 'Sys-ZR seit >' || to_char(nH_Ausbleibend) || ' Stunden nicht aktualisiert' );
				end if;
			end if;
		end loop;
		-- fehlende Systemwert-Zeitreihen: aktuelle ORI-Zeitreihe vorhanden, aber keine System-ZR
		if not bFound then
			InsUpdBadTis( nSid, rec.FK_OBJ, rec.ID, null, 'keine Sys-ZR vorhanden' );
		end if;
		if cFehlerhaft = 'J' then	
			if rec.TIME_LEVEL = 'MI15' then
				if not bH_found then
					InsUpdBadTis( nSid, rec.FK_OBJ, rec.ID, null, 'erwartete Stunden-Sys-ZR fehlt' );
				end if;
				if not bD_found then
					InsUpdBadTis( nSid, rec.FK_OBJ, rec.ID, null, 'erwartete Tages-Sys-ZR fehlt' );
				end if;
			elsif rec.TIME_LEVEL = 'H' then
				if not bD_found then
					InsUpdBadTis( nSid, rec.FK_OBJ, rec.ID, null, 'erwartete Tages-Sys-ZR fehlt' );
				end if;
			end if;
		end if;
	end loop; 
	
	for rec in (select t.ID, FK_OBJ from TIS t
				where TIME_LEVEL  = 'MI' and VALUE_CLASS = cORIG_VC 
					  and FK_OBJ in (select OBJID from MD_OBJECTS where MANDANT = cMandant)
				order by FK_OBJ) 
	loop
		InsUpdBadTis( nSid, rec.FK_OBJ, rec.ID, null, 'Minuten-Ori-ZR kann gel�scht werden' );
	end loop;
    commit;
	-- Suchen obsoleter Systemwert-Zeitreihen, die bisher nicht referenziert werden in POR_BAD_TIS	
	for recsys in (select t.ID, FK_OBJ, nvl(substr(t.QUANTITY,1,instr(t.QUANTITY,'.',1,1)-1),t.QUANTITY) as QUAN, QUANTITY, TIME_LEVEL, 
			FROM_DATE_TS, UNTIL_DATE_TS, UNIT, q.DEFAULT_UNIT from TIS t, QUANT q  
			where nvl(substr(t.QUANTITY,1,instr(t.QUANTITY,'.',1,1)-1),t.QUANTITY) = q.ID
			  and VALUE_CLASS = cSYS_VC and FK_OBJ is not null 
			  -- and t.ID not in (select FK_TIS_SYS from POR_BAD_TIS where SID = nSid)  -- funktioniert seltsamerweise nicht
			  and FK_OBJ in (select OBJID from MD_OBJECTS where MANDANT = cMandant)
			  order by FK_OBJ, QUANTITY, TIME_LEVEL, UNTIL_DATE_TS desc) 
	loop
        bFound := false;
		for recBad in (select FK_TIS_SYS from POR_BAD_TIS where SID = nSid and FK_TIS_SYS=recsys.ID) loop
			bFound := true;
		end loop;
		if not bFound then
			if cFehlerhaft = 'J' then	
				-- stimmt die Einheit
				if recsys.UNIT <> recsys.DEFAULT_UNIT then
					InsUpdBadTis( nSid, recsys.FK_OBJ, null, recsys.ID, ' Sys-ZR Einheit weicht von Standard ab - ' || recsys.DEFAULT_UNIT );
				end if;  
			end if;
			if nH_Ausbleibend > 0 then
				-- Systemwert-Zeitreihen, die mehr als x Stunden ausbleiben 
				if recsys.UNTIL_DATE_TS < recsys.UNTIL_DATE_TS - nH_Ausbleibend/24 then
					InsUpdBadTis( nSid, recsys.FK_OBJ, null, recsys.ID, 'Sys-ZR seit >' || to_char(nH_Ausbleibend) || ' Stunden nicht aktualisiert' );
				end if;
			end if;
		end if;
	end loop;
	
	if cFehlerhaft = 'J' then
		-- Zeitreihen, deren Kopfattribute von den Spartenvorgaben abweichen
		for recBA in (select * from MD_BUS_AREA where SHORT_NAME in (select distinct SPARTE	from MD_OBJECTS where MANDANT = cMandant)) 
		loop
			for recTis in (select ID, FK_OBJ, ARCHIVETYPE, DAY_BEGIN, MONTH_BEGIN, YEAR_BEGIN, TIMEZONE from TIS 
				where FK_OBJ in (select OBJID from MD_OBJECTS where MANDANT = cMandant and SPARTE = recBA.SHORT_NAME) ) 
			loop
				if recTis.DAY_BEGIN <> recBA.DAY_BEGIN or recTis.MONTH_BEGIN <> recBA.MONTH_BEGIN or recTis.YEAR_BEGIN <> recBA.YEAR_BEGIN 
					or recTis.TIMEZONE <> recBA.TIMEZONE then       
					if recTis.ARCHIVETYPE = 1 then	-- Ori          
						nOriId := recTis.ID; nSysId := null;
					else
						nOriId := null; nSysId := recTis.ID;
					end if;
					if recTis.DAY_BEGIN <> recBA.DAY_BEGIN then
						InsUpdBadTis( nSid, recTis.FK_OBJ, nOriId, nSysId, ' Tagesbeginn der Zeitreihe <' || to_char(recTis.DAY_BEGIN)
							|| '> weicht von Sparten-Tagesbeginn ab <' || to_char(recBA.DAY_BEGIN) || '>');
					elsif recTis.MONTH_BEGIN <> recBA.MONTH_BEGIN  then
						InsUpdBadTis( nSid, recTis.FK_OBJ, nOriId, nSysId, ' Monatsbeginn der Zeitreihe <' || to_char(recTis.MONTH_BEGIN)
							|| '> weicht von Sparten-Monatsbeginn ab <' || to_char(recBA.MONTH_BEGIN) || '>');
					elsif recTis.YEAR_BEGIN <> recBA.YEAR_BEGIN then
						InsUpdBadTis( nSid, recTis.FK_OBJ, nOriId, nSysId, ' Jahresbeginn der Zeitreihe <' || to_char(recTis.YEAR_BEGIN)
							|| '> weicht von Sparten-Jahresbeginn ab <' || to_char(recBA.YEAR_BEGIN) || '>');
					elsif recTis.TIMEZONE <> recBA.TIMEZONE then
						InsUpdBadTis( nSid, recTis.FK_OBJ, nOriId, nSysId, ' Zeitzone der Zeitreihe <' || recTis.TIMEZONE
							|| '> weicht von Sparten-Zeitzone <' || recBA.TIMEZONE || '>');
					end if;
				end if;
			end loop;
		end loop;
		
		-- Suchen der Objekte mit mehreren Zeitreihen, die die gleichen Kopfdaten bzgl. QUANTITY,UNIT, TIME_LEVEL,DAY_BEGIN aufweisen
		for recTis in (	select FK_OBJ,QUANTITY,UNIT, TIME_LEVEL,DAY_BEGIN,count(*) from TIS
						where ARCHIVETYPE=1  and FK_OBJ in (select OBJID from MD_OBJECTS where MANDANT = cMandant)
						HAVING count(*)>1 group by FK_OBJ,QUANTITY,UNIT, TIME_LEVEL, DAY_BEGIN
						order by FK_OBJ,TIME_LEVEL ) 
		loop
			InsUpdBadTis( nSid, recTis.FK_OBJ, null, null, ' >1 Sys-ZR mit gleicher Gr��e '|| recTis.QUANTITY || ', Einh. '|| recTis.UNIT || 
				', Z-Stufe '|| recTis.TIME_LEVEL || ', Beginn='|| recTis.DAY_BEGIN  );
		end loop;
	end if;
		
	commit; 
/*	
	nSek := (sysdate - dStart)*24*60*60;
	psi_pckg.InsHit2( nNr, 'Laufzeit in Sek: ' || to_char(nSek,'999.99') );
*/
	return nSid;
end;

/*
	Parameter:
	nObj_Id_Parent	- Root-Objekt-ID, von dem aus die Zeitreihen unterlagerter Objekte untersucht werden
	dVon			- Beginn Zeitbereich
	dBis			- Ende untersuchter Zeitbereich
	return:  SID  
*/
function R_Disturbed_Data( nObj_Id_Parent in number, dVon in date, dBis in date) return number
as
	-- LIH 21.05.2014
	  
/*
Dieser pl/sql-Modul erstellt eine tabellarische Liste mit Objekten, die gest�rte oder fehlende Daten in Zeitreihen haben.
Zu den Zeitreihen wird jeweils der von/bis-Bereich der St�rung ausgewiesen.
*/
           
	nAnz		number	:= 0;
	nMaxLines   number	:= 500;
	nLastObj	POR_BAD_TIS.FK_OBJID%type := 0;
	nLastQuant	TIS.QUANTITY%type   := '-';
	nOldTL		TIS.TIME_LEVEL%type := '-';
	cORIG_VC	varchar2(1) := 1;
	cSYS_VC		varchar2(1) := 2;
	cLabel		MD_OBJECTS.LABEL%type;
	cMandant	MD_OBJECTS.MANDANT%type;
	cSparte		MD_OBJECTS.SPARTE%type;
	cPath		MD_OBJECTS.ID_PATH%type;
	nSid		number;
	bFound		boolean;
	bH_found	boolean;
	bD_found	boolean; 
	nLastTis	SYS_ERRINFO.TIS_ID%type := 0;
	cLastTL		SYS_ERRINFO.TIME_LEVEL%type;
	cLastCond	SYS_ERRINFO.CONDITION%type;
	dFrom		date;
	dTo			date;
	cText		POR_BAD_TIS.BESCHREIBUNG%type;
	nDiff		number;
	nPathLen	number := 0;
	pragma autonomous_transaction;  -- necessary because of insert or deletes while the function is used in a select                                                 

begin
	execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
    execsqldirect( 'alter session set nls_numeric_characters = '',.''');                       

	select userenv('sessionid') into nSid from dual;
	
	-- Reportgruppe und -objekte erzeugen   
	DelSid( nSid ); 
	
	insert into REPORTGRP_TA (SID, REPNAME, ERSTELLT_AM) values( nSid, 'Report Gest�rte Zeitbereiche von Zeitreihen - Mandant ' || cMandant , sysdate );
	select LABEL, MANDANT, SPARTE, ID_PATH into cLabel, cMandant, cSparte, cPath from MD_OBJECTS where OBJID = nObj_Id_Parent;
	nPathLen := length(cPath);
	
	for rec in (select * from SYS_ERRINFO where substr(ID_PATH, 1, nPathLen) = cPath and REF_DATE between dVon and dBis
		order by LABEL, TIS_ID, TIME_LEVEL, REF_DATE, CONDITION) loop
		-- solange die St�rung f�r eine Zeitreihe anh�lt, wird nicht registriert in BAD_TIS
		if  nLastTis = 0 then
			nLastTis  := rec.TIS_ID;
			cLastCond := rec.CONDITION;
			dFrom := rec.REF_DATE;
			dTo   := rec.REF_DATE;
			nDiff := 1/IVjeTag(rec.TIME_LEVEL);      
		else
			if rec.TIS_ID <> nLastTis or cLastCond <> rec.CONDITION or (rec.REF_DATE > dTo + nDiff) then
				-- Satz registrieren                              
				if cLastCond = 'D' then
					cText := 'gest�rter Zeitbereich';
				elsif cLastCond = 'M' then
					cText := 'Zeitbereich mit fehlenden Werten';
				elsif cLastCond = '1' then
					cText := 'Bereich manuell korrigierter Werte';
				elsif cLastCond = '2' then
					cText := 'Bereich autom. korrigierter Werte';
				elsif cLastCond = '3' then
					cText := 'Bereich importierter Ersatzwerte';
				else
					cText := 'Bereich mit (unbekannter) St�rungskennung ' || cLastCond;
				end if;
				nAnz := nAnz + 1;
				InsUpdBadTis( nSid, rec.OBJID, null, rec.TIS_ID, cText, dFrom, dTo );
				nLastTis  := rec.TIS_ID;
				cLastCond := rec.CONDITION;
				dFrom := rec.REF_DATE;
				dTo   := rec.REF_DATE;      
			else
				dTo   := rec.REF_DATE;
			end if;
		end if;
		if nAnz > nMaxLines then
			InsUpdBadTis( nSid, rec.OBJID, null, rec.TIS_ID, 'Abbruch - max. Ausgabezeilenzahl erreicht', dFrom, null );
			exit;
		end if;			
	end loop;
		
	commit; 
	return nSid;
end;

function R_TP_9List_SID( nObjlistId number, dVon in date, dBis in date, cTimelevel in varchar2, cSource in varchar2 default 'O' ) return number

	-- �bertr�gt bis zu 9 Objekte der vorgeg. Liste in Report-Objektliste und erzeugt Header-Info und Zeitreihen-Daten f�r Zeitraum cVon und cBis
	-- holt die Werte aus cSource = 'O' - dd_original oder 'S' - dd_system
	--
	-- Inputparameter:  cvon, cBis - Zeiten, die im Portal in gesetzlicher Zeit vorgegeben werden 
	-- Return: SID
as
	cError	varchar2(1024) 	:= null;
	bError	boolean 		:= FALSE;      
--	dVon	date			:= psi_pckg.UTCtime(to_date(cVon, DATE_FORMAT_GER));
--	dBis	date			:= psi_pckg.UTCtime(to_date(cBis, DATE_FORMAT_GER));
	nSid	number;
	iLfdnr	integer	:= 0; 
	cDataSrc	varchar2(12) := 'DD_ORIGINAL';
	cArchivtyp  varchar2(1)  := '4';
	cPict	REPOBJ.PICTURE%type;
	cDesc	varchar2(100);
	cRet	varchar2(100) := 'ERROR - no data found';
	cText	varchar2(80);
	cLab1	varchar2(20); 
	cDefUnit	varchar2(50);
	pragma autonomous_transaction;  -- necessary because of insert or deletes while the function is used in a select                                                 
	
begin                                         
	if cSource = 'S' then
		cDataSrc := 'DD_SYS';
		cArchivtyp := '1';
	end if;
	
	execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
    execsqldirect( 'alter session set nls_numeric_characters = '',.''');                       
	--execsqldirect( 'alter session set NLS_LANG=German_Germany.AL32UTF8');
	select userenv('sessionid') into nSid from dual;
	
	-- Reportgruppe und -objekte erzeugen   
	DelSid( nSid ); 
	
	for rec in (select distinct LIST_SHORT_NAME, DESCRIPTION from MD_OBJLIST where ID = nObjlistId) loop
		insert into REPORTGRP_TA (SID, REPNAME, ERSTELLT_AM) values( nSid, rec.LIST_SHORT_NAME || '  ' || rec.DESCRIPTION, sysdate );
	end loop;

	-- f�r alle Objekte einer Liste werden die Zeitreihen-Daten geholt und in REPORTOBJ_TA gespeichert 
	-- Achtung: TP-Objekte - Gr��en haben meist Punkte und es gibt ggf. viele Zeitreihen je Objekt
	for rec in (select POSITION, FK_OBJID, SHORT_NAME, LABEL from MD_OBJLIST where ID = nObjlistId order by POSITION) loop
		-- Variablentext R_9R_NAME holen oder den Namen der Station
		cText :=   rec.SHORT_NAME;            
		for recVar in (select VALUE from MD_VARIABLES where OBJID = rec.FK_OBJID and ATTR_NAME='R_9_NAME') loop
			cText := recVar.VALUE;
		end loop;
		
		-- Zeitreihen: max 9
		if rec.POSITION < 10 then
			for recTis in (select ID, UNIT, QUANTITY, FK_OBJ, VALUE_CATEGORY from TIS
				where TIS.FK_OBJ = rec.FK_OBJID and TIS.TIME_LEVEL = cTimelevel and TIS.ARCHIVETYPE=cArchivtyp )
			loop
				if cArchivtyp = '1' then
					-- nur die Archive der Standardzeitstufe bei Archivtyp S ber�cksichtigen
					cDefUnit := recTis.UNIT;
						
					select SHORT_NAME into cDefUnit from UNITS where ID = cDefUnit;
				end if;
				if recTis.QUANTITY is not null then
					--iLfdNr := rec.POSITION;
					-- es muss sichergestellt werden, dass f�r die erste Position auch eine Zeitreihe existiert
					iLfdNr := iLfdNr + 1;
					cPict  := Getpict(recTis.ID);		
					insert into REPORTOBJ_TA  (SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, UNIT, QUANTITY, FK_OBJID)
						values (nSid, nObjlistId,iLfdNr, '0',recTis.ID, cPict, cText, rec.LABEL, recTis.VALUE_CATEGORY, rec.LABEL, recTis.UNIT, recTis.QUANTITY, rec.FK_OBJID );
				end if;
			end loop;
		end if;
		commit;
	end loop;   
	
	if iLfdNr > 0 then
		-- Tabellenheader erzeugen - 2 zeilig: 1. Zeile: Obj-Shortname, 2. Zeile: Quantity[unit]
		cRet := GenTisHeaderCols( nSid, nObjlistId );             
		        
		if cRet = 'OK' then
			-- Tabellenwerte erzeugen
			cRet := GenTisValues( nSid, nObjlistId, to_char(dVon, DATE_FORMAT_GER), to_char(dBis, DATE_FORMAT_GER), cDataSrc ); 
		end if;
	end if;
	return nSid;
exception	
	WHEN OTHERS THEN
		raise;
end;
 
end; -- body ReportPckg
/
