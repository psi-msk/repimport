create or replace Package TPMpckg AUTHID DEFINER
is 
	-- LIH 09.06.15 R_ChkTrans - cTo eingef�hrt/Korr. 
	-- LIH 08.06.15 R_ChkTrans - cBis anpassen, wenn Until_date < dBis
	-- LIH 05.06.15 R_CompareTrans: erweitert - Sicherheitsstop (max 1 Label je Report)
	-- LIH 02.06.15 verbessert: R_ChkTrans
	-- LIH 01.06.15 Neu: CompareTrans; verbessert: R_ChkTrans
	-- LIH 28.05.15 Ersterstellung
/*	                                
	Diese package enth�lt allg. Funktionen und Reportfunktionen f�r das TPM-Schema (PSITrans)
	Voraussetzung f�r den Compile:  Datenbanklink TPMdb:   create database link TPMdb connect to TPM identified by "TPM" using 'gobsux02/tgtd';
	
	Die folgenden Reports werden abgedeckt:
	1. R_ChkTrans
	   			  
	2. R_CompareTrans

*/	

	Function TPCnt ( cKz in varchar2, cVon in varchar2, cBis in varchar2, nAnzOk in out number, nAnzNull in out number, dLast in out date ) return date;

	function R_CompareTrans( cFs in varchar2, dVon in date, dBis in date, cWild in varchar2 ) return number;
	function R_ChkTrans( cFs in varchar2, dVon in date, dBis in date, cWild in varchar2 ) return number;
end;
/

create or replace Package body TPMpckg
is
	-- �nderungsinfos siehe Header

---------------------------------------------------------------------------------------------

	DATE_FORMAT_GER 				constant varchar2(21) := 'DD.MM.YYYY HH24:MI:SS';

---------------------------------------------------------------------------------------------

Function TPCnt ( cKz in varchar2, cVon in varchar2, cBis in varchar2, nAnzOk in out number, nAnzNull in out number, dLast in out date ) return date
as
     -- LIH, 21.05.2015 - Ermitteln der Anzahl Werte und Anzahl L�cken f�r Vorgabe im Stundenwertarchiv (jeweils f�r komplette Gastage) 
     -- Param: cKz - Archivspurkennzeichen
     --  		cVon - Datum von in der Form 'dd.mm.yy'
     --  		cBis - Datum von in der Form 'dd.mm.yy'
     --			nAnzOk, nAnzNull - Werte g�ltig und null
     --			dLast	Zeitstempel des letzten Containersatzes
     -- return  dFirst	Zeitstempel des ersten Containersatzes

     dVonTag	date := to_date(substr(cVon,1,8), 'dd.mm.yy');
     dBisTag	date := to_date(substr(cBis,1,8) || ' 23', 'dd.mm.yy hh24');
     dFirst		date;
     
begin
     nAnzOk		 := 0;
     nAnzNull	 := 0;
     dLast		 := dVonTag;
	
	for rec in (select * from TPMAWTHN_TA@tpmdb where ARVSPRKNZ = cKz and  ZP >= dVonTag and ZP <= dBisTag) loop
		if dFirst is null then
			dFirst := rec.ZP;
			dLast  := rec.ZP;
		end if;  
		-- Es k�nnen komplette Tage in der Aufzeichnung fehlen
		if rec.ZP - dLast > 1 then
			nAnzNull := nAnzNull + (rec.ZP - dLast - 1)*24;
		end if;
		dLast := rec.ZP;
		if rec.WERT01 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT02 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT03 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT04 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT05 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT06 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT07 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT08 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT09 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT10 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT11 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT12 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT13 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT14 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT15 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT16 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT17 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT18 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT19 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT20 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT21 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT22 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT23 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
		if rec.WERT24 is null then
			nAnzNull := nAnzNull + 1;
		else
			nAnzOk := nAnzOk + 1;
		end if;
	end loop; 
	if dFirst is null then
		dFirst := dVonTag;
	end if;
      
	-- 'F�r ' || cKz || ' wurden im Zeitbereich ' || to_char(dFirst, 'dd.mm.yy hh24') || ' bis ' || to_char(dLast, 'dd.mm.yy hh24') || ' gez�hlt: ' || to_char(nAnzOk) || ' g�ltige Werte und ' || to_char(nAnzNull) || ' Nullwert(e) (L�cken)');
    return (dFirst);
end;

function R_CompareTrans( cFs in varchar2, dVon in date, dBis in date, cWild in varchar2 ) return number
as
    dFirst		date;
    dRef		date;
    dLast		date;
    nFirst		number;
    nLast		number; 
    nSid		number; 
    nTid 		number;
    iLfdNr		integer := 0;
    bFound		boolean;
    bGleich		boolean;
    bLag1		boolean;
    bLag2		boolean;
    cLag		varchar2(10) := 'L�cke';
    cEqual		varchar2(20) := 'Werte sind gleich';
    cUnequal	varchar2(20) := 'Werte ungleich !!'; 
    nSummand	number;
    nFaktor		number;
    nDAB		number;
    nWert		number; 
    iRound		integer;
    cSpurKz		varchar2(80);  
    nTranspFaktor number;
    nPorFaktor 	number;
    cLastKz		varchar2(80);
	cShort	QUANT.SHORT_NAME%type;  

	pragma autonomous_transaction;  -- necessary because of insert or deletes while the function is used in a select                                                 
     
begin 
	 
	-- Ablauf:
	-- 1. Daten aus TPM und Portal-DD_SYS auslesen und in TabValues eintragen
	-- 2. Lauf �ber TabValues - eliminieren Eintr�ge bzw. Ersetzen durch Texte 
	psireports.execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
    psireports.execsqldirect( 'alter session set nls_numeric_characters = '',.''');                       
	--psireports.execsqldirect( 'alter session set NLS_LANG=German_Germany.AL32UTF8');
	select userenv('sessionid') into nSid from dual;
	
	-- Reportgruppe und -objekte erzeugen   
	psireports.delsid( nSid ); 
	
	insert into REPORTGRP_TA (SID, REPNAME, ERSTELLT_AM) values( nSid, 'R_CompareTrans', sysdate );

	-- f�r alle Zeitreihen von PSITrans
	for recTis in (select objlabel(fk_obj) LABEL, TIS.* from TIS where ARCHIVETYPE='1' and TIME_LEVEL='H' and FK_FS= cFs and (cWild is null or cWild='*' or objlabel(fk_obj) like '%' || cWild || '%' ) order by LABEL) loop
		iLfdNr := iLfdNr + 1;
		--cPict  := Getpict(recTis.ID);		
		if cLastKz is null then
			cLastKz := recTis.LABEL;
		end if;
		insert into REPORTOBJ_TA  (SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, UNIT, QUANTITY, FK_OBJID)
			values (nSid, 0, iLfdNr, '0',recTis.ID, '', '', recTis.LABEL, '', recTis.LABEL, recTis.UNIT, recTis.QUANTITY, recTis.FK_OBJ );
		if recTis.Label <> cLastKz then
			-- Detailvergleich nur f�r 1 Label erlaubt (aber alle Zeitreihen zum Kennzeichen)
			update REPORTOBJ_TA set OBJLABEL = 'STOP! Details nur f�r 1 Kennz.', FK_TIS=0, QUANTITY=null, UNIT=null where SID = nSid and FK_OBJLIST=0 and POS=iLfdNr;
		end if;
		commit;
	end loop;   
	
	if iLfdNr > 0 then
		
		for rec in (select * from REPORTOBJ_TA where SID = nSid order by POS) loop
			if rec.FK_TIS = 0 then
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE) values (nSid, rec.POS, sysdate);
				exit;
				commit;
			end if;				
			dFirst := null;
			nTid := null; 
			-- Archivspur ermitteln - setzt sich zusammen aus Objektlabe und Gr�sse; Gr��en mit "_" am Ende m�ssen sonderbehandelt werden (_ muss weg)
			-- Gr��en, deren Kurzname mit 'GK_' beginnt -> dieser string-Teil muss weg
			if length(rec.QUANTITY) > 3 then
				if substr(rec.QUANTITY,1,3) = 'GK_' then
					rec.QUANTITY := substr(rec.QUANTITY,4);
				end if;
			end if;
			cShort := rec.QUANTITY;
			if substr(cShort,length(cShort),1) = '_' then
				cShort := substr(cShort,1,length(cShort)-1);
			end if;
			
			cSpurKz := rec.OBJLABEL || '.' || cShort || '.H';   
			-- T-Einheit in UNIT-Tab. lokalisieren ->T-Faktor
			-- dito f�r Systemwert-Einheit ->P-Faktor  =>Umrechnungsfaktor f�r P-Wert ist T-Faktor/P-Faktor
			nPorFaktor := 1;
			for recUnit in (select FACTOR, SUMMAND, PRECISION  from UNITS where SHORT_NAME = rec.UNIT) loop
				nPorFaktor := recUnit.FACTOR; 
				iRound := recUnit.PRECISION;
			end loop;

			for recUnit in (select PRECISION from UNITS where SHORT_NAME = rec.UNIT) loop
				iRound := recUnit.PRECISION;
			end loop;
                                                           
			nTranspFaktor := 1; 
			for recDim in (select EHT, DEZFAKTOR from TPMAS_VI@tpmdb where  ARVSPRKNZ = cSpurKz and VRSKNZ='VRS.TG.BETR') loop 
				bFound := false; 
				for recUnit in (select FACTOR, SUMMAND, BASE_UNIT  from UNITS where SHORT_NAME = recDim.EHT) loop
					bFound := true;
					nTranspFaktor  := recUnit.FACTOR;
				end loop;
			end loop;
			nFaktor := nPorFaktor / nTranspFaktor ;

			for recArv in (select * from TPMAWTHN_TA@tpmdb where ARVSPRKNZ = cSpurKz and  ZP between dVon and dBis) loop
				-- eintrag in POR_TABVALUES muss in UTC erfolgen, da framework umrechnet
				if psi_pckg.ismesz(recArv.ZP) = 1 then
					dRef := recArv.ZP -1/24;
				else
					dRef := recArv.ZP -2/24;
				end if;
				if dFirst is null then
					dFirst := dRef + 1/24;	-- in UTC
				end if;
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+1/24, round(nFaktor*recArv.WERT01,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+2/24, round(nFaktor*recArv.WERT02,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+3/24, round(nFaktor*recArv.WERT03,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+4/24, round(nFaktor*recArv.WERT04,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+5/24, round(nFaktor*recArv.WERT05,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+6/24, round(nFaktor*recArv.WERT06,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+7/24, round(nFaktor*recArv.WERT07,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+8/24, round(nFaktor*recArv.WERT08,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+9/24, round(nFaktor*recArv.WERT09,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+10/24, round(nFaktor*recArv.WERT10,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+11/24, round(nFaktor*recArv.WERT11,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+12/24, round(nFaktor*recArv.WERT12,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+13/24, round(nFaktor*recArv.WERT13,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+14/24, round(nFaktor*recArv.WERT14,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+15/24, round(nFaktor*recArv.WERT15,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+16/24, round(nFaktor*recArv.WERT16,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+17/24, round(nFaktor*recArv.WERT17,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+18/24, round(nFaktor*recArv.WERT18,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+19/24, round(nFaktor*recArv.WERT19,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+20/24, round(nFaktor*recArv.WERT20,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+21/24, round(nFaktor*recArv.WERT21,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+22/24, round(nFaktor*recArv.WERT22,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+23/24, round(nFaktor*recArv.WERT23,iRound));
				insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL2) values (nSid, rec.POS, dRef+1, round(nFaktor*recArv.WERT24,iRound));
                dLast := dRef+1;
				commit;
			end loop;

--			for recSys in (select * from por.MTR_EXT_ARC4_FORVAL_VI where FK_TIS = rec.FK_TIS and REF_DATE between dFirst and dLast) loop
			for recSys in (select * from DD_SYS where FK_TIS = rec.FK_TIS and REF_DATE between dFirst and dLast) loop
				bFound := false;
				for recT in (select rowid from POR_TABVALUES where SID = nSid and FK_OBJLIST = rec.POS and REFDATE = recSys.REF_DATE) loop
					bFound := true;
					update POR_TABVALUES set VAL1 = recSys.VAL where rowid = recT.rowid;
				end loop;       
				if not bFound then                                                                                        
					insert into POR_TABVALUES (SID, FK_OBJLIST, REFDATE, VAL1) values (nSid, rec.POS, recSys.REF_DATE, recSys.VAL);
				end if;
				commit;
			end loop;
				
			-- 2. Lauf �ber TabValues - eliminieren eintr�ge bzw. ersetzen durch Texte
			bGleich := false;
			bLag1 := false; 
			bLag2 := false; 
			for recTV in (select rowid, p.* from POR_TABVALUES p where SID = nSid and FK_OBJLIST = rec.POS 
				and REFDATE<(select max(REFDATE) from POR_TABVALUES p where SID = nSid and FK_OBJLIST = rec.POS)
				order by REFDATE) loop
				-- auf L�cke pr�fen
				if recTV.VAL1 is null then
					bGleich := false;
					if bLag1 then
						if recTV.VAL2 is null then	-- beide Reihen mit L�cke
							if bLag2 then
								iRound := 0;
								delete from POR_TABVALUES where rowid = recTV.rowid;
							else
								bLag2 := true;
								update POR_TABVALUES set STATUS2 = cLag where rowid = recTV.rowid;
							end if;
						else
							if bLag2 then
								bLag2 := false;                           
							else
								iRound := 0;
								delete from POR_TABVALUES where rowid = recTV.rowid;
							end if;								
						end if;
					else
						bLag1 := true;
						update POR_TABVALUES set STATUS1 = cLag where rowid = recTV.rowid;
						if recTV.VAL2 is null then	-- beide Reihen mit L�cke
							bLag2 := true;
							update POR_TABVALUES set STATUS2 = cLag where rowid = recTV.rowid;
						end if;
					end if;
				else
					bLag1 := false;
					if recTV.VAL2 is null then
						bGleich := false;
						if not bLag2 then
							bLag2 := true;
							update POR_TABVALUES set STATUS2 = cLag where rowid = recTV.rowid;
						end if;
					else
						bLag2 := false;
						-- wenn Werte gleich sind -> 1. Satz mit Text "Werte gleich", Folges�tze l�schen
						if round(recTV.VAL1,iRound) = round(recTV.VAL2,iRound) then
							if bGleich then
								iRound := 0;
								delete from POR_TABVALUES where rowid = recTV.rowid;
							else
								bGleich := true;
								update POR_TABVALUES set STATUS2 = cEqual where rowid = recTV.rowid;
							end if;
						else
							bGleich := false;
							update POR_TABVALUES set STATUS1 = cUnequal where rowid = recTV.rowid;
						end if;
					end if;            
				end if;
			end loop;
			commit;
		end loop; 
	end if;   
	commit;
	return nSid;
exception	
	WHEN OTHERS THEN
		raise;
end;

function R_ChkTrans( cFs in varchar2, dVon in date, dBis in date, cWild in varchar2 ) return number

	-- Die Funktion greift  via dblink TPMDB auf die PSItransport-DB zu 
	-- Sie iteriert �ber die TIS (f�r alle Datens�tze von Systemwertzeitreihen aus Fremdsystem IMP_TP), ermittelt je Zeitreihe die Anzahl Werte und Anzahl L�cken (in ARC_CONDITIION_TA)
	-- und ruft die Funktion TPcount im tpm-Schema auf, um die Daten der zugeh�rigen Archivspuren zu ermitteln
	-- Die Ergebnisdaten werden wie folgt eingetragen:                                                          
	--  in REPORTOBJ_TA: die Objekte und Zeitreihen (in NGA): POS - Lfd Nr, OBJLABEL - Kennzeichen, FK_TIS - TIS-ID, DESCRIPTION - Archivspur tpm, UNIT, UNIT, QUANTITY
	--  in POR_TABVALUES:   VAL1 - # Werte  in Fremdsystem-Zeitreihe
	--						VAL2 - # L�cken in Fremdsystem-Zeitreihe
	--						VAL3 - # Werte in NGA-Zeitreihe
	--						VAL4 - # L�cken in NGA-Zeitreihe
	--						VAL5 - Diff. # Werte 
	--						VAL6 - Diff. # L�cken 
	-- Verwendung von Report: R_PSI_ChkTrans
	-- Inputparameter:  cFs - Fremdsystem-Id, cvon, cBis - Zeiten, die im Portal in gesetzlicher Zeit vorgegeben werden , cWild - Objektkennzeichenvorgabe als Wildcard (ohne %)
	-- Return: SID
as
	cError	varchar2(1024) 	:= null;
	bError	boolean 		:= FALSE;      
--	dVon	date			:= psi_pckg.UTCtime(to_date(cVon, DATE_FORMAT_GER));
--	dBis	date			:= psi_pckg.UTCtime(to_date(cBis, DATE_FORMAT_GER));
	nSid	number;
	iLfdnr	integer	:= 0; 
	cDataSrc	varchar2(12) := 'DD_SYS';
	cArchivtyp  varchar2(1)  := '1';
	cPict	REPOBJ.PICTURE%type;
	cDesc	varchar2(100);
	cRet	varchar2(100) := 'ERROR - no data found';
	cText	varchar2(180);
	cLab1	varchar2(20); 
	cDefUnit	varchar2(50);
	cShort	QUANT.SHORT_NAME%type;  
    nAnzOk		number	 := 0;
    nAnzNull 	number	 := 0;
    nNgaOk		number	 := 0;
    nNgaNull 	number	 := 0;
    nUeber		number;
    dMin		date;
    dMax		date;            
    cVon		varchar2(20) := to_char(dVon,'dd.mm.yy');
    cBis		varchar2(20) := to_char(dBis,'dd.mm.yy');
    cAb			varchar2(20);
    cTo			varchar2(20);
    dUntil		date;
    dAb			date;
    dsys		date := sysdate;
    cZrKnz		varchar2(80); 
    dFirst		date;
    dLast		date;
    bFound	boolean;
	
	pragma autonomous_transaction;  -- necessary because of insert or deletes while the function is used in a select                                                 
	
begin                                         
	
	PSIReports.execsqldirect( 'alter session set nls_date_format = ''' || DATE_FORMAT_GER || '''' );
    PSIReports.execsqldirect( 'alter session set nls_numeric_characters = '',.''');                       
	--execsqldirect( 'alter session set NLS_LANG=German_Germany.AL32UTF8');
	select userenv('sessionid') into nSid from dual;
	
	-- Reportgruppe und -objekte erzeugen   
	PSIReports.DelSid( nSid ); 
	
	insert into REPORTGRP_TA (SID, REPNAME, ERSTELLT_AM) values( nSid, 'R_PSI_ChkTrans', sysdate );

	-- f�r alle Zeitreihen von PSItransport
	for recTis in (select objlabel(fk_obj) LABEL, TIS.* from TIS where ARCHIVETYPE='1' and TIME_LEVEL='H' and FK_FS= cFs and (cWild is null or cWild='*' or objlabel(fk_obj) like '%' || cWild || '%' )
		order by LABEL) loop
		iLfdNr := iLfdNr + 1;
		--cPict  := Getpict(recTis.ID);
		-- QUANTITY	-Angabe in TIS ist der SHORT_NAME !!	
		insert into REPORTOBJ_TA  (SID, FK_OBJLIST, POS, NR, FK_TIS, PICTURE, SHORT_NAME, DESCRIPTION, VALUE_CATEGORY, OBJLABEL, UNIT, QUANTITY, FK_OBJID)
			values (nSid, 0, iLfdNr, '0',recTis.ID, '', '', recTis.LABEL, '', recTis.LABEL, recTis.UNIT, recTis.QUANTITY, recTis.FK_OBJ );
		commit;
	end loop;   
	
	if iLfdNr > 0 then
		for rec in (select * from REPORTOBJ_TA where SID = nSid order by POS) loop
			--select SHORT_NAME into cShort from QUANT where ID = rec.QUANTITY;
			cShort := rec.QUANTITY;
			if substr(cShort,length(cShort),1) = '_' then
				cShort := substr(cShort,1,length(cShort)-1);
			end if;
			cZrKnz := rec.OBJLABEL || '.' || cShort ; 
			-- pr�fen erst ab FROM_DATE in TIS
			cAb := cVon;                        
			cTo := cBis;
			for recTis in (select FROM_DATE_TS, UNTIL_DATE_TS from TIS where ID = rec.FK_TIS) loop
				if recTis.FROM_DATE_TS > dVon then
					cAb := to_char(recTis.FROM_DATE_TS,'dd.mm.yy');
				end if;
				if recTis.UNTIL_DATE_TS < dBis then
					cTo := to_char(recTis.UNTIL_DATE_TS,'dd.mm.yy');
				end if;
				dAb := recTis.FROM_DATE_TS;                       
				dUntil := recTis.UNTIL_DATE_TS;
			end loop;
			-- dAb := to_date(cAb, 'dd.mm.yy');
			-- Werte in tpm ermitteln
			-- Abh�ngig vom Archivtyp (S(spontan) oder T(getaktet) stehen die Werte in den Tabellen TPMAWSN_TA oder  TPMAWTHN_TA
			-- spontane Werte werden als vollst�ndig vorhanden gez�hlt, wenn es einen Eintrag in TPMAWSN_TA <= dVon gibt
			for recTP in (select ARVSPRKNZ, ARVTYP from TPMAS_TA@tpmdb where ZRKNZ = cZrKnz and TKT='H' and VRSKNZ='VRS.TG.BETR') loop
				-- es sollte nur 1 Satz gefunden werden !  
				if recTP.ARVTYP='S' then 
					-- wenn der Zeitbereich au�erhalb des Bereichs liegt, f�r den Daten in Portal vorliegen, dann muss nichts gerechnet werden
					nAnzNull := 0;
					if dVon < dUntil and dBis > dAb then   
						bFound := false;
						for recS in (select count(*) from TPMAWSN_TA@tpmdb where ARVSPRKNZ = recTP.ARVSPRKNZ and ZP <= to_date(cAb, 'dd.mm.yy')) loop
							bFound := true;                                     
							dFirst := to_date(cAb, 'dd.mm.yy');
							-- max. sind Daten zu vergleichen bis dUntil
							dLast  := dBis;                             
							if dBis > dUntil then
								dLast := dUntil;
							end if;
						end loop;
						if not bFound then
							bFound := false;
							for recS in (select min(ZP) ZP from TPMAWSN_TA@tpmdb where ARVSPRKNZ= recTP.ARVSPRKNZ and ZP >= to_date(cAb, 'dd.mm.yy') ) loop
								bFound := true;        
								dFirst := recS.ZP;
								dLast  := dBis;
								if dBis > dUntil then
									dLast := dUntil;
								end if;
							end loop;
						end if;
						nAnzOk := 0;
						if bfound then 
							nAnzOk := (dLast - dFirst) * 24;		-- Achtung: stimmt nur, wenn beide gleiche Uhrzeit (00:00) haben
						end if;       
					else
						nAnzOk := 0;
					end if;
				else
					dFirst := tpcnt(recTP.ARVSPRKNZ, cAb, cTo, nAnzOk, nAnzNull, dLast);  
					dLast := dLast + 1;		-- geht bis Folgetage 0 Uhr (nach letztem gelesenen Containersatz)
					-- wenn dUntil < dVon, dann entf�llt Nachkorrektur
/*					if dUntil > dVon then
						-- wenn dLast > dUntil -> dann wird erwartet, dass L�cke nach dUntil vorliegt sofern #L�cken>nUeber, d.h. die # L�cken wird reduziert
						if dLast > dUntil then
							nUeber := (dLast - dUntil) * 24;
							if nAnzNull >= nUeber then
								nAnzNull := nAnzNull - nUeber;
							else 
								nAnzOk := nAnzOk - nUeber;
							end if;
						end if;
						-- wenn dFirst < dAb -> dann wird erwartet, dass L�cke nach dUntil vorliegt, d.h. die # L�cken wird reduziert
						if dFirst < dAb then
							nUeber := (dAb - dFirst) * 24;
							nAnzNull := nAnzNull - nUeber;
						end if;
					end if;
*/					
					-- Wenn im Zeitbereich keine Werte vorliegen (ergo nichts an Portal �bertragen wurde), dann wird auch die #L�cken=0 gesetzt (obwohl S�tze in TP vorliegen mit null-Werten)
					if nAnzOk = 0 then
						nAnzNull := 0;
					end if;					
				end if;
			end loop;
			-- Werte in NGA ermitteln
			nNgaOk := 0;
			select min(REF_DATE),  max(REF_DATE), count(*) into dMin, dMax, nNgaOk from DD_SYS where FK_TIS=rec.FK_TIS and REF_DATE > dFirst and REF_DATE <= dLast;
			--  in POR_TABVALUES:   VAL1 - # Werte  in Fremdsystem-Zeitreihe
			--						VAL2 - # L�cken in Fremdsystem-Zeitreihe
			--						VAL3 - # Werte in NGA-Zeitreihe
			--						VAL4 - # L�cken in NGA-Zeitreihe
			--						VAL5 - Diff. # Werte 
			--						VAL6 - Diff. # L�cken
			-- L�cken in NGA
			select count(*) into nNgaNull from por.MTR_ARC_CONDITION_TA  where FK_TIS=rec.FK_TIS and REF_DATE > dFirst and REF_DATE <= dLast and CONDITION='M';
			
			if dFirst is not null then
				insert into POR_TABVALUES(SID, FK_OBJLIST, REFDATE, REFCDATE, VAL1, VAL2, VAL3, VAL4, VAL5, VAL6)
						values( nSid, rec.POS, dFirst, to_char(dLast,'dd.mm.yy hh24:mi'), nAnzOk, nAnzNull, nNgaOk, nNgaNull, round(nAnzOk-nNgaOk,0),round(nAnzNull-nNgaNull,0));
				commit;
			end if;
		end loop; 

	end if;
	return nSid;
exception	
	WHEN OTHERS THEN
		raise;
end;


end; -- body TPMPckg
/
