create or replace Package PSI_Pckg AUTHID DEFINER
is 
--------------------------------------------------------------------------------
-- package mit allgemein n�tzlichen Funktionen f�r andere pl/sql-packages
--------------------------------------------------------------------------------
-- LIH 02.05.2014 Performanz-Module in gesondertes package verschoben
-- LIH 02.05.2014 PerformanzIOT: Umstellung - nur Zeitreihen mit mind. 100 Tagen an Daten; Lesen 1 Monat von until-60 bis until-30
-- LIH 24.04.2014 Neu:LongIOT, PerformanzIOT, WaitSec
-- LIH 10.04.2014 Report Find_bad_tis -> nach PSI_Reports verschoben
-- LIH 07.04.2014 Neu: Find_bad_tis - finde und protokolliere Zeitreihenfehler
-- LIH 03.04.2014 Neu: Longtest
-- LIH 31.03.2014 Neu: procedure Performanztest
-- LIH 05.03.2014 UTC2MESZhour - statt 02bXX jetzt 02B00  
-- LIH 21.02.2014 UTC2MESZhour - Korrektur f�r Umschaltstunde Winter (Von-Zeit)
-- LIH 23.10.2013 header erg�nzt
-- LIH 26.04.2013 Neu: Gasday
-- LIH 04.03.2013 Neu: CreateTable, CreateAllIx
-- LIH 25.02.2013 Neu: UTCMonatsbeginn, UTCMonatsendeStd, UTCGasjahrbeginn
-- LIH 13.02.2013 Neu: GetParentId
-- LIH 13.11.2012 Neu: UTCGasDayFirstHour
-- LIH 04.11.2012 Neu: UTCtime
-- LIH 02.11.2012 Neu: UTC2MESZhour
-- LIH 01.11.2012 Erg�nzung hVonBis
-- LIH 30.10.2012 Erstellung        - allg. Funktionen : LastSundayInOct. LastSundayInMar, UTCtoMESZ
	
--------------------------------------------------------------------------------
-- Konstanten
--------------------------------------------------------------------------------
	iGasjahrStartMonat	integer := 10;

	function UTCtime( dDate in date default sysdate ) return date;
	function UTCtoMESZ ( dUTC in date ) return date;
	function UTC2MESZhour ( dUTC in date ) return varchar2;
	function UTCGasDayFirstHour( dDate IN DATE DEFAULT SYSDATE, nStartGasday in number default 6 )	return date;
	function hVonBis( dDate in date ) return varchar2;
	function Gasday( dEndofDay in date ) return varchar2;
	function GetParentId( nObjId in number, cParent_Objtyp in varchar2 ) return number;
	function UTCMonatsbeginn( dDate in date )	return date;													-- Ermittelt die Beginnzeit des (Gas)Monats (in UTC) zum vorgegebenen Zeitstempel, stationsbezogen 
	function UTCMonatsendeStd( dDate in date )	return date;													-- Ermittelt die letzte Stunde des Monats (in UTC) zum vorgegebenen Zeitstempel
	function UTCGasjahrbeginn( dDate in date )	return date;													-- Ermittelt die Beginnzeit des Gasjahres (in UTC) zum vorgegebenen Zeitstempel, stationsbezogen 

	function isMesz( dUTC in date default UTCtime(sysdate) ) return integer;
	function ObjLabel( nObjId in number ) return varchar2;						-- Label zum Objekt mit vorgeg. ID ermitteln
	function WaitSec( iSek in number ) return  number;

	procedure CreateTable  ( cTable in varchar2 default null );
	procedure CreateAllIx( cTabnam in varchar2 default null);
	procedure InsHit2( nLfdNr in out number, cLine in varchar2 );
	Procedure SaveHit;
end;
/

create or replace Package body PSI_Pckg
is

function ObjLabel( nObjId in number ) return varchar2
as
	-- Objektlabel f�r vorgebebene Objekt-ID ermitteln
	cName	varchar2(80) := to_char(nObjId) || ' - Object-ID not found';
begin
	for rec in (select LABEL from MD_OBJECTS where OBJID = nObjId) loop
		cName := rec.LABEL;
	end loop;
	return cName;
end;

function WaitSec( iSek in number ) return  number
as
	-- vorgeg. Anzahl Sekunden warten
	dSys		date := sysdate; 
	iSecPerDay	integer := 24*60*60;
	nC			integer := 0;
begin                       
	While ( (sysdate - dSys)*iSecPerDay < iSek ) loop
		nC := nC + 1;
	end loop;
	return nC;
end;
		
Procedure SaveHit
as
-- Sichern der Tabelle HIT2 in die Langzeit-Tabelle HIT3
-- Ablauf:
--  Max(LFDNR) into nMax3 in HIT3 ermitteln
--  nMax3 auf LFDNR in HIT2 aufaddieren
--  alle S�tze aus HIT2 in HIT3 inserten
--  HIT2 l�schen
--
	nMax3	integer;
	nNr		number(12) := 0;
	iC		integer := 0;
begin
	select max(LFDNR) into nMax3 from HIT3;
	if nMax3 is null then
		nMax3 := 0;
	end if;
	for rec in (select rowid from HIT2 where LFDNR is null) loop
		iC := iC + 1;
		update HIT2 set LFDNR=iC where rowid = rec.rowid;
	end loop;
	update HIT2 set LFDNR = LFDNR + nMax3;
	commit;
	insert into HIT3 (select * from HIT2);
	commit;
	delete from HIT2;
	commit;
end;

procedure InsHit2( nLfdNr in out number, cLine in varchar2 )
as
begin
	if nLfdNr = 0 then
		delete from hit2;
		commit;
	end if;

	nLfdNr := nLfdNr + 1;
	insert into HIT2 values( nLfdNr, cLine );
	commit;
end;


function GetParentId( nObjId in number, cParent_Objtyp in varchar2 ) return number
as
	nParObjId	number := 0;
begin
	-- im Baum hochhangeln bis Objekttyp gefunden ist
	for rec in (select OBJID, OBJECTTYPE from POR_OBJECTS_TA where OBJID = (select PARENTID from POR_OBJECTS_TA where OBJID = nObjId)) loop
		if rec.OBJECTTYPE = cParent_Objtyp then
			nParObjId := rec.OBJID;
		else
			nParObjId := GetParentId( rec.OBJID, cParent_Objtyp );
		end if;
	end loop;
	return nParObjId;
end;

function hVonBis( dDate in date ) return varchar2
as
	-- Datum umformen in hh-1:00 - hh:00
    cBis	varchar2(5) := to_char(dDate,'hh24:mi');
    dVon	date := dDate - 1/24;
    cVon	varchar2(5) := to_char(dVon,'hh24:mi');
begin
    return cVon || ' - ' || cBis;
end;

function Gasday( dEndofDay in date ) return varchar2
as
	-- Eingangsdatum ist eine UTC Zeit mit Gastag-Ende - R�ckgabe ist der zugeh�rige Gastag in der Form XX dd. mit XX= Wochentag, z.B. So
begin
    return to_char(dEndofDay-1,'Dy') || ' ' || to_char(dEndofDay-1,'dd.');
end;


function LastSundayInOct( cYear in varchar2 ) return date
as
	dEndMar	date := to_date( '31.10.' || cYear , 'dd.mm.yyyy' );
	dResult date;
begin
	dResult := dEndMar - (to_char( dEndMar, 'd' ) );
	return dResult;
end;

function LastSundayInMarch( cYear in varchar2 ) return date
as
	dEndMar	date := to_date( '31.03.' || cYear , 'dd.mm.yyyy' );
	dResult date;
begin
	dResult := dEndMar - (to_char( dEndMar, 'd' ) );
	return dResult;
end;

function isMesz( dUTC in date default UTCtime(sysdate) ) return integer
as
	-- pr�fe, ob die vorgegebene UTC-Zeit im Sommerzeitbereich (DE) liegt
	iMesz		integer := 0;
	dMesz		date := UTCtoMESZ(dUTC);  
	nYear		number := to_number(to_char(dMesz,'yyyy'));
	dStartDST 	date := LastSundayInMarch( to_char(dMesz,'yyyy') ) + 2/24;	--  MEZ = 02 Uhr (Umschaltstunde)
	dEndDST		date := LastSundayInOct(to_char(dMesz,'yyyy')) + 3/24;		-- ab 1996
	
begin

	-- MEZ ist eine Stunde sp�ter als UTC
	if nYear >= 1981 then
		if nYear <= 1995 then
			--    1981..1995  : Begin
			dEndDST   := dStartDST + 182 + 1/24;
		end if;             
	end if;	
	if ( dMesz >= dStartDST and dMesz < dEndDST ) then
		iMesz := 1;
	end if;
	return iMesz;
end;
	

function UTCtime( dDate in date default sysdate ) return date
as
	-- ermittle UTC-Zeit aus gesetzlicher Zeit
	dMesz		date := dDate;  
	dUTC		date := dMesz - 1/24;
	nYear		number := to_number(to_char(dMesz,'yyyy'));
	dStartDST 	date := LastSundayInMarch( to_char(dMesz,'yyyy') ) + 2/24;	--  MEZ = 02 Uhr (Umschaltstunde)
	dEndDST		date := LastSundayInOct(to_char(dMesz,'yyyy')) + 3/24;		-- ab 1996
	
begin

	-- MEZ ist eine Stunde sp�ter als UTC
	if nYear >= 1981 then
		if nYear <= 1995 then
			--    1981..1995  : Begin
			dEndDST   := dStartDST + 182 + 1/24;
		end if;             
	end if;	
	if ( dMesz > dStartDST and dMesz < dEndDST ) then
		dUTC := dUTC - 1/24;
	end if;
	return dUTC;
end;

	

function UTCtoMESZ ( dUTC in date ) return date
as
/*
Vor Wiedereinf�hrung der Sommerzeit 1980 war die Berechnung der lokalen Zeit recht einfach : z.B. MEZ := UTC + eine Stunde. 
Um nun aus einem UTC-Zeitstempel die " g�ltige " MEZ bzw. MESZ zu berechnen, mu� eine weitere Stunde zu der Zeit zugez�hlt werden,
wenn die Zeit innerhalb der Sommerzeit liegt. Seit 1981 beginnt die Sommerzeit am letzten Sonntag im M�rz. 
Bis einschlie�lich 1995 endete die S. am letzten Sonntag im September ( 182 Tage / 26 Wochen ), danach am letzten Sonntag im 
Oktober ( 210 Tage / 30 Wochen ).
 
Folgende Function gibt f�r ein Datum/Uhrzeit in UTC die korrekte MEZ/MESZ (gesetzliche Zeit) zur�ck 
*/ 
	dMesz		date;
	nYear		number := to_number(to_char(dUTC,'yyyy'));
	dStartDST 	date := LastSundayInMarch( to_char(dUTC,'yyyy') ) + 1/24;	-- bei UTC 01 Uhr ist MEZ = 02 Uhr (Umschaltstunde)
	dEndDST		date := LastSundayInOct(to_char(dUTC,'yyyy')) + 1/24;		-- ab 1996
	
begin

	-- MEZ ist eine Stunde sp�ter als UTC
	dMesz := dUTC + 1/24;
	if nYear >= 1981 then
		if nYear <= 1995 then
			--    1981..1995  : Begin
			dEndDST   := dStartDST + 182 + 1/24;
		end if;             
	end if;	
	if ( dUTC > dStartDST and dUTC < dEndDST ) then
		dMesz := dMesz + 1/24;
	end if;
	return dMesz;
end;

function UTC2MESZhour ( dUTC in date ) return varchar2
as
/*
	liefert zur vorgegebenen UTC-Zeit die Von-Bis-Zeit in der Form hh-1:mi - hh:mi zur�ck,
	wobei zum Umschaltzeitpunkt MESZ-MEZ geliefert werden:
	02:00 - 02B00  f�r 2A-Stunde
	02BXX - 03:00  f�r 2B-Stunde
*/ 
	dMesz		date;
	nYear		number := to_number(to_char(dUTC,'yyyy'));
	dStartDST 	date := LastSundayInMarch( to_char(dUTC,'yyyy') ) + 1/24;	-- bei UTC 01 Uhr ist MEZ = 02 Uhr (Umschaltstunde)
	dEndDST		date := LastSundayInOct(to_char(dUTC,'yyyy')) + 1/24;		-- ab 1996
    cBis		varchar2(5);
    dVon		date;
    cVon		varchar2(5);
	
begin

	-- MEZ ist eine Stunde sp�ter als UTC
	dMesz := dUTC + 1/24;
	if nYear >= 1981 then
		if nYear <= 1995 then
			--    1981..1995  : Begin
			dEndDST   := dStartDST + 182 + 1/24;
		end if;             
	end if;	
	if ( dUTC > dStartDST and dUTC < dEndDST ) then
		dMesz := dMesz + 1/24;
	end if;                
	
	-- Datum umformen in hh-1:00 - hh:00
    dVon	:= dMesz - 1/24;
	if dUTC = dEndDST then
	    cBis	:= to_char(dMesz,'hh24')  || 'B00';
	    dVon    := dVon + 1/24;  -- wir schalten um
	else
	    cBis	:= to_char(dMesz,'hh24:mi');
	end if;
	if dUTC = dEndDST + 1/24 then
	    cVon	:= to_char(dVon,'hh24') || 'B00';
	else
	    cVon	:= to_char(dVon,'hh24:mi');
	end if;
    return cVon || ' - ' || cBis;
end;

--------------------------------------------------------------------------------
-- UTCGasDayFirstHour
-- Ermittelt zu vorgegebener gesetzlicher Zeit den Gastagstart in UTC-Zeit
-- F�r 6 Uhr Gastagbeginn ist die erste Stunde die 7-Uhr Stunde (gesetzl. Zeit), in UTC also 5-Uhr im Sommer, sonst 6 Uhr
-- In: 	dDate			date	Vorgabedatum
--      nStartGasday    number	Gastag-Beginn-Zeit, z.B. 0 oder 6
-- Return: date		UTC-Zeit der ersten registrierten Stunde des Gastags
function UTCGasDayFirstHour( dDate IN DATE DEFAULT SYSDATE, nStartGasday in number default 6 )	return date
as
	cGasDay varchar2(10) := to_char( dDate, 'dd.mm.yyyy' );
	cDate	varchar2(19) := to_char( dDate, 'dd.mm.yyyy hh24:mi:ss' );
	nHour	number(2);
	nMin	number(2);
	nSec	number(2);
	dStartDate	date;

begin
	nHour := to_number( substr(cDate,12,2),'99');
	nMin := to_number( substr(cDate,15,2),'99');
	nSec := to_number( substr(cDate,18,2),'99');

	if nStartGasday = 0 then
		if nHour = 0 and nMin = 0 and nSec = 0 then
			cGasDay :=  to_char( dDate-1, 'dd.mm.yyyy' );  -- 0-Uhr Stunde geh�rt zu Vortag
		end if;
	else
		if nHour < nStartGasday or ( nHour = nStartGasday and nMin = 0 and nSec = 0) then
			cGasDay :=  to_char( dDate-1, 'dd.mm.yyyy' );
		end if;
	end if;

	dStartDate := to_date( cGasDay || to_char( nStartGasday+1, '00') || '00:00', 'dd.mm.yyyy hh24:mi:ss' );

	RETURN UTCTime(dStartDate);
end;

--------------------------------------------------------------------------------
-- UTCMonatsendeStd
-- Ermittelt die letzte Stunde des Monats (in UTC) zum vorgegebenen UTC-Zeitstempel (i.a. 01.mm.yyyy 06:00)
-- In: 	dDate		date	Vorgabedatum - 
-- Return: date - Gasmonatsendestunde in UTC 
function UTCMonatsendeStd( dDate in date )	return date	
as	
    dEnde		date;
begin
	dEnde := add_months( UTCMonatsbeginn(dDate), 1);
	return dEnde;
end;
--------------------------------------------------------------------------------
-- UTCMonatsbeginn
-- Ermittelt die Beginnzeit des (Gas)Monats (in UTC) zum vorgegebenen Zeitstempel
-- In: 	dDate		date	Vorgabedatum (in UTC)
-- Return: date - Gasmonatsbeginnstunde in UTC 
function UTCMonatsbeginn( dDate in date )
	return date	
as	
	nBeginn		number := 1040000;      -- 1. Tag, UTC 4 Uhr  => 6 MESZ
    dBeginn		date;	-- Beginn-Datum in UTC           
    nZeit		number := to_number(to_char(dDate,'ddhh24miss'));
    nMonat		number := to_number( to_char( dDate, 'MM' ));
    
begin
	if nMonat <= 4 or nMonat >= 11 then
		nBeginn := 1050000; 
		if nMonat = 4 and nZeit >= 1040000 then
				-- Winterzeit -> 5 Uhr UTC ist Gastagbeginn
				nBeginn := 1040000;
			end if;
		else                                        
			-- Gasmonat Okt endet am 1. Nov 5 Uhr UTC
			if nMonat = 11 and nZeit < 1050000 then
				nBeginn := 1040000; 
		end if;               
	end if;
	if nBeginn = 1040000 then
		dBeginn := to_date( '01.' || to_char(dDate,'mm.yyyy') || '04:00:00', 'dd.mm.yyyy hh24:mi:ss');
	else
		dBeginn := to_date( '01.' || to_char(dDate,'mm.yyyy') || '05:00:00', 'dd.mm.yyyy hh24:mi:ss');
	end if;	
	if nZeit < nBeginn then
		dBeginn := add_months( dBeginn, -1);		-- gleiche Zeit im Vormonat
	end if;			
	return dBeginn;
end;

--------------------------------------------------------------------------------
-- UTCGasjahrbeginn
-- Ermittelt die Beginnzeit des Gasjahres (in UTC) zum vorgegebenen Zeitstempel, stationsbezogen 
-- Gasjahrbeginn immer am 01.10.   6 Uhr (MESZ)
-- In: 	dDate		date	Vorgabedatum - 
-- Return: date - Gasmonatsbeginnstunde in UTC 
function UTCGasjahrbeginn( dDate in date )
	return date													
as	
	nBeginn		number := 6;    
    dBeginn		date;	-- Beginn-Datum in UTC 
    cGasjahr	varchar2(5);
    nZeit		number := to_number(to_char(dDate,'ddhh24miss'));
    nMonat		number := to_number( to_char( dDate, 'MM' ));
begin
	if nMonat >= iGasjahrStartMonat then
		if nMonat = iGasjahrStartMonat and nZeit < 1040000 then
			cGasjahr := to_char(to_number(to_char( dDate, 'YYYY' )) - 1,'0009');
		else
			cGasjahr := to_char( dDate, 'YYYY' );
		end if;
	else
		cGasjahr := to_char(to_number(to_char( dDate, 'YYYY' )) - 1,'0009');
	end if;	
	-- Gasjahr-Start = 01. + iGasjahrStartMonat + Gasjahr
	dBeginn := to_date( '01.' || to_char(iGasjahrStartMonat,'09') || cGasjahr || to_char(nBeginn,'09') || '00:00', 'dd.mm.yyyy hh24:mi:ss');
	return UTCtime(dBeginn);
end;  

procedure CreateTable  ( cTable in varchar2 default null )
is
--	cTable varchar2(32) := '';
	cTS   varchar2(30);
	cLine varchar2(80);
	iCount integer;
	i integer := 0;
    bAll	integer := 0;
    cTabnam	USER_TABLES.TABLE_NAME%type;
begin               
	if cTable is null then
		bAll := 1;
	end if;
	
	select TABLESPACE_NAME into cTS from USER_TABLES where TABLE_NAME='REPORTGRP_TA';
	if cTs is null then
		dbms_output.put_line( 'Tabelle REPORTGRP_TA nicht im Schema enthalten ->falsches Schema ->Abbruch');
	else	
		for recUsertab in (select TABLE_NAME from USER_TABLES where bAll=1 or TABLE_NAME=cTable) loop
			cTabnam := recUsertab.TABLE_NAME;
			dbms_output.put_line('create table '|| cTabnam || '(');
			select count(*) into iCount from USER_TAB_COLUMNS where TABLE_NAME = cTabnam;
			i := 0;
			for rec in (select COLUMN_NAME, DATA_TYPE, DATA_LENGTH, DATA_PRECISION, DATA_SCALE, NULLABLE, COLUMN_ID 
							from  USER_TAB_COLUMNS where TABLE_NAME = cTabnam order by column_id) loop
				i := i + 1;
				cLine := '    ' || rpad(rec.COLUMN_NAME,30) || '   ' || rec.DATA_TYPE;
				if rec.DATA_TYPE <> 'DATE' then
					if rec.DATA_TYPE <> 'NUMBER' then
					    cLine := cLine || '(' || to_char(rec.DATA_LENGTH) || ')';
					else
						if rec.DATA_PRECISION is not null then
						    cLine := cLine || '(' || to_char(rec.DATA_PRECISION);
							if rec.DATA_SCALE is not null and rec.DATA_SCALE > 0
							then
								cLine := cLine || ', ' || rec.DATA_SCALE;
							end if;
						    cLine := cLine || ')'; 
						 end if;
					end if;
				end if;
				if rec.nullable = 'Y' then
					cLine := cLine || ',';
				else
					cLine := rpad(cLine,60) || 'not null,';
				end if;
				if i = iCount then
					cLine := substr(cLine, 1, length(cLine)-1); -- ggf. das Komma am Ende l�schen
				end if;
				dbms_output.put_line(  cLine );
			end loop;
			dbms_output.put_line( ') tablespace ' || cTS );
			dbms_output.put_line( '/');
			dbms_output.put_line( ' ');
		end loop;
	end if;
end;
 
procedure CreateAllIx( cTabnam in varchar2 default null) 
is
	cStmt		varchar2(1000) := 'create index '; 
	iAll		integer := 0;
	cTS   		varchar2(30);
begin
	select TABLESPACE_NAME into cTS from USER_TABLES where TABLE_NAME='REPORTGRP_TA';
	cTs := substr(cTs,1,4) || 'IND_TS';
	if cTabnam is null then
		iAll := 1;
	end if;
	for recIx in (select INDEX_NAME, TABLE_NAME, UNIQUENESS from USER_INDEXES where iAll=1 or TABLE_NAME=cTabnam  order by TABLE_NAME) loop 
		if recIx.UNIQUENESS = 'UNIQUE' then
		    cStmt		:= 'alter table ' ||  recIx.TABLE_NAME || ' add ( constraint ' || recIx.INDEX_NAME || ' primary key('; 
		else		
		    cStmt		:= 'create index ' || recIx.INDEX_NAME || ' on ' ||  recIx.TABLE_NAME || ' ('; 
		end if;	    
		for recCol in (select COLUMN_NAME, COLUMN_POSITION from USER_IND_COLUMNS where INDEX_NAME = recIx.INDEX_NAME	order by COLUMN_POSITION) loop
			if recCol.COLUMN_POSITION = 1 then
				cStmt := cStmt || recCol.COLUMN_NAME;
			else
				cStmt := cStmt || ', ' || recCol.COLUMN_NAME;
			end if;
		end loop;

		if recIx.UNIQUENESS = 'UNIQUE' then
			cStmt := cStmt || ') )';
			dbms_output.put_line( 'alter table ' ||  recIx.TABLE_NAME || ' drop constraint ' || recIx.INDEX_NAME );
			execute immediate  'alter table ' ||  recIx.TABLE_NAME || ' drop constraint ' || recIx.INDEX_NAME ;
		else
			cStmt := cStmt || ') tablespace ' || cTS;
			dbms_output.put_line( 'drop index '|| recIx.INDEX_NAME );
			execute immediate 'drop index ' || recIx.INDEX_NAME;
		end if;
		dbms_output.put_line( cStmt );
		execute immediate cStmt;
	end loop;		
		
end;

end; -- pckg
/
