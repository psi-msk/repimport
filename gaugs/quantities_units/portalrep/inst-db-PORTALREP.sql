--  ******************************************************************
--  PSI AG, 2014
-- 
--  Data model PSIMetering
--
--  $Author: emshp $
--  $Date: 2011/07/11 14:58:05 $
--  $Revision: 1.4 $
--  $State: Exp $
--
--
--  Date        Author       Comments
--  ----------  -----------  -------------------------------------------
--  01.10.2012  nnn          Create
--  15.02.2013  R. Undt      New File Names
--  06.05.2014  H. Pohl		 Add synonyms for MTR_EXT_IMPORTCTL_VI, MTR_EXT_WF_HIST_VI,
--                           MTR_EXT_ABSENTVALUES_VI, MTR_EXT_BILANZOBJ_VI,
--                           MTR_EXT_ERRORINFO_VI, MTR_EXT_IMPORT_OBJ_VI
--							 Del synonym for MTR_SUMMERTIME_TA 
--  08.05.2014  H. Liebhold	 Added creates for tables, synonyms, functions, packages and views
--  21.07.2014  H. Liebhold	 Define synonyms before packages
--  26.08.2014  H. Liebhold  package perform_pckg past psi_pckg
--  03.09.2014  H. Liebhold  package perform_pckg past psireports_pckg
--  ******************************************************************

set serveroutput on
set term on 

spool inst-db-PORTALREP.log

--
-- Defines
--
@./defines.sql

whenever sqlerror exit
declare
   v_text varchar2(100);
begin
   select 'Schema ok' 
     into v_text
     from dual 
    where upper(user) = upper('&porrepuser.');
   dbms_output.put_line(v_text);
exception when others then 
   dbms_output.put_line('!');
   dbms_output.put_line('!!!!!!!!!!!!!');
   dbms_output.put_line('!');
   dbms_output.put_line('!  Connected user is not the defined PORTALREP-User: &porrepuser. !');
   dbms_output.put_line('!');
   dbms_output.put_line('!!!!!!!!!!!!!');
   dbms_output.put_line('!');
   raise no_data_found;
end;
/

--  ******************************************************************
--  **************************** TABLES ******************************
--  ******************************************************************

   -- Parameter-No.                           01          02              03
@tab/DDSYS.sql                          &ts_name.   &ts_name_ind.   &ck_cons
@tab/HIT2.sql                          &ts_name.   &ts_name_ind.   &ck_cons
@tab/HIT3.sql                          &ts_name.   &ts_name_ind.   &ck_cons
@tab/JSTAT.sql                          &ts_name.   &ts_name_ind.   &ck_cons
@tab/OBJSTATE_TA.sql                    &ts_name.   &ts_name_ind.   &ck_cons
@tab/PERFORMANZ.sql               		&ts_name.   &ts_name_ind.   &ck_cons
@tab/POR_BAD_TIS.sql               		&ts_name.   &ts_name_ind.   &ck_cons
@tab/POR_OBJECTS_TA.sql                 &ts_name.   &ts_name_ind.   &ck_cons
@tab/POR_TABHEAD.sql                    &ts_name.   &ts_name_ind.   &ck_cons
@tab/POR_TABVALUES.sql               	&ts_name.   &ts_name_ind.   &ck_cons
@tab/QUANZUO.sql              		&ts_name.   &ts_name_ind.   &ck_cons
@tab/REPORTGRP_TA.sql                 	&ts_name.   &ts_name_ind.   &ck_cons
@tab/REPORTOBJ_TA.sql              		&ts_name.   &ts_name_ind.   &ck_cons
                            
--  ******************************************************************
--  *************************** SYNONYMS  ****************************
--  ******************************************************************

@syn/SYNONYMS_portalrep.sql

--  ******************************************************************
--  *************************** FUNCTIONS ****************************
--  ******************************************************************
@fnc/objlabel.sql
                            
--  ******************************************************************
--  *************************** PROCEDURES ***************************
--  ******************************************************************
--@prc/xx.sql

--  ******************************************************************
--  *************************** SEQUENCES ****************************
--  ******************************************************************
--@seq/cre_sequences.sql

--  ******************************************************************
--  **************************** VIEWS *******************************
--  ******************************************************************
@vi/v_bad_tis.sql
@vi/v_dist_tis.sql

--  ******************************************************************
--  *************************** PACKAGES *****************************
--  ******************************************************************
@pac/psi_pckg.sql
@pac/psireports.sql

--  ******************************************************************
--  ***************************** DATA  ******************************
--  ******************************************************************

--@ntd/mtr_insert_MtrEventsTa_&instlang..sql
-- Is there no internationalized version of this file?
--@ntd/mtr_insert_MtrAttribtextTa_en.sql;

--  ******************************************************************
--  **************************** GRANTS  *****************************
--  ******************************************************************


spool off

commit;

--  *************************** STOP - SUCCESS ****************************
prompt STOP - Success

spool off
