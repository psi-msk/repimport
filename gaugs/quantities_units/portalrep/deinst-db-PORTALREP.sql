--  ******************************************************************
--  PSI AG, 2013
-- 
--  Data model PSImetering
--
--  $Author: emshp $
--  $Date: 2011/07/08 14:55:19 $
--  $Revision: 1.3 $
--  $State: Exp $
--
--  ----------  -----------  -------------------------------------------
--  18.11.2011  nnn          Create
--  15.02.2013  R. Undt      Changed to PLSQL-delete script
--  ******************************************************************

set serveroutput on
set term on 
whenever SQLERROR continue

spool deinst-db-PORTALREP.log

SET SERVEROUTPUT ON SIZE 1000000
BEGIN
  FOR cur_rec IN (SELECT object_name, object_type 
                  FROM   user_objects
                  WHERE  object_type IN ('TABLE', 'VIEW', 'PACKAGE', 'PROCEDURE', 'FUNCTION', 'SEQUENCE', 'SYNONYM')) LOOP
    BEGIN
      IF cur_rec.object_type = 'TABLE' THEN
        EXECUTE IMMEDIATE 'DROP ' || cur_rec.object_type || ' "' || cur_rec.object_name || '" CASCADE CONSTRAINTS PURGE';
      ELSE
        EXECUTE IMMEDIATE 'DROP ' || cur_rec.object_type || ' "' || cur_rec.object_name || '"';
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        DBMS_OUTPUT.put_line('FAILED: DROP ' || cur_rec.object_type || ' "' || cur_rec.object_name || '"');
    END;
  END LOOP;
END;
/


--  *************************** STOP - SUCCESS ****************************
whenever SQLERROR exit SQL.SQLCODE
prompt STOP - Success

spool off