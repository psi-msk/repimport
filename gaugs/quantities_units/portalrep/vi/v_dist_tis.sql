--  ******************************************************************
--  Copyright (C) PSI AG, 2014
--  PSI AG, 2014
-- 
--  Data model PSIportal
--
--  $Author: emshl $
--
--  Date        Author       Comments
--  ----------  -----------  -----------------------------------------
--  21.05.2014  H. Liebhold  creation with header
--  ******************************************************************
CREATE OR REPLACE VIEW V_DIST_TIS  AS
select b.SID,o.MANDANT, objlabel(FK_OBJID) "OBJEKTLABEL", FK_OBJID, 
	FK_TIS_SYS, t2.QUANTITY || '[' || t2.UNIT || ']' "Sys-Gr��e[Einh]", t2.TIME_LEVEL "Sys-Zeitstufe", VON, BIS, BESCHREIBUNG
 from POR_BAD_TIS b, TIS t2, MD_OBJECTS o
 where b.FK_TIS_SYS = t2.ID(+)
   and b.FK_OBJID = o.OBJID