--  ******************************************************************
--  Copyright (C) PSI AG, 2014
--  PSI AG, 2014
-- 
--  Data model PSIportal
--
--  $Author: emshl $
--
--  Date        Author       Comments
--  ----------  -----------  -----------------------------------------
--  07.05.2014  H. Liebhold  creation with header
--  ******************************************************************
create or replace VIEW V_BAD_TIS  AS
select b.SID,o.MANDANT, objlabel(FK_OBJID) "OBJEKTLABEL", FK_OBJID, FK_TIS_ORI, t1.QUANTITY || '[' || t1.UNIT || ']' "Ori-Gr��e[Einh]", 
	t1.TIME_LEVEL "Ori-Zeitstufe", t1.FROM_DATE_TS "ORI-Von", t1.UNTIL_DATE_TS "ORI-Bis",
	FK_TIS_SYS, t2.QUANTITY || '[' || t2.UNIT || ']' "Sys-Gr��e[Einh]", t2.TIME_LEVEL "Sys-Zeitstufe", t2.FROM_DATE_TS "SYS-Von", 
	t2.UNTIL_DATE_TS "SYS-Bis", BESCHREIBUNG
 from POR_BAD_TIS b, TIS t1, TIS t2, MD_OBJECTS o
 where b.FK_TIS_ORI = t1.ID(+)
   and b.FK_TIS_SYS = t2.ID(+)
   and b.FK_OBJID = o.OBJID
/
