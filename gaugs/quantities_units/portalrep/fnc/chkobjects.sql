create or replace function ChkObjects return number
as
	-- LIH 08.12.2013 S�tze mit gleicher Objekt-ID d�rfen nur 1x �bernommen werden/ Einbau Schleife
	-- LIH 12.02.2013 Datensatz in OBJSTATE_TA automatisch anlegen, falls nicht vorhanden              
	-- LIH 29.12.2012 Erweiterung bzgl LASTUPDATE
	-- LIH 28.11.2012 Erstellung
	
	-- Die Funktion pr�ft, ob die Tabelle POR_OBJECTS_TA aktuell ist und aktualisiert sie, wenn sie nicht aktuell ist.
	-- R�ckgabewert: vorgefundener Zustand - 0 = nicht aktuell, 1 = aktuell
	-- Das Attribut AKTUELL wird �ber einen insert,update,delete-Trigger auf POR.MTR_OBJECTS_TA auf 0 (=nicht aktuell) gesetzt
	-- und �ber die Funktion ChkObjects() auf 1 gesetzt, nachdem die Tabelle POR_OBJECTS_TA zuvor von ihr aktualisiert wurde.
	nAktuell	number;
	dLast		date := sysdate - 1;
	iC			integer; 
	nObjId		number;
	pragma autonomous_transaction;  -- necessary because of insert or deletes while the function is used in a select                                                 
begin
	select count(*) into iC from OBJSTATE_TA;
	if iC > 0 then
		select AKTUELL, LASTUPDATE into nAktuell, dLast from OBJSTATE_TA where rownum <2;
	else
		insert into OBJSTATE_TA values( 0, dLast );
		commit;
	end if;
	          
	-- Um ein mehrfaches Ausl�sen der �bernahme aufgrund einer Massen�bernahme von Objekten in die Portal-Objekttabelle auszuschlie�en,
	-- wird maximal 1x pro Minute �bernommen
	if nAktuell = 0 and dLast < sysdate - 1/(24*60) then
		execute immediate 'truncate table POR_OBJECTS_TA';       
		nObjId := 0;
		for rec in (select * from MD_OBJECTS order by OBJID) loop
			if rec.OBJID <> nObjId then
				insert into POR_OBJECTS_TA values rec;
			end if;
			nObjId := rec.OBJID;
		end loop;
		update OBJSTATE_TA set AKTUELL = 1, LASTUPDATE = sysdate;
		commit;                            
	end if;
	return nAktuell;
end;	
/
