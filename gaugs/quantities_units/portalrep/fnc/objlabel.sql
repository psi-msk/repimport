create or replace function ObjLabel( nObjId in number ) return varchar2 AUTHID DEFINER
as
	-- Objektlabel f�r vorgebebene Objekt-ID ermitteln
	-- LIH 09.11.2012 Erstellung
	-- LIH 18.07.2014 Zugriff �ber View statt Tabelle in por-Schema
	cName	varchar2(80);
begin
	for rec in (select LABEL from MD_OBJECTS where OBJID = nObjId) loop
		cName := rec.LABEL;
	end loop;
	return cName;
end;
/
