create or replace function GetEHT( nId  in number ) return varchar2  authid definer
as                                                                   
	-- LIH 17.10.2014 nicht-Standard-Einheit ermitteln
	-- Inputpar: nId = FK_QUN aus QUANT
	cUnit	varchar2(50);
begin
	for recQ in (select DEFAULT_UNIT from QUANT where FK_QUN = nId) loop
		cUnit := recQ.DEFAULT_UNIT;
		for rec in (select * from por.MTR_QUANTITY_UNITS_TA where ID = nId) loop
			if rec.FK_UNITS <> recQ.DEFAULT_UNIT then
				cUnit := rec.FK_UNITS;
			end if;
		end loop;        
	end loop;
	return (cUnit);
end;