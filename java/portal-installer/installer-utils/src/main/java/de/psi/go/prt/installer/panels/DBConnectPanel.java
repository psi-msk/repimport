/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.prt.installer.panels;

import java.awt.TextArea;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;

import com.izforge.izpack.api.data.Panel;
import com.izforge.izpack.core.resource.ResourceManager;
import com.izforge.izpack.installer.data.GUIInstallData;
import com.izforge.izpack.installer.gui.InstallerFrame;
import com.izforge.izpack.installer.gui.IzPanel;

public abstract class DBConnectPanel extends IzPanel
{
    private final static String driverClassName = "oracle.jdbc.driver.OracleDriver";

    private enum SERVICE_TYPE
    {
        SID("", ":"),
        SERVICE_NAME("//", "/");

        final String hostPrefix;
        final String delimiter;

        private SERVICE_TYPE(String hostPrefix, String delimiter)
        {
            this.hostPrefix = hostPrefix;
            this.delimiter = delimiter;
        }

        static SERVICE_TYPE fromString(final String izVar)
        {
            for(SERVICE_TYPE m : SERVICE_TYPE.values())
            {
                if(m.toString().equals(izVar))
                {
                    return m;
                }
            }

            throw new IllegalArgumentException(izVar + " cannot be converted to " + SERVICE_TYPE.class.getName());
        }
    }

    // //////////////////////////////////////////////////////////////////////
    DBConnectPanel(
            String userVar,
            String passwordVar,
            String hostVar,
            String portVar,
            String sidVar,
            String connectStringVar,
            String doCheckConnVar,
            String serviceTypeVar,
            Panel panel,
            InstallerFrame iframe,
            GUIInstallData idata,
            ResourceManager rman)
    {
        super(panel, iframe, idata, rman);

        this.userVar = userVar;
        this.passwordVar = passwordVar;
        this.hostVar = hostVar;
        this.portVar = portVar;
        this.sidVar = sidVar;
        this.connectStringVar = connectStringVar;
        this.doCheckConnVar = doCheckConnVar;
        this.serviceTypeVar = serviceTypeVar;
    }

    // //////////////////////////////////////////////////////////////////////
    @Override
    public void panelActivate()
    {
        final String connString = this.makeConnectString();
        setConnectStringVar(connString);

        final String doCheckConnStr = this.installData.getVariable(doCheckConnVar);
        final boolean doCheckConnBool = boolVarToBoolean(doCheckConnStr);

        if(!doCheckConnBool)
        {
            parent.skipPanel();
            return;
        }

        if(null == ta)
        {
            ta = new TextArea("", 20, 100);
            add(ta);
        }

        final String connMessageLine = "Connect String " + connString;
        final char underlineChar = '=';
        char[] chars = new char[connMessageLine.length()];
        Arrays.fill(chars, underlineChar);
        final String underline = String.valueOf(chars);
        final String header = connMessageLine + "\n" + underline + "\n" + "\n";

        Connection connection = null;
        boolean connectionWorks = true;
        try
        {
            Class.forName(driverClassName);
            connection = DriverManager.getConnection(connString);
        }
        catch(SQLException ex)
        {
            ta.setText(header + ex.getMessage());
            connectionWorks = false;
        }
        catch(Exception ex)
        {
            StringBuilder sb = new StringBuilder();
            sb.append(header);
            sb.append("Connection cannot be checked due to internal installer error.\n");
            sb.append("If reporting this error, please include the following diagnostic information:\n");
            sb.append(underline).append("\n");
            sb.append("Exception type:\n");
            sb.append(ex.getClass().getName()).append("\n");
            sb.append(underline).append("\n");
            sb.append("Exception message:\n");
            sb.append(ex.getMessage()).append("\n");
            sb.append(underline).append("\n");
            sb.append("Stack trace:\n");
            sb.append(stackTraceToString(ex)).append("\n");
            ta.setText(sb.toString());
            connectionWorks = false;
        }
        finally
        {
            try
            {
                if(null != connection)
                {
                    connection.close();
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        if(connectionWorks)
        {
            ta.setText(header + "Connection works!");
        }

        // Debug.trace("DBConnectPanel: PanelActivate done");
    }

    // //////////////////////////////////////////////////////////////////////
    protected String stackTraceToString(Throwable thr)
    {
        StringBuilder sbld = new StringBuilder();
        for(StackTraceElement selem : thr.getStackTrace())
        {
            sbld.append(selem.toString()).append("\n");
        }
        return sbld.toString();
    }

    // //////////////////////////////////////////////////////////////////////
    private String makeConnectString()
    {
        final SERVICE_TYPE stype = SERVICE_TYPE.fromString(this.installData.getVariable(this.serviceTypeVar));

        final String user = this.installData.getVariable(this.userVar);
        final String password = this.installData.getVariable(this.passwordVar);
        final String host = this.installData.getVariable(this.hostVar);
        final String port = this.installData.getVariable(this.portVar);
        final String sid = this.installData.getVariable(this.sidVar);

        return "jdbc:oracle:thin:"
                + user
                + "/"
                + password
                + "@"
                + stype.hostPrefix
                + host
                + ":"
                + port
                + stype.delimiter
                + sid;
    }

    // //////////////////////////////////////////////////////////////////////
    private void setConnectStringVar(String connectString)
    {
        this.installData.setVariable(this.connectStringVar, connectString);
    }

    protected boolean boolVarToBoolean(final String izVar)
    {
        if(null == izVar || !izVar.equals("true"))
        {
            return false;
        }

        return true;
    }

    private TextArea ta = null;

    // Names of the variables.
    private final String userVar;
    private final String passwordVar;
    private final String hostVar;
    private final String portVar;
    private final String sidVar;
    private final String connectStringVar;
    private final String doCheckConnVar;
    private final String serviceTypeVar;

    private static final long serialVersionUID = -671492806268647170L;
}
