/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.prt.installer.panels;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import com.izforge.izpack.api.data.InstallData;
import com.izforge.izpack.api.resource.Resources;

public class EditClientWarsMethodHelper
{
    private final Resources resources;

    public EditClientWarsMethodHelper(Resources resources)
    {
        this.resources = resources;
    }

    public void editClientWars(InstallData installData)
    {
        String deployLastDir = null;
        if(installData.getVariable("InstallType").equals("webserver"))
        {
            deployLastDir = installData.getVariable("WEBAPPSDIR");
        }
        else
        {
            deployLastDir = installData.getVariable("DEPLOYDIR") + "/deploy.last";
        }
        ZipFile zf = null;
        List<String> clientNames = new ArrayList<String>();
        if(installData.getVariable("client_de").equals("true"))
        {
            clientNames.add("/portalde_tmp.war");
        }
        if(installData.getVariable("client_en").equals("true"))
        {
            clientNames.add("/portalen_tmp.war");
        }
        if(installData.getVariable("client_ru").equals("true"))
        {
            clientNames.add("/portalru_tmp.war");
        }

        for(String clientName : clientNames)
        {
            try
            {
                zf = new ZipFile(deployLastDir + clientName);
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }

            Enumeration<? extends ZipEntry> entries = zf.entries();
            ZipOutputStream zosOutputStream = null;
            try
            {
                zosOutputStream = new ZipOutputStream(
                        new FileOutputStream(deployLastDir + clientName.replace("_tmp", "")));
            }
            catch(FileNotFoundException e)
            {
                e.printStackTrace();
            }

            ZipOutputStream zosCustomOutputStream = null;
            if(installData.getVariable("custom_guis").equals("true"))
            {
                try
                {
                    zosCustomOutputStream = new ZipOutputStream(
                            new FileOutputStream(deployLastDir + clientName.replace("_tmp", "_custom")));
                }
                catch(FileNotFoundException e)
                {
                    e.printStackTrace();
                }
            }

            final String bindAddress = installData.getVariable("bindAddress");
            final String portSetOptions = installData.getVariable("portSetOptions");
            String port = "";
            if(portSetOptions.equals("ports-default"))
            {
                port = "1099";
            }
            else if(portSetOptions.equals("ports-01"))
            {
                port = "1199";
            }
            else if(portSetOptions.equals("ports-02"))
            {
                port = "1299";
            }
            else if(portSetOptions.equals("ports-03"))
            {
                port = "1399";
            }

            while(entries.hasMoreElements())
            {
                ZipEntry entry = entries.nextElement();
                ZipEntry newEntry = new ZipEntry(entry.getName());
                try
                {
                    zosOutputStream.putNextEntry(newEntry);
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                }
                BufferedInputStream bis = null;
                try
                {
                    bis = new BufferedInputStream(zf.getInputStream(entry));
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                }

                BufferedInputStream bisCustom = null;
                if(installData.getVariable("custom_guis").equals("true"))
                {
                    ZipEntry newCustomEntry = new ZipEntry(entry.getName());
                    try
                    {
                        zosCustomOutputStream.putNextEntry(newCustomEntry);
                    }
                    catch(IOException e)
                    {
                        e.printStackTrace();
                    }
                    try
                    {
                        bisCustom = new BufferedInputStream(zf.getInputStream(entry));
                    }
                    catch(IOException e)
                    {
                        e.printStackTrace();
                    }
                }

                if("psiportal-config.xml".equals(entry.getName()) && bis != null)
                {
                    BufferedReader in = new BufferedReader(new InputStreamReader(bis));
                    String line;
                    try
                    {
                        while((line = in.readLine()) != null)
                        {
                            line = line.replaceAll(Pattern.quote("127.0.0.1"), bindAddress); // NOPMD by sthomas on
                            // 29.08.13 16:39
                            line = line.replaceAll(Pattern.quote("1299"), port);
                            line += "\n";
                            zosOutputStream.write(line.getBytes());
                        }
                    }
                    catch(IOException e)
                    {
                        e.printStackTrace();
                    }
                    try
                    {
                        in.close();
                    }
                    catch(IOException e1)
                    {
                        e1.printStackTrace();
                    }
                    if(installData.getVariable("custom_guis").equals("true"))
                    {
                        BufferedReader inCustom = new BufferedReader(
                                new InputStreamReader(resources.getInputStream("psiportal-config_custom.xml")));
                        String lineCustom;
                        try
                        {
                            while((lineCustom = inCustom.readLine()) != null)
                            {
                                lineCustom = lineCustom.replaceAll(
                                        Pattern.quote("127.0.0.1"), // NOPMD by sthomas on
                                        // 29.08.13 16:38
                                        bindAddress);
                                lineCustom = lineCustom.replaceAll(Pattern.quote("1299"), port);
                                lineCustom += "\n";
                                zosCustomOutputStream.write(lineCustom.getBytes());
                            }
                        }
                        catch(IOException e)
                        {
                            e.printStackTrace();
                        }
                        try
                        {
                            inCustom.close();
                        }
                        catch(IOException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
                else
                {
                    try
                    {
                        if(bis != null)
                        {
                            while(bis.available() > 0)
                            {
                                zosOutputStream.write(bis.read());
                            }
                        }
                    }
                    catch(IOException e)
                    {
                        e.printStackTrace();
                    }
                    if(installData.getVariable("custom_guis").equals("true"))
                    {
                        try
                        {
                            while(bisCustom.available() > 0)
                            {
                                zosCustomOutputStream.write(bisCustom.read());
                            }
                        }
                        catch(IOException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
                try
                {
                    zosOutputStream.closeEntry();
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                }
                try
                {
                    if(bis != null)
                    {
                        bis.close();
                    }
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                }
                if(installData.getVariable("custom_guis").equals("true"))
                {
                    try
                    {
                        zosCustomOutputStream.closeEntry();
                    }
                    catch(IOException e)
                    {
                        e.printStackTrace();
                    }
                    try
                    {
                        bisCustom.close();
                    }
                    catch(IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
            try
            {
                if(zosOutputStream != null)
                {
                    zosOutputStream.finish();
                }
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
            try
            {
                if(zosOutputStream != null)
                {
                    zosOutputStream.close();
                }
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
            if(installData.getVariable("custom_guis").equals("true"))
            {
                try
                {
                    zosCustomOutputStream.finish();
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                }
                try
                {
                    zosCustomOutputStream.close();
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                }
            }
            try
            {
                zf.close();
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    public void cleanupClientWars(InstallData installData)
    {
        String deployLastDir = null;
        if(installData.getVariable("InstallType").equals("webserver"))
        {
            deployLastDir = installData.getVariable("WEBAPPSDIR");
        }
        else
        {
            deployLastDir = installData.getVariable("DEPLOYDIR") + "/deploy.last";
        }
        File directory = new File(deployLastDir);

        File[] toBeDeleted = directory.listFiles(new FileFilter()
        {
            @Override
            public boolean accept(File theFile)
            {
                if(theFile.isFile())
                {
                    return theFile.getName().endsWith("_tmp.war");
                }
                return false;
            }
        });

        boolean error = false;
        for(File deletableFile : toBeDeleted)
        {
            if(deletableFile.delete() == false)
            {
                error = true;
            }
        }
        if(error)
        {
            System.out.println("[ Could not delete all temporary client WAR files. ]");
        }

    }
}
