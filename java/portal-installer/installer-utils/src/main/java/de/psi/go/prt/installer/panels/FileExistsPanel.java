/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.prt.installer.panels;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.io.File;

import com.izforge.izpack.api.data.Panel;
import com.izforge.izpack.core.resource.ResourceManager;
import com.izforge.izpack.gui.LabelFactory;
import com.izforge.izpack.installer.data.GUIInstallData;
import com.izforge.izpack.installer.gui.InstallerFrame;
import com.izforge.izpack.installer.gui.IzPanel;

abstract class FileExistsPanel extends IzPanel
{

    abstract String getFileName();

    abstract String getReason();

    abstract String getHint();

    public FileExistsPanel(Panel panel, InstallerFrame parent, GUIInstallData idata, ResourceManager rman)
    {
        super(panel, parent, idata, rman);
        //Debug.trace("FileExistsPanel: constructor ...");
    }

    @SuppressWarnings("deprecation")
    public void panelActivate()
    {
        //Debug.trace("FileExistsPanel: PanelActivate ...");

        if(existsFile())
        {
            //Debug.trace("File exists: " + makeFullPath());
            parent.skipPanel();
        }

        parent.lockNextButton();
        parent.lockPrevButton();
        parent.setQuitButtonText(parent.getLangpack().getString("FinishPanel.done"));

        Insets inset = new Insets(10, 20, 2, 2);

        GridBagConstraints constraints1 = new GridBagConstraints(
                0,
                0,
                1,
                1,
                0,
                0,
                GridBagConstraints.LINE_START,
                GridBagConstraints.CENTER,
                inset,
                0,
                0);
        add(LabelFactory.create(parent.getLangpack().getString("FinishPanel.fail"), null, LEADING), constraints1);

        GridBagConstraints constraints2 = new GridBagConstraints(
                0,
                2,
                1,
                1,
                0,
                0,
                GridBagConstraints.LINE_START,
                GridBagConstraints.CENTER,
                inset,
                0,
                0);
        add(LabelFactory.create("Reason: " + getReason(), null, LEADING), constraints2);

        GridBagConstraints constraints3 = new GridBagConstraints(
                0,
                3,
                1,
                1,
                0,
                0,
                GridBagConstraints.LINE_START,
                GridBagConstraints.CENTER,
                inset,
                0,
                0);
        add(LabelFactory.create("Hint: " + getHint(), null, LEADING), constraints3);

        this.installData.setVariable("JBossInstalled", String.valueOf(existsFile()));

        // Debug.trace("FileExistsPanel: PanelActivate done");
    }

    private boolean existsFile()
    {
        File file = new File(makeFullPath());
        return file.exists();
    }

    private String makeFullPath()
    {
        return this.installData.getVariable("INSTALL_PATH") + File.separator + getFileName();
    }

    private static final long serialVersionUID = -8502547286776980735L;
}
