/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.prt.installer.panels;

import java.io.PrintWriter;
import java.util.Properties;

import com.izforge.izpack.api.data.InstallData;
import com.izforge.izpack.installer.console.ConsolePanel;
import com.izforge.izpack.util.Console;

// Diese Klasse ist momentan leer und wird bereitgestellt, damit der Installer
// im Konsolenmodus laeuft. Erst wenn die DBConnect ConsolePanelHelper Versionen
// ausgebaut werden, stehen diese Funktionen im Konsolenmodus zur Verfuegung.

public class ReportingEngineDBConnectPanelConsoleHelper implements ConsolePanel
{

    /**
     * Generates a properties file for each input field or variable.
     * <p/>
     * This implementation is a no-op.
     * 
     * @param installData
     *            the installation data
     * @param printWriter
     *            the properties file to write to
     * @return {@code true}
     */
    @Override
    public boolean generateProperties(InstallData installData, PrintWriter printWriter)
    {
        return true;
    }

    @Override
    public boolean run(InstallData installData, Properties properties)
    {
        return true;
    }

    @Override
    public boolean run(InstallData installData, Console console)
    {
        return true;
    }
}
