/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.prt.installer.panels;

import com.izforge.izpack.api.data.Panel;
import com.izforge.izpack.core.resource.ResourceManager;
import com.izforge.izpack.installer.data.GUIInstallData;
import com.izforge.izpack.installer.gui.InstallerFrame;
import com.izforge.izpack.installer.gui.IzPanel;

public class VariablesPanel extends IzPanel
{
    private ResourceManager resources;

    public VariablesPanel(Panel panel, InstallerFrame iframe, GUIInstallData idata, ResourceManager rman)
    {
        super(panel, iframe, idata, rman);
        this.resources = rman;
    }

    @Override
    public void panelActivate()
    {
        VariablesMethodHelper variablesPanelMethods = new VariablesMethodHelper(resources);
        variablesPanelMethods.setVariableDefaults(installData);
        parent.skipPanel();
    }

    private static final long serialVersionUID = -6269946338761410567L;
}
