/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.prt.installer.validators;

import com.izforge.izpack.panels.userinput.processorclient.ProcessingClient;
import com.izforge.izpack.panels.userinput.validator.Validator;

public class PortValidator implements Validator
{

    @Override
    public boolean validate(ProcessingClient client)
    {
        try
        {
            final String content = client.getText();
            if(null == content)
            {
                return false;
            }
            final Integer value = Integer.valueOf(content);
            return value > 0 && value < 65536;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }
}
