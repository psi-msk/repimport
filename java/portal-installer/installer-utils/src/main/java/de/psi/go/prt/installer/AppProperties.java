/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.prt.installer;

// import java.io.FileInputStream;
// import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AppProperties
{
    private static final String propFileName = "de/psi/go/prt/installer/git.properties";
    private static final String gitCommitID = "git.commit.id";
    private static final String gitCommitDescribe = "git.commit.id.describe";

    public static void main(String[] args)
    {
        instance();
    }

    // //////////////////////////////////////////////////////////////////////
    public static AppProperties instance()
    {
        return instance;
    }

    // //////////////////////////////////////////////////////////////////////
    public String getCommitID()
    {
        return commitID;
    }

    // //////////////////////////////////////////////////////////////////////
    public String getAppVersion()
    {
        return appVersion;
    }

    // //////////////////////////////////////////////////////////////////////
    private AppProperties() throws PropertiesException
    {
        Properties props = new Properties();
        InputStream stream = null;
        try
        {
            // BufferedInputStream stream = new BufferedInputStream(new FileInputStream(propFileName));
            // props.load(stream);
            // stream.close();
            stream = AppProperties.class.getClassLoader().getResourceAsStream(propFileName);
            props.load(stream);
        }
        catch(FileNotFoundException e)
        {
            throw new PropertiesException("Could not find the file " + propFileName);
        }
        catch(IOException e)
        {
            throw new PropertiesException("Could not open the file " + propFileName);
        }
        finally
        {
            try
            {
                if(stream != null)
                {
                    stream.close();
                }
            }
            catch(IOException e)
            {
                // Auto-generated catch block
                throw new PropertiesException("Could not close the stream");
            }
        }

        commitID = props.getProperty(gitCommitID);
        if(commitID == null)
        {
            throw new PropertiesException("No property " + gitCommitID + " defined");
        }

        appVersion = props.getProperty(gitCommitDescribe);
        if(appVersion == null)
        {
            throw new PropertiesException("No property " + gitCommitDescribe + " defined");
        }
    }

    private final String commitID;
    private final String appVersion;
    private static final AppProperties instance;

    static
    {
        try
        {
            instance = new AppProperties();
        }
        catch(PropertiesException e)
        {
            throw new RuntimeException(e.getMessage());
        }
    }
}
