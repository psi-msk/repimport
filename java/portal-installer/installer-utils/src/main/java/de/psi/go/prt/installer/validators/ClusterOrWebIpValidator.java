/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.prt.installer.validators;

import com.izforge.izpack.panels.userinput.processorclient.ProcessingClient;
import com.izforge.izpack.panels.userinput.validator.Validator;

public class ClusterOrWebIpValidator implements Validator
{

    @Override
    public boolean validate(ProcessingClient client)
    {
        // ----------------------------------------------------
        // Depending on the izPack version, the client might
        // contain a single field or four fields. Both variants
        // will be handled by this validator.
        // ----------------------------------------------------
        boolean fourFields;
        if(client.getNumFields() == 4)
        {
            fourFields = true;
        }
        else
        {
            fourFields = false;
        }

        // ----------------------------------------------------
        // client contains a single field
        // ----------------------------------------------------
        if(!fourFields)
        {
            String ip = client.getFieldContents(0);
            try
            {
                if(ip == null || ip.isEmpty() || ip.equals("0.0.0.0"))
                { // NOPMD
                    return false;
                }

                String[] parts = ip.split("\\.");
                if(parts.length != 4)
                {
                    return false;
                }

                for(String s : parts)
                {
                    int i = Integer.parseInt(s);
                    if(i < 0 || i > 255)
                    {
                        return false;
                    }
                }
                return true;
            }
            catch(NumberFormatException nfe)
            {
                return false;
            }
        }

        // ----------------------------------------------------
        // client contains four fields
        // ----------------------------------------------------
        else
        {
            // A size that's not 1 or 4 is not expected, therefore false is returned.
            if(client.getNumFields() != 4)
            {
                return false;
            }
            boolean notZero = false;
            for(int i = 0; i < 4; i++)
            {
                int value;
                try
                {
                    value = Integer.parseInt(client.getFieldContents(i));
                    if(value < 0 || value > 255)
                    {
                        return false;
                    }
                    if(value != 0)
                    {
                        notZero = true;
                    }
                }
                catch(Throwable exception)
                {
                    return false;
                }
            }
            if(notZero)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
