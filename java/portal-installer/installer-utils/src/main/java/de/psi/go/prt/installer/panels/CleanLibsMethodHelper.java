/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.prt.installer.panels;

import java.io.File;

import com.izforge.izpack.api.data.InstallData;

public class CleanLibsMethodHelper
{

    public CleanLibsMethodHelper()
    {
    }

    public void deleteLibs(InstallData installData)
    {
        File libDir = new File(installData.getVariable("SERVERDIR") + "/lib");
        deleteFiles(libDir);
    }

    private void deleteFiles(File directory)
    {
        String files[] = directory.list();
        boolean error = false;
        for(String temp : files)
        {
            File tempFile = new File(directory, temp);
            if(tempFile.delete() == false)
            {
                error = true;
            }
        }
        if(error)
        {
            System.out.println("[ Could not delete all 3rd party libs. ]");
        }
    }
}
