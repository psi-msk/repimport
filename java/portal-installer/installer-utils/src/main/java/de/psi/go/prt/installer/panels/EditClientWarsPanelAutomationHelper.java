/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.prt.installer.panels;

import com.izforge.izpack.api.data.InstallData;
import com.izforge.izpack.api.resource.Resources;
import com.izforge.izpack.installer.automation.PanelAutomation;
import com.izforge.izpack.api.adaptator.IXMLElement;

public class EditClientWarsPanelAutomationHelper implements PanelAutomation
{
    private final Resources resources;

    public EditClientWarsPanelAutomationHelper(Resources resources)
    {
        this.resources = resources;
    }

    @Override
    public void makeXMLData(InstallData installData, IXMLElement panelRoot)
    {
        // no traversion over the XML data needed, all information is in the installData
    }

    @Override
    public void runAutomated(InstallData installData, IXMLElement panelRoot)
    {
        System.out.println("[ Editing PSIportal client files ... ]");
        EditClientWarsMethodHelper clientWarsMethods = new EditClientWarsMethodHelper(resources);
        clientWarsMethods.editClientWars(installData);
        clientWarsMethods.cleanupClientWars(installData);
        System.out.println("[ Editing PSIportal client files finished ]");
    }
}
