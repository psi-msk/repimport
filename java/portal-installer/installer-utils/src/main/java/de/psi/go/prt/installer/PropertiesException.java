/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.prt.installer;

public class PropertiesException extends Exception
{

    public PropertiesException(String reason)
    {
        super(reason);
    }

    private static final long serialVersionUID = 3394779306882735506L;

}
