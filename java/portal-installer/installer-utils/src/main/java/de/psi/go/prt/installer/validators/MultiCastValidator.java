/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.prt.installer.validators;

import java.net.InetAddress;
import java.net.UnknownHostException;

import com.izforge.izpack.panels.userinput.processorclient.ProcessingClient;
import com.izforge.izpack.panels.userinput.validator.Validator;

public class MultiCastValidator implements Validator
{

    @Override
    public boolean validate(ProcessingClient client)
    {
        // ----------------------------------------------------
        // Depending on the izPack version, the client might
        // contain a single field or four fields. Both variants
        // will be handled by this validator.
        // ----------------------------------------------------
        boolean fourFields;
        if(client.getNumFields() == 4)
        {
            fourFields = true;
        }
        else
        {
            fourFields = false;
        }

        InetAddress ip = null;
        // ----------------------------------------------------
        // client contains a single field
        // ----------------------------------------------------
        if(!fourFields)
        {
            try
            {
                ip = InetAddress.getByName(client.getFieldContents(0));
            }
            catch(UnknownHostException e)
            {
                e.printStackTrace();
                return false;
            }
        }
        // ----------------------------------------------------
        // client contains four fields
        // ----------------------------------------------------
        else
        {
            // A size that's not 1 or 4 is not expected, therefore false is returned.
            if(client.getNumFields() != 4)
            {
                return false;
            }
            String completeAddress = String.valueOf(client.getFieldContents(0))
                    + "."
                    + String.valueOf(client.getFieldContents(1))
                    + "."
                    + String.valueOf(client.getFieldContents(2))
                    + "."
                    + String.valueOf(client.getFieldContents(3));
            try
            {
                ip = InetAddress.getByName(completeAddress);
            }
            catch(UnknownHostException e)
            {
                e.printStackTrace();
                return false;
            }
        }

        return ip.isMulticastAddress();
    }
}
