/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.prt.installer.panels;

import com.izforge.izpack.api.data.Panel;
import com.izforge.izpack.core.resource.ResourceManager;
import com.izforge.izpack.installer.data.GUIInstallData;
import com.izforge.izpack.installer.gui.InstallerFrame;

public class ReportingEngineDBConnectPanel extends DBConnectPanel
{
    // //////////////////////////////////////////////////////////////////////
    public ReportingEngineDBConnectPanel(Panel panel, InstallerFrame iframe, GUIInstallData idata, ResourceManager rman)
    {
        super(
                "repDsUser",
                "repDsPassword",
                "repDsHostName",
                "repDsPort",
                "repDsSid",
                "repDsConnString",
                "repDsCheckConnection",
                "repDsServiceType",
                panel,
                iframe,
                idata,
                rman);
    }

    private static final long serialVersionUID = -2037136820049705948L;
}
