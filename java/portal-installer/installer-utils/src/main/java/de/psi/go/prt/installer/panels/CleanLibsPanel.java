/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.prt.installer.panels;

import com.izforge.izpack.api.data.Panel;
import com.izforge.izpack.core.resource.ResourceManager;
import com.izforge.izpack.installer.data.GUIInstallData;
import com.izforge.izpack.installer.gui.InstallerFrame;
import com.izforge.izpack.installer.gui.IzPanel;

public class CleanLibsPanel extends IzPanel
{
    private static final long serialVersionUID = 5028638385090532993L;

    public CleanLibsPanel(Panel panel, InstallerFrame iframe, GUIInstallData idata, ResourceManager rman)
    {
        super(panel, iframe, idata, rman);
    }

    @Override
    public void panelActivate()
    {
        CleanLibsMethodHelper cleanLibsPanelMethods = new CleanLibsMethodHelper();
        cleanLibsPanelMethods.deleteLibs(installData);
        parent.skipPanel();
    }

}
