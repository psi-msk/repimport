/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.prt.installer.panels;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.hyperic.sigar.Mem;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;

import com.izforge.izpack.api.data.InstallData;
import com.izforge.izpack.api.resource.Resources;

public class VariablesMethodHelper
{

    private final Resources resources;

    public VariablesMethodHelper(Resources resources)
    {
        this.resources = resources;
    }

    public void setVariableDefaults(InstallData installData)
    {
        setIP(installData);
        setMemory(installData);
    }

    private void setIP(InstallData installData)
    {
        InetAddress ip = null;
        try
        {
            ip = InetAddress.getLocalHost();
        }
        catch(UnknownHostException e)
        {
            e.printStackTrace();
        }
        if(ip != null)
        {
            String[] parts = ip.getHostAddress().split("\\.");
            if(parts.length != 4)
            {
                return;
            }
            for(int i = 0; i < 4; i++)
            {
                installData.setVariable("bindAddressDefault" + i, parts[i]);
            }
        }
    }

    private void setMemory(InstallData installData)
    {

        // this block extracts all libraries that are needed by SIGAR to a temporary
        // directory
        String tmpPath = installData.getVariable("INSTALL_PATH") + "/tmp";
        File tmpDir = new File(tmpPath);
        if(!tmpDir.exists() && tmpDir.mkdir() == false)
        {
            System.out.println("[ Could not create the tmp directory. ]");
        }

        List<String> libNames = new ArrayList<String>();
        // libNames.add(".sigar_shellrc");
        // libNames.add("libsigar-amd64-freebsd-6.so");
        // libNames.add("libsigar-amd64-solaris.so");
        // libNames.add("libsigar-ia64-hpux-11.sl");
        // libNames.add("libsigar-ia64-linux.so");
        // libNames.add("libsigar-pa-hpux-11.sl");
        // libNames.add("libsigar-ppc64-aix-5.so");
        // libNames.add("libsigar-ppc64-linux.so");
        // libNames.add("libsigar-ppc-aix-5.so");
        // libNames.add("libsigar-ppc-linux.so");
        // libNames.add("libsigar-s390x-linux.so");
        // libNames.add("libsigar-sparc64-solaris.so");
        // libNames.add("libsigar-sparc-solaris.so");
        // libNames.add("libsigar-universal64-macosx.dylib");
        // libNames.add("libsigar-universal-macosx.dylib");
        // libNames.add("libsigar-x86-freebsd-5.so");
        // libNames.add("libsigar-x86-freebsd-6.so");
        // libNames.add("libsigar-x86-solaris.so");
        if(System.getProperty("os.name").toLowerCase().contains("linux"))
        {
            if(System.getProperty("os.arch").toLowerCase().contains("x86"))
            {
                libNames.add("libsigar-x86-linux.so");
            }
            else if(System.getProperty("os.arch").toLowerCase().contains("amd64"))
            {
                libNames.add("libsigar-amd64-linux.so");
            }
        }
        else if(System.getProperty("os.name").toLowerCase().contains("win"))
        {
            if(System.getProperty("os.arch").toLowerCase().contains("x86"))
            {
                libNames.add("sigar-x86-winnt.dll");
                libNames.add("sigar-x86-winnt.lib");
            }
            else if(System.getProperty("os.arch").toLowerCase().contains("amd64"))
            {
                libNames.add("sigar-amd64-winnt.dll");
            }
        }

        for(String libName : libNames)
        {
            InputStream readLib = resources.getInputStream(libName);
            File outputFile = new File(tmpPath, libName);
            if(!outputFile.exists())
            {
                OutputStream writeLib = null;
                try
                {
                    writeLib = new FileOutputStream(outputFile);
                }
                catch(FileNotFoundException e)
                {
                    e.printStackTrace();
                }
                byte[] buffer = new byte[1024];
                int numchars;
                try
                {
                    while((numchars = readLib.read(buffer)) > 0)
                    {
                        writeLib.write(buffer, 0, numchars);
                    }
                }
                catch(IOException e1)
                {
                    e1.printStackTrace();
                }
                finally
                {
                    if(readLib != null)
                    {
                        try
                        {
                            readLib.close();
                        }
                        catch(IOException e2)
                        {
                            e2.printStackTrace();
                        }
                    }
                    if(writeLib != null)
                    {
                        try
                        {
                            writeLib.close();
                        }
                        catch(IOException e2)
                        {
                            e2.printStackTrace();
                        }
                    }

                }
            }
        }

        // add the temporary directory to the java.library.path, so SIGAR can find them
        String libPath = System.getProperty("java.library.path") + System.getProperty("path.separator") + tmpPath;
        System.setProperty("java.library.path", libPath);

        // determine the RAM
        Sigar sigar = new Sigar();
        Mem mem = null;
        try
        {
            mem = sigar.getMem();
        }
        catch(SigarException e1)
        {
            e1.printStackTrace();
        }

        long totalRAM = 0;
        if(mem != null)
        {
            totalRAM = mem.getRam();
        }

        // set the installer variables
        double heapMemoryDefault = 0.7 * totalRAM;
        String heapMemoryDefaultString = String.valueOf(heapMemoryDefault).replaceAll("\\.(.*)", "");
        installData.setVariable("heapMemoryDefault", heapMemoryDefaultString);
        // double permMemoryDefault = 0.06 * totalRAM;
        // String permMemoryDefaultString = String.valueOf(permMemoryDefault)
        // .replaceAll("\\.(.*)", "");
        // installData.setVariable("permMemoryDefault", permMemoryDefaultString);

        sigar.close();

        // Try to delete the temporary directory, which currently does neither with delete() nor with deleteOnExit()
        // work because seemingly Sigar does not free the file handle even when it gets closed.
        cleanupTmpDir(tmpPath);
    }

    private void cleanupTmpDir(String tmpPath)
    {
        File directory = new File(tmpPath);

        File[] toBeDeleted = directory.listFiles();
        for(File deletableFile : toBeDeleted)
        {
            deletableFile.deleteOnExit();
        }
        directory.deleteOnExit();
    }
}
