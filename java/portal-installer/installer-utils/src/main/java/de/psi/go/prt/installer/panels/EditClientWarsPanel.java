/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.prt.installer.panels;

import com.izforge.izpack.api.data.Panel;
import com.izforge.izpack.core.resource.ResourceManager;
import com.izforge.izpack.installer.data.GUIInstallData;
import com.izforge.izpack.installer.gui.InstallerFrame;
import com.izforge.izpack.installer.gui.IzPanel;

public class EditClientWarsPanel extends IzPanel
{
    private ResourceManager resources;

    public EditClientWarsPanel(Panel panel, InstallerFrame iframe, GUIInstallData idata, ResourceManager rman)
    {
        super(panel, iframe, idata, rman);
        this.resources = rman;
    }

    public void panelActivate()
    {
        System.out.println("[ Editing PSIportal client files ... ]");
        EditClientWarsMethodHelper clientWarsMethods = new EditClientWarsMethodHelper(resources);
        clientWarsMethods.editClientWars(installData);
        clientWarsMethods.cleanupClientWars(installData);
        System.out.println("[ Editing PSiportal client files finished ]");
        parent.skipPanel();
    }

    private static final long serialVersionUID = 2271908561868426120L;
}
