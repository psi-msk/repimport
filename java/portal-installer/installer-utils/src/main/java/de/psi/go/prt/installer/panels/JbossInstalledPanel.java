/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.prt.installer.panels;

import com.izforge.izpack.api.data.Panel;
import com.izforge.izpack.core.resource.ResourceManager;
import com.izforge.izpack.installer.data.GUIInstallData;
import com.izforge.izpack.installer.gui.InstallerFrame;

public class JbossInstalledPanel extends FileExistsPanel
{
    public JbossInstalledPanel(Panel panel, InstallerFrame parent, GUIInstallData idata, ResourceManager rman)
    {
        super(panel, parent, idata, rman);
    }

    @Override
    String getFileName()
    {
        return "jboss-6.1.0.Final";
    }

    @Override
    String getReason()
    {
        return "JBoss6.1.Final is not installed.";
    }

    private static final long serialVersionUID = 3757412022281068433L;

    @Override
    String getHint()
    {
        return "Install JBoss6.1.Final to "
                + this.installData.getVariable("INSTALL_PATH")
                + " in subdirectory "
                + getFileName();
    }
}
