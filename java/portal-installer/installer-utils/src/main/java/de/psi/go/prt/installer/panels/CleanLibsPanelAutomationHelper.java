/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.prt.installer.panels;

import com.izforge.izpack.api.adaptator.IXMLElement;
import com.izforge.izpack.api.data.InstallData;
import com.izforge.izpack.installer.automation.PanelAutomation;

public class CleanLibsPanelAutomationHelper implements PanelAutomation
{
    public CleanLibsPanelAutomationHelper()
    {
    }

    @Override
    public void makeXMLData(InstallData installData, IXMLElement panelRoot)
    {
        // no traversion over the XML data needed, all information is in the installData
    }

    @Override
    public void runAutomated(InstallData installData, IXMLElement panelRoot)
    {
        CleanLibsMethodHelper cleanLibsMethods = new CleanLibsMethodHelper();
        cleanLibsMethods.deleteLibs(installData);
    }
}
