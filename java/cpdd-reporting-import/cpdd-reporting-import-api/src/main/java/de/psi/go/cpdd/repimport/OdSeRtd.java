package de.psi.go.cpdd.repimport;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Data container for incoming archive data. To be processed by a JDBC persistence module.
 * 
 * @author kulbrich
 * 
 */

public class OdSeRtd
{
    public OdSeRtd(
            RepImportTimeLevel seanceType,
            Timestamp rptDate,
            String guid,
            Timestamp changeDate,
            Integer status,
            BigDecimal rptValue)
    {
        this.seanceType = seanceType;
        this.rptDate = rptDate;
        this.parameterId = guid;
        this.changeDate = changeDate;
        this.flag = status;
        this.rptValue = rptValue;
    }

    public String getParameterId()
    {
        return parameterId;
    }

    public Timestamp getChangeDate()
    {
        return changeDate;
    }

    public Integer getFlag()
    {
        return flag;
    }

    public BigDecimal getRptValue()
    {
        return rptValue;
    }

    public RepImportTimeLevel getSeanceType()
    {
        return seanceType;
    }

    public Timestamp getRptDate()
    {
        return rptDate;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder();

        sb.append("parameterId=\"").append(parameterId).append("\" (length=").append(parameterId.length()).append(")");
        sb.append(" seanceType=").append(seanceType);
        sb.append(" rptDate=").append(rptDate);
        sb.append(" changeDate=").append(changeDate);
        sb.append(" flag=").append(flag);
        sb.append(" rptValue=").append(rptValue);

        return sb.toString();
    }

    /**
     * Throws an {@link IllegalArgumentException}, if not all is well.
     */
    void check()
    {
        StringBuilder errBuffer = null;

        if(null == seanceType)
        {
            errBuffer = StringBuilderSafeAppender.ensureStringBuilder(errBuffer, "seanceType is null, but must be set");
        }

        if(null == rptDate)
        {
            errBuffer = StringBuilderSafeAppender.ensureStringBuilder(errBuffer, "rptDate is null, but must be set");
        }

        if(null != errBuffer)
        {
            throw new IllegalArgumentException(
                    "Record [ " + this.toString() + " ] has errors: " + errBuffer.toString());
        }
    }

    private final RepImportTimeLevel seanceType;
    private final Timestamp rptDate;
    private final String parameterId;
    private final Timestamp changeDate;
    private final Integer flag;
    private final BigDecimal rptValue;
}
