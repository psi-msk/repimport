package de.psi.go.cpdd.repimport.config;

public enum PersistenceStrategy
{
    INSERT,
    MERGE;
}
