package de.psi.go.cpdd.repimport;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public class ISO8601Converter
{
    public static Timestamp toTimestamp(String iso8601String)
    {
        return new Timestamp(isoTimeFormatter.parseDateTime(iso8601String).getMillis());
    }

    public static Calendar toCalendar(String iso8601String)
    {
        Calendar cal = new GregorianCalendar();

        cal.setTimeInMillis(isoTimeFormatter.parseDateTime(iso8601String).getMillis());

        return cal;
    }

    public static GregorianCalendar toGregorianCalendar(String iso8601String)
    {
        return (GregorianCalendar)toCalendar(iso8601String);
    }

    private static final DateTimeFormatter isoTimeFormatter = ISODateTimeFormat.dateTimeNoMillis();
}
