package de.psi.go.cpdd.repimport.listener;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import de.psi.go.cpdd.repimport.AsduAdmLogger;

public abstract class AFileListener implements FilenameFilter
{
    AFileListener(final String path, final IFileProcessor fileProcessor) throws FileListenerException
    {
        this.watchedDir = new File(path);

        ensureExistingDir(watchedDir, false);

        this.fileProcessor = fileProcessor;
    }

    // //////////////////////////////////////////////////////////////////////
    protected abstract File markProcessing(File file) throws FileListenerException;

    // //////////////////////////////////////////////////////////////////////
    protected abstract void unmarkProcessing(File file) throws FileListenerException;

    // //////////////////////////////////////////////////////////////////////
    public abstract void handleError(File file) throws HandlingException;

    // //////////////////////////////////////////////////////////////////////
    public abstract void handleOk(File file) throws HandlingException;

    // //////////////////////////////////////////////////////////////////////
    public abstract AFileListener incrementNofScans();

    // //////////////////////////////////////////////////////////////////////
    public abstract long getNofScans();

    // //////////////////////////////////////////////////////////////////////
    public void scan() throws HandlingException
    {
        if(SCAN_IN_PROGRESS)
        {
            LOG.debug("Processing of directory in progress, skipping.");
            return;
        }

        try
        {

            SCAN_IN_PROGRESS = true;

            LOG.debug(
                    new StringBuffer().append("Implementation ").append(getClass().getName()).append(
                            ", scan No.").append(incrementNofScans().getNofScans()));

            // List the files in my directory applying me as filename filter.

            final File[] fileArray = watchedDir.listFiles(this);
            // This test can be done only on the array.
            if(null == fileArray)
            {
                // I/O error or watchedDir is no directory.
                final StringBuffer err = new StringBuffer();
                err.append("AFileListener.scan(): file listing returned null on path [");
                err.append(watchedDir.getAbsolutePath());
                err.append("]. I/O error or no directory. Check path.");
                LOG.warn(err);
                throw new HandlingException(err.toString());
            }
            if(0 == fileArray.length)
            {
                LOG.debug(new StringBuffer("No data in directory ").append(watchedDir.getAbsolutePath()));
                return;
            }
            final List<File> files = Arrays.asList(fileArray);
            // Sort the files alphanumerically with respect to the filename
            Collections.sort(files);
            final int nofFiles = files.size();
            // Process only one file. Makes it easier in a split brain situation
            // of a clustered system.
            final File fileToBeProcessed = files.get(0);

            final StringBuffer logString = new StringBuffer().append("Found ").append(nofFiles).append(
                    " files/subdirs in ").append(watchedDir.getAbsolutePath()).append(". Will process file [").append(
                            fileToBeProcessed.getAbsolutePath()).append("]");

            AsduAdmLogger.INFO(logString);

            File markedFile = null;
            try
            {
                markedFile = this.markProcessingIntern(fileToBeProcessed);
                fileProcessor.process(markedFile);
                handleOk(markedFile);
            }
            catch(Exception e)
            {
                LOG.warn(
                        new StringBuffer().append("Caught ").append(e.getClass().getName()).append(": ").append(
                                e.getMessage()));
                this.handleError(markedFile);
            }

            LOG.debug("scan done");

        }
        finally
        {
            SCAN_IN_PROGRESS = false;
        }
    }

    private File markProcessingIntern(File file) throws FileListenerException
    {
        final File markedFile = markProcessing(file);

        if(!markedFile.exists())
        {
            final String err = String.format(
                    "Tried to rename \"%s\" to \"%s\", but \"%s\" does not exist.",
                    file.getAbsolutePath(),
                    markedFile.getAbsolutePath(),
                    markedFile.getAbsolutePath());
            LOG.warn(err);
            throw new FileListenerException(err);
        }
        if(this.accept(watchedDir, markedFile.getName()))
        {
            throw new FileListenerException("File " + file + " was not processing marked successfully.");
        }

        return markedFile;
    }

    // //////////////////////////////////////////////////////////////////////
    protected void ensureExistingDir(final File dir, boolean doCreate) throws FileListenerException
    {
        if(dir.exists())
        {
            ensureIsDirectory(dir);
            ensureCanReadWrite(dir);
            return;
        }
        else
        {
            if(doCreate)
                ensureCreate(dir);
            else
            {
                final String err = "Directory " + dir.getAbsolutePath() + " does not exist.";
                LOG.warn(err);
                throw new FileListenerException(err);
            }
        }
    }

    // //////////////////////////////////////////////////////////////////////
    protected void ensureIsDirectory(final File dir) throws FileListenerException
    {
        if(!dir.isDirectory())
        {
            throw new FileListenerException("There is a file in " + watchedDir + ", but it is no directory.");
        }
    }

    // //////////////////////////////////////////////////////////////////////
    protected void ensureCanReadWrite(final File dir) throws FileListenerException
    {
        final boolean readable = dir.canRead();
        final boolean writeable = dir.canWrite();

        final boolean success = readable && writeable;

        if(!success)
        {
            String errtext = "Directory " + dir.getAbsolutePath() + ":";
            if(!readable)
                errtext += " not readable";
            if(!writeable)
                errtext += " not writeable";

            throw new FileListenerException(errtext);
        }
    }

    // //////////////////////////////////////////////////////////////////////
    protected void ensureCreate(final File dir) throws FileListenerException
    {
        final boolean couldCreate = dir.mkdir();

        if(!couldCreate)
        {
            throw new FileListenerException(
                    "Needed directory " + dir.getAbsolutePath() + " did not exist and could not be created.");
        }
        ensureCanReadWrite(dir);
    }

    // //////////////////////////////////////////////////////////////////////
    protected File ensureRename(File file, String newName) throws FileListenerException
    {
        final File newFile = new File(newName);

        LOG.debug(String.format("Renaming file \"%s\" to \"%s\"", file.getAbsolutePath(), newName));

        if(!file.renameTo(newFile))
        {
            final String msg = String.format("Could not rename \"%s\" to \"%s\".", file.getAbsolutePath(), newName);
            LOG.warn(msg);
            throw new FileListenerException(msg);
        }

        return newFile;
    }

    protected final File watchedDir;
    private final IFileProcessor fileProcessor;

    private static boolean SCAN_IN_PROGRESS = false;

    private static final Logger LOG = Logger.getLogger(AFileListener.class);
}
