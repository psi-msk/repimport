package de.psi.go.cpdd.repimport;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.EJBTransactionRolledbackException;
import javax.persistence.PersistenceException;

import org.hibernate.HibernateException;
import org.hibernate.JDBCException;

import de.psi.go.cpdd.repimport.config.ImportConfiguration;

public class OdSeRtdException extends Exception
{

    private enum ErrorCategory
    {
        NON_RETRIABLE,
        RETRIABLE;
    }

    public OdSeRtdException(String msg, Throwable e)
    {
        super(msg, e);

        final ErrorCategory myErrorType = (isContinuableException(e)
                ? ErrorCategory.RETRIABLE
                : ErrorCategory.NON_RETRIABLE);

        this.errorType = myErrorType;
    }

    public ErrorCategory getErrorType()
    {
        return errorType;
    }

    public boolean isRetriable()
    {
        return ErrorCategory.RETRIABLE == errorType;
    }

    private static final Set<Class<?>> DB_EXCEPTION_CLASSES = new HashSet<Class<?>>();
    static
    {
        DB_EXCEPTION_CLASSES.add(HibernateException.class);
        DB_EXCEPTION_CLASSES.add(PersistenceException.class);
        DB_EXCEPTION_CLASSES.add(EJBTransactionRolledbackException.class);
    }

    private static final Set<Integer> NON_RETRIABLE_ORACLE_CODES = ImportConfiguration.getInstance().getNonRetriableOracleErrorCodes();

    private static boolean isContinuableException(final Throwable e)
    {
        // Treat SQLExceptions
        if((e instanceof SQLException))
        {
            return isRetriableSqlException((SQLException)e);
        }
        else if(e instanceof JDBCException)
        {
            final JDBCException jdbcex = (JDBCException)e;
            return isContinuableException(jdbcex.getSQLException());
        }

        // One of the explicitely named classes?
        for(Class<?> cls : DB_EXCEPTION_CLASSES)
        {
            if(cls.isInstance(e))
            {
                return true;
            }
        }

        return e.getClass().getName().startsWith("java.sql.");
    }

    private static boolean isRetriableSqlException(final SQLException sqlex)
    {
        return !NON_RETRIABLE_ORACLE_CODES.contains(sqlex.getErrorCode());
    }

    private final ErrorCategory errorType;

    private static final long serialVersionUID = 1L;
}
