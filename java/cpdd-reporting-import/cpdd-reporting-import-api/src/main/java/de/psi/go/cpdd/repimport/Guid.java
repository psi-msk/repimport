package de.psi.go.cpdd.repimport;

import java.math.BigInteger;

/**
 * Encapsulates the RAW(16) GUID of an object in the NIS data. The GUID is also called PARAMETER_ID in some contexts.
 * 
 * @author emsku
 * 
 */
public class Guid
{
    private static final int MAX_BYTES = 16;
    private static final BigInteger MIN_PARAMETER_ID = BigInteger.ZERO;
    private static final BigInteger MAX_PARAMETER_ID = BigInteger.valueOf(2).pow(MAX_BYTES * 8).subtract(
            BigInteger.ONE);

    /**
     * Constructs a Guid from a passed string. If the hexadecimal number has an uneven number of digits, 0 is prepended,
     * because thus it will be written into the data base.
     * 
     * @param rawValue
     * @throws GuidException
     *             if the passed string cannot be interpreted as a hexadecimal integer or lies outside the allowed
     *             interval.
     */
    public Guid(String guidString) throws GuidException
    {

        this.value = parseString(guidString);
        this.dbRepresentation = makeDbRepresentation(this.value);
    }

    public String getDbRepresentation()
    {
        return dbRepresentation;
    }

    public BigInteger getValue()
    {
        return value;
    }

    private BigInteger parseString(final String guidString) throws GuidException
    {
        try
        {
            final BigInteger result = new BigInteger(guidString, 16);

            if(-1 == result.compareTo(MIN_PARAMETER_ID) || 1 == result.compareTo(MAX_PARAMETER_ID))
            {
                throw new GuidException(
                        "parameterId has invalid value "
                                + result.toString(16)
                                + ", but must satisfy "
                                + MIN_PARAMETER_ID.toString(16)
                                + " <= parameterId <= "
                                + MAX_PARAMETER_ID.toString(16));
            }

            return result;

        }
        catch(final NumberFormatException nofex)
        {
            final GuidException guidex = new GuidException(
                    "error converting parameterId to RAW: \""
                            + guidString
                            + "\" cannot be interpreted as hexadecimal integer.");
            guidex.initCause(nofex);
            throw guidex;
        }
    }

    private String makeDbRepresentation(final BigInteger guid)
    {
        final String rawValString = guid.toString(16).toUpperCase();
        return isEven(rawValString.length()) ? rawValString : new StringBuilder("0").append(rawValString).toString();
    }

    private static boolean isEven(int val)
    {
        return(0 == (val % 2));
    }

    private final BigInteger value;
    private final String dbRepresentation;
}
