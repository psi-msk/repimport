package de.psi.go.cpdd.repimport.config;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.xml.sax.SAXException;

import de.psi.go.mtr.tools.xml.ValidatingParserFactoryBase;
import de.psi.go.mtr.tools.xml.XmlToolsException;

public class ConfigValidatingParserFactory extends ValidatingParserFactoryBase
{
    private static final ConfigValidatingParserFactory INSTANCE = new ConfigValidatingParserFactory();

    private ConfigValidatingParserFactory()
    {
        registerXsdName(ImportConfiguration.class, "ImportConfiguration.xsd");
    }

    public Unmarshaller makeUnmarshaller() throws JAXBException, XmlToolsException, SAXException
    {
        return makeUnmarshaller(ImportConfiguration.class, JAXBContext.newInstance(ImportConfiguration.class));
    }

    public static ConfigValidatingParserFactory getInstance()
    {
        return INSTANCE;
    }
}
