/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.psiportalxml.v2;

import de.psi.go.psiportalxml.v2.dd.PsiportalDdXml;

public class PsiPortalXmlConstants
{
    public static final String MASTER_DATA_SCHEMA_NAME = "PSIportalMasterData_v2.0.0.xsd";

    public static final String DYNAMIC_DATA_SCHEMA_NAME = "PSIportalDynamicData_v2.0.0.xsd";

    public static final String DYNAMIC_DATA_PACKAGE_NAME = PsiportalDdXml.class.getPackage().getName();

    public static final String DYNAMIC_DATA_NAMESPACE = "http://go.psi.de/v2/psiportal-xml/dynamicdata";

    public static final String TIMESERIES_ELEMENT_NAME = "timeseries";

    public static final String VALUE_ELEMENT_NAME = "val";

    public static final String SPONTANEOUS_SERIES_ELEMENT_NAME = "spontaneousSeries";

    public static final String SPVALUE_ELEMENT_NAME = "spval";
}
