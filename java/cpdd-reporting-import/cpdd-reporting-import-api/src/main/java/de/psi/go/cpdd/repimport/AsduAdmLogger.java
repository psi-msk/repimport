package de.psi.go.cpdd.repimport;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * This class is to be configured for DB.ASDU.ADM.LOGGER Log4J appender.
 * Everything logged via this class will then be sent to this appender.
 * 
 * @author emsku
 * 
 */
public class AsduAdmLogger
{
    public static void INFO(Object message)
    {
        WRITE(Level.INFO, message);
    }

    public static void WARN(Object message)
    {
        WRITE(Level.WARN, message);
    }

    public static void ERROR(Object message)
    {
        WRITE(Level.ERROR, message);
    }

    public static void FATAL(Object message)
    {
        WRITE(Level.FATAL, message);
    }

    private static void WRITE(Level prio, Object message)
    {
        // The logging may fail, because the appender writes to a data base. Thus,
        // in case of failure in doing so, write something to the root logger.
        // There is not much more we can do in such a case.
        try
        {
            LOG.log(prio, message);
        }
        catch(Exception e)
        {
            StringBuffer err = new StringBuffer();
            err.append("Error writing message [").append(message).append("] by class ").append(
                    AsduAdmLogger.class.getName());
            err.append(" Reason: ").append(e.getClass().getName()).append(" ").append(e.getMessage());
            Logger.getRootLogger().error(err);
        }
    }

    private static final Logger LOG = Logger.getLogger(AsduAdmLogger.class);
}
