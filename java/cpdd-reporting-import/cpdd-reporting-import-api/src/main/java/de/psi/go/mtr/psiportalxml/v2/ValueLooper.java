/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.psiportalxml.v2;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import de.psi.go.mtr.tools.xml.IXmlElementProcessor;
import de.psi.go.mtr.tools.xml.ParameterizedElementFilter;
import de.psi.go.psiportalxml.v2.dd.PsiportalDdXml;
import de.psi.go.psiportalxml.v2.dd.ValueType;

/*
 * Loops over all <val/> elements in an XML document and applies a processor to each one.
 * Applies validation with the schema used to generate the objects.
 */

public class ValueLooper
{
    public ValueLooper(InputStream inputStream, IXmlElementProcessor<ValueType> xmlElementProcessor)
    {
        this.inputStream = inputStream;
        this.xmlElementProcessor = xmlElementProcessor;
    }

    public void process() throws Exception
    {
        JAXBContext context = JAXBContext.newInstance(PsiPortalXmlConstants.DYNAMIC_DATA_PACKAGE_NAME);

        // create a new XML parser
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XMLReader reader = factory.newSAXParser().getXMLReader();

        ParameterizedElementFilter<ValueType> splitter = new ParameterizedElementFilter<ValueType>(
                context,
                PsiPortalXmlValidatingParserFactory.getInstance(),
                PsiportalDdXml.class,
                PsiPortalXmlConstants.DYNAMIC_DATA_NAMESPACE,
                PsiPortalXmlConstants.VALUE_ELEMENT_NAME,
                this.xmlElementProcessor);

        reader.setContentHandler(splitter);

        InputSource inputSource = new InputSource(this.inputStream);
        reader.parse(inputSource);
    }

    private final InputStream inputStream;
    private final IXmlElementProcessor<ValueType> xmlElementProcessor;
}
