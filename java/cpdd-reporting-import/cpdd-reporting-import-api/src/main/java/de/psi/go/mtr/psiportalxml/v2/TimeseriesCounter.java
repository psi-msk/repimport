package de.psi.go.mtr.psiportalxml.v2;

import org.apache.log4j.Logger;

import de.psi.go.mtr.tools.xml.IXmlElementProcessor;
import de.psi.go.psiportalxml.v2.dd.TimeseriesType;

public class TimeseriesCounter implements IXmlElementProcessor<TimeseriesType>
{
    private static final Logger LOG = Logger.getLogger(TimeseriesCounter.class);

    private int counter = 0;

    @Override
    public void process(TimeseriesType xmlelement) throws Exception
    {
        LOG.debug("Timeseries has " + xmlelement.getValues().getVal().size() + " values.");
        ++counter;
    }

    public int getCounter()
    {
        return counter;
    }
}
