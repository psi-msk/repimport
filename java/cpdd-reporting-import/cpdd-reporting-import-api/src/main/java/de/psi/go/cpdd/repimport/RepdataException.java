package de.psi.go.cpdd.repimport;

public class RepdataException extends Exception
{
    RepdataException(Exception e)
    {
        super(e);
    }

    RepdataException(String msg)
    {
        super(msg);
    }

    /**
    * 
    */
    private static final long serialVersionUID = -8389722661297143182L;

}
