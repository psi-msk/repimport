package de.psi.go.cpdd.repimport.listener;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.jboss.util.StopWatch;

import de.psi.go.cpdd.repimport.XmlValueBatchWriter;
import de.psi.go.cpdd.repimport.config.ImportConfiguration;
import de.psi.go.cpdd.repimport.config.PersistenceStrategy;
import de.psi.go.mtr.psiportalxml.v2.TimeseriesLooper;

/*
 * Processes the dynamic data files in an export directory.
 */
public class ExportDirectoryProcessor implements IFileProcessor
{
    public ExportDirectoryProcessor(int maxBatchSize)
    {
        this.maxBatchSize = maxBatchSize;
    }

    @Override
    public void process(File dir) throws FileProcessingException
    {
        if(!dir.isDirectory())
        {
            final String msg = "File " + dir.getAbsolutePath() + " is no directory.";
            LOG.warn(msg);
            throw new FileProcessingException(msg);
        }

        LOG.info("process: directory " + dir.getAbsolutePath());

        final StopWatch sw = new StopWatch();
        sw.start();

        final List<File> fileList = Arrays.asList(dir.listFiles(new DynamicDataFileFilter()));
        Collections.sort(fileList);

        LOG.info(String.format("process: found %d dynamic data files ", fileList.size()));

        final PersistenceStrategy pStrat = ImportConfiguration.getInstance().getPersistenceStragegy();

        XmlValueBatchWriter timeseriesProcessor;
        try
        {
            timeseriesProcessor = new XmlValueBatchWriter(maxBatchSize, pStrat);
        }
        catch(Exception e)
        {
            throw new FileProcessingException(e.getMessage(), e);
        }

        for(File file : fileList)
        {
            InputStream inputStream = null;
            try
            {
                LOG.info("Processing file " + file.getAbsolutePath());
                inputStream = new FileInputStream(file);
                timeseriesProcessor.setActualFile(file);
                TimeseriesLooper tslooper = new TimeseriesLooper(inputStream, timeseriesProcessor);
                tslooper.process();
            }
            catch(Exception e)
            {
                processException(file, e);
            }
            finally
            {
                if(inputStream != null)
                {
                    try
                    {
                        inputStream.close();
                    }
                    catch(IOException e)
                    {
                        LOG.warn("Error on closing input stream for file " + file, e);
                    }
                }
            }

        }

        try
        {
            timeseriesProcessor.finish();
        }
        catch(Exception e)
        {
            processException(dir, e);
        }

        LOG.info(String.format("Wrote %d values in %d ms.", timeseriesProcessor.getNofWritten(), sw.getTime()));
    }

    private void processException(final File file, final Exception e) throws FileProcessingException
    {
        final String fileType = file.isDirectory() ? "directory" : "file";
        final StringBuilder err = new StringBuilder("Caught Exception ").append(e.getClass().getName()).append(
                " while processing ").append(fileType).append(" ").append(file.getName()).append(": ").append(
                        e.getMessage());
        LOG.warn(err);
        if(LOG.isDebugEnabled())
        {
            LOG.debug(e);
        }

        throw new FileProcessingException(err.toString(), e);
    }

    private class DynamicDataFileFilter implements FileFilter
    {
        @Override
        public boolean accept(File pathname)
        {
            return pathname.getName().matches("ddata.*\\.xml");
        }
    }

    private final int maxBatchSize;

    private static final Logger LOG = Logger.getLogger(ExportDirectoryProcessor.class);

    // Classes of exceptions, which indicate the need for an import retrial
    // instead of error handling.
    private static final Set<Class<?>> CONTINUABLE_EXCEPTIONS = new HashSet<Class<?>>();
    static
    {
        CONTINUABLE_EXCEPTIONS.add(SQLException.class);
        CONTINUABLE_EXCEPTIONS.add(PersistenceException.class);
    }
}
