package de.psi.go.cpdd.repimport.config;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Configures a number format via 2 integer values: presicion and scale.
 * 
 * @author emsku
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class NumberFormat
{

    public int getPrecision()
    {
        return precision;
    }

    public int getScale()
    {
        return scale;
    }

    @XmlAttribute(name = "precision")
    private int precision;

    @XmlAttribute(name = "scale")
    private int scale;
}
