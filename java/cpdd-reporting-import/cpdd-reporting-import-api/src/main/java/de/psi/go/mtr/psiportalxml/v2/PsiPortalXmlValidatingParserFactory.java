/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.psiportalxml.v2;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;

import org.xml.sax.SAXException;

import de.psi.go.mtr.tools.xml.ValidatingParserFactoryBase;
import de.psi.go.mtr.tools.xml.XmlToolsException;
import de.psi.go.psiportalxml.v2.dd.PsiportalDdXml;
import de.psi.go.psiportalxml.v2.md.PsiportalMdXml;

public class PsiPortalXmlValidatingParserFactory extends ValidatingParserFactoryBase
{
    private static final PsiPortalXmlValidatingParserFactory instance = new PsiPortalXmlValidatingParserFactory();

    private PsiPortalXmlValidatingParserFactory()
    {
        registerXsdName(PsiportalMdXml.class, PsiPortalXmlConstants.MASTER_DATA_SCHEMA_NAME);
        registerXsdName(PsiportalDdXml.class, PsiPortalXmlConstants.DYNAMIC_DATA_SCHEMA_NAME);
    }

    public Unmarshaller makeMdUnmarshaller() throws JAXBException, XmlToolsException, SAXException
    {
        return makeUnmarshaller(PsiportalMdXml.class);
    }

    public Unmarshaller makeDdUnmarshaller() throws JAXBException, XmlToolsException, SAXException
    {
        return makeUnmarshaller(PsiportalDdXml.class);
    }

    public Marshaller makeMdMarshaller() throws JAXBException, XmlToolsException, SAXException
    {
        return makeMarshaller(PsiportalMdXml.class);
    }

    public Marshaller makeDdMarshaller() throws JAXBException, XmlToolsException, SAXException
    {
        return makeMarshaller(PsiportalDdXml.class);
    }

    public SAXParser makeDdSaxParser() throws ParserConfigurationException, SAXException, XmlToolsException
    {
        return makeSAXParser(PsiportalDdXml.class);
    }

    public static PsiPortalXmlValidatingParserFactory getInstance()
    {
        return instance;
    }
}
