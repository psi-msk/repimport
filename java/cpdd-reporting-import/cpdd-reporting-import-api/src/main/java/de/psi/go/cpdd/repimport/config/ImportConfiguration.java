package de.psi.go.cpdd.repimport.config;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.ScheduleExpression;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.log4j.Logger;
import org.joda.time.Period;
import org.xml.sax.SAXException;

import de.psi.go.mtr.tools.xml.XmlToolsException;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = ImportConfiguration.NAMESPACE)
@XmlRootElement(name = "RepImport", namespace = ImportConfiguration.NAMESPACE)
public class ImportConfiguration
{
    public static final String NAMESPACE = "http://psi.go.de/cpdd-reporting-import/config";

    public static final String PROPERTY_CONFIG_DIR = "jboss.server.config.url";
    public static final String CONFIG_FILE_NAME = "CpddReportingImportConfiguration.xml";
    private static final Logger LOG = Logger.getLogger(ImportConfiguration.class);

    @XmlElement(name = "InputDir", namespace = ImportConfiguration.NAMESPACE, required = true, nillable = false)
    private String inputDir;

    @XmlElement(
            name = "ExportDirectoryScanSchedule",
            namespace = ImportConfiguration.NAMESPACE,
            required = true,
            nillable = false)
    private ScheduleConfiguration exportDirectoryScanSchedule;

    @XmlElement(
            name = "PersistenceStrategy",
            namespace = ImportConfiguration.NAMESPACE,
            required = true,
            nillable = false)
    private PersistenceStrategy persistenceStrategy;

    @XmlElement(name = "MaxBatchSize", namespace = ImportConfiguration.NAMESPACE, required = true, nillable = false)
    private int maxBatchSize;

    @XmlElement(name = "MaxSqlBulkSize", namespace = ImportConfiguration.NAMESPACE, required = true, nillable = false)
    private int maxSqlBulkSize;

    @XmlElement(name = "NumberFormat", namespace = ImportConfiguration.NAMESPACE, required = true, nillable = false)
    private NumberFormat numberFormat;

    @XmlElement(
            name = "ExportDirectoryCleanSchedule",
            namespace = ImportConfiguration.NAMESPACE,
            required = true,
            nillable = false)
    private ScheduleConfiguration exportDirectoryCleanSchedule;

    @XmlElement(
            name = "RetentionProcessed",
            namespace = ImportConfiguration.NAMESPACE,
            required = true,
            nillable = false)
    private PeriodConfiguration retentionProcessed;

    @XmlElement(name = "RetentionError", namespace = ImportConfiguration.NAMESPACE, required = true, nillable = false)
    private PeriodConfiguration retentionError;

    @XmlElementWrapper(
            name = "NonRetriableOracleErrorCodes",
            namespace = ImportConfiguration.NAMESPACE,
            required = true,
            nillable = false)
    @XmlElement(name = "Code", namespace = ImportConfiguration.NAMESPACE, nillable = false)
    private Set<Integer> nonRetriableOracleErrorCodes = new HashSet<Integer>();

    public static ImportConfiguration getInstance()
    {
        return instance;
    }

    public String getInputDir()
    {
        return inputDir;
    }

    public ScheduleExpression getExportDirectoryScanSchedule()
    {
        return exportDirectoryScanSchedule.getScheduleExpression();
    }

    public int getMaxBatchSize()
    {
        return maxBatchSize;
    }

    public int getMaxSqlBulkSize()
    {
        return maxSqlBulkSize;
    }

    public NumberFormat getNumberFormat()
    {
        return numberFormat;
    }

    public ScheduleExpression getExportDirectoryCleanSchedule()
    {
        return exportDirectoryCleanSchedule.getScheduleExpression();
    }

    public Period getRetentionProcessed()
    {
        return retentionProcessed.getPeriod();
    }

    public Period getRetentionError()
    {
        return retentionError.getPeriod();
    }

    public Set<Integer> getNonRetriableOracleErrorCodes()
    {
        return Collections.unmodifiableSet(nonRetriableOracleErrorCodes);
    }

    public PersistenceStrategy getPersistenceStragegy()
    {
        return persistenceStrategy;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("InputDir: ").append(this.getInputDir());
        sb.append(" ExportDirectoryScanSchedule: ").append(this.getExportDirectoryCleanSchedule());
        sb.append(" PersistenceStrategy: ").append(this.getPersistenceStragegy());
        sb.append(" MaxBatchSize: ").append(this.getMaxBatchSize());
        sb.append(" MaxSqlBulkSize: ").append(this.getMaxSqlBulkSize());
        sb.append(" ExportDirectoryCleanSchedule: ").append(this.getExportDirectoryCleanSchedule());
        sb.append(" RetentionProcessed: ").append(this.getRetentionProcessed());
        sb.append(" RetentionError: ").append(this.getRetentionError());
        sb.append("\n\tNonRetriableOracleErrorCodes: number=").append(
                this.getNonRetriableOracleErrorCodes().size()).append(", listing:");
        for(final Integer code : this.getNonRetriableOracleErrorCodes())
        {
            sb.append("\n\t\t").append(code);
        }

        return sb.toString();
    }

    /*
     * Here come helper methods.
     */

    private static ImportConfiguration instance;

    static
    {
        try
        {
            instance = readConfiguration();
        }
        catch(Exception e)
        {
            throw new RuntimeException(
                    "Caught exception in initialization of " + ImportConfiguration.class.getName(),
                    e);
        }
    }

    private static ImportConfiguration readConfiguration()
            throws IOException,
            JAXBException,
            XmlToolsException,
            SAXException
    {
        final byte[] rawdata = readUrl();

        final Unmarshaller um = ConfigValidatingParserFactory.getInstance().makeUnmarshaller();
        final ImportConfiguration imc = (ImportConfiguration)um.unmarshal(new ByteArrayInputStream(rawdata));

        return imc;
    }

    private static URL makeConfigUrl() throws MalformedURLException
    {
        final String configDirUrl = System.getProperty(PROPERTY_CONFIG_DIR);

        if(null == configDirUrl)
        {
            String err = String.format(
                    "Property \"%s\" not set, cannot initialize Importer.",
                    ImportConfiguration.PROPERTY_CONFIG_DIR);
            LOG.warn(err);
            throw new IllegalArgumentException(err);
        }

        final String configUrl = configDirUrl + CONFIG_FILE_NAME;

        return new URL(configUrl);
    }

    private static byte[] readUrl() throws IOException
    {
        URL url = makeConfigUrl();

        LOG.info("Config URL: " + url);

        if(null == url)
            throw new IOException();

        InputStream is = null;
        byte[] result = null;

        try
        {
            is = new FileInputStream(url.getFile());
            int fileSize = is.available();
            result = new byte[fileSize];
            is.read(result);
        }
        catch(FileNotFoundException e)
        {
            LOG.warn("Could not find file " + url.getFile());
            throw e;
        }
        finally
        {
            try
            {
                if(is != null)
                    is.close();
            }
            catch(Exception e)
            {
            }
        }

        return result;
    }
}
