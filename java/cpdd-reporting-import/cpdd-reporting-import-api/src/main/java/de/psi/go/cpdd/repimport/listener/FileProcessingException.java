package de.psi.go.cpdd.repimport.listener;

public class FileProcessingException extends Exception
{
    public FileProcessingException(String msg)
    {
        super(msg);
    }

    public FileProcessingException(String msg, Throwable thr)
    {
        super(msg, thr);
    }

    private static final long serialVersionUID = -2871383277983431011L;
}
