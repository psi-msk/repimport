package de.psi.go.cpdd.repimport.config;

import javax.ejb.ScheduleExpression;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class ScheduleConfiguration
{
    public ScheduleExpression getScheduleExpression()
    {
        if(null == scheduleExpression)
        {
            scheduleExpression = new ScheduleExpression();

            scheduleExpression.second(second);
            scheduleExpression.minute(minute);
            scheduleExpression.hour(hour);
            scheduleExpression.month(month);
            scheduleExpression.year(year);
            scheduleExpression.dayOfWeek(dayOfWeek);
            scheduleExpression.dayOfMonth(dayOfMonth);
        }

        return scheduleExpression;
    }

    @XmlAttribute(name = "second", required = false)
    private String second = "0";

    @XmlAttribute(name = "minute", required = false)
    private String minute = "0";

    @XmlAttribute(name = "hour", required = false)
    private String hour = "0";

    @XmlAttribute(name = "month", required = false)
    private String month = "*";

    @XmlAttribute(name = "year", required = false)
    private String year = "*";

    @XmlAttribute(name = "dayOfWeek", required = false)
    private String dayOfWeek = "*";

    @XmlAttribute(name = "dayOfMonth", required = false)
    private String dayOfMonth = "*";

    private ScheduleExpression scheduleExpression = null;
}
