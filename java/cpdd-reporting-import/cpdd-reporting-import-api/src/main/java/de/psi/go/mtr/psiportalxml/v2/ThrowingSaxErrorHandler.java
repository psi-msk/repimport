package de.psi.go.mtr.psiportalxml.v2;

import org.apache.log4j.Logger;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;

/**
 * Ensures, that an {@link XMLReader} throws an exception on parse error.
 * 
 * @author emsku
 * 
 */
public class ThrowingSaxErrorHandler implements ErrorHandler
{
    private final Logger logger;

    ThrowingSaxErrorHandler(Logger logger)
    {
        this.logger = logger;
    }

    @Override
    public void warning(SAXParseException spex) throws SAXException
    {
        logger.warn(getParseExceptionInfo(spex));
    }

    @Override
    public void error(SAXParseException spex) throws SAXException
    {
        logger.error(getParseExceptionInfo(spex));
        throw spex;
    }

    @Override
    public void fatalError(SAXParseException spex) throws SAXException
    {
        logger.fatal(getParseExceptionInfo(spex));
        throw spex;
    }

    private static String getParseExceptionInfo(SAXParseException spex)
    {
        StringBuilder sb = new StringBuilder();

        sb.append("URI=").append(spex.getSystemId()).append(" Line=");
        sb.append(spex.getLineNumber()).append(": ").append(spex.getMessage());

        return sb.toString();
    }
}
