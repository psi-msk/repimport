package de.psi.go.cpdd.repimport;

public class XmlElementProcessingException extends Exception
{
    public XmlElementProcessingException(Object obj, String msg)
    {
        super(
                String.format(
                        "Error processing XML element of type %s, reason: %s",
                        (null != obj ? obj.getClass().getName() : "(null)"),
                        msg));
    }

    public XmlElementProcessingException(Class<? extends Object> aClass, Exception e)
    {
        super(String.format("Error processing XML element of type %s, reason: ", aClass.getName()) + e.getMessage(), e);
    }

    /**
    * 
    */
    private static final long serialVersionUID = -7654457231715332994L;

}
