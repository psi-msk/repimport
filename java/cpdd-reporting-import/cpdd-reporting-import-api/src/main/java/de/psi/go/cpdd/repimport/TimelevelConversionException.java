package de.psi.go.cpdd.repimport;

public class TimelevelConversionException extends RepdataException
{
    TimelevelConversionException(Exception e)
    {
        super(e);
    }

    TimelevelConversionException(String msg)
    {
        super(msg);
    }

    /**
    * 
    */
    private static final long serialVersionUID = -5737561502749491692L;

}
