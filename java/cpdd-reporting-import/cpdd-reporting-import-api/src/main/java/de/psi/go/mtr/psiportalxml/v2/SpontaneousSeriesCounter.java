package de.psi.go.mtr.psiportalxml.v2;

import org.apache.log4j.Logger;

import de.psi.go.mtr.tools.xml.IXmlElementProcessor;
import de.psi.go.psiportalxml.v2.dd.SpontaneousSeriesType;

public class SpontaneousSeriesCounter implements IXmlElementProcessor<SpontaneousSeriesType>
{
    private static final Logger LOG = Logger.getLogger(SpontaneousSeriesCounter.class);

    private int counter = 0;

    @Override
    public void process(SpontaneousSeriesType xmlelement) throws Exception
    {
        LOG.debug("SpontaneousSeries has " + xmlelement.getSpvalues().getSpval().size() + " values.");
        ++counter;
    }

    public int getCounter()
    {
        return counter;
    }
}
