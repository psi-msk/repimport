package de.psi.go.cpdd.repimport;

import java.io.File;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJBException;

import org.apache.log4j.Logger;

import de.psi.go.cpdd.repimport.config.ImportConfiguration;
import de.psi.go.cpdd.repimport.config.PersistenceStrategy;
import de.psi.go.mtr.tools.xml.IXmlElementProcessor;
import de.psi.go.psiportalxml.v2.dd.PsiControlV7TimelevelEnum;
import de.psi.go.psiportalxml.v2.dd.TimeseriesAttributeListType;
import de.psi.go.psiportalxml.v2.dd.TimeseriesAttributeType;
import de.psi.go.psiportalxml.v2.dd.TimeseriesType;
import de.psi.go.psiportalxml.v2.dd.ValueType;

/**
 * Processes {@link TimeseriesType} elements.
 *
 * @author emsku
 *
 */
public class XmlValueBatchWriter implements IXmlElementProcessor<TimeseriesType>
{
    public XmlValueBatchWriter(int maxBatchSize, PersistenceStrategy persistenceStrategy)
    {
        this.maxBatchSize = maxBatchSize;
        this.persistenceStrategy = persistenceStrategy;

        this.precision = ImportConfiguration.getInstance().getNumberFormat().getPrecision();
        this.scale = ImportConfiguration.getInstance().getNumberFormat().getScale();

        final StringBuilder nb = new StringBuilder("NUMBER");
        if(precision != 0 || scale != 0)
        {
            nb.append("(").append(precision).append(", ").append(scale).append(")");
        }
        nameOfValueType = nb.toString();

        this.maxVal = makeMaxval(precision, scale);
        LOG.debug("maximum for numerical values: " + maxVal);
    }

    public void setActualFile(File file)
    {
        this.actualFile = file;
    }

    public File getActualFile()
    {
        return actualFile;
    }

    public String getActualFileName()
    {
        return(null != actualFile ? actualFile.getName() : "(null)");
    }

    public int getNofWritten()
    {
        return this.nofWritten;
    }

    @Override
    public void process(TimeseriesType timeseries) throws XmlElementProcessingException, RepdataException
    {
        LOG.debug("timeseries: " + timeseries.getAddr().getObjectKey());
        final int nofValues = timeseries.getValues().getVal().size();
        LOG.debug("process: timeseries with " + nofValues + " values");
        nofValuesPassed += nofValues;

        final String psiControlV7Key = extractPSIcontrolV7Key(timeseries);

        try
        {
            final Guid guid = extractGuid(timeseries);

            final Calendar beginDate = timeseries.getBegin().toGregorianCalendar();
            final RepImportTimeLevel tl = extractTimeLevel(timeseries);

            for(final ValueType value : timeseries.getValues().getVal())
            {
                try
                {
                    final OdSeRtd entity = toEntity(beginDate, tl, guid, value);
                    LOG.debug(new StringBuilder("Entity: ").append(entity));
                    addToValueStack(entity);
                }
                catch(ValueException e)
                {
                    LOG.warn(e.getMessage() + ", value won't be written. Archive: " + show(timeseries));
                }
            }
        }
        catch(final GuidException guidex)
        {
            processingOk = false;
            LOG.error(
                    new StringBuilder("Error for timeseries ").append(psiControlV7Key).append(": ").append(
                            guidex.getMessage()));
            nofNotWrittenDueToError += nofValues;
        }
    }

    /**
     * Must be called after processing in order to write the last values still in
     * the list, which were not written.
     *
     * @throws XmlElementProcessingException
     */
    public void finish() throws XmlElementProcessingException
    {
        this.flush();

        if(!processingOk)
        {
            final String err = "Could not write "
                    + nofNotWrittenDueToError
                    + "/"
                    + nofValuesPassed
                    + " values. Wrote "
                    + nofWritten
                    + "/"
                    + nofValuesPassed
                    + " values. File name: "
                    + this.getActualFileName();
            LOG.error(err);
            AsduAdmLogger.ERROR(err);
            throw new XmlElementProcessingException(TimeseriesType.class, err);
        }
        final String info = "Wrote " + nofWritten + "/" + nofValuesPassed + " values.";
        LOG.info(info);
        AsduAdmLogger.INFO(info);
    }

    private void flush() throws XmlElementProcessingException
    {
        this.nofWritten += writeAndClear();
    }

    private int writeAndClear() throws XmlElementProcessingException
    {
        final int out = valueStack.size();
        boolean success = false;
        long nofTrials = 0;

        if(!faultyEntities.isEmpty())
        {
            processingOk = false;
            LOG.warn("The following entities cannot not written:");
            for(EntityAndErrorMessage entry : faultyEntities)
            {
                LOG.warn(entry.errorMessage);
                AsduAdmLogger.WARN(entry.errorMessage);
            }
            faultyEntities.clear();
        }

        while(!success)
        {
            ++nofTrials;
            final long retrial = nofTrials - 1;
            try
            {
                if(retrial > 0)
                {
                    final StringBuilder msg = new StringBuilder().append(retrial).append(
                            ". retrial to write data for file ").append(actualFile);
                    LOG.warn(msg);
                    if(doLogToAsduAdmLogger(retrial))
                    {
                        AsduAdmLogger.WARN(msg);
                    }
                }
                final OdSeRtdApi valueWriter = OdSeRtdApiProvider.getLocalJdbcBean();
                if(PersistenceStrategy.INSERT == persistenceStrategy)
                {
                    valueWriter.insertBatch(valueStack);
                }
                else
                {
                    valueWriter.mergeBatch(valueStack);
                }
                valueStack.clear();
                success = true;
            }
            catch(Exception e)
            {
                // The beans pack their exceptions into javax.ejb.EJBExceptions
                if(e instanceof javax.ejb.EJBException)
                {
                    EJBException ej = (EJBException)e;
                    Exception nested = ej.getCausedByException();
                    processOnException(nested, retrial);
                }
                else
                {
                    processOnException(e, retrial);
                }
            }
        }

        return out;
    }

    /**
     * Convert a val to an entity.
     *
     * @param ts
     * @param value
     * @return
     * @throws StatusConversionException
     * @throws ValueException
     * @throws ConversionException
     */
    private OdSeRtd toEntity(final Calendar beginDate, final RepImportTimeLevel tl, final Guid guid, ValueType value)
            throws StatusConversionException,
            ValueException
    {
        final Timestamp beginTs = new Timestamp(beginDate.getTimeInMillis());
        final Timestamp repTs = tl.incrementNTimes(value.getI().intValue() + tl.getBound().getIndexOffset(), beginTs);
        final Timestamp modTs = new Timestamp(value.getMd().toGregorianCalendar().getTimeInMillis());
        final PsiControlArchiveValueStatus psiControlStatus = new PsiControlArchiveValueStatus(value.getState());
        final boolean hasValue = !psiControlStatus.isValueNotExistent();
        final Integer valStat = hasValue ? psiControlStatus.getRepImportStatus().getNumval() : null;
        final BigDecimal decValue = hasValue ? value.getValue() : null;
        testValue(decValue);

        return new OdSeRtd(tl, repTs, guid.getDbRepresentation(), modTs, valStat, decValue);
    }

    // Tests, whether the passed value fits into the configured precision, which
    // must reflect the one in the data base.
    private void testValue(BigDecimal inVal) throws ValueException
    {
        if(null == inVal)
        {
            return;
        }
        if(inVal.abs().compareTo(maxVal) > 0)
        {
            throw new ValueException(
                    new StringBuffer().append(inVal).append(" cannot be represented as ").append(
                            nameOfValueType).toString());
        }
    }

    private String show(TimeseriesType ts)
    {
        final StringBuilder sb = new StringBuilder();

        sb.append("Tid = ").append(ts.getAddr().getObjectKey()).append(", timelevel = ").append(
                ts.getAddr().getTimelevel());

        return sb.toString();
    }

    private static BigDecimal makeMaxval(int precision, int scale)
    {
        // precision = scale = 0 denotes, that a regular Oracle NUMBER
        // is used. It is a configuration convention of this service
        if(precision == 0 && scale == 0)
        {
            return TEN.pow(38).subtract(ONE).multiply(TEN.pow(125 - 37));
        }
        return TEN.pow(precision).subtract(ONE).divide(TEN.pow(scale));
    }

    private void lieDown(long millis)
    {
        try
        {
            Thread.sleep(millis);
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    private void addToValueStack(OdSeRtd entity) throws XmlElementProcessingException
    {
        try
        {
            entity.check();
        }
        catch(final IllegalArgumentException illex)
        {
            faultyEntities.add(new EntityAndErrorMessage(entity, illex.getMessage()));
            ++nofNotWrittenDueToError;
            return;
        }

        this.valueStack.add(entity);
        LOG.debug("Add to value stack: " + entity);

        if(valueStack.size() >= maxBatchSize)
        {
            LOG.debug(
                    String.format("Batch has %d of maximal %d values, write to DB.", valueStack.size(), maxBatchSize));
            flush();
        }
    }

    private void processOnException(Exception e, long retrial) throws XmlElementProcessingException
    {
        if(e instanceof OdSeRtdException)
        {
            final OdSeRtdException o = (OdSeRtdException)e;

            if(o.isRetriable())
            {
                final StringBuffer msg = new StringBuffer().append("Retriable error while processing file ").append(
                        actualFile.getAbsolutePath()).append(": [ " + e + " ], waiting ").append(
                                RETRIAL_SECONDS).append(" s");
                LOG.warn(msg);
                if(doLogToAsduAdmLogger(retrial))
                {
                    AsduAdmLogger.WARN(msg);
                }
                lieDown(1000 * RETRIAL_SECONDS);
            }
            else
            {
                throw new XmlElementProcessingException(TimeseriesType.class, e);
            }
        }
        else
        {
            throw new XmlElementProcessingException(TimeseriesType.class, e);
        }
    }

    private boolean doLogToAsduAdmLogger(long retrial)
    {
        return(0 == ((retrial - 1) % ASDU_LOG_RETRIAL_EVERY));
    }

    /**
     *
     * @param ts
     *            an XML time series element
     * @return the {@link TimeseriesType#getAttributes()} entries with names {@link XmlValueBatchWriter#CONTROL_V7_KEY}
     *         and {@link XmlValueBatchWriter#GUID_ATTRIBUTE_KEY}
     * @throws XmlElementProcessingException
     *             if time series attributes are defined more than once or if not both variables named
     *             {@link XmlValueBatchWriter#CONTROL_V7_KEY}, {@link XmlValueBatchWriter#GUID_ATTRIBUTE_KEY} are
     *             present in the list
     *             of time series attributes {@link TimeseriesType#getAttributes()}
     * @throws GuidException
     */
    private static Guid extractGuid(final TimeseriesType ts) throws XmlElementProcessingException, GuidException
    {
        final String guidString = extractAttribute(ts, GUID_ATTRIBUTE_KEY);

        return new Guid(guidString);
    }

    private static String extractPSIcontrolV7Key(final TimeseriesType ts) throws XmlElementProcessingException
    {
        return extractAttribute(ts, PSI_CONTROL_V7_KEY_ATTRIBUTE_KEY);
    }

    private static String extractAttribute(final TimeseriesType ts, final String attrName)
            throws XmlElementProcessingException
    {
        final Map<String, String> attributes = new HashMap<String, String>();

        final TimeseriesAttributeListType attrs = ts.getAttributes();
        final boolean hasAttributes = (null != attrs && !attrs.getAttr().isEmpty());
        if(hasAttributes)
        {
            for(final TimeseriesAttributeType attr : attrs.getAttr())
            {
                final String name = attr.getName();
                final String value = attr.getValue();

                if(name.equals(attrName) && attributes.containsKey(name))
                {
                    throw new XmlElementProcessingException(
                            ts,
                            "Multiple definition of time series attribute \"" + name + "\"");
                }

                attributes.put(name, value);
            }
        }

        final String attrValue = attributes.get(attrName);
        final boolean missesAttribute = (null == attrValue);

        if(missesAttribute)
        {
            final StringBuilder errBuffer = new StringBuilder("Missing obligatory time series attribute: ").append(
                    attrName);
            throw new XmlElementProcessingException(ts, errBuffer.toString());
        }

        return attrValue;
    }

    public static RepImportTimeLevel extractTimeLevel(TimeseriesType ts) throws TimelevelConversionException
    {
        // First choice is the conversion by the optional PSIcontrol time level (it is finer grained).
        final PsiControlV7TimelevelEnum psiControlTimelevel = ts.getPsiControlV7Timelevel();
        if(null != psiControlTimelevel)
        {
            return RepImportTimeLevel.fromPsiControlV7Timelevel(psiControlTimelevel);
        }
        else
        {
            return RepImportTimeLevel.fromPsiportalTimelevel(ts.getAddr().getTimelevel());
        }
    }

    private static class EntityAndErrorMessage
    {
        @SuppressWarnings("unused")
        private final OdSeRtd entity;
        private final String errorMessage;

        EntityAndErrorMessage(OdSeRtd entity, String errorMessage)
        {
            this.entity = entity;
            this.errorMessage = errorMessage;
        }
    }

    private final PersistenceStrategy persistenceStrategy;
    private final int maxBatchSize;
    private final List<OdSeRtd> valueStack = new ArrayList<OdSeRtd>();
    private final List<EntityAndErrorMessage> faultyEntities = new ArrayList<EntityAndErrorMessage>();

    // NUMBER or NUMBER(p, s), depending on configuration.
    private final String nameOfValueType;
    private static final BigDecimal TEN = new BigDecimal(10);
    private static final BigDecimal ONE = new BigDecimal(1);
    private final int precision;
    private final int scale;
    private final BigDecimal maxVal;

    private int nofWritten = 0;
    private int nofNotWrittenDueToError = 0;
    private int nofValuesPassed = 0;

    private static final String GUID_ATTRIBUTE_KEY = "GUID";
    private static final String PSI_CONTROL_V7_KEY_ATTRIBUTE_KEY = "PSICONTROLV7-KEY";

    // Retrial interval in case of data base inaccessibility
    private static final int RETRIAL_SECONDS = 10;
    // How frequently log DB retrials to the ASDU syslog?
    private static final int ASDU_LOG_RETRIAL_EVERY = 5;

    private File actualFile = null;
    private boolean processingOk = true;

    private static final Logger LOG = Logger.getLogger(XmlValueBatchWriter.class);
}
