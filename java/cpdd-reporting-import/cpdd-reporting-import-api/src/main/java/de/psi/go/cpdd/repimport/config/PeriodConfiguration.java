package de.psi.go.cpdd.repimport.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

import org.joda.time.MutablePeriod;
import org.joda.time.Period;

@XmlAccessorType(XmlAccessType.FIELD)
public class PeriodConfiguration
{

    public Period getPeriod()
    {
        MutablePeriod period = new MutablePeriod();

        period.setSeconds(seconds);
        period.setMinutes(minutes);
        period.setHours(hours);
        period.setDays(days);
        period.setWeeks(weeks);
        period.setMonths(months);
        period.setYears(years);

        return new Period(period);
    }

    @XmlAttribute(name = "seconds", required = false)
    private Integer seconds = 0;

    @XmlAttribute(name = "minutes", required = false)
    private Integer minutes = 0;

    @XmlAttribute(name = "hours", required = false)
    private Integer hours = 0;

    @XmlAttribute(name = "days", required = false)
    private Integer days = 0;

    @XmlAttribute(name = "weeks", required = false)
    private Integer weeks = 0;

    @XmlAttribute(name = "months", required = false)
    private Integer months = 0;

    @XmlAttribute(name = "years", required = false)
    private Integer years = 0;
}
