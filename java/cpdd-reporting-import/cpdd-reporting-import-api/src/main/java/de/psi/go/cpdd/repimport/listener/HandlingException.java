package de.psi.go.cpdd.repimport.listener;

public class HandlingException extends Exception
{
    HandlingException(String msg)
    {
        super(msg);
    }

    /**
    * 
    */
    private static final long serialVersionUID = 3188664458072633717L;

}
