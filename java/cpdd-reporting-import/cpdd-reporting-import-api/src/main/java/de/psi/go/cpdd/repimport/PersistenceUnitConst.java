package de.psi.go.cpdd.repimport;

public interface PersistenceUnitConst
{
    public static final String REPEXP_UNIT = "cpdd-reporting-import-persistence-unit";
    public static final String DATA_SOURCE = "java:CPDD-REPORTING-IMPORT-DS";
}
