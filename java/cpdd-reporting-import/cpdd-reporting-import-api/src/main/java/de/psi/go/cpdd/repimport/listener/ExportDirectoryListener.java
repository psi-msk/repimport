package de.psi.go.cpdd.repimport.listener;

import java.io.File;
import java.io.FilenameFilter;

import org.apache.log4j.Logger;

public class ExportDirectoryListener extends AFileListener
{
    // //////////////////////////////////////////////////////////////////////
    public ExportDirectoryListener(
            final String dirName,
            final String activeExtension,
            final String passiveExtension,
            final IFileProcessor fileProcessor) throws Exception
    {
        super(dirName, fileProcessor);

        ensureExistingDir(this.watchedDir, false);

        this.okDir = new File(dirName + File.separator + "ok");
        this.errDir = new File(dirName + File.separator + "err");

        ensureExistingDir(okDir, true);
        ensureExistingDir(errDir, true);

        this.activeAxtension = "." + activeExtension;
        this.passiveExtension = "." + passiveExtension;

        unmarkLeftOvers();
    }

    // //////////////////////////////////////////////////////////////////////
    @Override
    public boolean accept(File dir, String name)
    {
        return dir.equals(this.watchedDir) && name.endsWith(this.activeAxtension);
    }

    // //////////////////////////////////////////////////////////////////////
    @Override
    protected File markProcessing(File file) throws FileListenerException
    {
        final String newName = file.getAbsolutePath() + this.passiveExtension;

        return ensureRename(file, newName);
    }

    // //////////////////////////////////////////////////////////////////////
    @Override
    protected void unmarkProcessing(File file) throws FileListenerException
    {
        final String oldname = file.getAbsolutePath();

        if(!oldname.endsWith(this.passiveExtension))
        {
            final String msg = String.format(
                    "Cannot unmark \"%s\", because it ends not in mark suffix \"%s\".",
                    oldname,
                    this.passiveExtension);
            LOG.warn(msg);
            throw new FileListenerException(msg);
        }

        final String newName = removeProcessingMark(oldname);

        ensureRename(file, newName);
    }

    // //////////////////////////////////////////////////////////////////////
    @Override
    public void handleError(File file) throws HandlingException
    {
        try
        {
            LOG.debug("handleError for " + isWhat(file) + " " + file.getAbsolutePath());
            ensureExistingDir(errDir, true);
            moveToDirAndUnmark(file, errDir);
        }
        catch(Exception e)
        {
            final String msg = String.format(
                    "handleError() for %s \"%s\" caught generic exception: %s %s",
                    isWhat(file),
                    file.getAbsolutePath(),
                    e.getMessage(),
                    e.getCause());
            LOG.error(msg);
            throw new HandlingException(msg);
        }
    }

    // //////////////////////////////////////////////////////////////////////
    @Override
    public void handleOk(File file) throws HandlingException
    {
        try
        {
            LOG.debug("handleOk for " + isWhat(file) + " " + file.getAbsolutePath());
            ensureExistingDir(okDir, true);
            moveToDirAndUnmark(file, okDir);
        }
        catch(Exception e)
        {
            final String msg = String.format(
                    "handleOk() for %s \"%s\" caught generic exception: %s %s",
                    isWhat(file),
                    file.getAbsolutePath(),
                    e.getMessage(),
                    e.getCause());
            LOG.error(msg);
            throw new HandlingException(msg);
        }
    }

    // //////////////////////////////////////////////////////////////////////
    @Override
    public AFileListener incrementNofScans()
    {
        nofScans += 1;

        return this;
    }

    // //////////////////////////////////////////////////////////////////////
    @Override
    public long getNofScans()
    {
        return nofScans;
    }

    // //////////////////////////////////////////////////////////////////////
    private void moveToDirAndUnmark(final File file, final File dir) throws FileListenerException
    {
        final String newName = removeProcessingMark(dir.getAbsolutePath() + File.separator + file.getName());

        ensureRename(file, newName);
    }

    // Used at construction time. If an instance of this class is constructed and
    // there are still directories marked as not being progressed, unmark them.
    // Thus, they are being processed again.
    private void unmarkLeftOvers() throws FileListenerException
    {
        final File[] files = watchedDir.listFiles(new ProcessedFilesFilter());
        for(File file : files)
        {
            LOG.warn(
                    String.format(
                            "Found leftover %s \"%s\". Removing mark for new processing.",
                            isWhat(file),
                            file.getAbsolutePath()));
            unmarkProcessing(file);
        }
    }

    // Finds files with the processing mark
    private class ProcessedFilesFilter implements FilenameFilter
    {
        @Override
        public boolean accept(File dir, String name)
        {
            return dir.equals(watchedDir) && name.endsWith(passiveExtension);
        }
    }

    // For aesthetics ...
    String isWhat(final File file)
    {
        return(file.isDirectory() ? "directory" : "file");
    }

    // Remove the progress mark suffix from the string.
    private String removeProcessingMark(final String fileName)
    {
        return fileName.substring(0, fileName.lastIndexOf(this.passiveExtension));
    }

    private final String activeAxtension;
    private final String passiveExtension;
    private final File okDir;
    private final File errDir;

    private static final Logger LOG = Logger.getLogger(ExportDirectoryListener.class);

    private static long nofScans = 0;
}
