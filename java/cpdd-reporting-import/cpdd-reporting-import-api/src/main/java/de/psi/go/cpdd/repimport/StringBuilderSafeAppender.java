package de.psi.go.cpdd.repimport;

public class StringBuilderSafeAppender
{
    /**
     *
     * Appends to a {@link StringBuilder}. If passed instance is null, instantiate a new one.
     *
     * @param sb
     * @param txt
     * @return A {@link StringBuilder} with txt appended. If sb was null, a new one will be instantiated, otherwise
     *         return the same one.
     */
    public static StringBuilder ensureStringBuilder(StringBuilder sb, final String txt)
    {
        final boolean wasInitialized = (null != sb);
        final StringBuilder result = (wasInitialized ? sb : new StringBuilder());
        // There is something in the builder already. Append a separator
        if(wasInitialized)
        {
            result.append(" / ");
        }
        result.append(txt);
        return result;
    }

    private StringBuilderSafeAppender()
    {
    }
}
