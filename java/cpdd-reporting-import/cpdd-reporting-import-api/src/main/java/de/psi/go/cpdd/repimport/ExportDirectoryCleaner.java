package de.psi.go.cpdd.repimport;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Period;

public class ExportDirectoryCleaner
{
    public ExportDirectoryCleaner(String path, Period retentionProcessed, Period retentionError)
    {
        this.rootDir = new File(path);
        this.processedDir = new File(path + File.separatorChar + PROCESSED_SUBDIR_NAME);
        this.errorDir = new File(path + File.separatorChar + ERROR_SUBDIR_NAME);
        this.retentionProcessed = retentionProcessed;
        this.retentionError = retentionError;
    }

    public long clean() throws ExportDirCleanException
    {
        checkFileExists(rootDir);
        checkFileIsDir(rootDir);

        final DateTime now = DateTime.now();

        long nofDeleted = 0;
        nofDeleted += cleanSubdir(processedDir, retentionProcessed, now);
        nofDeleted += cleanSubdir(errorDir, retentionError, now);

        return nofDeleted;
    }

    public String getRootDirPath()
    {
        return this.rootDir.getAbsolutePath();
    }

    void checkFileExists(File dir) throws ExportDirCleanException
    {
        if(!dir.exists())
            throw new ExportDirCleanException("Directory " + dir.getAbsolutePath() + " does not exist.");
    }

    void checkFileIsDir(File dir) throws ExportDirCleanException
    {
        if(!dir.isDirectory())
            throw new ExportDirCleanException("File " + dir.getAbsolutePath() + " is no directory.");
    }

    private long cleanSubdir(File dir, Period retention, DateTime now) throws ExportDirCleanException
    {
        if(dir.exists())
            checkFileIsDir(dir);
        else
            return 0;

        DateTime keepUpTo = now.minus(retention);

        final File[] files = dir.listFiles();

        long nofDeleted = 0;
        for(File file : files)
        {
            if(FileUtils.isFileOlder(file, keepUpTo.getMillis()))
            {
                try
                {
                    FileUtils.forceDelete(file);
                }
                catch(IOException ioe)
                {
                    LOG.error(
                            new StringBuffer().append("Cannot delete file ").append(file.getAbsolutePath()).append(
                                    ". Reason: ").append(ioe.getMessage()));
                }
                ++nofDeleted;
            }
        }
        LOG.info(
                new StringBuffer().append(dir.getAbsolutePath()).append(": deleted ").append(nofDeleted).append(
                        " directories / files."));

        return nofDeleted;
    }

    final File rootDir;
    final File processedDir;
    final File errorDir;

    static final String PROCESSED_SUBDIR_NAME = "ok";
    static final String ERROR_SUBDIR_NAME = "err";

    final Period retentionProcessed;
    final Period retentionError;

    static final Logger LOG = Logger.getLogger(ExportDirectoryCleaner.class);
}
