package de.psi.go.cpdd.repimport;

public class PsiControlArchiveValueStatus
{
    private final int numStatus;
    private final RepImportValueStatus repImportStatus;
    private final boolean isValueNonExistent;

    public PsiControlArchiveValueStatus(String psiControlStatus) throws StatusConversionException
    {
        try
        {
            numStatus = Integer.valueOf(psiControlStatus);
        }
        catch(NumberFormatException e)
        {
            String msg = String.format("Could not convert [%s] to int", psiControlStatus);
            throw new StatusConversionException(msg);
        }
        // Now, we are sure to have an integer in our hand:  
        this.repImportStatus = determineRepImportStatus(numStatus);
        this.isValueNonExistent = ((numStatus & STATUS_NULL_VALUE) != 0);
    }

    public RepImportValueStatus getRepImportStatus()
    {
        return repImportStatus;
    }

    public boolean isValueNotExistent()
    {
        return isValueNonExistent;
    }

    private static RepImportValueStatus determineRepImportStatus(int numStatus)
    {
        if((numStatus & StatusOK) != 0)
        {
            return RepImportValueStatus.VALID;
        }

        if((numStatus & MaskForeignSystem) != 0)
        {
            return RepImportValueStatus.AUTOMATICALLYCORRECTED;
        }

        if((numStatus & MaskMankorr) != 0)
        {
            return RepImportValueStatus.MANUALLYCORRECTED;
        }

        return RepImportValueStatus.DISTURBED;
    }

    /**
     * The incoming status follows a bit mask logic. Here are the masks to be
     * considered (these values are taken from the IDL-layer of the PSIControl
     * archive system)
     */
    private static final int StatusOK = 0x00004000;
    private static final int StatusMANKOR = 0x00002000;
    private static final int StatusERSALG = 0x00001000;
    private static final int StatusREF = 0x00000800;
    private static final int StatusFORT = 0x00000100;
    private static final int Status1EW = 0x00800000;
    private static final int Status2EW = 0x00400000;
    private static final int Status3EW = 0x00200000;
    private static final int StatusERSMAN = 0x00000080;
    private static final int StatusAUTO = 0x00000040;
    private static final int StatusABRERW = 0x00000400;
    private static final int StatusNORM = 0x00008000;

    // Make the combined status values.
    private static final int MaskForeignSystem = makeMaskForeignSystem();
    private static final int MaskMankorr = makeMaskMankorr();

    /**
     * This status indicates an undefined value. Member introduced because of the counterintuitive original name of the
     * status bit.
     */
    private static final int STATUS_NULL_VALUE = StatusNORM;

    private static int makeMaskForeignSystem()
    {
        int MaskForeignSystem = 0;

        MaskForeignSystem |= StatusERSALG;
        MaskForeignSystem |= StatusREF;
        MaskForeignSystem |= StatusFORT;
        MaskForeignSystem |= Status1EW;
        MaskForeignSystem |= Status2EW;
        MaskForeignSystem |= Status3EW;
        MaskForeignSystem |= StatusAUTO;
        MaskForeignSystem |= StatusABRERW;

        return MaskForeignSystem;
    }

    private static int makeMaskMankorr()
    {
        int MaskMankorr = 0;

        MaskMankorr |= StatusMANKOR;
        MaskMankorr |= StatusERSMAN;

        return MaskMankorr;
    }
}
