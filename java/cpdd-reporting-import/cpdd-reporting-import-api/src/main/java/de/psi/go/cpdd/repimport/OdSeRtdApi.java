package de.psi.go.cpdd.repimport;

import java.util.List;

/**
 * Access to the flat table "OD_SE_RTD" for data exchange between PSIportal and
 * the CPDD reporting.
 * 
 * @author kulbrich
 * 
 */

public interface OdSeRtdApi
{
    /**
     * Makes a simple insert for the batch.
     * 
     * @param transientList
     *            values to be INSERTed
     * @throws OdSeRtdException
     */
    void insertBatch(List<OdSeRtd> transientList) throws OdSeRtdException;

    /**
     * Merges a batch according to the table's unique constraint.
     * 
     * @param transientList
     *            values to be MERGEd
     * @throws OdSeRtdException
     */
    void mergeBatch(List<OdSeRtd> transientList) throws OdSeRtdException;
}
