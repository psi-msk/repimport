package de.psi.go.cpdd.repimport;

public class GuidException extends Exception
{
    public GuidException(String msg)
    {
        super(msg);
    }

    public GuidException(StringBuilder msgBuff)
    {
        super(msgBuff.toString());
    }

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}
