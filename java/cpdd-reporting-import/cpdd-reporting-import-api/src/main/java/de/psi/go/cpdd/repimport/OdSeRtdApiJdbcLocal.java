package de.psi.go.cpdd.repimport;

public interface OdSeRtdApiJdbcLocal extends OdSeRtdApi
{
    public static final String JNDI_NAME = "de.psi.go.repdata.OdSeRtdApiJdbc/local";
}
