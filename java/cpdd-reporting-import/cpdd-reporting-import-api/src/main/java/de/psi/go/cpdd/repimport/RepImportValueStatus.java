package de.psi.go.cpdd.repimport;

public enum RepImportValueStatus
{
    VALID(0),
    AUTOMATICALLYCORRECTED(1),
    MANUALLYCORRECTED(2),
    DISTURBED(3);

    public Integer getNumval()
    {
        return numval;
    }

    private RepImportValueStatus(int numval)
    {
        this.numval = numval;
    }

    private final Integer numval;
}
