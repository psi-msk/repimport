/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.psiportalxml.v2;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;

import org.apache.log4j.Logger;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;

import de.psi.go.mtr.tools.xml.IXmlElementProcessor;
import de.psi.go.mtr.tools.xml.ParameterizedElementFilter;
import de.psi.go.psiportalxml.v2.dd.PsiportalDdXml;
import de.psi.go.psiportalxml.v2.dd.SpontaneousSeriesType;

/*
 * Loops over all <timeseries/> in an XML document and applies a processor to each one.
 * Applies validation with the schema used to generate the objects.
 */

public class SpontaneousSeriesLooper
{
    public SpontaneousSeriesLooper(
            InputStream inputStream,
            IXmlElementProcessor<SpontaneousSeriesType> xmlElementProcessor)
    {
        this.inputStream = inputStream;
        this.xmlElementProcessor = xmlElementProcessor;
    }

    public void process() throws Exception
    {
        JAXBContext context = JAXBContext.newInstance(PsiPortalXmlConstants.DYNAMIC_DATA_PACKAGE_NAME);

        XMLReader reader = PsiPortalXmlValidatingParserFactory.getInstance().makeDdSaxParser().getXMLReader();
        reader.setErrorHandler(new MyErrorHandler());

        ParameterizedElementFilter<SpontaneousSeriesType> splitter = new ParameterizedElementFilter<SpontaneousSeriesType>(
                context,
                PsiPortalXmlValidatingParserFactory.getInstance(),
                PsiportalDdXml.class,
                PsiPortalXmlConstants.DYNAMIC_DATA_NAMESPACE,
                PsiPortalXmlConstants.SPONTANEOUS_SERIES_ELEMENT_NAME,
                this.xmlElementProcessor);

        reader.setContentHandler(splitter);

        InputSource inputSource = new InputSource(this.inputStream);
        reader.parse(inputSource);
    }

    /**
     * Ensures, that an {@link XMLReader} throws an exception on parse error.
     * 
     * @author emsku
     * 
     */
    private static class MyErrorHandler implements ErrorHandler
    {
        @Override
        public void warning(SAXParseException spex) throws SAXException
        {
            LOG.warn(getParseExceptionInfo(spex));
        }

        @Override
        public void error(SAXParseException spex) throws SAXException
        {
            LOG.error(getParseExceptionInfo(spex));
            throw spex;
        }

        @Override
        public void fatalError(SAXParseException spex) throws SAXException
        {
            LOG.fatal(getParseExceptionInfo(spex));
            throw spex;
        }

        private static String getParseExceptionInfo(SAXParseException spex)
        {
            StringBuilder sb = new StringBuilder();

            sb.append("URI=").append(spex.getSystemId()).append(" Line=");
            sb.append(spex.getLineNumber()).append(": ").append(spex.getMessage());

            return sb.toString();
        }
    }

    private static final Logger LOG = Logger.getLogger(SpontaneousSeriesLooper.class);

    private final InputStream inputStream;
    private final IXmlElementProcessor<SpontaneousSeriesType> xmlElementProcessor;
}
