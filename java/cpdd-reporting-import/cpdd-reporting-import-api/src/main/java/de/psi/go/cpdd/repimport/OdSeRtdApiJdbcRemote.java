package de.psi.go.cpdd.repimport;

public interface OdSeRtdApiJdbcRemote extends OdSeRtdApi
{
    public static final String JNDI_NAME = "de.psi.go.repdata.OdSeRtdApiJdbc/remote";
}
