package de.psi.go.cpdd.repimport;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.Map;

import de.psi.go.mtr.tools.MillisIncrementation;
import de.psi.go.mtr.tools.MillisIncrementation.DayIncrementor;
import de.psi.go.mtr.tools.MillisIncrementation.HourIncrementor;
import de.psi.go.mtr.tools.MillisIncrementation.MinuteIncrementor;
import de.psi.go.mtr.tools.MillisIncrementation.MonthIncrementor;
import de.psi.go.mtr.tools.MillisIncrementation.SecondIncrementor;
import de.psi.go.mtr.tools.MillisIncrementation.YearIncrementor;
import de.psi.go.psiportalxml.v2.dd.PsiControlV7TimelevelEnum;
import de.psi.go.psiportalxml.v2.dd.TimelevelEnum;

public enum RepImportTimeLevel
{
    S("S", PsiControlV7TimelevelEnum.SEC, null, new SecondIncrementor(1), IntervallBound.UPPER),
    S5("S5", PsiControlV7TimelevelEnum._5SEC, TimelevelEnum.S_5, new SecondIncrementor(5), IntervallBound.UPPER),
    S10("S10", PsiControlV7TimelevelEnum._10SEC, null, new SecondIncrementor(10), IntervallBound.UPPER),
    S20("S20", PsiControlV7TimelevelEnum._20SEC, null, new SecondIncrementor(20), IntervallBound.UPPER),
    S30("S30", PsiControlV7TimelevelEnum._30SEC, null, new SecondIncrementor(30), IntervallBound.UPPER),

    MIN("MIN", PsiControlV7TimelevelEnum.MIN, TimelevelEnum.MI, new MinuteIncrementor(1), IntervallBound.UPPER),
    MIN2("MIN2", PsiControlV7TimelevelEnum._2MIN, null, new MinuteIncrementor(2), IntervallBound.UPPER),
    MIN3("MIN3", PsiControlV7TimelevelEnum._3MIN, TimelevelEnum.MI_3, new MinuteIncrementor(3), IntervallBound.UPPER),
    MIN4("MIN4", PsiControlV7TimelevelEnum._4MIN, null, new MinuteIncrementor(4), IntervallBound.UPPER),
    MIN5("MIN5", PsiControlV7TimelevelEnum._5MIN, TimelevelEnum.MI_5, new MinuteIncrementor(5), IntervallBound.UPPER),
    MIN6("MIN6", PsiControlV7TimelevelEnum._6MIN, null, new MinuteIncrementor(6), IntervallBound.UPPER),
    MIN15(
            "MIN15",
            PsiControlV7TimelevelEnum._15MIN,
            TimelevelEnum.MI_15,
            new MinuteIncrementor(15),
            IntervallBound.UPPER),
    MIN30("MIN30", PsiControlV7TimelevelEnum._30MIN, null, new MinuteIncrementor(30), IntervallBound.UPPER),

    H("H", PsiControlV7TimelevelEnum.HOUR, TimelevelEnum.H, new HourIncrementor(1), IntervallBound.UPPER),
    H2("H2", PsiControlV7TimelevelEnum._2HOUR, TimelevelEnum.H_2, new HourIncrementor(2), IntervallBound.UPPER),
    H3("H3", PsiControlV7TimelevelEnum._3HOUR, null, new HourIncrementor(3), IntervallBound.UPPER),
    H4("H4", PsiControlV7TimelevelEnum._4HOUR, null, new HourIncrementor(4), IntervallBound.UPPER),
    H6("H6", PsiControlV7TimelevelEnum._6HOUR, null, new HourIncrementor(6), IntervallBound.UPPER),
    H8("H8", PsiControlV7TimelevelEnum._8HOUR, null, new HourIncrementor(8), IntervallBound.UPPER),
    H12("H12", PsiControlV7TimelevelEnum._12HOUR, null, new HourIncrementor(12), IntervallBound.UPPER),

    D("D", PsiControlV7TimelevelEnum.DAY, TimelevelEnum.D, new DayIncrementor(1), IntervallBound.LOWER),
    D6("D", PsiControlV7TimelevelEnum.DAY_6, null, new DayIncrementor(1), IntervallBound.LOWER),
    D7("D", PsiControlV7TimelevelEnum.DAY_7, null, new DayIncrementor(1), IntervallBound.LOWER),
    D8("D", PsiControlV7TimelevelEnum.DAY_8, null, new DayIncrementor(1), IntervallBound.LOWER),
    D10("D", PsiControlV7TimelevelEnum.DAY_10, null, new DayIncrementor(1), IntervallBound.LOWER),

    M("M", PsiControlV7TimelevelEnum.MONTH, TimelevelEnum.M, new MonthIncrementor(1), IntervallBound.LOWER),
    M6("M", PsiControlV7TimelevelEnum.MONTH_6, null, new MonthIncrementor(1), IntervallBound.LOWER),
    M7("M", PsiControlV7TimelevelEnum.MONTH_7, null, new MonthIncrementor(1), IntervallBound.LOWER),
    M8("M", PsiControlV7TimelevelEnum.MONTH_8, null, new MonthIncrementor(1), IntervallBound.LOWER),
    M10("M", PsiControlV7TimelevelEnum.MONTH_10, null, new MonthIncrementor(1), IntervallBound.LOWER),

    Q("Q", PsiControlV7TimelevelEnum.QUARTER, null, new MonthIncrementor(3), IntervallBound.LOWER),
    Q6("Q", PsiControlV7TimelevelEnum.QUARTER_6, null, new MonthIncrementor(3), IntervallBound.LOWER),
    Q7("Q", PsiControlV7TimelevelEnum.QUARTER_7, null, new MonthIncrementor(3), IntervallBound.LOWER),
    Q8("Q", PsiControlV7TimelevelEnum.QUARTER_8, null, new MonthIncrementor(3), IntervallBound.LOWER),
    Q10("Q", PsiControlV7TimelevelEnum.QUARTER_10, null, new MonthIncrementor(3), IntervallBound.LOWER),

    Q_JAN6("Q", PsiControlV7TimelevelEnum.QUARTER_JAN_6, null, new MonthIncrementor(3), IntervallBound.LOWER),
    Q_JAN7("Q", PsiControlV7TimelevelEnum.QUARTER_JAN_7, null, new MonthIncrementor(3), IntervallBound.LOWER),
    Q_JAN8("Q", PsiControlV7TimelevelEnum.QUARTER_JAN_8, null, new MonthIncrementor(3), IntervallBound.LOWER),
    Q_JAN10("Q", PsiControlV7TimelevelEnum.QUARTER_JAN_10, null, new MonthIncrementor(3), IntervallBound.LOWER),

    Q_OCT("Q", PsiControlV7TimelevelEnum.QUARTER_OCT, null, new MonthIncrementor(3), IntervallBound.LOWER),
    Q_OCT6("Q", PsiControlV7TimelevelEnum.QUARTER_OCT_6, null, new MonthIncrementor(3), IntervallBound.LOWER),
    Q_OCT7("Q", PsiControlV7TimelevelEnum.QUARTER_OCT_7, null, new MonthIncrementor(3), IntervallBound.LOWER),
    Q_OCT8("Q", PsiControlV7TimelevelEnum.QUARTER_OCT_8, null, new MonthIncrementor(3), IntervallBound.LOWER),
    Q_OCT10("Q", PsiControlV7TimelevelEnum.QUARTER_OCT_10, null, new MonthIncrementor(3), IntervallBound.LOWER),

    Y("Y", PsiControlV7TimelevelEnum.YEAR, TimelevelEnum.Y, new YearIncrementor(1), IntervallBound.LOWER),
    Y6("Y", PsiControlV7TimelevelEnum.YEAR_6, null, new YearIncrementor(1), IntervallBound.LOWER),
    Y7("Y", PsiControlV7TimelevelEnum.YEAR_7, null, new YearIncrementor(1), IntervallBound.LOWER),
    Y8("Y", PsiControlV7TimelevelEnum.YEAR_8, null, new YearIncrementor(1), IntervallBound.LOWER),
    Y10("Y", PsiControlV7TimelevelEnum.YEAR_10, null, new YearIncrementor(1), IntervallBound.LOWER),

    Y_JAN6("Y", PsiControlV7TimelevelEnum.YEAR_JAN_6, null, new YearIncrementor(1), IntervallBound.LOWER),
    Y_JAN7("Y", PsiControlV7TimelevelEnum.YEAR_JAN_7, null, new YearIncrementor(1), IntervallBound.LOWER),
    Y_JAN8("Y", PsiControlV7TimelevelEnum.YEAR_JAN_8, null, new YearIncrementor(1), IntervallBound.LOWER),
    Y_JAN10("Y", PsiControlV7TimelevelEnum.YEAR_JAN_10, null, new YearIncrementor(1), IntervallBound.LOWER),

    Y_APR("Y", PsiControlV7TimelevelEnum.YEAR_APR, null, new YearIncrementor(1), IntervallBound.LOWER),
    Y_APR6("Y", PsiControlV7TimelevelEnum.YEAR_APR_6, null, new YearIncrementor(1), IntervallBound.LOWER),
    Y_APR7("Y", PsiControlV7TimelevelEnum.YEAR_APR_7, null, new YearIncrementor(1), IntervallBound.LOWER),
    Y_APR8("Y", PsiControlV7TimelevelEnum.YEAR_APR_8, null, new YearIncrementor(1), IntervallBound.LOWER),
    Y_APR10("Y", PsiControlV7TimelevelEnum.YEAR_APR_10, null, new YearIncrementor(1), IntervallBound.LOWER),

    Y_OCT("Y", PsiControlV7TimelevelEnum.YEAR_OCT, null, new YearIncrementor(1), IntervallBound.LOWER),
    Y_OCT6("Y", PsiControlV7TimelevelEnum.YEAR_OCT_6, null, new YearIncrementor(1), IntervallBound.LOWER),
    Y_OCT7("Y", PsiControlV7TimelevelEnum.YEAR_OCT_7, null, new YearIncrementor(1), IntervallBound.LOWER),
    Y_OCT8("Y", PsiControlV7TimelevelEnum.YEAR_OCT_8, null, new YearIncrementor(1), IntervallBound.LOWER),
    Y_OCT10("Y", PsiControlV7TimelevelEnum.YEAR_OCT_10, null, new YearIncrementor(1), IntervallBound.LOWER);

    /**
     * Represents, whether the reference time for an interval of the given scale is the interval's lower or upper bound.
     *
     * @author emsku
     *
     */
    public enum IntervallBound
    {
        LOWER(0),
        UPPER(1);

        private IntervallBound(int indexOffset)
        {
            this.indexOffset = indexOffset;
        }

        /**
         * The offset (0, 1) of the reference time with respect to the interval's lower bound.
         *
         * @return
         */
        public int getIndexOffset()
        {
            return indexOffset;
        }

        private final int indexOffset;
    }

    public static RepImportTimeLevel fromPsiportalTimelevel(final TimelevelEnum xmlRepr)
            throws TimelevelConversionException
    {
        RepImportTimeLevel out = psiPortalTimelevelMap.get(xmlRepr);

        if(null == out)
        {
            throw new TimelevelConversionException(
                    String.format("Cannot convert %s to %s", xmlRepr, RepImportTimeLevel.class.getName()));
        }

        return out;
    }

    public static RepImportTimeLevel fromPsiControlV7Timelevel(final PsiControlV7TimelevelEnum xmlRepr)
            throws TimelevelConversionException
    {
        RepImportTimeLevel out = psiControlV7TimelevelMap.get(xmlRepr);

        if(null == out)
        {
            throw new TimelevelConversionException(
                    String.format("Cannot convert %s to %s", xmlRepr, RepImportTimeLevel.class.getName()));
        }

        return out;
    }

    public long incMillis(long val)
    {
        return incrementer.inc(val);
    }

    public Date increment(Date date)
    {
        long newMillis = this.incMillis(date.getTime());
        return new Date(newMillis);
    }

    public Timestamp increment(Timestamp timestamp)
    {
        long newMillis = this.incMillis(timestamp.getTime());
        return new Timestamp(newMillis);
    }

    public long incMillisNTimes(int ntimes, long val)
    {
        return incrementer.incNtimes(ntimes, val);
    }

    public Date incrementNTimes(int ntimes, Date date)
    {
        long newMillis = this.incMillisNTimes(ntimes, date.getTime());
        return new Date(newMillis);
    }

    public Timestamp incrementNTimes(int ntimes, Timestamp timestamp)
    {
        long newMillis = this.incMillisNTimes(ntimes, timestamp.getTime());
        return new Timestamp(newMillis);
    }

    private final PsiControlV7TimelevelEnum psiControlV7Timelevel;
    private final TimelevelEnum psiPortalTimelevel;
    private final String dbValue;
    private final IntervallBound bound;

    private RepImportTimeLevel(
            String dbValue,
            PsiControlV7TimelevelEnum psiControlV7Timelevel,
            TimelevelEnum psiPortalTimelevel,
            MillisIncrementation.Incrementor incrementer,
            IntervallBound bound)
    {
        this.dbValue = dbValue;
        this.psiControlV7Timelevel = psiControlV7Timelevel;
        this.psiPortalTimelevel = psiPortalTimelevel;
        this.incrementer = incrementer;
        this.bound = bound;
    }

    private static final Map<PsiControlV7TimelevelEnum, RepImportTimeLevel> psiControlV7TimelevelMap = makePsiControlTimelevelMap();

    private static final Map<TimelevelEnum, RepImportTimeLevel> psiPortalTimelevelMap = makePsiPortalTimelevelMap();

    private static Map<PsiControlV7TimelevelEnum, RepImportTimeLevel> makePsiControlTimelevelMap()
    {
        Map<PsiControlV7TimelevelEnum, RepImportTimeLevel> madeMap = new EnumMap<PsiControlV7TimelevelEnum, RepImportTimeLevel>(
                PsiControlV7TimelevelEnum.class);

        for(RepImportTimeLevel member : RepImportTimeLevel.values())
        {
            madeMap.put(member.psiControlV7Timelevel, member);
        }

        return Collections.unmodifiableMap(madeMap);
    }

    private static Map<TimelevelEnum, RepImportTimeLevel> makePsiPortalTimelevelMap()
    {
        Map<TimelevelEnum, RepImportTimeLevel> madeMap = new EnumMap<TimelevelEnum, RepImportTimeLevel>(
                TimelevelEnum.class);

        for(RepImportTimeLevel member : RepImportTimeLevel.values())
        {
            // Only members having an associated TimelevelEnum
            if(null != member.psiPortalTimelevel)
            {
                madeMap.put(member.psiPortalTimelevel, member);
            }
        }

        return Collections.unmodifiableMap(madeMap);
    }

    public String getDbValue()
    {
        return dbValue;
    }

    public IntervallBound getBound()
    {
        return bound;
    }

    private final MillisIncrementation.Incrementor incrementer;
}
