package de.psi.go.cpdd.repimport.listener;

import java.io.File;

/*
 * Processes a file.
 */

public interface IFileProcessor
{
    void process(File file) throws Exception;
}
