package de.psi.go.cpdd.repimport;

public class ExportDirCleanException extends RepdataException
{
    ExportDirCleanException(String msg)
    {
        super(msg);
    }

    private static final long serialVersionUID = -949447251129974674L;
}
