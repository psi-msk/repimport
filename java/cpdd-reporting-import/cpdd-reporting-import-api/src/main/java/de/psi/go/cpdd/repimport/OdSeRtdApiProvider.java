package de.psi.go.cpdd.repimport;

import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Encapsulates the fetching of proxies to the API beans.
 * 
 * @author kulbrich
 * 
 */
public class OdSeRtdApiProvider
{
    public static OdSeRtdApi getLocalJdbcBean() throws NamingException
    {
        Object proxy = getContext().lookup(OdSeRtdApiJdbcLocal.JNDI_NAME);
        return (OdSeRtdApi)proxy;
    }

    public static OdSeRtdApi getRemoteJdbcBean() throws NamingException
    {
        Object proxy = getContext().lookup(OdSeRtdApiJdbcRemote.JNDI_NAME);
        return (OdSeRtdApi)proxy;
    }

    private static InitialContext getContext() throws NamingException
    {
        return new InitialContext();
    }
}
