package de.psi.go.mtr.psiportalxml.v2;

import org.apache.log4j.Logger;

import de.psi.go.mtr.tools.xml.IXmlElementProcessor;
import de.psi.go.psiportalxml.v2.dd.ValueType;

public class ValueCounter implements IXmlElementProcessor<ValueType>
{
    @SuppressWarnings("unused")
    private static final Logger LOG = Logger.getLogger(ValueCounter.class);

    private int counter = 0;

    @Override
    public void process(ValueType xmlelement) throws Exception
    {
        ++counter;
    }

    public int getCounter()
    {
        return counter;
    }
}
