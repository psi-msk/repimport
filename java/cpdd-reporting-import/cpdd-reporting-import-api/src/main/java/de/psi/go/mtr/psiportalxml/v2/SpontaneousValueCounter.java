package de.psi.go.mtr.psiportalxml.v2;

import org.apache.log4j.Logger;

import de.psi.go.mtr.tools.xml.IXmlElementProcessor;
import de.psi.go.psiportalxml.v2.dd.SpontaneousValueType;

public class SpontaneousValueCounter implements IXmlElementProcessor<SpontaneousValueType>
{
    @SuppressWarnings("unused")
    private static final Logger LOG = Logger.getLogger(SpontaneousValueCounter.class);

    private int counter = 0;

    @Override
    public void process(SpontaneousValueType xmlelement) throws Exception
    {
        ++counter;
    }

    public int getCounter()
    {
        return counter;
    }
}
