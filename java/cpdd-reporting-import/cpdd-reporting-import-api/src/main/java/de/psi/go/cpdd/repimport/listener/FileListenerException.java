package de.psi.go.cpdd.repimport.listener;

public class FileListenerException extends Exception
{
    public FileListenerException(String reason)
    {
        super(reason);
    }

    private static final long serialVersionUID = 148528342167033652L;
}
