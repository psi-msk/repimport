package de.psi.go.cpdd.repimport;

public class ValueException extends Exception
{
    public ValueException(String msg)
    {
        super(msg);
    }

    private static final long serialVersionUID = 2044452957207197268L;

}
