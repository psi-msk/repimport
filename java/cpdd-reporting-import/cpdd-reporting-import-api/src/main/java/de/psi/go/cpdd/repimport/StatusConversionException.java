package de.psi.go.cpdd.repimport;

public class StatusConversionException extends RepdataException
{
    StatusConversionException(Exception e)
    {
        super(e);
    }

    StatusConversionException(String msg)
    {
        super(msg);
    }

    /**
    * 
    */
    private static final long serialVersionUID = -5737561502749491692L;

}
