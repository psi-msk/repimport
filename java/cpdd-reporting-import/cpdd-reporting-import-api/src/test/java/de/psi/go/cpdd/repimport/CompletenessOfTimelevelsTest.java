package de.psi.go.cpdd.repimport;

import static org.junit.Assert.fail;

import java.util.EnumSet;
import java.util.Set;

import org.junit.Test;

import de.psi.go.psiportalxml.v2.dd.PsiControlV7TimelevelEnum;

public class CompletenessOfTimelevelsTest
{
    @Test
    public void testCompleteness()
    {
        final Set<PsiControlV7TimelevelEnum> unhandled = EnumSet.noneOf(PsiControlV7TimelevelEnum.class);

        for(final PsiControlV7TimelevelEnum en : PsiControlV7TimelevelEnum.values())
        {
            try
            {
                RepImportTimeLevel.fromPsiControlV7Timelevel(en);
            }
            catch(TimelevelConversionException e)
            {
                unhandled.add(en);
            }
        }

        if(!unhandled.isEmpty())
        {
            fail(
                    "The following enums of type "
                            + PsiControlV7TimelevelEnum.class
                            + " are not handled in RepImportTimeLevel.fromPsiControlV7Timelevel(...): "
                            + unhandled);
        }
    }
}
