package de.psi.go.cpdd.repimport;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.TimeZone;

import org.hibernate.cfg.Configuration;
import org.hibernate.mapping.Column;
import org.hibernate.mapping.Table;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.psi.go.cpdd.repimport.OdSeRtd;
import de.psi.go.cpdd.repimport.RepImportTimeLevel;
import de.psi.go.mtr.tools.FileHelper;

public class OdSeRtdTest
{
    // private Session session1 = null;
    // private Session session2 = null;

    @Before
    public void setup() throws IOException
    {
        @SuppressWarnings("unused")
        final String ddl = FileHelper.readWholeFileFromClasspath(this, "schema.sql");
    }

    @Test
    public void testPersistence()
    {
        final Timestamp rptDate = new Timestamp(0);
        final Timestamp changeDate = new Timestamp(0);
        final String guid = new BigInteger("AABBCC", 16).toString(16);
        final RepImportTimeLevel seanceType = RepImportTimeLevel.H2;
        final int flag = 1;
        final BigDecimal rptValue = new BigDecimal(Math.PI);

        OdSeRtd dteIn = new OdSeRtd(seanceType, rptDate, guid, changeDate, flag, rptValue);
        // OdSeRtd dteOut = null;

        TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT+9"));
        // session1 = h2sf.getSession();

        // writeTable(h2sf.getConfiguration());

        // session1.persist(dteIn);
        // session1.flush();
        // session1.close();

        write(String.format("ID-timestamp: %s / %d", rptDate, rptDate.getTime()));
        write(dteIn.toString());

        TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-9"));
        // session2 = h2sf.getSession();

        // dteOut = (OdSeRtd) session2.get(OdSeRtd.class, id);

        // assertNotNull(dteOut);

        write(String.format("ID-timestamp: %s / %d", rptDate, rptDate.getTime()));
    }

    @After
    public void after()
    {
        try
        {
            // session1.close();
            // session2.close();
            // h2sf.close();
        }
        catch(Exception e)
        {
        }
    }

    private void write(String txt)
    {
        System.out.println(txt);
    }

    @SuppressWarnings("unused")
    private void writeTable(Configuration cfg)
    {
        Iterator<Table> tabIt = cfg.getTableMappings();
        while(tabIt.hasNext())
        {
            Table tab = tabIt.next();
            write("TABLE " + tab.getName());
            @SuppressWarnings("unchecked")
            Iterator<Column> colIt = tab.getColumnIterator();
            while(colIt.hasNext())
            {
                Column col = colIt.next();
                write("   " + col.getName() + " " + col.getTypeIndex() + " " + col.getPrecision());
            }
        }
    }
}
