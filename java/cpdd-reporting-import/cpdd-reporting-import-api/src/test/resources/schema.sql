CREATE TABLE OD_SE_RTD(
                         KEY_ID VARCHAR(128),
                         SEANCE_TYPE VARCHAR(128),
                         RPT_DATE TIMESTAMP,
                         PARAMETER_ID VARCHAR(128),
                         CHANGE_DATE TIMESTAMP,
                         FLAG INTEGER,
                         RPT_VALUE DECIMAL(15, 2),
                         PRIMARY KEY(KEY_ID, SEANCE_TYPE, RPT_DATE)
                         );
