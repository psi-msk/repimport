package de.psi.go.cpdd.repimport;

import javax.ejb.Timer;
import javax.ejb.TimerService;
import org.apache.log4j.Logger;

public class ServiceUtilities
{
    private static final Logger LOG = Logger.getLogger(ServiceUtilities.class);

    static int cancelAllExistingTimers(TimerService ts)
    {
        int nofCancelled = 0;

        if(null != ts.getTimers())
        {
            for(Timer timer : ts.getTimers())
            {
                LOG.info(
                        "Cancelling timer "
                                + timer.getInfo()
                                + (timer.isCalendarTimer() ? " / " + timer.getSchedule() : ""));
                try
                {
                    timer.cancel();
                    ++nofCancelled;
                }
                catch(final Exception e)
                {
                    LOG.warn("Could not cancel timer " + timer.getInfo() + ", reason: " + e);
                }
            }
        }

        return nofCancelled;
    }
}
