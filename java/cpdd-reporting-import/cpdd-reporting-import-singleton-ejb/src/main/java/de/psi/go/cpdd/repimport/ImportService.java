package de.psi.go.cpdd.repimport;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.PrePassivate;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;
import org.jboss.ejb3.annotation.Clustered;
import org.quartz.SchedulerException;

import de.psi.go.cpdd.repimport.config.ImportConfiguration;
import de.psi.go.cpdd.repimport.listener.ExportDirectoryListener;
import de.psi.go.cpdd.repimport.listener.ExportDirectoryProcessor;
import de.psi.go.cpdd.repimport.listener.IFileProcessor;
import de.psi.go.mtr.server.ServerInformation;

@Singleton
@Clustered
@Startup
// If ConcurrencyManagement is left on default
// (ConcurrencyManagementType.CONTAINER), there occur exceptions due to
// concurrent access to the @Timeout annotated method.
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class ImportService implements IImportService
{
    private static final int INIT_RETRIAL_SECONDS = 1;

    private static final TimerConfig INIT_TIMER_CONFIG = new TimerConfig("INIT_TIMER", false);
    private static final TimerConfig SCAN_TIMER_CONFIG = new TimerConfig("SCAN_TIMER", false);
    private static final Object INIT_TIMER_INFO = INIT_TIMER_CONFIG.getInfo();
    private static final Object SCAN_TIMER_INFO = SCAN_TIMER_CONFIG.getInfo();

    private int initializationTrial = 0;

    @Resource
    private TimerService scanTimer;

    public ImportService()
    {
        LOG.info("ImportService() ...");

        LOG.info("ImportService() done");
    }

    @PostConstruct
    void postConstruct()
    {
        LOG.debug("postConstruct ...");
        makeInitTrialShot();
        LOG.debug("postConstruct done");
    }

    void makeInitTrialShot()
    {
        LOG.debug("makeInitTrialShot ...");

        scanTimer.createSingleActionTimer(INIT_RETRIAL_SECONDS * 1000, INIT_TIMER_CONFIG);

        LOG.debug("makeInitTrialShot done");
    }

    public void init()
    {
        LOG.info("init ...");

        try
        {
            myConfig = ImportConfiguration.getInstance();
            LOG.info("Configuration: " + myConfig.toString());

            IFileProcessor fileProc = new ExportDirectoryProcessor(myConfig.getMaxBatchSize());
            LOG.info("Initialized File Processor.");

            listener = new ExportDirectoryListener(myConfig.getInputDir(), "expdir", "workInProgress", fileProc);

            final int nofCancelled = ServiceUtilities.cancelAllExistingTimers(scanTimer);
            LOG.debug("Cancelled " + nofCancelled + " timers on startup.");

            final ScheduleExpression scanSchedule = myConfig.getExportDirectoryScanSchedule();
            scanTimer.createCalendarTimer(scanSchedule, SCAN_TIMER_CONFIG);

            LOG.info(new StringBuffer().append("Scheduled directory scanning with ").append(scanSchedule));

            LOG.info("Initialized Directory Listener.");
        }
        catch(Exception e)
        {
            StringBuffer msg = new StringBuffer().append("Error in initialization of ImportService, ").append(
                    "caught exception ").append(e.getClass().getName()).append(" Message: [").append(
                            e.getMessage()).append("] Cause: ").append(e.getCause());
            LOG.fatal(msg);
            AsduAdmLogger.FATAL(msg);

            final RuntimeException rex = new RuntimeException(
                    "Exception on " + ImportService.class.getSimpleName() + ".init()");
            rex.initCause(e);
            throw rex;
        }

        LOG.info("init done");
    }

    @Timeout
    @TransactionAttribute(TransactionAttributeType.NEVER)
    void timeout(Timer timer)
    {
        final Object timerInfo = timer.getInfo();

        LOG.debug("timeout for timer " + timerInfo + " ...");

        try
        {

            if(INIT_TIMER_INFO.equals(timerInfo))
            {
                tryInitialization();
            }
            else if(SCAN_TIMER_INFO.equals(timerInfo))
            {

                if(!ServerInformation.isMasterNode())
                {
                    LOG.debug("I am not the master node. Bailing out of timeout.");
                    return;
                }

                scan();
            }
        }
        finally
        {
            LOG.debug("timeout for timer " + timerInfo + " done");
        }
    }

    public void tryInitialization()
    {
        ++initializationTrial;
        LOG.debug("tryInitialization, trial #" + initializationTrial + " ...");
        try
        {
            if(isServerUp())
            {
                init();
            }
            else
            {
                makeInitTrialShot();
            }
        }
        catch(Exception e)
        {
            LOG.warn("Caught exception " + e + "in timer()");
            if(LOG.isDebugEnabled())
            {
                e.printStackTrace();
            }
        }
        LOG.debug("tryInitialization, trial #" + initializationTrial + " done");
    }

    public void scan()
    {
        LOG.debug("scan ...");

        try
        {
            listener.scan();
        }
        catch(Exception e)
        {
            LOG.error("Caught exception while scanning: " + e.getMessage());
        }
        LOG.debug("scan done");
    }

    @PrePassivate
    void deInit() throws SchedulerException
    {
        LOG.info("ImportService deInit() ...");

        LOG.info("ImportService deInit() done");
    }

    // //////////////////////////////////////////////////////////////////////
    /**
     * @return Is the Server started already?
     */
    private boolean isServerUp()
    {
        return ServerInformation.isServerUp();
    }

    private ImportConfiguration myConfig;
    private ExportDirectoryListener listener;

    private static final Logger LOG = Logger.getLogger(ImportService.class);
}
