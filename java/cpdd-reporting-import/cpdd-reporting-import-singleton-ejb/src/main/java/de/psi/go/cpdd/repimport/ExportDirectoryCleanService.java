package de.psi.go.cpdd.repimport;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.apache.log4j.Logger;
import org.jboss.ejb3.annotation.Clustered;
import org.joda.time.Period;

import de.psi.go.cpdd.repimport.AsduAdmLogger;
import de.psi.go.cpdd.repimport.ExportDirectoryCleaner;
import de.psi.go.cpdd.repimport.IExportDirectoryCleanService;
import de.psi.go.cpdd.repimport.config.ImportConfiguration;

/**
 * In the XML exchange directory, there are 2 subfolders for successfully
 * and unsuccessfully processed files, "ok" and "err" respectively.
 * 
 * This service cleans these directories. For each directories the maximal
 * age of retained files is configured in the application configuration,
 * 
 * @see {@link ImportConfiguration#getRetentionProcessed()} and @see {@link ImportConfiguration#getRetentionError()}.
 * 
 *      The schedule for cleaning the directories is configurable as well,
 * @see {@link ImportConfiguration#getExportDirectoryCleanSchedule()}.
 */

@Singleton
@Clustered
@Startup
public class ExportDirectoryCleanService implements IExportDirectoryCleanService
{
    private static final TimerConfig CLEAN_TIMER_CONFIG = new TimerConfig("CLEAN_TIMER", false);

    @Resource
    private TimerService cleanTimer;

    public ExportDirectoryCleanService()
    {
        LOG.info("ExportDirectoryCleanService ...");

        LOG.info("ExportDirectoryCleanService done");
    }

    @PostConstruct
    public void init()
    {
        LOG.info("init ...");

        try
        {
            myConfig = ImportConfiguration.getInstance();

            final String inputDir = myConfig.getInputDir();
            final ScheduleExpression cleanSchedule = myConfig.getExportDirectoryCleanSchedule();
            final Period okPeriod = myConfig.getRetentionProcessed();
            final Period errPeriod = myConfig.getRetentionError();

            final int nofCancelled = ServiceUtilities.cancelAllExistingTimers(cleanTimer);
            LOG.warn(
                    "Cancelled " + nofCancelled + " timers on startup. Should be 0. Are there persistent timers left?");

            cleaner = new ExportDirectoryCleaner(inputDir, okPeriod, errPeriod);
            cleanTimer.createCalendarTimer(cleanSchedule, CLEAN_TIMER_CONFIG);

            LOG.info(new StringBuffer().append("Scheduled directory cleaning with ").append(cleanSchedule));
            LOG.info(new StringBuffer().append("Retention period for processed files: ").append(okPeriod));
            LOG.info(new StringBuffer().append("Retention period for error raising files: ").append(errPeriod));
        }
        catch(Exception e)
        {
            final StringBuffer msg = new StringBuffer().append(
                    "Initialization of ExportDirectoryCleanService: Caught exception ").append(
                            e.getClass().getName()).append(" Message: [").append(e.getMessage()).append(
                                    "] Cause: [").append(e.getCause()).append("]");
            LOG.fatal(msg);
            AsduAdmLogger.FATAL(msg);

            final RuntimeException rex = new RuntimeException(
                    "Exception on " + ImportService.class.getSimpleName() + ".init()");
            rex.initCause(e);

            throw rex;
        }

        LOG.info("init done");
    }

    @Timeout
    public void clean()
    {
        LOG.debug("clean ...");
        long nofDeleted = 0;

        try
        {
            nofDeleted = cleaner.clean();
        }
        catch(Exception e)
        {
            final StringBuffer msg = new StringBuffer().append("Error while cleaning export directory ").append(
                    cleaner.getRootDirPath()).append(", reason: ").append(e.getMessage());
            LOG.error(msg);
            AsduAdmLogger.ERROR(msg);
        }

        if(nofDeleted > 0)
        {
            final StringBuffer msg = new StringBuffer().append(cleaner.getRootDirPath()).append(": deleted ").append(
                    nofDeleted).append(" directories / files.");
            LOG.info(msg);
            AsduAdmLogger.INFO(msg);
        }

        LOG.debug("clean done");
    }

    private ImportConfiguration myConfig;
    private ExportDirectoryCleaner cleaner;

    private static final Logger LOG = Logger.getLogger(ExportDirectoryCleanService.class);
}
