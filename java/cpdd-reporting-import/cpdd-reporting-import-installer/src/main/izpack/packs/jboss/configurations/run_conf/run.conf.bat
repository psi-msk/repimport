rem ### -*- batch file -*- ######################################################
rem #                                                                          ##
rem #  JBoss Bootstrap Script Configuration                                    ##
rem #                                                                          ##
rem #############################################################################

rem # $Id: run.conf.bat 88820 2009-05-13 15:25:44Z dimitris@jboss.org $

rem #
rem # This batch file is executed by run.bat to initialize the environment 
rem # variables that run.bat uses. It is recommended to use this file to
rem # configure these variables, rather than modifying run.bat itself. 
rem #

rem #
rem # Specify the JBoss Profiler configuration file to load.
rem #
rem # Default is to not load a JBoss Profiler configuration file.
rem #
rem set "PROFILER=%JBOSS_HOME%\bin\jboss-profiler.properties"

rem #
rem # Specify the location of the Java home directory (it is recommended that
rem # this always be set). If set, then "%JAVA_HOME%\bin\java" will be used as
rem # the Java VM executable; otherwise, "%JAVA%" will be used (see below).
rem #
rem set "JAVA_HOME=C:\opt\jdk1.6.0_13"

rem #
rem # Specify the exact Java VM executable to use - only used if JAVA_HOME is
rem # not set. Default is "java".
rem #
rem set "JAVA=C:\opt\jdk1.6.0_13\bin\java"

rem #
rem # Specify options to pass to the Java VM. Note, there are some additional
rem # options that are always passed by run.bat.
rem #

rem # JVM memory allocation pool parameters - modify as appropriate. The default values assume 16GB of RAM with only the PSI software running.
set "JAVA_OPTS=%JAVA_OPTS% -Xms${heapMemory}M -Xmx${heapMemory}M -XX:NewRatio=3 -XX:PermSize=${permMemory}M -XX:MaxPermSize=${permMemory}M"

rem # Change the RMI GCs to once every thirty minutes.
set "JAVA_OPTS=%JAVA_OPTS% -Dsun.rmi.dgc.client.gcInterval=1800000 -Dsun.rmi.dgc.server.gcInterval=1800000"

rem # Change to a better performing Garbage Collector for multi-CPU systems.
set "JAVA_OPTS=%JAVA_OPTS% -XX:+UseConcMarkSweepGC"

rem # Warn when resolving remote XML DTDs or schemas.
set "JAVA_OPTS=%JAVA_OPTS% -Dorg.jboss.resolver.warning=true"

rem # Use the modified logging.properties.
set "JAVA_OPTS=%JAVA_OPTS% -Dlogging.configuration=file:${CONFDIR}/logging.properties"

rem # Configure client logging.
set "JAVA_OPTS=%JAVA_OPTS% -Dlog4j.configuration=file:${CONFDIR}/log4j.properties"

rem Added for PSIportal installer
IF "$portSetOptions" == "ports-default" (
	set "JBOSS_PORT_OPTIONS=-Djboss.service.binding.set=ports-default -Dpsi.naming.port=1099 -Dpsi.jmxremote.port=1090"
	GOTO CONTINUE )
IF "$portSetOptions" == "ports-01" (
	set "JBOSS_PORT_OPTIONS=-Djboss.service.binding.set=ports-01 -Dpsi.naming.port=1199 -Dpsi.jmxremote.port=1190"
	GOTO CONTINUE )
IF "$portSetOptions" == "ports-02" (
	set "JBOSS_PORT_OPTIONS=-Djboss.service.binding.set=ports-02 -Dpsi.naming.port=1299 -Dpsi.jmxremote.port=1290"
	GOTO CONTINUE )
IF "$portSetOptions" == "ports-03" (
	set "JBOSS_PORT_OPTIONS=-Djboss.service.binding.set=ports-03 -Dpsi.naming.port=1399 -Dpsi.jmxremote.port=1390"
	GOTO CONTINUE )
set "JBOSS_PORT_OPTIONS="

:CONTINUE
set "JAVA_OPTS=%JAVA_OPTS% %JBOSS_PORT_OPTIONS% $additionalOptions %DEBUG_OPTS%"

rem # Sample JPDA settings for remote socket debugging
rem set "JAVA_OPTS=%JAVA_OPTS% -Xrunjdwp:transport=dt_socket,address=8787,server=y,suspend=n"

rem # Sample JPDA settings for shared memory debugging 
rem set "JAVA_OPTS=%JAVA_OPTS% -Xrunjdwp:transport=dt_shmem,address=jboss,server=y,suspend=n"

