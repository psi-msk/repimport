(c) 2013-01-23, PSI AG

In order to install the PSIportal server as a Windows service, the
script jboss_service.bat has to be executed with administrator rights
and the parameter install. It also accepts the parameters uninstall,
start, stop, restart and signal.

Any existing service installation set up by this script will be
overwritten.
