cd $PORTALBINDIR
call clean_jboss.bat $JBOSSSERVER
set "RUN_CONF=$CONFDIR\run.conf.bat"
cd $SERVERBINDIR
call run.bat -c $JBOSSSERVER -b $bindAddress -u $multiCastAddress -g $clusterName -Djboss.messaging.ServerPeerId=$peerId -Djava.net.preferIPv4Stack=true