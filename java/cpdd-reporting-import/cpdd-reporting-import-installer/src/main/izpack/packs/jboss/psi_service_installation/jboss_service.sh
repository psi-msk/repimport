#! /bin/bash

# ########################################################################
#
# This script installs PSIportal as a Linux service or uninstalls the
# service. It takes exactly one argument: -install or -uninstall.
#
# It must be run with root privileges.
#
# # (c) 2012-09-11, PSI AG, Kay Ulbrich
#
# ########################################################################

set -u
set -e

function ERRMSG()
{
    echo
    echo "ERROR: $1" 1>&2
    echo
    exit 1
}

INST="-install"
UINST="-uninstall"

[ $EUID -eq 0 ] || ERRMSG "This script must be run with root privileges. You have no root privileges."

[ $# -eq 1 ] || ERRMSG "The script takes exactly 1 argument: $INST or $UINST"
ACTION=$1
[ $ACTION = "$INST" ] || [ $ACTION = "$UINST" ] || ERRMSG "$ACTION not valid, argument must be $INST or $UINST"

INITD="/etc/init.d"
SERVICE_SCRIPT="psiportal-as"
SCRIPT_SOURCE="`dirname $0`/linux_service_source/${SERVICE_SCRIPT}"
SCRIPT_TARGET="${INITD}/${SERVICE_SCRIPT}"
PERMS="755"

RUNLEVELS="3 5"
RELATIVE_SCRIPT="../$SERVICE_SCRIPT"

[ -w $INITD ] || ERRMSG "Cannot write to $INITD. root must have write access to this directory."

# Full path to rcN.d directory for given runlevel
function rc()
{
    local RL=$1
    echo "${INITD}/rc${RL}.d"
}

# Full path to service start link for given runlevel
function start_link()
{
    local RC=`rc $1`
    echo "${RC}/S99psiportal-as"
}

# Full path to service stop link for given runlevel
function stop_link()
{
    local RC=`rc $1`
    echo "${RC}/K01psiportal-as"
}

# Test for preconditions of making start and stop link for given runlevel
function test_for_links()
{
    local RL=$1
    local RC=`rc $RL`
    [ -d $RC ] || ERRMSG "There is no directory $RC . There is something wrong, because on Linux systems it should exist in order to install to runlevel $RL."
    [ -w $RC ] || ERRMSG "Cannot write to $RC. root must have write access to this directory."
}

# Is everything ok for all intended runlevels?
for rl in $RUNLEVELS; do
    test_for_links $rl
done

# Make start and stop link for given runlevel
function make_links()
{
    local RL=$1
    local START_LINK=`start_link $RL`
    local STOP_LINK=`stop_link $RL`
    ln -s -f $RELATIVE_SCRIPT $START_LINK || "Cannot create symbolic link $START_LINK -> $RELATIVE_SCRIPT (start entry)"
    echo "$START_LINK -> $RELATIVE_SCRIPT"
    ln -s -f $RELATIVE_SCRIPT $STOP_LINK || "Cannot create symbolic link $STOP_LINK -> $RELATIVE_SCRIPT (stop link)"
    echo "$STOP_LINK -> $RELATIVE_SCRIPT"
}

# Delete start and stop link for given runlevel
function rm_links()
{
    local RL=$1
    local START_LINK=`start_link $RL`
    local STOP_LINK=`stop_link $RL`
    \rm $START_LINK    || ERRMSG "Cannot remove start link $START_LINK"
    \rm $STOP_LINK     || ERRMSG "Cannot remove stop link $STOP_LINK"
}

if [ $ACTION = $INST ]; then
    echo "Installing PSIportal service ... "
    \cp $SCRIPT_SOURCE $SCRIPT_TARGET || ERRMSG "Cannot copy $SCRIPT_SOURCE to $SCRIPT_TARGET."
    chmod $PERMS $SCRIPT_TARGET || ERRMSG "Cannot set permissions of $SCRIPT_TARGET to $PERMS"
    for rl in $RUNLEVELS; do
        make_links $rl
    done
    echo "done"
else
    echo -n "Uninstalling PSIportal service ... "
    \rm $SCRIPT_TARGET || ERRMSG "Cannot remove service script $SCRIPT_TARGET"
    for rl in $RUNLEVELS; do
        rm_links $rl
    done
    echo "done"
fi
