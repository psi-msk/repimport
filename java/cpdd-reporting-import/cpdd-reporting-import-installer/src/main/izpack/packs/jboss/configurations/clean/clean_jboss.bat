@ECHO OFF

set SERVER=%1
set SDIR=${INSTALL_PATH}\${JBOSS}\server\%SERVER%

set DATA=%SDIR%\data
set TMPDIR=%SDIR%\tmp
set WORK=%SDIR%\work

ECHO removing %DATA%
rmdir /S/Q %DATA%

ECHO removing %TMPDIR%
rmdir /S/Q %TMPDIR%

ECHO removing %WORK%
rmdir /S/Q %WORK%

ECHO creating empty %DATA%
mkdir %DATA%
