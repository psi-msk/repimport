#! /bin/sh

$PORTALBINDIR/clean_jboss.sh $JBOSSSERVER
DEBUG_OPTS="$debugOptions" RUN_CONF="$CONFDIR/run.conf" $SERVERBINDIR/run.sh -c $JBOSSSERVER -b $bindAddress -u $multiCastAddress -g $clusterName -Djboss.messaging.ServerPeerId=$peerId -Djava.net.preferIPv4Stack=true
 