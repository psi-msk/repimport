SERVER=$1
SDIR="${INSTALL_PATH}/${JBOSS}/server/${SERVER}"

DATA="${SDIR}/data"
TMPDIR="${SDIR}/tmp"
WORK="${SDIR}/work"

echo "removing $DATA"
rm -rf "$DATA"

echo "removing $TMPDIR"
rm -rf "$TMPDIR"

echo "removing $WORK"
rm -rf "$WORK"

echo "creating empty $DATA"
mkdir "$DATA"
