You are about to install PSIportal.

The installer will guide you through the necessary steps and ask you for input.

You will be able to choose whether you install into a JBoss single server/cluster node or a Tomcat webserver.


If you are doing a new installation for an application server, you need to have the parameters of your data 
sources at hand. There is one data source for the main schema and one for the optional reporting schema. 
Each data source configuration needs the following parameters:

- Hostname or IP address
- Port (usually 1521)
- SID
- Login (user/schema and password)


If you aren't installing into a JBoss application server that was provided by PSI, you also have to make sure 
that one of these directories exist, depending on your choice:

...<JBossDir>\server\PSIportalSingleServer (rename from "default")
...<JBossDir>\server\PSIportalCluster (rename from "all")


Please note that the installer will prepare the PSIportal client files after he has copied all necessary files. 
This may take up to two minutes, depending on your language and customGUI choices.


You can find instructions on how to install PSIportal as a service in the service_README.txt in the doc
directory of your chosen installation path.