package de.psi.go.cpdd.repimport;

import junit.framework.TestCase;

import org.joda.time.Period;
import org.junit.Test;

import de.psi.go.cpdd.repimport.config.ImportConfiguration;
import de.psi.go.cpdd.repimport.config.PersistenceStrategy;

public class ImportConfigurationTest extends TestCase
{
    @Test
    public void testParsing() throws Exception
    {
        System.setProperty(
                ImportConfiguration.PROPERTY_CONFIG_DIR,
                "file://" + ImportConfigurationTest.class.getResource("/").getPath());

        final ImportConfiguration cnf = ImportConfiguration.getInstance();

        assertEquals("D:/data/repimport-cpdd", cnf.getInputDir());
        // assertEquals(new ScheduleExpression().second(0).minute("*"),
        // cnf.getExportDirectoryScanSchedule());
        assertEquals(4711, cnf.getMaxBatchSize());

        assertEquals(15, cnf.getNumberFormat().getPrecision());
        assertEquals(2, cnf.getNumberFormat().getScale());

        // assertEquals(new ScheduleExpression().second(0).minute("*"),
        // cnf.getExportDirectoryCleanSchedule());
        assertEquals(new Period().withYears(9).withWeeks(3), cnf.getRetentionProcessed());
        assertEquals(new Period().withMonths(12), cnf.getRetentionError());

        assertEquals(PersistenceStrategy.INSERT, cnf.getPersistenceStragegy());
    }
}
