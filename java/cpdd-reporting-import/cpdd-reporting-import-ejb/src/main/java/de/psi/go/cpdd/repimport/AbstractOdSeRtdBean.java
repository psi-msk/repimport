package de.psi.go.cpdd.repimport;

import de.psi.go.cpdd.repimport.config.ImportConfiguration;

/**
 * Provides common functionality of both implementations for {@link OdSeRtdApi}
 * 
 * @author emsku
 * 
 */
public abstract class AbstractOdSeRtdBean implements OdSeRtdApi
{
    protected static int getMaxSqlBulkSize()
    {
        return ImportConfiguration.getInstance().getMaxSqlBulkSize();
    }
}
