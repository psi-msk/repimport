package de.psi.go.cpdd.repimport;

class SeanceTableCleaner extends BufferTableCleaner
{
    SeanceTableCleaner()
    {
        super(ProcedureCaller.CLEANING_PROCEDURE_SEANCES);
    }
}
