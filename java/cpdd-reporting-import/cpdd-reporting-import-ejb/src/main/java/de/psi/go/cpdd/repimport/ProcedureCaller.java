package de.psi.go.cpdd.repimport;

import java.sql.CallableStatement;
import java.sql.Connection;

import org.apache.log4j.Logger;

class ProcedureCaller
{
    /**
     * The name of the customer side procedures to free the buffer tables.
     */
    static final String CLEANING_PROCEDURE_REALTIME = "LOAD_PSI_DATA.LOAD_ALL_DATA_5MI";
    static final String CLEANING_PROCEDURE_SEANCES = "LOAD_PSI_DATA.LOAD_ALL_DATA_S";

    private static final Logger LOG = Logger.getLogger(ProcedureCaller.class);

    static void executeProcedure(String procedureName, Connection con) throws OdSeRtdException
    {
        LOG.info("Executing procedure " + procedureName);

        final long start = System.currentTimeMillis();

        CallableStatement cs = null;

        try
        {
            cs = con.prepareCall(makeProcedureCall(procedureName));
            cs.execute();
        }
        catch(Exception e)
        {
            ExceptionProcessor.processException(e, LOG, "While calling procedure " + procedureName);
        }
        finally
        {
            try
            {
                if(null != cs)
                {
                    cs.close();
                }
            }
            catch(Exception e)
            {
            }
        }

        final long end = System.currentTimeMillis();

        LOG.info("Executed procedure " + procedureName + " in " + (end - start) + " ms");
    }

    private static String makeProcedureCall(String procedureName)
    {
        return new StringBuilder("{ CALL ").append(procedureName).append("() }").toString();
    }
}
