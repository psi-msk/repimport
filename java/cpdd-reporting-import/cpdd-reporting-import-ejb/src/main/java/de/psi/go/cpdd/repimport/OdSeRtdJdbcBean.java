package de.psi.go.cpdd.repimport;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.EnumMap;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.jboss.ejb3.annotation.LocalBinding;
import org.jboss.ejb3.annotation.RemoteBinding;
import org.jboss.util.StopWatch;

@Stateless
@Local(OdSeRtdApiJdbcLocal.class)
@LocalBinding(jndiBinding = OdSeRtdApiJdbcLocal.JNDI_NAME)
@Remote(OdSeRtdApiJdbcRemote.class)
@RemoteBinding(jndiBinding = OdSeRtdApiJdbcRemote.JNDI_NAME)
@TransactionManagement(TransactionManagementType.BEAN)
public class OdSeRtdJdbcBean extends AbstractOdSeRtdBean
{
    private static final Logger LOG = Logger.getLogger(OdSeRtdJdbcBean.class);

    private static final String TABLE_NAME_MIN5 = "OD_SE_RTD_5MI";
    private static final String TABLE_NAME_SEANCES = "OD_SE_RTD_S";

    private static final TimeZone WRITE_TIME_ZONE = TimeZone.getTimeZone("GMT");

    private enum ValueCategory
    {
        MIN5,
        SEANCES;

        private static ValueCategory forTimelevel(RepImportTimeLevel tl)
        {
            return tl == RepImportTimeLevel.MIN5 ? MIN5 : SEANCES;
        }
    }

    private static class EntityLists
    {
        EntityLists()
        {
            lists.put(ValueCategory.MIN5, new ArrayList<OdSeRtd>());
            lists.put(ValueCategory.SEANCES, new ArrayList<OdSeRtd>());
        }

        void addEntity(ValueCategory category, OdSeRtd entity)
        {
            lists.get(category).add(entity);
        }

        List<OdSeRtd> getEntityList(ValueCategory category)
        {
            return Collections.unmodifiableList(lists.get(category));
        }

        final Map<ValueCategory, List<OdSeRtd>> lists = new EnumMap<ValueCategory, List<OdSeRtd>>(ValueCategory.class);
    }

    @Override
    public void insertBatch(List<OdSeRtd> transientList) throws OdSeRtdException
    {
        LOG.debug("insertBatch ...");

        final StopWatch sw = new StopWatch();
        sw.start();

        final EntityLists splitEnties = splitLists(transientList);

        executeSqlCommand(INSERT_STRING_SEANCES, splitEnties.getEntityList(ValueCategory.SEANCES), SEANCE_CLEANER);
        executeSqlCommand(INSERT_STRING_MIN5, splitEnties.getEntityList(ValueCategory.MIN5), REALTIME_CLEANER);

        LOG.debug(String.format("Inserted [%d] data sets in [%d] ms", transientList.size(), sw.getTime()));

        LOG.debug("insertBatch done");
    }

    @Override
    public void mergeBatch(List<OdSeRtd> transientList) throws OdSeRtdException
    {
        LOG.debug("mergeBatch ...");

        final StopWatch sw = new StopWatch();
        sw.start();

        final EntityLists splitEnties = splitLists(transientList);

        executeSqlCommand(MERGE_STRING_SEANCES, splitEnties.getEntityList(ValueCategory.SEANCES), null);
        executeSqlCommand(MERGE_STRING_MIN5, splitEnties.getEntityList(ValueCategory.MIN5), null);

        LOG.debug(String.format("Merged [%d] data sets in [%d] ms", transientList.size(), sw.getTime()));

        LOG.debug("mergeBatch done");
    }

    private void executeSqlCommand(String cmd, List<OdSeRtd> transientList, BufferTableCleaner cleaner)
            throws OdSeRtdException
    {
        if(transientList.isEmpty())
        {
            return;
        }

        PreparedStatement pstmt = null;
        Connection con = null;
        int nofAllValues = 0;
        int nofInActualBatch = 0;

        try
        {
            final int batchSize = getMaxSqlBulkSize();

            final Context ctx = new InitialContext();
            final DataSource ds = (DataSource)ctx.lookup(PersistenceUnitConst.DATA_SOURCE);
            con = ds.getConnection();
            con.setAutoCommit(false);
            pstmt = con.prepareStatement(cmd);

            final Calendar tzCal = new GregorianCalendar(WRITE_TIME_ZONE);

            for(OdSeRtd entity : transientList)
            {
                fillPreparedStatementWithValues(pstmt, entity, tzCal);
                pstmt.addBatch();
                ++nofAllValues;
                ++nofInActualBatch;
                if(nofAllValues % batchSize == 0)
                {
                    LOG.debug(
                            new StringBuffer().append("intermediate: execute batch and commit at batch size ").append(
                                    nofInActualBatch).append(" ..."));
                    pstmt.executeBatch();
                    if(null != cleaner)
                    {
                        cleaner.clean(con);
                    }
                    con.commit();
                    LOG.debug("done");
                    pstmt.clearBatch();
                    nofInActualBatch = 0;
                }
            }
            LOG.debug(
                    new StringBuffer().append("final: execute batch and commit at batch size ").append(
                            nofInActualBatch).append(" ..."));
            pstmt.executeBatch();
            if(nofInActualBatch > 0 && null != cleaner)
            {
                cleaner.clean(con);
            }
            con.commit();
            pstmt.clearBatch();
        }
        catch(Exception e)
        {
            ExceptionProcessor.processException(e, LOG, "While executing SQL command");
        }
        finally
        {
            try
            {
                // May succeed or not, because the transaction may not be
                // active.
                if(con != null)
                {
                    con.commit();
                }
            }
            catch(Exception e)
            {
                LOG.warn("Error committing: ", e);
            }

            try
            {
                if(con != null)
                {
                    con.close();
                }
            }
            catch(Exception e)
            {
                LOG.warn("Error on closing connection: ", e);
            }
        }
    }

    private void fillPreparedStatementWithValues(PreparedStatement stmt, OdSeRtd entity, Calendar tzCal)
            throws SQLException
    {
        stmt.setString(1, entity.getParameterId());
        stmt.setString(2, entity.getSeanceType().getDbValue());
        stmt.setTimestamp(3, entity.getRptDate(), tzCal);
        stmt.setBigDecimal(4, entity.getRptValue());
        stmt.setObject(5, entity.getFlag());
        stmt.setTimestamp(6, entity.getChangeDate(), tzCal);
    }

    private static EntityLists splitLists(List<OdSeRtd> allEntities)
    {
        final EntityLists result = new EntityLists();
        for(OdSeRtd entity : allEntities)
        {
            final ValueCategory myCategory = ValueCategory.forTimelevel(entity.getSeanceType());
            result.addEntity(myCategory, entity);
        }
        return result;
    }

    private static String makeInsertString(String tableName)
    {
        return "INSERT INTO "
                + tableName
                + " (PARAMETER_ID, SEANCE_TYPE, RPT_DATE, RPT_VALUE, FLAG, CHANGE_DATE) VALUES (?, ?, ?, ?, ?, ?)";
    }

    private static String makeMergeString(String tableName)
    {
        return "MERGE INTO "
                + tableName
                + " o "
                + "USING (SELECT ? PARAMETER_ID, ? SEANCE_TYPE, ? RPT_DATE, ? RPT_VALUE, ? FLAG, ? CHANGE_DATE FROM dual) input "
                + "ON (UPPER(o.PARAMETER_ID) = UPPER(input.PARAMETER_ID) AND o.RPT_DATE = input.RPT_DATE AND o.SEANCE_TYPE = input.SEANCE_TYPE) "
                + "WHEN MATCHED THEN "
                + "UPDATE SET o.RPT_VALUE = input.RPT_VALUE, o.FLAG = input.FLAG, o.CHANGE_DATE = input.CHANGE_DATE "
                + "WHEN NOT MATCHED THEN "
                + "INSERT (PARAMETER_ID, SEANCE_TYPE, RPT_DATE, RPT_VALUE, FLAG, CHANGE_DATE) "
                + "VALUES (UPPER(input.PARAMETER_ID), input.SEANCE_TYPE, input.RPT_DATE, input.RPT_VALUE, input.FLAG, input.CHANGE_DATE)";
    }

    private static final String INSERT_STRING_MIN5 = makeInsertString(TABLE_NAME_MIN5);
    private static final String MERGE_STRING_MIN5 = makeMergeString(TABLE_NAME_MIN5);
    private static final String INSERT_STRING_SEANCES = makeInsertString(TABLE_NAME_SEANCES);
    private static final String MERGE_STRING_SEANCES = makeMergeString(TABLE_NAME_SEANCES);

    private static final BufferTableCleaner SEANCE_CLEANER = new SeanceTableCleaner();
    private static final BufferTableCleaner REALTIME_CLEANER = new RealtimeTableCleaner();
}
