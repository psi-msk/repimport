package de.psi.go.cpdd.repimport;

import java.sql.Connection;

class BufferTableCleaner
{
    //private static final Logger LOG = Logger.getLogger(BufferTableCleaner.class);

    private final String procedureName;

    protected BufferTableCleaner(String procedureName)
    {
        this.procedureName = procedureName;
    }

    public void clean(Connection con) throws OdSeRtdException
    {
        ProcedureCaller.executeProcedure(this.procedureName, con);
    }
}
