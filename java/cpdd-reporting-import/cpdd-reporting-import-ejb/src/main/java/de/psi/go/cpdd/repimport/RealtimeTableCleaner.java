package de.psi.go.cpdd.repimport;

class RealtimeTableCleaner extends BufferTableCleaner
{
    public RealtimeTableCleaner()
    {
        super(ProcedureCaller.CLEANING_PROCEDURE_REALTIME);
    }
}
