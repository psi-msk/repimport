package de.psi.go.cpdd.repimport;

import org.apache.log4j.Logger;

public class ExceptionProcessor
{
    static void processException(Exception e, Logger log, String textForNewOdSeRtdException) throws OdSeRtdException
    {
        final String msg = new StringBuffer(textForNewOdSeRtdException).append(": caught exception ").append(
                e.getClass().getName()).append(": ").append(e.getMessage()).toString();

        log.warn(msg);

        if((e instanceof OdSeRtdException))
        {
            throw(OdSeRtdException)e;
        }

        if(log.isDebugEnabled())
        {
            e.printStackTrace();
        }

        final OdSeRtdException odex = new OdSeRtdException(msg, e);

        log.warn("Exception is of type " + odex.getErrorType());

        throw odex;
    }
}
