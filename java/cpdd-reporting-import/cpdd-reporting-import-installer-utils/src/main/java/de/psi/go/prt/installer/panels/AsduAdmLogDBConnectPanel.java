package de.psi.go.prt.installer.panels;

import com.izforge.izpack.api.data.Panel;
import com.izforge.izpack.core.resource.ResourceManager;
import com.izforge.izpack.installer.data.GUIInstallData;
import com.izforge.izpack.installer.gui.InstallerFrame;

public class AsduAdmLogDBConnectPanel extends DBConnectPanel
{
    // //////////////////////////////////////////////////////////////////////
    public AsduAdmLogDBConnectPanel(Panel panel, InstallerFrame iframe, GUIInstallData idata, ResourceManager rman)
    {
        super(
                "asduAdmLogDsUser",
                "asduAdmLogDsPassword",
                "asduAdmLogDsHostName",
                "asduAdmLogDsPort",
                "asduAdmLogDsSid",
                "asduAdmLogDsConnString",
                "asduAdmLogDsCheckConnection",
                "asduAdmLogDsServiceType",
                panel,
                iframe,
                idata,
                rman);
    }

    private static final long serialVersionUID = -7064815870602506740L;
}
