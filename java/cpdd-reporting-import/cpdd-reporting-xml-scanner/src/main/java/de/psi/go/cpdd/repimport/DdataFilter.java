package de.psi.go.cpdd.repimport;

import java.io.File;
import java.util.regex.Pattern;

import org.apache.commons.io.filefilter.IOFileFilter;

public class DdataFilter implements IOFileFilter
{
    public static final String FNAME_PATTERN = "ddata.*\\.xml";
    private static final Pattern PATTERN = Pattern.compile(FNAME_PATTERN);

    @Override
    public boolean accept(File file)
    {
        return PATTERN.matcher(file.getName()).matches();
    }

    @Override
    public boolean accept(File dir, String name)
    {
        return false;
    }

}
