package de.psi.go.cpdd.repimport;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.xml.bind.Unmarshaller;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;

import de.psi.go.mtr.psiportalxml.v2.PsiPortalXmlValidatingParserFactory;
import de.psi.go.psiportalxml.v2.dd.AddressType;
import de.psi.go.psiportalxml.v2.dd.PsiportalDdXml;
import de.psi.go.psiportalxml.v2.dd.TimelevelEnum;
import de.psi.go.psiportalxml.v2.dd.TimeseriesType;
import de.psi.go.psiportalxml.v2.dd.ValueType;

public class XmlScanner
{
    private static final DateFormat DATE_FORMATTER = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    static
    {
        DATE_FORMATTER.setTimeZone(TimeZone.getTimeZone("Europe/Moscow"));
    }

    public static void main(String[] argv) throws Exception
    {
        final Options opts = new Options();
        final CommandLineParser parser = new GnuParser();

        opts.addOption("d", "directory", true, "the directory to search for ddata files");
        opts.addOption("k", "key", true, "the key of the time series to scan");
        opts.addOption("t", "timelevel", true, "the timelevel to scan, one of " + EnumSet.allOf(TimelevelEnum.class));
        opts.addOption("h", "help", false, "show this help message");

        final CommandLine cmd = parser.parse(opts, argv);
        if(cmd.hasOption("h"))
        {
            new HelpFormatter().printHelp(
                    "cpdd-reporting-xml-scanner",
                    null,
                    opts,
                    "\nSearches recursively data in ddata-XML-files for a certain object and a time series",
                    true);
            System.exit(0);
        }

        final String dir = cmd.getOptionValue("d");
        if(null == dir)
        {
            throw new ParseException("Option \"d\" must be present.");
        }
        final String key = cmd.getOptionValue("k");
        if(null == key)
        {
            throw new ParseException("Option \"k\" must be present.");
        }
        final TimelevelEnum timelevel = TimelevelEnum.valueOf(cmd.getOptionValue("t"));

        final Collection<File> ddata = FileUtils.listFiles(
                new File(dir),
                new DdataFilter(),
                DirectoryFileFilter.DIRECTORY);
        write("Found " + ddata.size() + " files for pattern \"" + DdataFilter.FNAME_PATTERN + "\"");
        final List<File> ddataList = new ArrayList<File>(ddata);
        Collections.sort(ddataList);

        final Unmarshaller um = PsiPortalXmlValidatingParserFactory.getInstance().makeDdUnmarshaller();

        final Map<Date, ValueWithChangeDate> valueMap = new TreeMap<Date, ValueWithChangeDate>();

        for(File file : ddataList)
        {
            // write(file);
            final byte[] bytes = IOUtils.toByteArray(new FileInputStream(file));
            final ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            final PsiportalDdXml xmlData = (PsiportalDdXml)um.unmarshal(bis);
            for(TimeseriesType ts : xmlData.getData().getTimeseries())
            {
                final AddressType addr = ts.getAddr();
                final TimelevelEnum tl = addr.getTimelevel();
                if(addr.getObjectKey().equals(key) && (tl == timelevel))
                {
                    write("Found timeseries key:\"" + key + "\" timelevel:" + tl + " in file " + file.getName());
                    final Date beginTs = new Date(ts.getBegin().toGregorianCalendar().getTimeInMillis());
                    final RepImportTimeLevel rtl = XmlValueBatchWriter.extractTimeLevel(ts);
                    final List<ValueType> vals = ts.getValues().getVal();
                    for(ValueType val : vals)
                    {
                        final Date repTs = rtl.incrementNTimes(
                                val.getI().intValue() + rtl.getBound().getIndexOffset(),
                                beginTs);
                        if(valueMap.containsKey(repTs))
                        {
                            throw new Exception("Double date " + DATE_FORMATTER.format(repTs));
                        }
                        valueMap.put(
                                repTs,
                                new ValueWithChangeDate(
                                        val.getValue(),
                                        new Date(val.getMd().toGregorianCalendar().getTimeInMillis())));
                    }
                }
            }
        }

        write("Found " + valueMap.size() + " values.");

        for(Map.Entry<Date, ValueWithChangeDate> e : valueMap.entrySet())
        {
            write(DATE_FORMATTER.format(e.getKey()) + " -> " + e.getValue());
        }
    }

    private static class ValueWithChangeDate
    {
        ValueWithChangeDate(BigDecimal value, Date changeDate)
        {
            this.value = value;
            this.changeDate = changeDate;
        }

        final BigDecimal value;
        final Date changeDate;

        @Override
        public String toString()
        {
            return new StringBuilder(value.toString()).append(" | ").append(
                    DATE_FORMATTER.format(changeDate)).toString();
        }
    }

    private static void write(Object obj)
    {
        System.out.println(obj.toString());
    }
}
