package de.psi.go.psiportalxml.v2;

import java.math.BigDecimal;

/**
 * Converter used in the JAXB bindings to represent xsd:double as a {@link BigDecimal}. If using plain {@link Double},
 * one gets floating point errors. On the other hand, if using xsd:decimal, which by default is represented as
 * {@link BigDecimal}, scientific floating point notation is not allowed in the XML file, which can be awkward.
 * 
 * @author emsku
 * 
 */
public class Double2BigDecimalConverter
{
    /**
     * 
     * @param xmlValue
     *            the XML text representation of a double value
     * @return a {@link BigDecimal} instance constructed from the XML text
     */
    public static BigDecimal fromXml(final String xmlValue)
    {
        return new BigDecimal(xmlValue);
    }

    /**
     * 
     * @param javaValue
     *            the Java value
     * @return the XML text representation for the Java value
     */
    public static String toXml(final BigDecimal javaValue)
    {
        return javaValue.toString();
    }
}
