/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools;

import java.sql.Timestamp;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public class Iso8601DateFormatWithMillis
{
    public static String toString(Date date)
    {
        return toString(date.getTime());
    }

    public static String toString(Timestamp date)
    {
        return toString(date.getTime());
    }

    public static String toString(DateTime dt)
    {
        return FORMATTER.withZone(DateTimeZone.UTC).print(dt);
    }

    public static String toString(long millis)
    {
        return FORMATTER.withZone(DateTimeZone.UTC).print(millis);
    }

    public static Date toDate(String iso8601)
    {
        return new Date(toMillis(iso8601));
    }

    public static Timestamp toTimestamp(String iso8601)
    {
        return new Timestamp(toMillis(iso8601));
    }

    public static long toMillis(String iso8601)
    {
        return FORMATTER.parseMillis(iso8601);
    }

    private static final DateTimeFormatter FORMATTER = ISODateTimeFormat.dateTime();
}
