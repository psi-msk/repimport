/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.server;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Hashtable;
import java.util.List;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.log4j.Logger;

import de.psi.go.mtr.tools.PathConcatenator;

public class ServerInformation
{
    private static final Logger LOG = Logger.getLogger(ServerInformation.class);
    private static final String JMXACCOUNT = "jmxAccount";

    public static JMXConnector getConnectedJMXConnector() throws IOException
    {
        final String jmxPw = readJmxPassword();

        LOG.debug("jmxPw " + jmxPw + " length " + jmxPw.length());
        final String port = System.getProperty("psi.jmxremote.port");
        String hostName = "localhost";
        if(System.getProperty("java.rmi.server.hostname") != null)
        {
            hostName = System.getProperty("java.rmi.server.hostname");
        }
        JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + hostName + ":" + port + "/jmxrmi");
        LOG.debug("url " + url.toString());
        Registry reg = null;
        try
        {
            reg = LocateRegistry.getRegistry(Integer.parseInt(port));
            if(reg == null)
            {
                LocateRegistry.createRegistry(Integer.parseInt(port));
            }
        }
        catch(Exception e)
        {
            LOG.debug("port " + port + " already registered");
        }
        final Hashtable<String, String[]> env = new Hashtable<String, String[]>();
        final String[] credentials = new String[] { JMXACCOUNT, jmxPw };
        env.put(JMXConnector.CREDENTIALS, credentials);
        final JMXConnector jmxConn = JMXConnectorFactory.newJMXConnector(url, env);
        jmxConn.connect();
        return jmxConn;

    }

    /**
     * Is the actual node the master node?
     * 
     * @return Whether the server is the first in the list of all nodes, i.e. the master node. If the server is not part
     *         of a cluster, always returns true.
     */
    public static boolean isMasterNode()
    {
        JMXConnector jmxConn = null;

        try
        {
            jmxConn = ServerInformation.getConnectedJMXConnector();
            final MBeanServerConnection mBeanConnection = jmxConn.getMBeanServerConnection();

            if(null == mBeanConnection)
            {
                throw new javax.jms.IllegalStateException(
                        "Could not get "
                                + MBeanServerConnection.class.getSimpleName()
                                + " in ServerManagement.jmxServerConnection()");
            }

            String partitionName = null;
            try
            {
                // First, find the partition name
                final ObjectName hajndiMBeanObjectName = new ObjectName("jboss:service=HAJNDI");
                partitionName = (String)mBeanConnection.getAttribute(hajndiMBeanObjectName, "PartitionName");
            }
            catch(final InstanceNotFoundException inofex)
            {
                // If the service HAJNDI is not running, we have no cluster, and thus are the master node.
                LOG.debug("Service HAJNDI not found, no cluster: " + inofex.getMessage());
                return true;
            }
            // Now, find the node informations
            final String haPartitionMBeanName = new StringBuffer("jboss:partition=").append(partitionName).append(
                    ",service=HAPartition").toString();
            final ObjectName haPartitionMBeanObjectName = new ObjectName(haPartitionMBeanName);

            final String myNodeName = (String)mBeanConnection.getAttribute(haPartitionMBeanObjectName, "NodeName");
            LOG.debug("My node name = " + myNodeName);

            @SuppressWarnings("unchecked")
            final List<String> nodeNameList = (List<String>)mBeanConnection.getAttribute(
                    haPartitionMBeanObjectName,
                    "CurrentView");
            if(LOG.isDebugEnabled())
            {
                int i = 1;
                for(String nodeName : nodeNameList)
                {
                    LOG.debug("Node " + i + " =  " + nodeName);
                    ++i;
                }
            }

            if(nodeNameList.isEmpty())
            {
                // No cluster?
                return true;
            }

            return nodeNameList.get(0).equals(myNodeName);

        }
        catch(Exception e)
        {
            final IllegalStateException illex = new IllegalStateException("Caught exception in timeout");
            illex.initCause(e);
            throw illex;
        }
        finally
        {
            nonThrowingFinalize(jmxConn);
        }
    }

    public static boolean isServerUp()
    {
        JMXConnector jmxConn = null;
        try
        {
            jmxConn = getConnectedJMXConnector();
            final MBeanServerConnection mbeanSv = jmxConn.getMBeanServerConnection();
            return Boolean.parseBoolean(
                    (mbeanSv.getAttribute(new ObjectName("jboss.system:type=Server"), "Started")).toString());
        }
        catch(Exception e)
        {
            LOG.error(e.getMessage());
            return false;
        }
        finally
        {
            nonThrowingFinalize(jmxConn);
        }
    }

    public static String readJmxPassword()
    {
        final String jbossServerHomeDir = System.getProperty("jboss.server.home.dir");
        if(null == jbossServerHomeDir)
        {
            throw new IllegalStateException("Error reading jboss.server.home.dir");
        }
        try
        {
            final String path = PathConcatenator.makePath(
                    jbossServerHomeDir,
                    "conf",
                    "props",
                    "jmx-console-users.properties");
            return readLineByLine(path);
        }
        catch(Exception e)
        {
            final IllegalStateException illex = new IllegalStateException("Error reading JMX password");
            illex.initCause(e);
            throw illex;
        }
    }

    public static void nonThrowingFinalize(final JMXConnector jmxConn)
    {
        if(null != jmxConn)
        {
            try
            {
                jmxConn.close();
            }
            catch(final Exception exc)
            {
                LOG.warn(
                        "Could not close "
                                + JMXConnector.class.getSimpleName()
                                + " in nonThrowingFinalize(), reason: "
                                + exc.getMessage());
            }
        }
    }

    private static String readLineByLine(String path)
    {
        String ret = null;
        FileInputStream fstream = null;
        DataInputStream in = null;
        try
        {
            // Open the file
            fstream = new FileInputStream(path);
            // Get the object of DataInputStream
            in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            // Read file line by line
            while((strLine = br.readLine()) != null)
            {
                // If line contains jmxaccount remove them to get the pw
                if(strLine.contains(JMXACCOUNT))
                {
                    ret = strLine.replace(JMXACCOUNT + "=", "");
                }
                LOG.debug(strLine);
            }
        }
        catch(Exception e)
        {
            LOG.error("Error: " + e.getMessage());
        }
        finally
        {

            try
            {
                in.close();
            }
            catch(final Exception e)
            {
            }

            try
            {
                fstream.close();
            }
            catch(final Exception e)
            {
            }

        }

        return ret;
    }
}
