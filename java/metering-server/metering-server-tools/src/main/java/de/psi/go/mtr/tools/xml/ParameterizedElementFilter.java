/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools.xml;

import java.util.Enumeration;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.UnmarshallerHandler;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.NamespaceSupport;
import org.xml.sax.helpers.XMLFilterImpl;

/**
 * Filter for parsing an XML file SAX style. The type of the corresponding JAXB
 * class is the generics parameter. The namespace and name of the element have
 * to be passed in the constructor.
 * 
 * An @see IXmlElementProcessor is used in order to act ob the extracted
 * elements.
 * 
 * @author kulbrich
 * 
 * @param <XML_ELEMENT_TYPE>
 */
public class ParameterizedElementFilter<XML_ELEMENT_TYPE> extends XMLFilterImpl
{
    /**
     * 
     * @param context
     *            a valid JAXB context
     * @param namespaceName
     *            the name of the XML namespace, the element is in
     * @param elementName
     *            the name of the element in the XML file
     * @param xmlProcessor
     *            an implementation of @see IXmlElementProcessor
     */
    public ParameterizedElementFilter(
            JAXBContext context,
            ValidatingParserFactoryBase umFactory,
            Class<?> xmlRootClass,
            String namespaceName,
            String elementName,
            IXmlElementProcessor<XML_ELEMENT_TYPE> xmlProcessor)
    {
        this.context = context;
        this.umFactory = umFactory;
        this.xmlRootClass = xmlRootClass;
        this.namespaceName = namespaceName;
        this.elementName = elementName;
        this.xmlProcessor = xmlProcessor;
    }

    /**
     * We will create unmarshallers from this context.
     */
    private final JAXBContext context;

    public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException
    {

        if(depth != 0)
        {
            // we are in the middle of forwarding events.
            // continue to do so.
            depth++;
            super.startElement(namespaceURI, localName, qName, atts);
            return;
        }

        if(namespaceURI.equals(this.namespaceName) && localName.equals(this.elementName))
        {
            // start a new unmarshaller

            Unmarshaller unmarshaller;
            try
            {
                unmarshaller = umFactory.makeUnmarshaller(xmlRootClass, this.context);
            }
            catch(Exception e)
            {
                // there's no way to recover from this error.
                // we will abort the processing.
                throw new SAXException(e);
            }
            unmarshallerHandler = unmarshaller.getUnmarshallerHandler();

            // set it as the content handler so that it will receive
            // SAX events from now on.
            setContentHandler(unmarshallerHandler);

            // fire SAX events to emulate the start of a new document.
            unmarshallerHandler.startDocument();
            unmarshallerHandler.setDocumentLocator(locator);

            Enumeration<?> e = namespaces.getPrefixes();
            while(e.hasMoreElements())
            {
                String prefix = (String)e.nextElement();
                String uri = namespaces.getURI(prefix);

                unmarshallerHandler.startPrefixMapping(prefix, uri);
            }
            String defaultURI = namespaces.getURI("");
            if(defaultURI != null)
                unmarshallerHandler.startPrefixMapping("", defaultURI);

            super.startElement(namespaceURI, localName, qName, atts);

            // count the depth of elements and we will know when to stop.
            depth = 1;
        }
    }

    @SuppressWarnings("unchecked")
    public void endElement(String namespaceURI, String localName, String qName) throws SAXException
    {
        // forward this event
        super.endElement(namespaceURI, localName, qName);

        if(depth != 0)
        {
            depth--;
            if(depth == 0)
            {
                // just finished sending one chunk.

                // emulate the end of a document.
                Enumeration<?> prefixes = namespaces.getPrefixes();
                while(prefixes.hasMoreElements())
                {
                    String prefix = (String)prefixes.nextElement();
                    unmarshallerHandler.endPrefixMapping(prefix);
                }
                String defaultURI = namespaces.getURI("");
                if(defaultURI != null)
                    unmarshallerHandler.endPrefixMapping("");
                unmarshallerHandler.endDocument();

                // stop forwarding events by setting a dummy handler.
                // XMLFilter doesn't accept null, so we have to give it
                // something,
                // hence a DefaultHandler, which does nothing.
                setContentHandler(new DefaultHandler());

                // then retrieve the fully unmarshalled object
                try
                {
                    XML_ELEMENT_TYPE result;
                    final Object genericResult = unmarshallerHandler.getResult();

                    // This ugly if - statement is necessary because of a not
                    // understood behaviour of the parsing framework:
                    // sometimes, it returns a JAXBElement<XML_ELEMENT_TYPE>,
                    // sometimes an XML_ELEMENT_TYPE - Object.
                    if(genericResult instanceof JAXBElement)
                    {
                        final JAXBElement<XML_ELEMENT_TYPE> jaxEl = (JAXBElement<XML_ELEMENT_TYPE>)unmarshallerHandler.getResult();
                        result = jaxEl.getValue();
                    }
                    else
                    {
                        result = (XML_ELEMENT_TYPE)genericResult;
                    }

                    LOG.debug("result: " + result);
                    xmlProcessor.process(result);
                }
                catch(final Exception e)
                {
                    final String err = String.format(
                            "Caught exception %s: Message \"%s\" Cause \"%s\"",
                            e.getClass().getName(),
                            e.getMessage(),
                            e.getCause());
                    LOG.warn(err);
                    SAXException saxex = new SAXException(err);
                    if(LOG.isDebugEnabled())
                    {
                        e.printStackTrace();
                    }
                    throw saxex;
                }
            }
        }
    }

    /**
     * Remembers the depth of the elements as we forward SAX events to a JAXB
     * unmarshaller.
     */
    private int depth;

    /**
     * Reference to the unmarshaller which is unmarshalling an object.
     */
    private UnmarshallerHandler unmarshallerHandler;

    /**
     * Keeps a reference to the locator object so that we can later pass it to a
     * JAXB unmarshaller.
     */
    private Locator locator;

    public void setDocumentLocator(Locator locator)
    {
        super.setDocumentLocator(locator);
        this.locator = locator;
    }

    /**
     * Used to keep track of in-scope namespace bindings.
     * 
     * For JAXB unmarshaller to correctly unmarshal documents, it needs to know
     * all the effective namespace declarations.
     */
    private NamespaceSupport namespaces = new NamespaceSupport();

    public void startPrefixMapping(String prefix, String uri) throws SAXException
    {
        namespaces.pushContext();
        namespaces.declarePrefix(prefix, uri);

        super.startPrefixMapping(prefix, uri);
    }

    public void endPrefixMapping(String prefix) throws SAXException
    {
        namespaces.popContext();

        super.endPrefixMapping(prefix);
    }

    private final ValidatingParserFactoryBase umFactory;
    private final Class<?> xmlRootClass;
    private final String namespaceName;
    private final String elementName;
    private final IXmlElementProcessor<XML_ELEMENT_TYPE> xmlProcessor;

    private static final Logger LOG = Logger.getLogger(ParameterizedElementFilter.class);
}
