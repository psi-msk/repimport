/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools.hibernate.usertypes;

import java.util.Calendar;
import java.util.TimeZone;

class Constants
{
    /**
     * Global timezone for all DATE write and read operations.
     */
    public static final String UTC_TIMEZONE_ID = "GMT";

    public static final TimeZone UTC_TIMEZONE = TimeZone.getTimeZone(UTC_TIMEZONE_ID);

    public static final Calendar makeUtcCalendar()
    {
        return Calendar.getInstance(Constants.UTC_TIMEZONE);
    }
}
