/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Boilerplate for checking, that variables are non-null.
 * 
 * @author emsku
 * 
 */
public class NullChecker
{
    /**
     * Checks all fields of an object for non-nullity using reflection. It is intended to be
     * used in instance methods (i.e. constructors), so that it is passed the "this" reference,
     * which implicitely is not null.
     * 
     * @param obj
     *            the object, which fields are to be checked. Must not be null (is not checked).
     * @throws IllegalArgumentException
     *             if there are null fields
     */
    public static void check(Object obj) throws IllegalArgumentException
    {
        if(null == obj)
        {
            throw new IllegalArgumentException("NullChecker: passed null object");
        }

        List<String> nullVals = null;

        try
        {
            for(final Field fld : obj.getClass().getDeclaredFields())
            {
                fld.setAccessible(true);

                // skip static fields
                if(java.lang.reflect.Modifier.isStatic(fld.getModifiers()))
                {
                    continue;
                }

                final Object value = fld.get(obj);
                if(null == value)
                {
                    if(null == nullVals)
                    {
                        nullVals = new ArrayList<String>();
                    }
                    nullVals.add(fld.getName());
                }
            }
        }
        catch(IllegalAccessException illex)
        {
            final IllegalArgumentException illarg = new IllegalArgumentException(
                    new StringBuilder("NullChecker.check(): class ").append(obj.getClass()).append(
                            ": cannot access a field.").toString());
            illarg.initCause(illex);
            throw illarg;
        }

        if(null != nullVals)
        {
            StringBuilder sb = new StringBuilder("NullChecker.check(): the following fields in object of type ").append(
                    obj.getClass()).append(" are null: ");
            for(String name : nullVals)
            {
                sb.append(name).append(" ");
            }
            throw new IllegalArgumentException(sb.toString());
        }
    }
}
