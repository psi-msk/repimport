/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools.hibernate;

/**
 * 
 * Session factory for memory based H2SQL DB connections. Applicable in JUnit
 * tests for Entity Beans.
 * 
 * @author emsku
 * 
 */
public class H2SqlSessionFactoryAutoDdl extends AbstractBaseH2SqlSessionFactory
{
    public H2SqlSessionFactoryAutoDdl(Class<?>... annotatedClasses)
    {
        cfg.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        cfg.setProperty("hibernate.connection.driver_class", "org.h2.Driver");

        cfg.setProperty("hibernate.show_sql", "false");
        cfg.setProperty("hibernate.connection.url", "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
        cfg.setProperty("hibernate.hbm2ddl.auto", "create");

        // Make the parameterized classes known to the cfg.
        for(Class<?> clazz : annotatedClasses)
        {
            cfg.addAnnotatedClass(clazz);
        }

        this.sf = cfg.buildSessionFactory();
    }
}
