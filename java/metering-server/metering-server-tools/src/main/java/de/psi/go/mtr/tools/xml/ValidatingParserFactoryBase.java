/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools.xml;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

/*
 * Encapsulates creation of marshallers and unmarshallers, which validate against XSD schemas in the class path.
 */
public class ValidatingParserFactoryBase
{
    private static final Logger LOG = Logger.getLogger(ValidatingParserFactoryBase.class);

    /**
     * @param classOfDocumentRoot
     * @return
     * @throws JAXBException
     * @throws XmlToolsException
     * @throws SAXException
     */
    public Unmarshaller makeUnmarshaller(Class<?> classOfDocumentRoot)
            throws JAXBException,
            XmlToolsException,
            SAXException
    {
        JAXBContext context = JAXBContext.newInstance(classOfDocumentRoot.getPackage().getName());
        return makeUnmarshaller(classOfDocumentRoot, context);
    }

    /**
     * @param classOfDocumentRoot
     * @param context
     * @return Validating marshaller for given XSD schema name and context.
     * @throws XmlToolsException
     * @throws SAXException
     * @throws JAXBException
     */
    public Unmarshaller makeUnmarshaller(Class<?> classOfDocumentRoot, final JAXBContext context)
            throws XmlToolsException,
            SAXException,
            JAXBException
    {
        final Unmarshaller um = context.createUnmarshaller();
        final Schema schema = makeSchema(classOfDocumentRoot);
        um.setSchema(schema);
        return um;
    }

    /**
     * @param classOfDocumentRoot
     * @param context
     * @return
     * @throws JAXBException
     * @throws XmlToolsException
     * @throws SAXException
     */
    public Marshaller makeMarshaller(Class<?> classOfDocumentRoot, final JAXBContext context)
            throws JAXBException,
            XmlToolsException,
            SAXException
    {
        final Marshaller m = context.createMarshaller();
        final Schema schema = makeSchema(classOfDocumentRoot);
        m.setSchema(schema);
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        return m;
    }

    /**
     * @param classOfDocumentRoot
     * @return
     * @throws JAXBException
     * @throws XmlToolsException
     * @throws SAXException
     */
    public Marshaller makeMarshaller(Class<?> classOfDocumentRoot) throws JAXBException, XmlToolsException, SAXException
    {
        JAXBContext context = JAXBContext.newInstance(classOfDocumentRoot.getPackage().getName());
        return makeMarshaller(classOfDocumentRoot, context);
    }

    public SAXParser makeSAXParser(Class<?> classOfDocumentRoot)
            throws ParserConfigurationException,
            SAXException,
            XmlToolsException
    {
        final SAXParserFactory fc = SAXParserFactory.newInstance();

        fc.setSchema(makeSchema(classOfDocumentRoot));
        fc.setNamespaceAware(true);

        // It seems paradoxical, that a parser factory, which is
        // to produce validating parsers, gets validating set to
        // false. But according to what I found, this is the way
        // it has to be done in case of an external XSD. This information
        // was confirmed by the fact, that I got a parser error ("no
        // grammar found"), when the value was set to true.
        fc.setValidating(false);

        final SAXParser parser = fc.newSAXParser();

        return parser;
    }

    /**
     * @param classOfDocumentRoot
     * @param xsdFileName
     */
    public void registerXsdName(Class<?> classOfDocumentRoot, String xsdFileName)
    {
        LOG.info(
                new StringBuilder("Registering XSD schema \"").append(xsdFileName).append("\" for class ").append(
                        classOfDocumentRoot));

        schemaMap.put(classOfDocumentRoot, xsdFileName);
    }

    private URL getXsdPath(Class<?> classOfDocumentRoot) throws XmlToolsException
    {
        final String xsdName = schemaMap.get(classOfDocumentRoot);

        if(null == xsdName)
        {
            final String err = "No schema configured for class " + classOfDocumentRoot.getName() + ".";
            throw new XmlToolsException(err);
        }

        final URL urlOfXsdSchema = classOfDocumentRoot.getResource("/" + xsdName);
        if(null == urlOfXsdSchema)
        {
            final String err = "Could not find schema document " + xsdName + " in class path.";
            throw new XmlToolsException(err);
        }

        return urlOfXsdSchema;
    }

    private Schema makeSchema(Class<?> classOfDocumentRoot) throws XmlToolsException, SAXException
    {
        final URL urlOfXsdSchema = getXsdPath(classOfDocumentRoot);

        final SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        return sf.newSchema(urlOfXsdSchema);
    }

    private final Map<Class<?>, String> schemaMap = new HashMap<Class<?>, String>();
}
