/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools.hibernate.usertypes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.type.StandardBasicTypes;

/**
 * Allows to persist Timestamp objects as UTC ms (INTEGER) in DB.
 * 
 * @author emsku
 * 
 */

public class UtcMillis extends AbstractImmutableUserType
{
    public static final String TYPE = "de.psi.go.mtr.tools.hibernate.usertypes.UtcMillis";

    @Override
    public Object nullSafeGet(ResultSet resultSet, String[] names, Object owner) throws HibernateException, SQLException
    {
        LOG.debug("nullSafeGet ...");

        Long value = resultSet.getLong(names[0]);
        if(resultSet.wasNull())
        {
            return null;
        }

        LOG.debug("nullSafeGet done");

        return (value != null) ? new Timestamp(value) : null;
    }

    @Override
    public void nullSafeSet(PreparedStatement preparedStatement, Object value, int index)
            throws HibernateException,
            SQLException
    {
        LOG.debug("nullSafeSet ...");

        if(value == null)
        {
            preparedStatement.setNull(index, StandardBasicTypes.INTEGER.sqlType());
        }
        else
        {
            Timestamp Timestamp = (Timestamp)value;
            preparedStatement.setLong(index, Timestamp.getTime());
        }

        LOG.debug("Object type: " + (value != null ? value.getClass().getName() : "(null)"));

        LOG.debug("nullSafeSet done");
    }

    @Override
    public Class<Timestamp> returnedClass()
    {
        return Timestamp.class;
    }

    @Override
    public int[] sqlTypes()
    {
        return new int[] { StandardBasicTypes.LONG.sqlType() };
    }

    private static final Logger LOG = Logger.getLogger(UtcMillis.class);
}
