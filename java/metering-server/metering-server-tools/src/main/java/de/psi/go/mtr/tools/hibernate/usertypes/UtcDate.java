/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools.hibernate.usertypes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.type.StandardBasicTypes;

/**
 * Allows to persist Date objects as SQL DATE in UTC.
 * 
 * @author emsku
 * 
 */

public class UtcDate extends AbstractMutableUserType
{
    public static final String TYPE = "de.psi.go.mtr.tools.hibernate.usertypes.UtcDate";

    @Override
    public Object deepCopy(Object value) throws HibernateException
    {
        if(null == value)
        {
            return null;
        }

        Date in = (Date)value;
        return new Date(in.getTime());
    }

    @Override
    public Object nullSafeGet(ResultSet resultSet, String[] names, Object owner) throws HibernateException, SQLException
    {
        LOG.debug("nullSafeGet ...");

        Date value = resultSet.getTimestamp(names[0], Constants.makeUtcCalendar());
        if(resultSet.wasNull())
        {
            return null;
        }

        LOG.debug("nullSafeGet done");

        return value;
    }

    @Override
    public void nullSafeSet(PreparedStatement preparedStatement, Object value, int index)
            throws HibernateException,
            SQLException
    {
        LOG.debug("nullSafeSet ...");

        if(value == null)
        {
            preparedStatement.setNull(index, StandardBasicTypes.TIMESTAMP.sqlType());
        }
        else
        {
            Timestamp date = new Timestamp(((Date)value).getTime());
            preparedStatement.setTimestamp(index, date, Constants.makeUtcCalendar());
        }

        LOG.debug("nullSafeSet done");
    }

    @Override
    public Class<Date> returnedClass()
    {
        return Date.class;
    }

    @Override
    public int[] sqlTypes()
    {
        return new int[] { StandardBasicTypes.TIMESTAMP.sqlType() };
    }

    private static final Logger LOG = Logger.getLogger(UtcDate.class);
}
