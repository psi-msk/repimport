/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools.hibernate.usertypes;

import java.io.Serializable;

import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

import de.psi.go.mtr.tools.ObjectIdentity;

/**
 * Boilerplate code for immutable user type.
 * 
 * 
 * @author emsku
 * 
 */

public abstract class AbstractImmutableUserType implements UserType
{

    @Override
    public boolean isMutable()
    {
        return false;
    }

    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException
    {
        return cached;
    }

    @Override
    public Object deepCopy(Object other) throws HibernateException
    {
        return other;
    }

    @Override
    public Serializable disassemble(Object obj) throws HibernateException
    {
        return (Serializable)obj;
    }

    @Override
    public boolean equals(Object obj0, Object obj1) throws HibernateException
    {
        return ObjectIdentity.safeEquals(obj1, obj1);
    }

    @Override
    public int hashCode(Object arg0) throws HibernateException
    {
        return ObjectIdentity.safeHashCode(arg0);
    }

    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException
    {
        return original;
    }
}
