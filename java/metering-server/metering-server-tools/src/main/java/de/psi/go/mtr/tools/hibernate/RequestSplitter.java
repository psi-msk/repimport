/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools.hibernate;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;

/**
 * Contains methods to split requests on lists into subrequests honoring limitations on lengths of list parameters.
 * 
 */

public class RequestSplitter<ENTITY, PROPERTY>
{
    /**
     * The maximal size of a sublist for "where ... in LIST" query: LIST will be
     * split into sublists of maximally {@code MAX_IN_SIZE} values.
     */
    public static final int MAX_IN_SIZE = 1000;

    private static final Logger LOG = Logger.getLogger(RequestSplitter.class);

    /**
     * 
     * Consider a query with an IN clause against a list:
     * <p>
     * select * from ENTITY where PROPERTY in inlist
     * <p>
     * If inlist is larger than 1000 elements, ORACLE will give an error
     * <p>
     * ORA-01795 maximum number of expressions in a list is 1000
     * <p>
     * This method divides inlist into sublists of <= 1000 elements, for each sublist runs the query and returns the sum
     * of all subqueries.
     * 
     * 
     * @param <ENTITY>
     *            the type of the entity queried and returned
     * @param <PROPERTY>
     *            the property, against an IN clause is applied
     * @param q
     *            a prepared query
     * @param parameterName
     *            the parameter for the property in the statement
     * @param inlist
     *            the set of values, @{code PROPERTY} is compared against
     * @return the result of the query
     *         "select * from ENTITY where PROPERTY in inlist"
     */
    public List<ENTITY> splitAndApplyBulkQuery(TypedQuery<ENTITY> q, String parameterName, List<PROPERTY> inlist)
    {
        LOG.debug(new StringBuffer().append(q.toString()));

        final List<ENTITY> res = new ArrayList<ENTITY>();
        final int nofSelectVals = inlist.size();

        for(int i = 0; i < nofSelectVals; i += MAX_IN_SIZE)
        {
            List<PROPERTY> subList;
            if(nofSelectVals > i + MAX_IN_SIZE)
            {
                subList = inlist.subList(i, (i + MAX_IN_SIZE));
            }
            else
            {
                subList = inlist.subList(i, nofSelectVals);
            }
            q.setParameter(parameterName, subList);
            res.addAll(q.getResultList());
        }

        return res;
    }

    /**
     * The same as {@see RequestSplitter#splitAndApplyBulkQuery(TypedQuery,
     * String, List)} above with the exception, that here an untyped query is
     * used.
     * 
     * @param <ENTITY>
     * @param <PROPERTY>
     * @param q
     * @param parameterName
     * @param inlist
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<ENTITY> splitAndApplyBulkQuery(Query q, String parameterName, List<PROPERTY> inlist)
    {
        LOG.debug(new StringBuffer().append(q.toString()));

        final List<ENTITY> res = new ArrayList<ENTITY>();
        final int nofSelectVals = inlist.size();

        for(int i = 0; i < nofSelectVals; i += MAX_IN_SIZE)
        {
            List<PROPERTY> subList;
            if(nofSelectVals > i + MAX_IN_SIZE)
            {
                subList = inlist.subList(i, (i + MAX_IN_SIZE));
            }
            else
            {
                subList = inlist.subList(i, nofSelectVals);
            }
            q.setParameter(parameterName, subList);
            res.addAll(q.getResultList());
        }

        return res;
    }
}
