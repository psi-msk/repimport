/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class AbstractBaseH2SqlSessionFactory
{

    public Session getSession()
    {
        return sf.openSession();
    }

    public Configuration getConfiguration()
    {
        return cfg;
    }

    public void close()
    {
        sf.close();
    }

    protected SessionFactory sf;
    protected Configuration cfg = new Configuration();
}
