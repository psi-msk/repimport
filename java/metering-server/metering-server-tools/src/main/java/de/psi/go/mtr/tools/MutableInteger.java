/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools;

public class MutableInteger
{
    public MutableInteger()
    {
    }

    public MutableInteger(int value)
    {
        this.value = value;
    }

    public void setValue(int value)
    {
        this.value = value;
    }

    public int getValue()
    {
        return value;
    }

    public void increment()
    {
        ++value;
    }

    public void decrement()
    {
        --value;
    }

    private int value = 0;
}
