/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;

public class FileHelper
{
    /**
     *
     * @param object
     *            The object, which resources are used
     * @param filePath
     *            The path relative to "/" of the classpath (resource path)
     * @return The content of the file as a string.
     * @throws IOException
     */
    public static String readWholeFileFromClasspath(Object object, String filePath) throws IOException
    {
        BufferedReader inBuff = new BufferedReader(
                new InputStreamReader(object.getClass().getResourceAsStream("/" + filePath)));

        String line;
        StringBuilder result = new StringBuilder();
        while((line = inBuff.readLine()) != null)
        {
            result.append(line);
        }

        return result.toString();
    }

    /**
     *
     * @param file
     * @return The file's content as a byte array.
     * @throws IOException
     */
    public static byte[] fileToByteArray(File file) throws IOException
    {
        RandomAccessFile f = null;
        try
        {
            f = new RandomAccessFile(file.getName(), "r");
            byte[] b = new byte[(int)f.length()];
            f.read(b);
            return b;
        }
        finally
        {
            if(f != null)
            {
                f.close();
            }
        }
    }
}
