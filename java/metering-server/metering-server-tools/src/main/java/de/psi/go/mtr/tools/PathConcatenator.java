package de.psi.go.mtr.tools;

import java.io.File;

public class PathConcatenator
{
    public static String makePath(String... elements)
    {
        final StringBuilder sb = new StringBuilder();

        int cnt = 0;
        for(String element : elements)
        {
            ++cnt;
            sb.append(element);
            // Do not append the separator to the last element.
            if(cnt < elements.length)
            {
                sb.append(File.separator);
            }
        }

        return sb.toString();
    }
}
