/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools.hibernate;

/**
 * 
 * Session factory for memory based H2SQL DB connections. Applicable in JUnit
 * tests for Entity Beans.
 * 
 * @author emsku
 * 
 */
public class H2SqlSessionFactoryManualDdl extends AbstractBaseH2SqlSessionFactory
{
    public H2SqlSessionFactoryManualDdl(String initDdl, Class<?>... annotatedClasses)
    {
        cfg.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        cfg.setProperty("hibernate.connection.driver_class", "org.h2.Driver");

        cfg.setProperty("hibernate.show_sql", "false");
        cfg.setProperty("hibernate.connection.url", makeInitStatement(initDdl));
        cfg.setProperty("hibernate.hbm2ddl.auto", "validate");

        // Insert the parameterized classes known to the cfg.
        for(Class<?> clazz : annotatedClasses)
        {
            cfg.addAnnotatedClass(clazz);
        }

        this.sf = cfg.buildSessionFactory();
    }

    private String makeInitStatement(String initDdl)
    {
        StringBuilder sb = new StringBuilder();

        sb.append("jdbc:h2:mem:");
        if(!initDdl.isEmpty())
        {
            sb.append(";INIT=").append(initDdl);
        }

        return sb.toString();
    }
}
