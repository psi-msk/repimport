/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools.xml;

/**
 * Interface to be passed to a SAX based parser for processing of the found
 * elements.
 * 
 * @author kulbrich
 * 
 * @param <XMLType>
 */
public interface IXmlElementProcessor<XMLType>
{
    void process(XMLType xmlelement) throws Exception;
}
