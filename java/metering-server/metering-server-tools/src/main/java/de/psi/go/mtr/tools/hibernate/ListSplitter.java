package de.psi.go.mtr.tools.hibernate;

import java.util.ArrayList;
import java.util.List;

public class ListSplitter<T>
{
    private static final int MAX_IN_SIZE = RequestSplitter.MAX_IN_SIZE;

    public static <T> List<List<T>> split(List<T> in)
    {
        List<List<T>> out = new ArrayList<List<T>>();

        final int nofSelectVals = in.size();

        for(int i = 0; i < nofSelectVals; i += MAX_IN_SIZE)
        {
            if(nofSelectVals > i + MAX_IN_SIZE)
            {
                out.add(in.subList(i, (i + MAX_IN_SIZE)));
            }
            else
            {
                out.add(in.subList(i, nofSelectVals));
            }
        }
        return out;
    }
}
