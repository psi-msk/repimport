/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools;

import org.joda.time.DateTimeZone;
import org.joda.time.MutableDateTime;

/**
 * Contains interface and implementation for millisecond incrementation.
 * 
 * @author emsku
 * 
 */

public class MillisIncrementation
{
    public interface Incrementor
    {
        /**
         * Increment value according to time implemented time interval.
         * 
         * @param val
         * @return
         */
        long inc(long val);

        long incNtimes(int ntimes, long val);

        long inc(long val, DateTimeZone tz);

        long incNtimes(int ntimes, long val, DateTimeZone tz);
    }

    /**
     * This class has a member, which tells how many times the desired timespan
     * (minute, day ...) is to be added. Thus, it is an incrementer, increments n
     * times the basic interval. So one can for example construct incrementers
     * for 1, 2, ... N minutes. I.e.: IncrementorN(3) increments 3 times a basic
     * interval.
     * 
     * @author emsku
     * 
     */
    abstract static class IncrementorN implements Incrementor
    {
        IncrementorN(int n)
        {
            this.n = n;
        }

        @Override
        public final long inc(long val)
        {
            return incInternal(this.n, val);
        }

        @Override
        public final long incNtimes(int ntimes, long val)
        {
            return incInternal(ntimes * this.n, val);
        }

        @Override
        public final long inc(long val, DateTimeZone tz)
        {
            return incInternal(this.n, val);
        }

        @Override
        public final long incNtimes(int ntimes, long val, DateTimeZone tz)
        {
            return incInternal(ntimes * this.n, val, tz);
        }

        /**
         * This method implements the incrementation of the value by m times the
         * implemented time interval. For fixed time intervals (minutes, hours
         * ...) this is something else than for non fixed time intervals (months,
         * years ...)
         * 
         * @param m
         * @param val
         * @return
         */
        protected abstract long incInternal(int m, long val);

        /**
         * The same as incInternal(int m, long val), but assuming, that it is done
         * in a certain time zone.
         * 
         * @param m
         * @param val
         * @param tz
         * @return
         */
        protected abstract long incInternal(int m, long val, DateTimeZone tz);

        protected final int n;
    }

    // ------------------------------------------------------------------------

    public static class SecondIncrementor extends IncrementorN
    {
        public SecondIncrementor(int n)
        {
            super(n);
        }

        @Override
        public long incInternal(int m, long val)
        {
            return val + m * 1000;
        }

        @Override
        protected long incInternal(int m, long val, DateTimeZone tz)
        {
            return val + m * 1000;
        }
    }

    // ------------------------------------------------------------------------

    public static class MinuteIncrementor extends IncrementorN
    {
        public MinuteIncrementor(int n)
        {
            super(n);
        }

        @Override
        public long incInternal(int m, long val)
        {
            return val + m * msPerMinute;
        }

        @Override
        protected long incInternal(int m, long val, DateTimeZone tz)
        {
            return val + m * msPerMinute;
        }
    }

    // ------------------------------------------------------------------------

    public static class HourIncrementor extends IncrementorN
    {
        public HourIncrementor(int n)
        {
            super(n);
        }

        @Override
        protected long incInternal(int m, long val)
        {
            return val + m * msPerHour;
        }

        @Override
        protected long incInternal(int m, long val, DateTimeZone tz)
        {
            return val + m * msPerHour;
        }
    }

    // ------------------------------------------------------------------------

    public static class DayIncrementor extends IncrementorN
    {
        public DayIncrementor(int n)
        {
            super(n);
        }

        @Override
        protected long incInternal(int m, long val)
        {
            return val + m * msPerDay;
        }

        @Override
        protected long incInternal(int m, long val, DateTimeZone tz)
        {
            MutableDateTime mdt = new MutableDateTime(val, tz);
            mdt.addDays(m);
            return mdt.getMillis();
        }
    }

    // ------------------------------------------------------------------------

    public static class MonthIncrementor extends IncrementorN
    {
        public MonthIncrementor(int n)
        {
            super(n);
        }

        @Override
        protected long incInternal(int m, long val)
        {
            MutableDateTime mdt = new MutableDateTime(val);
            mdt.addMonths(m);
            return mdt.getMillis();
        }

        @Override
        protected long incInternal(int m, long val, DateTimeZone tz)
        {
            MutableDateTime mdt = new MutableDateTime(val, tz);
            mdt.addMonths(m);
            return mdt.getMillis();
        }
    }

    // ------------------------------------------------------------------------

    public static class YearIncrementor extends IncrementorN
    {
        public YearIncrementor(int n)
        {
            super(n);
        }

        @Override
        protected long incInternal(int m, long val)
        {
            MutableDateTime mdt = new MutableDateTime(val);
            mdt.addYears(m);
            return mdt.getMillis();
        }

        @Override
        protected long incInternal(int m, long val, DateTimeZone tz)
        {
            MutableDateTime mdt = new MutableDateTime(val, tz);
            mdt.addYears(m);
            return mdt.getMillis();
        }
    }

    // ------------------------------------------------------------------------

    public static class NullIncrementor implements Incrementor
    {
        @Override
        public long inc(long val)
        {
            return val;
        }

        @Override
        public long incNtimes(int ntimes, long val)
        {
            return val;
        }

        @Override
        public long inc(long val, DateTimeZone tz)
        {
            return val;
        }

        @Override
        public long incNtimes(int ntimes, long val, DateTimeZone tz)
        {
            return val;
        }
    }

    // ------------------------------------------------------------------------

    private static final long msPerMinute = 60 * 1000;
    private static final long msPerHour = 60 * msPerMinute;
    private static final long msPerDay = 24 * msPerHour;
}
