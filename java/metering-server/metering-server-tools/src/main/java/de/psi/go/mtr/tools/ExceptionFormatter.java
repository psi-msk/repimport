/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class ExceptionFormatter
{
    public static StringBuilder format(Exception e)
    {
        if(e.getClass() == NullPointerException.class)
            e.printStackTrace();

        final StringBuilder msgBuff = new StringBuilder();
        msgBuff.append("Exception ").append(e.getClass().getName()).append(" message:[").append(e.getMessage()).append(
                "]");

        if(null != e.getCause())
        {
            msgBuff.append(" cause: ").append("[").append(e.getCause().getClass().getName()).append(" : ").append(
                    e.getCause().getMessage()).append("]");
        }

        return msgBuff;
    }

    public static void log(Exception e, Logger logger, Level level)
    {
        logger.log(level, format(e));
        if(logger.isDebugEnabled())
            e.printStackTrace();
    }

    public static void recursiveLog(Throwable e, StringBuilder out)
    {
        out.append(e.toString()).append("\n");

        final Throwable cause = e.getCause();

        if(null != cause)
            recursiveLog(cause, out);
    }

    public static void recursiveStackTraces(Throwable e)
    {
        final Throwable cause = e.getCause();

        e.printStackTrace();

        if(null == cause)
            return;

        recursiveStackTraces(cause);
    }
}
