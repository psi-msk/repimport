/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools;

/**
 * 
 * safeEquals() and safeHashCode() for Object instances. Takes care of null
 * values. To reduce verbosity in Object.equals() and Object.hashCode() -
 * implementations.
 * 
 * @author emsku
 * 
 */

public class ObjectIdentity
{

    /**
     * Encapsulates null checking for equality. Apply it to equality checking of {@link Object} type fields in
     * implementations of {@link Object#equals(Object)}
     * 
     * @param o1
     * @param o2
     * @return
     */
    static public boolean safeEquals(Object o1, Object o2)
    {
        if(o1 == o2)
        {
            return true;
        }

        if(o1 == null || o2 == null)
        {
            return false;
        }

        return o1.equals(o2);
    }

    /**
     * Encapsulates null checking for hash code. Apply it to getting the hash
     * code of {@link Object} in implementations of {@link Object#hashCode()}
     * 
     * @param o
     * @return
     */
    static public int safeHashCode(Object o)
    {
        return o == null ? 0 : o.hashCode();
    }
}
