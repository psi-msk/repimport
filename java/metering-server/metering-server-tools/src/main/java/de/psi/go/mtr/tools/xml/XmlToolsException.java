/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools.xml;

public class XmlToolsException extends Exception
{
    XmlToolsException(String msg)
    {
        super(msg);
    }

    /**
    * 
    */
    private static final long serialVersionUID = 5726849810036167401L;

}
