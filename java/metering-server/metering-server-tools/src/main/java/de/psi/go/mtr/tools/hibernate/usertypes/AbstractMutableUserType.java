/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools.hibernate.usertypes;

import java.io.Serializable;

import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

import de.psi.go.mtr.tools.ObjectIdentity;

/**
 * Boilerplate code for mutable user type.
 * 
 * 
 * @author emsku
 * 
 */

public abstract class AbstractMutableUserType implements UserType
{

    @Override
    public boolean isMutable()
    {
        return true;
    }

    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException
    {
        return deepCopy(cached);
    }

    @Override
    public Serializable disassemble(Object value) throws HibernateException
    {
        return (Serializable)deepCopy(value);
    }

    @Override
    public boolean equals(Object obj0, Object obj1) throws HibernateException
    {
        return ObjectIdentity.safeEquals(obj0, obj1);
    }

    @Override
    public int hashCode(Object value) throws HibernateException
    {
        return ObjectIdentity.safeHashCode(value);
    }

    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException
    {
        return deepCopy(original);
    }
}
