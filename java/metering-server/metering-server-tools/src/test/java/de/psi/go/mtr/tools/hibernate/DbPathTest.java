/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools.hibernate;

import org.apache.log4j.Logger;
import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.h2.H2DataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.hibernate.Session;

public class DbPathTest extends DBTestCase
{
    private AbstractBaseH2SqlSessionFactory h2sf = new H2SqlSessionFactoryAutoDdl(DbPathTestEntity.class);
    Session session = h2sf.getSession();

    public DbPathTest() throws Exception
    {
        System.setProperty(
                PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS,
                h2sf.getConfiguration().getProperty("hibernate.connection.driver_class"));
        System.setProperty(
                PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL,
                h2sf.getConfiguration().getProperty("hibernate.connection.url"));
    }

    @Override
    protected IDataSet getDataSet() throws Exception
    {
        return new FlatXmlDataSetBuilder().build(DbPathTest.class.getResourceAsStream("/path_test_data.xml"));
    }

    @Override
    protected void setUpDatabaseConfig(DatabaseConfig config)
    {
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new H2DataTypeFactory());
        super.setUpDatabaseConfig(config);
    }

    public void testPath() throws Exception
    {
        for(Long id : new Long[] { 1L, 2L, 3L, 4L })
        {

            DbPathTestEntity entity = (DbPathTestEntity)session.get(DbPathTestEntity.class, id);

            if(null == entity)
            {
                LOG.warn("No entity for ID = " + id);
                continue;
            }

            System.out.println(entity.getPath());
        }

    }

    protected DatabaseOperation getSetUpOperation() throws Exception
    {
        return DatabaseOperation.REFRESH;
    }

    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.DELETE_ALL;
    }

    private static final Logger LOG = Logger.getLogger(DbPathTest.class);
}
