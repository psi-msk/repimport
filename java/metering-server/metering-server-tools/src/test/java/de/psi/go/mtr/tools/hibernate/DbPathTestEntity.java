/*******************************************************************************
 * Copyright (c) 2013 PSI AG. All rights reserved.
 * This file is part of the software PSIportal by PSI Oil & Gas, Germany.
 ******************************************************************************/
package de.psi.go.mtr.tools.hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = DbPathTestEntity.TABLE_NAME)
public class DbPathTestEntity
{
    public static final String TABLE_NAME = "PATH_TEST";

    public DbPathTestEntity()
    {

    }

    @Id
    @Column(name = "ID")
    public Long getId()
    {
        return id;
    }

    @Column(name = "PATH", length = 1024)
    public String getPath()
    {
        return path;
    }

    public void setPath(String path)
    {
        this.path = path;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    private Long id;
    private String path;
}
