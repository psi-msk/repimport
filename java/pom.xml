<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>de.psi.go.metering</groupId>
	<artifactId>metering</artifactId>
	<name>mtr</name>
	<version>1.0.0-SNAPSHOT</version>
	<packaging>pom</packaging>

	<organization>
		<name>PSI AG (GO)</name>
	</organization>

	<modules>
		<module>metering-server</module>
		<module>portal-installer</module>
		<module>cpdd-reporting-import</module>
	</modules>

	<properties>
		<psiportal.version>1.0.0-SNAPSHOT</psiportal.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<jboss.version>6.1.0.Final</jboss.version>
		<jboss.host>localhost</jboss.host>
		<jboss.jndi.port>1099</jboss.jndi.port>
		<!-- Date / Time parsing -->
		<joda.version>1.6.2</joda.version>
	</properties>

	<dependencyManagement>
		<dependencies>

			<dependency>
				<groupId>de.psi.go.metering</groupId>
				<artifactId>metering-server-model-api</artifactId>
				<version>${psiportal.version}</version>
			</dependency>

			<dependency>
				<groupId>de.psi.go.metering</groupId>
				<artifactId>metering-server-tools</artifactId>
				<version>${psiportal.version}</version>
			</dependency>

			<dependency>
				<groupId>de.psi.go.metering</groupId>
				<artifactId>metering-server-psiportal-xml</artifactId>
				<version>${psiportal.version}</version>
			</dependency>

			<dependency>
				<groupId>junit</groupId>
				<artifactId>junit</artifactId>
				<version>4.8.1</version>
				<scope>test</scope>
			</dependency>
			<dependency>
				<groupId>log4j</groupId>
				<artifactId>log4j</artifactId>
				<version>1.2.16</version>
			</dependency>
			<dependency>
				<groupId>joda-time</groupId>
				<artifactId>joda-time</artifactId>
				<version>${joda.version}</version>
			</dependency>
			<dependency>
				<groupId>org.apache.servicemix.bundles</groupId>
				<artifactId>org.apache.servicemix.bundles.drools</artifactId>
				<version>5.1.1_2</version>
			</dependency>
			<dependency>
				<groupId>org.drools</groupId>
				<artifactId>knowledge-api</artifactId>
				<version>5.4.0.Final</version>
			</dependency>
			<dependency>
				<groupId>org.drools</groupId>
				<artifactId>drools-compiler</artifactId>
				<version>5.4.0.Final</version>
			</dependency>
			<dependency>
				<groupId>org.jbpm</groupId>
				<artifactId>jbpm-bpmn2</artifactId>
				<version>5.4.0.Final</version>
			</dependency>
			<dependency>
				<groupId>org.jbpm</groupId>
				<artifactId>jbpm-persistence-jpa</artifactId>
				<version>5.4.0.Final</version>
			</dependency>
			<dependency>
				<groupId>org.drools</groupId>
				<artifactId>drools-persistence-jpa</artifactId>
				<version>5.4.0.Final</version>
			</dependency>
			<dependency>
				<groupId>org.jbpm</groupId>
				<artifactId>jbpm-bam</artifactId>
				<version>5.4.0.Final</version>
			</dependency>
			<dependency>
				<groupId>org.drools</groupId>
				<artifactId>drools-core</artifactId>
				<version>5.4.0.Final</version>
			</dependency>
			<dependency>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-remote-resources-plugin</artifactId>
				<version>1.4</version>
			</dependency>
			<dependency>
				<groupId>com.thoughtworks.xstream</groupId>
				<artifactId>xstream</artifactId>
				<version>1.4.3</version>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<build>
		<defaultGoal>install</defaultGoal>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-compiler-plugin</artifactId>
					<version>2.2</version>
					<configuration>
						<source>1.6</source>
						<target>1.6</target>
						<encoding>UTF-8</encoding>
					</configuration>
					<executions>
						<!-- This "dummy" execution "help" is here in order to force execution 
							of this plugin for sub-projects, it normally is not executed for. The execution 
							is necessary for forcing the execution environment to the desired version 
							(here 1.6). Without this, it is set to some default value for subprojects, 
							the plugin is not called on automatically. -->
						<execution>
							<goals>
								<goal>help</goal>
							</goals>
						</execution>
					</executions>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-ejb-plugin</artifactId>
					<version>2.1</version>
					<configuration>
						<ejbVersion>3.0</ejbVersion>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.jvnet.jaxb2.maven2</groupId>
					<artifactId>maven-jaxb2-plugin</artifactId>
					<version>0.8.1</version>
					<executions>
						<execution>
							<goals>
								<goal>generate</goal>
							</goals>
						</execution>
					</executions>
					<configuration>
						<extension>true</extension>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>

		<plugins>
			<!-- Im target werden auch die Source-Bundle gebildet. -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<version>2.1.2</version>
				<executions>
					<execution>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<!-- Source code analyzer. Execute by running mvn pmd:pmd. See http://pmd.sourceforge.net/ 
				for more information. -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-pmd-plugin</artifactId>
				<version>3.0.1</version>
			</plugin>
		</plugins>
	</build>

	<reporting>
		<plugins>
			<!-- Bytecode analyzer. Execute by running mvn findbugs:findbugs. See 
				http://findbugs.sourceforge.net/ for more information. -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>findbugs-maven-plugin</artifactId>
				<version>2.5.2</version>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>2.9.1</version>
			</plugin>
		</plugins>
	</reporting>

</project>
